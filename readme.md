## GerentePRO
O conceito do GerentePRO, é ser um Gerenciador Financeiro para Serviços de Internet.
* Agências Digitais
* Empresas de Hospedagem de Sites
* Empresas de Conteúdo Digital

No geral, todos os negócios que gerenciam assinaturas recorrentes. Porém, com a total
autonomia que o sistema oferece.

#### Botando pra rodar

Antes de começar, certifique-se que tenha instalado:
* Docker
* NodeJS
* Composer


Abra o terminal na raíz do projeto, e execute.

`$ composer install`

Feito? Agora é só levantar o Docker

`$ sudo docker-compose up -d`

Não se esqueça de fazer o copy do .env `cp .env.example .env`

Pronto! :)

É só acessar no seu navegador: http://localhost
e será carregado a aplicação principal (feito em VueJS)

A API está acessível na porta 80