<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 16/08/19 19:09
 * Projeto: API cPanel/WHM
 */
require '../vendor/autoload.php';

use CyberPanel\CyberPanel;

try{
    CyberPanel::setHost("192.168.0.105");
    CyberPanel::setAuth('admin', '1234567');

    //$cpanel = cPanel::packages()->create([
    //    'name' => 'Iniciante',
    //    'disk' => 5000,
    //    'bwlimit' => 50000,
    //    'maxsql' => 5,
    //    'maxpop' => 5,
    //    'maxaddon' => 0
    //]);

    //$cpanel = cPanel::packages()->remove('Iniciante');

    //$cpanel = cPanel::packages()->update('Profissional', [
    //    'disk' => 2000,
    //    'bwlimit' => 20000
    //]);

    $cp = CyberPanel::accounts()->create([
        'user' => 'carpediem',
        'domain' => 'carpediem.com.br',
        'plan' => 'Iniciante',
        'email' => 'lucas.codemax@gmail.com'
    ]);

    //$cp = CyberPanel::verifyConn();

    print_r($cp);

}catch (Exception $e) {
    echo $e->getMessage()."\n";
}