<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 16/08/19 17:58
 * Projeto: API CyberPanel
 */

namespace CyberPanel;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * @method static accounts()
 * @method static packages()
 */
class CyberPanel
{
    public static $username;
    public static $password;

    public static $host;

    public function __construct(){

    }

    /**
     * @param $method
     * @param null $arguments
     * @return mixed
     */
    public static function __callStatic($method, $arguments = null)
    {
        $class = '\\CyberPanel\Functions\\'.ucfirst($method);
        if(!class_exists($class, true)){
            die("{$class} não existe.\n");
        }

        return new $class();
    }

    /**
     * @param $token
     * @throws \Exception
     */
    public static function setAuth($username, $password)
    {
        if(empty($username)) {
            throw new \Exception('Usuário é obrigatório.');
        }

        if(empty($password)) {
            throw new \Exception('Senha é obrigatória.');
        }

        self::$username = $username;
        self::$password = $password;
    }

    private function getHost()
    {
        if (empty(self::$host)){
            throw new \Exception('Hostname do servidor é obrigatório.');
        }

        return self::$host;
    }

    /**
     * Define a conexão com o servidor ao chamar a classe
     * @param $ip [obrigatório]
     * @throws \Exception
     */
    public static function setHost($hostname)
    {
        if(empty($hostname)) {
            throw new \Exception('Hostname do servidor é obrigatório.');
        }

        self::$host = $hostname;
    }

    public static function verifyConn()
    {
        $cp = self::query('verifyConn', []);
        if($cp->verifyConn == 0){
            throw new \Exception('Acesso negado');
        }

        return true;
    }

    /**
     * @param string $uri
     * @param array $args
     * @return mixed|object
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    protected function query($uri = '', $args = array())
    {
        $host_api = "https://".self::$host.":8090/api/".$uri;

        $args['adminUser'] = self::$username;
        $args['adminPass'] = self::$password;

        $call = curl_init();
        curl_setopt($call, CURLOPT_URL, $host_api);
        curl_setopt($call, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($call, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($call, CURLOPT_POST, true);
        curl_setopt($call, CURLOPT_POSTFIELDS, json_encode($args));
        curl_setopt($call, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($call, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($call);
        $info = curl_getinfo($call);
        curl_close($call);

        $result = json_decode($result);

        // Return data
        return $result;
    }
}
