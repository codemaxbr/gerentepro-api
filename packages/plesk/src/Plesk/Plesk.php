<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 16/08/19 17:58
 * Projeto: API Plesk
 */

namespace Plesk;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Spatie\ArrayToXml\ArrayToXml;

/**
 * @method static accounts()
 * @method static packages()
 */
class Plesk
{
    public static $user;
    public static $secret_key;
    public static $password;

    public static $host;
    public static $port;

    protected static $headers = [];
    protected static $options = [];

    public function __construct(){

    }

    /**
     * @param $method
     * @param null $arguments
     * @return mixed
     */
    public static function __callStatic($method, $arguments = null)
    {
        $class = '\\Plesk\Functions\\'.ucfirst($method);
        if(!class_exists($class, true)){
            die("{$class} não existe.\n");
        }

        return new $class();
    }

    public static function setCredentials($user, $password)
    {
        self::setHeaders([
            'HTTP_AUTH_LOGIN' => $user,
            'HTTP_AUTH_PASSWD' => $password,
        ]);
    }

    public static function setSecretKey($secret)
    {
        self::$secret_key = $secret;
    }

    /**
     * Define a conexão com o servidor ao chamar a classe
     * @param $ip [obrigatório]
     * @throws \Exception
     */
    public static function setHost($hostname, $port = 8443)
    {
        if(empty($hostname)) {
            throw new \Exception('Hostname do servidor Plesk é obrigatório.');
        }

        self::$host = $hostname;
        self::$port = $port;
    }

    protected function direct($body)
    {
        $base_uri = 'https://'.self::$host.':'.self::$port.'/enterprise/control/agent.php';
        $client   = new Client();

        $response = $client->request('POST', $base_uri, [
            'headers' => $this->getHeaders(),
            'body' => $body
        ]);

        return json_decode(json_encode(simplexml_load_string($response->getBody())));
    }

    /**
     * @param string $uri
     * @param array $args
     * @return mixed|object
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    protected function query($body)
    {
        $base_uri = 'https://'.self::$host.':'.self::$port.'/enterprise/control/agent.php';
        $client   = new Client();

        $parameters = ArrayToXml::convert($body, [
            'rootElementName' => 'packet',
            '_attributes' => [
                'version' => '1.6.9.1'
            ]
        ]);

        $response = $client->request('POST', $base_uri, [
            'headers' => $this->getHeaders(),
            'body' => $parameters
        ]);

        return json_decode(json_encode(simplexml_load_string($response->getBody())));
    }

    public function setHeaders(array $headers)
    {
        self::$headers = array_merge(self::$headers, $headers);
    }

    public function setOptions(array $options)
    {
        self::$options = array_merge(self::$options, $options);
    }

    private function getHeaders()
    {
        return self::$headers;
    }
}
