<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 18/08/19 23:20
 * Projeto: API Plesk para PHP
 */

namespace Plesk\Functions;

use Plesk\Plesk;

class Customers extends Plesk
{
    public function all()
    {
        $plesk = $this->query([
            'customer' => ['get' => ['filter' => [], 'dataset' => ['gen_info' => [], 'stat' => []]]]
        ]);

        $customers = [];

        if(is_array($plesk->customer->get->result)){
            foreach ($plesk->customer->get->result as $customer){
                array_push($customers, (object) [
                    'id' => $customer->id,
                    'guid' => $customer->data->gen_info->guid,
                    'name' => $customer->data->gen_info->pname,
                    'login' => $customer->data->gen_info->login,
                    'email' => $customer->data->gen_info->email,
                    'country' => $customer->data->gen_info->country,
                    'locale' => $customer->data->gen_info->locale,
                    'stats' => $customer->data->stat
                ]);
            }
        }else{
            $customer = $plesk->customer->get->result;
            if(is_null($customer)){
                array_push($customers, (object) [
                    'id' => $customer->id,
                    'guid' => $customer->data->gen_info->guid,
                    'name' => $customer->data->gen_info->pname,
                    'login' => $customer->data->gen_info->login,
                    'email' => $customer->data->gen_info->email,
                    'country' => $customer->data->gen_info->country,
                    'locale' => $customer->data->gen_info->locale,
                    'stats' => $customer->data->stat
                ]);
            }
        }

        return $customers;
    }

    public function create($param)
    {
        $errors = array('error' => array());

        if(!isset($param['name'])) {
            array_push($errors['error'], "Nome é obrigatório. [name]");
        }

        if(!isset($param['login'])) {
            array_push($errors['error'], "Login é obrigatório. [login]");
        }

        if(!isset($param['password'])) {
            array_push($errors['error'], "Senha é obrigatória. [password]");
        }

        if(count($errors['error']) > 0)
        {
            return $errors;
        }

        $args = [
            'pname' => $param['name'],
            'login' => $param['login'],
            'passwd' => $param['password']
        ];

        $plesk = $this->query([
            'customer' => ['add' => ['gen_info' => $args]]
        ]);

        if($plesk->customer->add->result->status == "ok"){
            return (object) [
                'status' => 'success',
                'message' => 'Usuário "'.$param['login'].'" foi criado.',
                'id' => $plesk->customer->add->result->id,
                'guid' => $plesk->customer->add->result->guid
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => 'Não foi possível criar um usuário para "'.$param['login'].'"',
                'data' => $plesk->customer->add->result
            ];
        }
    }

    public function search($email)
    {
        $plesk = $this->query([
            'customer' => [
                'get' => [
                    'filter' => [
                        'login' => $email
                    ],

                    'dataset' => [
                        'gen_info' => [],
                        'stat' => []
                    ]
                ]
            ]
        ]);

        if($plesk->customer->get->result->status == "ok"){
            return (object) [
                'status' => 'success',
                'id' => $plesk->customer->get->result->id
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => 'Não encontrado.'
            ];
        }
    }

    public function remove($email){
        $plesk = $this->query([
            'customer' => [
                'del' => [
                    'filter' => [
                        'login' => $email
                    ]
                ]
            ]
        ]);

        if($plesk->customer->del->result->status == "ok"){
            return (object) [
                'status' => 'success',
                'message' => 'Usuário "'.$email.'" foi removido.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => 'Não foi possível remover o usuário',
                'data' => $plesk->customer->del->result
            ];
        }
    }
}