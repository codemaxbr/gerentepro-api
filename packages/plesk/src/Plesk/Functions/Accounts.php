<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 19/08/19 00:25
 * Projeto: API Plesk para PHP
 */

namespace Plesk\Functions;
use Plesk\Plesk;
use Spatie\ArrayToXml\ArrayToXml;

/**
 * Gera senhas aleatórias
 *
 * @param int $qtyCaraceters quantidade de caracteres na senha, por padrão 8
 * @return String
 */
function gerarSenha($qtyCaraceters = 8)
{
    //Letras minúsculas embaralhadas
    $smallLetters = str_shuffle('abcdefghijklmnopqrstuvwxyz');

    //Letras maiúsculas embaralhadas
    $capitalLetters = str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ');

    //Números aleatórios
    $numbers = (((date('Ymd') / 12) * 24) + mt_rand(800, 9999));
    $numbers .= 1234567890;

    //Caracteres Especiais
    $specialCharacters = str_shuffle('!@#$*');

    //Junta tudo
    $characters = $capitalLetters.$smallLetters.$numbers.$specialCharacters;

    //Embaralha e pega apenas a quantidade de caracteres informada no parâmetro
    $password = substr(str_shuffle($characters), 0, $qtyCaraceters);

    //Retorna a senha
    return $password;
}

function limpaNumeros($numeros){
    $novo_numero = str_replace("(", "", $numeros);
    $novo_numero = str_replace(")", "", $novo_numero);
    $novo_numero = str_replace("-", "", $novo_numero);
    $novo_numero = str_replace(" ", "", $novo_numero);
    $novo_numero = str_replace("_", "", $novo_numero);
    $novo_numero = str_replace(".", "", $novo_numero);
    $novo_numero = str_replace("/", "", $novo_numero);
    $novo_numero = str_replace("\/", "", $novo_numero);

    return $novo_numero;
}

class Accounts extends Plesk
{
    public function create($param)
    {
        $errors = array('error' => array());

        if(!isset($param['email'])) {
            array_push($errors['error'], "E-mail é obrigatório. [email]");
        }

        if(!isset($param['user'])) {
            array_push($errors['error'], "Usuário é obrigatório. [user]");
        }

        if(!isset($param['domain'])) {
            array_push($errors['error'], "O Domínio é obrigatório. [domain]");
        }

        if(count($errors['error']) > 0)
        {
            return $errors;
        }

        $customer = Plesk::customers()->create([
            'name' => $param['email'],
            'login' => $param['user'],
            'password' => $param['password']
        ]);

        if($customer->status == "success"){
            $user_created = true;
            $customer_id = $customer->id;

            $request = '<?xml version="1.0"?>';
            $request .= '<packet version="1.6.9.1">
                    <webspace>
                        <add>
                            <gen_setup>
                                <name>'.$param['domain'].'</name>
                                <owner-id>'.$customer_id.'</owner-id>
                                <ip_address>'.gethostbyname(self::$host).'</ip_address>
                            </gen_setup>
                            <hosting>
                                <vrt_hst>
                                    <property>
                                        <name>ftp_login</name>
                                        <value>'.$param['user'].'</value>
                                    </property>
                                    <property>
                                        <name>ftp_password</name>
                                        <value>'.$param['password'].'</value>
                                    </property>
                                    <ip_address>'.gethostbyname(self::$host).'</ip_address>
                                </vrt_hst>
                            </hosting>
                            <plan-name>'.$param['plan'].'</plan-name>
                        </add>                        
                    </webspace>
                </packet>';

            $plesk = $this->direct($request);
            $response_plesk = $plesk->webspace->add->result;

            if($response_plesk->status == "ok"){
                return (object) [
                    'status' => 'success',
                    'message' => 'A conta para "'.$param['domain'].'" foi criada com sucesso.',
                    'data' => [
                        'domain' => $param['domain'],
                        'user' => $param['user'],
                        'password' => $param['password'],
                        'plan' => $param['plan']
                    ]
                ];
            }else{
                if($user_created){
                    Plesk::customers()->remove($param['user']);
                }

                return (object) [
                    'status' => 'error',
                    'message' => 'Não foi possível criar a nova conta.',
                    'data' => $plesk->webspace->add->result
                ];
            }
        }else{
            return $customer;
        }
    }

    /**
     * Status: 0 - Ativo | 16 - Suspenso pelo Administrador | 64 - Suspenso pelo Cliente
     *
     * @param $domain
     * @return mixed|object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function suspend($domain)
    {
        $request = '<?xml version="1.0"?>';
        $request .= '<packet version="1.6.9.1"><webspace><set><filter><name>'.$domain.'</name></filter><values><gen_setup><status>64</status></gen_setup></values></set></webspace></packet>';

        $plesk = $this->direct($request);
        if($plesk->webspace->set->result->status == "ok"){
            return (object) [
                'status' => 'success',
                'messsage' => 'A conta "'.$domain.'" foi suspensa.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'data' => $plesk->webspace->set->result
            ];
        }
    }

    /**
     * Status: 0 - Ativo | 16 - Suspenso pelo Administrador | 64 - Suspenso pelo Cliente
     *
     * @param $domain
     * @return mixed|object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function unsuspend($domain)
    {
        $request = '<?xml version="1.0"?>';
        $request .= '<packet version="1.6.9.1"><webspace><set><filter><name>'.$domain.'</name></filter><values><gen_setup><status>0</status></gen_setup></values></set></webspace></packet>';

        $plesk = $this->direct($request);
        if($plesk->webspace->set->result->status == "ok"){
            return (object) [
                'status' => 'success',
                'messsage' => 'A conta "'.$domain.'" foi reativada.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'data' => $plesk->webspace->set->result
            ];
        }
    }

    public function remove($domain)
    {
        $plesk = $this->query([
            'webspace' => ['get' => ['filter' => ['name' => $domain], 'dataset' => ['aps-filter' => [], 'disk_usage' => [], 'gen_info' => [], 'hosting' => [], 'limits' => [], 'mail' => [], 'packages' => [], 'performance' => [], 'permissions' => [], 'php-settings' => [], 'plan-items' => [], 'prefs' => [], 'resource-usage' => [], 'stat' => [], 'subscriptions' => []]]]
        ]);

        $espace_owner_id = "owner-id";

        $owner_id = $plesk->webspace->get->result->data->gen_info->$espace_owner_id;
        $customer = $this->query([
            'customer' => [
                'del' => [
                    'filter' => [
                        'id' => $owner_id
                    ]
                ]
            ]
        ]);

        if($customer->customer->del->result->status == "ok"){
            return (object) [
                'status' => 'success',
                'message' => 'A conta "'.$domain.'" foi removida com successo.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'data' => $plesk->webspace->del->result
            ];
        }
    }
}