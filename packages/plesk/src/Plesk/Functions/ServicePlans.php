<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 18/08/19 18:03
 * Projeto: API Plesk para PHP
 */

namespace Plesk\Functions;
use Plesk\Plesk;

function mb_size($bytes){
    if($bytes != "unlimited"){
        if($bytes >= 1000){
            return ($bytes / 1000) * 1073741824;
        }else{
            return $bytes * 1048576;
        }
    }

    return $bytes;
}

function unlimited($value)
{
    if($value == "unlimited"){
        return '-1';
    }else{
        return $value;
    }
}

class ServicePlans extends Plesk
{
    public function all()
    {
        $plesk = $this->query([
            'service-plan' => ['get' => ['filter' => [], 'owner-all' => []]]
        ]);

        $escape = "service-plan";
        $plans = [];

        if(is_array($plesk->$escape->get->result)){
            foreach ($plesk->$escape->get->result as $plan){
                array_push($plans, (object)[
                    'id' => $plan->id,
                    'name' => $plan->name,
                    'guid' => $plan->guid,
                    'limits' => (object) [
                        'max_site' => unlimited($plan->limits->limit[0]->value),
                        'max_subdom' => unlimited($plan->limits->limit[1]->value),
                        'max_dom_aliases' => unlimited($plan->limits->limit[2]->value),
                        'disk_space' => unlimited($plan->limits->limit[3]->value),
                        'max_traffic' => unlimited($plan->limits->limit[5]->value),
                        'max_db' => unlimited($plan->limits->limit[9]->value),
                        'max_mails' => unlimited($plan->limits->limit[12]->value)
                    ]
                ]);
            }

        }else{
            $plan = $plesk->$escape->get->result;
            if(!is_null($plan))
            array_push($plans, (object)[
                'id' => $plan->id,
                'name' => $plan->name,
                'guid' => $plan->guid,
                'limits' => (object) [
                    'max_site' => unlimited($plan->limits->limit[0]->value),
                    'max_subdom' => unlimited($plan->limits->limit[1]->value),
                    'max_dom_aliases' => unlimited($plan->limits->limit[2]->value),
                    'disk_space' => unlimited($plan->limits->limit[3]->value),
                    'max_traffic' => unlimited($plan->limits->limit[5]->value),
                    'max_db' => unlimited($plan->limits->limit[9]->value),
                    'max_mails' => unlimited($plan->limits->limit[12]->value)
                ]
            ]);
        }

        return $plans;
    }

    public function create($param)
    {
        $errors = array('error' => array());

        if(!isset($param['name']) || empty($param['name']))
        {
            array_push($errors['error'], "Nome é obrigatório. [name]");
        }

        if(!isset($param['disk']) || empty($param['disk']))
        {
            array_push($errors['error'], "O Espaço em Disco é obrigatório. [disk]");
        }

        if(!isset($param['bwlimit']) || empty($param['bwlimit']))
        {
            array_push($errors['error'], "O Limite de Tráfego é obrigatório. [bwlimit]");
        }

        if(!isset($param['maxaddon']))
        {
            array_push($errors['error'], "Qtd de Sites adicionais é obrigatório. [maxaddon]");
        }

        if(!isset($param['maxsql']) || empty($param['maxsql']))
        {
            array_push($errors['error'], "Qtd de Banco de dados é obrigatório. [maxsql]");
        }

        if(!isset($param['maxpop']) || empty($param['maxpop']))
        {
            array_push($errors['error'], "Qtd de E-mails é obrigatório. [maxpop]");
        }

        if(count($errors['error']) > 0)
        {
            return $errors;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $args['name'] = $param['name'];
        $args['limits']['limit'] = [];

        if(isset($param['maxaddon'])){
            array_push($args['limits']['limit'], [
                'name' => 'max_site',
                'value' => ($param['maxaddon'] == 0) ? 1 : unlimited($param['maxaddon'])
            ]);
        }

        if(isset($param['maxsql'])){
            array_push($args['limits']['limit'], [
                'name' => 'max_db',
                'value' => unlimited($param['maxsql'])
            ]);
        }

        if(isset($param['maxpop'])){
            array_push($args['limits']['limit'], [
                'name' => 'max_box',
                'value' => unlimited($param['maxpop'])
            ]);

            array_push($args['limits']['limit'], [
                'name' => 'mbox_quota',
                'value' => unlimited(mb_size(5000))
            ]);
        }

        if(isset($param['disk'])){ //disk_space
            array_push($args['limits']['limit'], [
                'name' => 'disk_space',
                'value' => unlimited(mb_size($param['disk']))
            ]);
        }

        if(isset($param['bwlimit'])){ //disk_space
            array_push($args['limits']['limit'], [
                'name' => 'max_traffic',
                'value' => unlimited(mb_size($param['bwlimit']))
            ]);
        }

        $plesk = $this->query([
            'service-plan' => ['add' => $args]
        ]);

        $escape = "service-plan";

        if($plesk->$escape->add->result->status == "ok"){
            return (object) [
                'status' => 'success',
                'message' => 'O pacote "'.$param['name'].'" foi criado com sucesso.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => 'O pacote "'.$param['name'].'" não pode ser criado.',
                'data' => $plesk->$escape->add->result
            ];
        }
    }

    public function remove($plan)
    {
        $plesk = $this->query([
            'service-plan' => ['del' => ['filter' => ['name' => $plan]]]
        ]);

        $escape = "service-plan";

        if($plesk->$escape->del->result->status == "ok")
        {
            return (object) [
                'status' => 'success',
                'message' => 'O plano "'.$plan.'" foi removido.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => 'O plano "'.$plan.' não existe.',
                'data' => $plesk->$escape->del->result
            ];
        }
    }

    public function update($plan, $param)
    {
        $errors = array('error' => array());

        if(!isset($param['name']) || empty($param['name']))
        {
            array_push($errors['error'], "Nome é obrigatório. [name]");
        }

        if(!isset($param['disk']) || empty($param['disk']))
        {
            array_push($errors['error'], "O Espaço em Disco é obrigatório. [disk]");
        }

        if(!isset($param['bwlimit']) || empty($param['bwlimit']))
        {
            array_push($errors['error'], "O Limite de Tráfego é obrigatório. [bwlimit]");
        }

        if(!isset($param['maxaddon']))
        {
            array_push($errors['error'], "Qtd de Sites adicionais é obrigatório. [maxaddon]");
        }

        if(!isset($param['maxsql']) || empty($param['maxsql']))
        {
            array_push($errors['error'], "Qtd de Banco de dados é obrigatório. [maxsql]");
        }

        if(!isset($param['maxpop']) || empty($param['maxpop']))
        {
            array_push($errors['error'], "Qtd de E-mails é obrigatório. [maxpop]");
        }

        if(count($errors['error']) > 0)
        {
            return $errors;
        }

        $args['filter']['name'] = $plan;
        $args['limits']['limit'] = [];

        if(isset($param['maxaddon'])){
            array_push($args['limits']['limit'], [
                'name' => 'max_site',
                'value' => ($param['maxaddon'] == 0) ? 1 : unlimited($param['maxaddon'])
            ]);
        }

        if(isset($param['maxsql'])){
            array_push($args['limits']['limit'], [
                'name' => 'max_db',
                'value' => unlimited($param['maxsql'])
            ]);
        }

        if(isset($param['maxpop'])){
            array_push($args['limits']['limit'], [
                'name' => 'max_box',
                'value' => unlimited($param['maxpop'])
            ]);

            array_push($args['limits']['limit'], [
                'name' => 'mbox_quota',
                'value' => unlimited(mb_size(5000))
            ]);
        }

        if(isset($param['disk'])){ //disk_space
            array_push($args['limits']['limit'], [
                'name' => 'disk_space',
                'value' => unlimited(mb_size($param['disk']))
            ]);
        }

        if(isset($param['bwlimit'])){ //disk_space
            array_push($args['limits']['limit'], [
                'name' => 'max_traffic',
                'value' => unlimited(mb_size($param['bwlimit']))
            ]);
        }

        $plesk = $this->query([
            'service-plan' => ['set' => $args]
        ]);

        $escape = "service-plan";

        if($plesk->$escape->set->result->status == "ok"){
            return (object) [
                'status' => 'success',
                'message' => 'O pacote "'.$plan.'" foi modificado com sucesso.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => 'O pacote "'.$plan.'" não pode ser modificado.',
                'data' => $plesk->$escape->set->result
            ];
        }
    }

    public function info($plan)
    {
        $plesk = $this->query([
            'service-plan' => ['get' => ['filter' => ['name' => $plan], 'owner-all' => []]]
        ]);

        $escape = "service-plan";

        if($plesk->$escape->get->result->status == "ok"){
            $plan = $plesk->$escape->get->result;

            return (object) [
                'status' => 'success',
                'data' => [
                    'id' => $plan->id,
                    'name' => $plan->name,
                    'guid' => $plan->guid,
                    'limits' => (object) [
                        'max_site' => ($plan->limits->limit[0]->value == '-1') ? 'unlimited' : $plan->limits->limit[0]->value,
                        'max_subdom' => ($plan->limits->limit[1]->value == '-1') ? 'unlimited' : $plan->limits->limit[1]->value,
                        'max_dom_aliases' => ($plan->limits->limit[2]->value == '-1') ? 'unlimited' : $plan->limits->limit[2]->value,
                        'disk_space' => ($plan->limits->limit[3]->value == '-1') ? 'unlimited' : $plan->limits->limit[3]->value,
                        'max_traffic' => ($plan->limits->limit[5]->value == '-1') ? 'unlimited' : $plan->limits->limit[5]->value,
                        'max_db' => ($plan->limits->limit[9]->value == '-1') ? 'unlimited' : $plan->limits->limit[9]->value,
                        'max_mails' => ($plan->limits->limit[10]->value == '-1') ? 'unlimited' : $plan->limits->limit[10]->value,
                    ]
                ]
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => 'O plano "'.$plan.'" não existe.'
            ];
        }
    }
}