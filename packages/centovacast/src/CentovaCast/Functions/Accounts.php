<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 16/08/19 19:05
 * Projeto: API CentovaCast
 */

namespace CentovaCast\Functions;
use CentovaCast\CentovaCast;

/**
 * Class Accounts
 * @package CentovaCast\Functions
 */

class Accounts extends CentovaCast
{
    /**
     * Lista todas as contas no servidor
     * @return mixed
     */
    public function all()
    {
        return $this->query('system.listaccounts');
    }

    /**
     * Retorna todos os servidores disponíveis
     * @return mixed
     */
    public function servers()
    {
        return $this->query('system.listhosts');
    }

    /**
     * Criar uma Nova Conta
     *
     * @param $args
     */
    public function create($args = array())
    {
        try{
            $errors = array('error' => array());

            if (empty($args['ouvintes']) || !isset($args['ouvintes'])) {
                array_push($errors['error'], 'Quantidade de Ouvintes não definido. [ouvintes]');
            }

            if (empty($args['bitrate']) || !isset($args['bitrate'])) {
                array_push($errors['error'], 'Qualidade Bitrate não definido. [bitrate]');
            }

            if (empty($args['disco']) || !isset($args['disco'])) {
                array_push($errors['error'], 'Espaço em Disco não definido. [disco]');
            }

            if (empty($args['senha']) || !isset($args['senha'])) {
                array_push($errors['error'], 'Senha não definida. [senha]');
            }

            if (empty($args['usuario']) || !isset($args['usuario'])) {
                array_push($errors['error'], 'Usuário não definido. [usuario]');
            }

            if (empty($args['email']) || !isset($args['email'])) {
                array_push($errors['error'], 'E-mail Usuário não definido. [email]');
            }

            if (empty($args['trafego']) || !isset($args['trafego'])) {
                array_push($errors['error'], 'Limite de Tráfego não definido. [trafego]');
            }

            if (empty($args['tipo']) || !isset($args['tipo'])) {
                array_push($errors['error'], 'Tipo não definido. [tipo]');
            }

            if(count($errors['error']) > 0)
            {
                return $errors;
            }

            $array = array(
                'hostname' => 'auto',
                'ipaddress' => 'auto',
                'port' => 'auto',
                'username' => @$args['usuario'],
                'adminpassword' => @$args['senha'],
                'sourcepassword' => @$args['senha'],
                'email' => @$args['email'],
                'title' => @$args['titulo'],
                'organization' => @$args['empresa'],
                'maxclients' => @$args['ouvintes'],
                'maxbitrate' => @$args['bitrate'],
                'transferlimit' => @$args['trafego'],
                'diskquota' => @$args['disco'],
                'servertype' => @$args['tipo'],
                'introfile' => '',
                'fallbackfile' => '',
                'autorebuildlist' => 1,
            );

            return $this->query('system.provision', $array);

        }catch (\Exception $e)
        {
            return $e->getMessage();
        }
    }

    /**
     * Edita uma Conta
     *
     * @param $username
     * @param $args
     * @return mixed
     */
    public function edit($username, $args)
    {
        try{
            $errors = array('error' => array());

            if (empty($args['ouvintes']) || !isset($args['ouvintes'])) {
                array_push($errors['error'], 'Quantidade de Ouvintes não definido. [ouvintes]');
            }

            if (empty($args['bitrate']) || !isset($args['bitrate'])) {
                array_push($errors['error'], 'Qualidade Bitrate não definido. [bitrate]');
            }

            if (empty($args['disco']) || !isset($args['disco'])) {
                array_push($errors['error'], 'Espaço em Disco não definido. [disco]');
            }

            if (empty($args['senha']) || !isset($args['senha'])) {
                array_push($errors['error'], 'Senha não definida. [senha]');
            }

            if (empty($args['email']) || !isset($args['email'])) {
                array_push($errors['error'], 'E-mail Usuário não definido. [email]');
            }

            if (empty($args['trafego']) || !isset($args['trafego'])) {
                array_push($errors['error'], 'Limite de Tráfego não definido. [trafego]');
            }

            if (empty($args['tipo']) || !isset($args['tipo'])) {
                array_push($errors['error'], 'Tipo não definido. [tipo]');
            }

            if(count($errors['error']) > 0)
            {
                return $errors;
            }

            $array = array(
                'hostname' => 'auto',
                'ipaddress' => 'auto',
                'port' => 'auto',
                'adminpassword' => @$args['senha'],
                'sourcepassword' => @$args['senha'],
                'email' => @$args['email'],
                'title' => @$args['titulo'],
                'organization' => @$args['empresa'],
                'maxclients' => @$args['ouvintes'],
                'maxbitrate' => @$args['bitrate'],
                'transferlimit' => @$args['trafego'],
                'diskquota' => @$args['disco'],
                'servertype' => @$args['tipo'],
            );

            return $this->query('server.reconfigure', array_merge(['username' => $username], $array));

        }catch (\Exception $e)
        {
            return $e->getMessage();
        }
    }

    /**
     * Suspende uma Conta
     *
     * @param $username
     * @return mixed
     */
    public function suspend($username)
    {
        return $this->query('system.setstatus', ['username' => $username, 'status' => 'disabled']);
    }

    /**
     * Reativa uma Conta
     *
     * @param $username
     * @return mixed
     */
    public function unsuspend($username)
    {
        return $this->query('system.setstatus', ['username' => $username, 'status' => 'enabled']);
    }

    /**
     * Remove uma Conta Permanentemente
     *
     * @param $username
     * @return mixed
     */
    public function remove($username)
    {
        return $this->query('system.terminate', ['username' => $username, 'clientaction' => 'delete']);
    }


    public function authenticate($username, $password)
    {
        return $this->query('server.authenticate', ['username' => $username, 'password' => $password]);
    }
}