<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 17/08/19 01:04
 * Projeto: API CentovaCast
 */

namespace CentovaCast\Functions;

use CentovaCast\CentovaCast;

class Stream extends CentovaCast
{
    /**
     * Define o usuário da Stream
     * @param $username
     */
    public function setStream($username)
    {
        self::$username = $username;
    }

    /**
     * Inicia o serviço Streaming
     *
     * @param $username
     * @return mixed
     */
    public function start()
    {
        return $this->query('server.start', ['username' => self::$username]);
    }

    /**
     * Reinicia o Serviço Streaming
     *
     * @param $username
     * @return mixed
     */
    public function restart()
    {
        return $this->query('server.restart', ['username' => self::$username]);
    }

    /**
     * Para o Serviço Streaming
     *
     * @param $username
     * @return mixed
     */
    public function stop()
    {
        return $this->query('server.stop', ['username' => self::$username]);
    }

    /**
     * Exibe o Streaming ativo
     *
     * @param $username
     * @param $mountpoints
     * @return mixed
     */
    public function status()
    {
        return $this->query('server.getstatus', ['username' => self::$username]);
    }

    /**
     * Exibe o histórico de musicas
     */
    public function history()
    {
        return $this->query('server.getsongs', ['username' => self::$username]);
    }

    /**
     * Retorna todos os logs da rádio
     *
     * @return mixed|object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function logs()
    {
        return $this->query('server.getlogs', ['username' => self::$username]);
    }

    /**
     * Retorna todos os ouvintes
     */
    public function listeners()
    {
        return $this->query('server.getlisteners', ['username' => self::$username]);
    }

    /**
     * Mostra detalhes de uma conta específica
     *
     * @param $username
     * @return mixed
     */
    public function info()
    {
        return $this->query('server.getaccount', ['username' => self::$username]);
    }

    /**
     * @param $username
     * @return mixed|object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function state()
    {
        return $this->query('server.enabled', ['username' => self::$username]);
    }
}