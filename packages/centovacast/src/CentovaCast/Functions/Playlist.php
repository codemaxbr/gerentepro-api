<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 17/08/19 02:02
 * Projeto: API CentovaCast
 */

namespace CentovaCast\Functions;
use CentovaCast\CentovaCast;

class Playlist extends CentovaCast
{
    //public static $username;

    public function all()
    {
        return $this->query('server.playlist', ['username' => self::$username, 'action' => 'list']);
    }

    public function remove($playlist)
    {
        return $this->query('server.playlist', ['username' => self::$username, 'action' => 'remove', 'playlist' => $playlist]);
    }
}