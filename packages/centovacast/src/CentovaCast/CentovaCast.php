<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 16/08/19 17:58
 * Projeto: API CentovaCast
 */

namespace CentovaCast;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * @method static accounts()
 * @method static stream()
 */
class CentovaCast
{
    public static $user;
    public static $pass;

    public static $host;
    public static $port;

    public static $username;

    private static $instance;

    private $args;
    private $options;

    public $error = array();

    private $format = 'json';

    public function __construct(){

    }

    /**
     * @param $method
     * @param null $arguments
     * @return mixed
     */
    public static function __callStatic($method, $arguments = null)
    {
        $class = '\\CentovaCast\Functions\\'.ucfirst($method);
        if(!class_exists($class, true)){
            die("{$class} não existe.\n");
        }

        return new $class();
    }

    /**
     * @param $token
     * @throws \Exception
     */
    public static function setAuth($user, $pass, $port)
    {
        if(empty($user)) {
            throw new \Exception('Usuário é obrigatório.');
        }

        if(empty($pass)) {
            throw new \Exception('Senha é obrigatório.');
        }

        if(empty($port)) {
            throw new \Exception('A porta é obrigatória.');
        }

        self::$user = $user;
        self::$pass = $pass;
        self::$port = $port;
    }

    private function getHost()
    {
        if (empty(self::$host)){
            throw new \Exception('Hostname do servidor CentovaCast é obrigatório.');
        }

        return self::$host;
    }

    /**
     * Define a conexão com o servidor ao chamar a classe
     * @param $ip [obrigatório]
     * @throws \Exception
     */
    public static function setHost($hostname)
    {
        if(empty($hostname)) {
            throw new \Exception('Hostname do servidor CentovaCast é obrigatório.');
        }

        self::$host = $hostname;
    }

    public function reportError($param, $message)
    {
        $array = [
            'param' => $param,
            'verbose' => $message,
        ];
        array_push($this->error, $array);
    }

    /**
     * @param string $uri
     * @param array $args
     * @return mixed|object
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    protected function query($uri = '', $args = array())
    {
        try
        {
            $host               = $this->getHost().':'.self::$port;
            $args['password']   = self::$user.'|'.self::$pass;

            $client = new Client(['base_uri' => 'http://'.$host]);
            $query  = urldecode('&f='. $this->format . (!empty($args) ? '&'.http_build_query(['a' => $args]) : ''));

            $response = $client->request('GET', '/api.php?xm='. $uri . $query);
            return json_decode($response->getBody()->getContents());

        } catch (RequestException $e) {
            if($e->hasResponse())
            {
                $http_code = $e->getResponse()->getStatusCode();
                $error = json_decode($e->getResponse()->getBody()->getContents());

                if($http_code == 403){
                    throw new \Exception("Acesso negado", $http_code);
                }

                return (object) [
                    'code' => $http_code,
                    'error' => $error
                ];
            }else{
                throw new \Exception("Não foi possível se conectar ao Servidor: ".self::$host);
            }
        }
    }
}