<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 20/08/19 22:21
 * Projeto: API Centos Web Panel
 */

namespace Cwp7;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * @method static accounts()
 * @method static packages()
 */
class Cwp7
{
    public static $api_token;
    public static $host;

    public function __construct(){

    }

    /**
     * @param $method
     * @param null $arguments
     * @return mixed
     */
    public static function __callStatic($method, $arguments = null)
    {
        $class = '\\Cwp7\Functions\\'.ucfirst($method);
        if(!class_exists($class, true)){
            die("{$class} não existe.\n");
        }

        return new $class();
    }

    public static function testConn()
    {
        $cwp = self::query('typeserver', ['action' => 'list']);
        if(!is_null($cwp)){
            if($cwp->status == "OK"){
                return true;
            }else{
                throw new \Exception("Acesso negado: Chave API inválida.");
            }
        }else{
            throw new \Exception("Não foi possível conectar ao servidor: ".self::$host);
        }
    }

    /**
     * @param $token
     * @throws \Exception
     */
    public static function setToken($host, $key)
    {
        if(empty($host)) {
            throw new \Exception('IP do servidor CWP7 é obrigatório.');
        }

        if(empty($key)) {
            throw new \Exception('Chave API é obrigatória.');
        }

        self::$host = $host;
        self::$api_token = $key;
    }

    /**
     * @param string $uri
     * @param array $args
     * @return mixed|object
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    protected function query($uri = '', $args = array(), $debug = false)
    {
        try{
            $args['key'] = self::$api_token;

            $client = new Client();
            $response = $client->request('POST', 'https://'.self::$host.':2304/v1/'.$uri, [
                'timeout' => 10,
                'verify' => false,
                'form_params' => $args
            ]);

            $response = $response->getBody()->getContents();
            if($debug){
                $response_bugado = substr($response, 0, strpos($response, "<pre>"));
                return json_decode($response_bugado);
            }else{
                return json_decode($response);
            }

        }catch (RequestException $e){
            if($e->hasResponse())
            {
                $http_code = $e->getResponse()->getStatusCode();
                $error = $e->getResponse()->getBody()->getContents();

                if($http_code == 403){
                    throw new \Exception("Acesso negado", $http_code);
                }

                return (object) [
                    'code' => $http_code,
                    'error' => $e->getMessage()
                ];
            }else{
                throw new \Exception("Não foi possível se conectar ao Servidor: ".self::$host);
            }
        }

    }
}
