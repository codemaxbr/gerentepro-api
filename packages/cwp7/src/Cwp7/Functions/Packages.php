<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 20/08/19 22:45
 * Projeto: API Centos Web Panel
 */
namespace Cwp7\Functions;
use Cwp7\Cwp7;
use GuzzleHttp\Client;

class Packages extends Cwp7
{
    public function all()
    {
        $client = new Client();
        $response = $client->request('POST', 'https://'.self::$host.':2304/v1/packages', [
            'timeout' => 10,
            'verify' => false,
            'form_params' => ['key' => self::$api_token, 'action' => 'list']
        ]);

        $cwp = json_decode($response->getBody()->getContents());

        if($cwp->status == "OK"){
            if(is_array($cwp->msj)){
                return $cwp->msj;
            }else{
                return [];
            }
        }else{
            return (object) [
                'status' => 'error',
                'message' => $cwp->msj
            ];
        }
    }

    /**
     * Criar um pacote
     *
     * @param array $param
     * @return object|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function create($param = array())
    {
        $errors = array('error' => array());

        if(!isset($param['name']) || empty($param['name']))
        {
            array_push($errors['error'], "Nome é obrigatório. [name]");
        }

        if(!isset($param['disk']))
        {
            array_push($errors['error'], "O Espaço em Disco é obrigatório. [disk]");
        }

        if(!isset($param['bwlimit']) || empty($param['bwlimit']))
        {
            array_push($errors['error'], "O Limite de Tráfego é obrigatório. [bwlimit]");
        }

        if(!isset($param['emails']) || empty($param['emails']))
        {
            array_push($errors['error'], "A Quantidade de contas de emails é obrigatório. [emails]");
        }

        if(isset($param['addons'])){
            $args['addons_domains'] = $param['addons'];
        }

        if(isset($param['maxsql'])){
            $args['databases'] = $param['maxsql'];
        }

        if(isset($param['maxpark'])){
            $args['parked_domains'] = $param['maxpark'];
        }

        if(count($errors['error']) > 0)
        {
            return $errors;
        }

        $args['action']         = "add";
        $args['package_name']   = $param['name'];
        $args['disk_quota']     = $param['disk'];
        $args['bandwidth']      = $param['bwlimit'];
        $args['email_accounts'] = $param['emails'];
        $args['email_lists']    = 99999999;
        $args['sub_domains']    = 99999999;
        $args['parked_domains'] = 99999999;
        $args['ftp_accounts']   = 99999999;

        $cwp = $this->query('packages', $args);

        if($cwp->status == "OK") {
            return (object) [
                'status' => 'success',
                'message' => 'Plano "'.$param['name'].'" criado com sucesso.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => $cwp->msj
            ];
        }
    }

    /**
     * Remover um pacote
     *
     * @param string $package
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function remove($package = '', $package_id = null)
    {
        if(empty($package))
        {
            throw new \Exception("Nome do plano é obrigatório.");
        }

        $cwp = $this->query('packages', ['action' => 'del', 'package_name' => $package]);
        if($cwp->status == "OK") {
            return (object) [
                'status' => 'success',
                'message' => 'Plano "'.$package.'" foi removido.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => $cwp->msj
            ];
        }
    }

    /**
     * Editar configuração de um pacote
     *
     * @param string $package
     * @param array $param
     * @return object|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update($package = '', $param = array())
    {
        $errors = array('error' => array());

        if(!isset($param['disk']))
        {
            array_push($errors['error'], "O Espaço em Disco é obrigatório. [disk]");
        }

        if(!isset($param['bwlimit']) || empty($param['bwlimit']))
        {
            array_push($errors['error'], "O Limite de Tráfego é obrigatório. [bwlimit]");
        }

        if(!isset($param['emails']) || empty($param['emails']))
        {
            array_push($errors['error'], "A Quantidade de contas de emails é obrigatório. [emails]");
        }

        if(isset($param['addons'])){
            $args['addons_domains'] = $param['addons'];
        }

        if(isset($param['maxsql'])){
            $args['databases'] = $param['maxsql'];
        }

        if(isset($param['maxpark'])){
            $args['parked_domains'] = $param['maxpark'];
        }

        if(count($errors['error']) > 0)
        {
            return $errors;
        }

        $args['action']         = "udp";
        $args['package_name']   = $package;
        $args['disk_quota']     = $param['disk'];
        $args['bandwidth']      = $param['bwlimit'];
        $args['email_accounts'] = $param['emails'];
        $args['email_lists']    = 99999999;
        $args['sub_domains']    = 99999999;
        $args['parked_domains'] = 99999999;
        $args['ftp_accounts']   = 99999999;

        $cwp = $this->query('packages', $args);
        if($cwp->status == "OK") {
            return (object) [
                'status' => 'success',
                'message' => 'Plano "'.$package.'" modificado com sucesso.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => $cwp->msj
            ];
        }
    }

    /**
     * Fazer Downgrade / Upgrade de pacote para um usuário
     *
     * @param string $username
     * @param string $package
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function change($username = '', $package = '')
    {
        if(empty($username)){
            throw new \Exception('O campo "Usuário" é obrigatório');
        }

        if(empty($package)){
            throw new \Exception('O campo "Plano" é obrigatório');
        }

        $packages = $this->all();
        $pkg_id = null;

        foreach ($packages as $pkg){
            if($pkg->package_name == $package){
                $pkg_id = $pkg->id;
            }
        }

        $cwp = $this->query('changepack', ['action' => 'udp', 'user' => $username, 'package' => $pkg_id]);
        if($cwp->status == "OK") {
            return (object) [
                'status' => 'success',
                'message' => 'A plano de "'.$username.'" modificado para "'.$package.'".'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => $cwp
            ];
        }
    }
}