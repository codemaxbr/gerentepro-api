<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 22/08/19 22:22
 * Projeto: API Centos Web Panel
 */

namespace Cwp7\Functions;
use Cwp7\Cwp7;

/**
 * Gera senhas aleatórias
 *
 * @param int $qtyCaraceters quantidade de caracteres na senha, por padrão 8
 * @return String
 */
function gerarSenha($qtyCaraceters = 8)
{
    //Letras minúsculas embaralhadas
    $smallLetters = str_shuffle('abcdefghijklmnopqrstuvwxyz');

    //Letras maiúsculas embaralhadas
    $capitalLetters = str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ');

    //Números aleatórios
    $numbers = (((date('Ymd') / 12) * 24) + mt_rand(800, 9999));
    $numbers .= 1234567890;

    //Caracteres Especiais
    $specialCharacters = str_shuffle('!@#$*-');

    //Junta tudo
    $characters = $capitalLetters.$smallLetters.$numbers.$specialCharacters;

    //Embaralha e pega apenas a quantidade de caracteres informada no parâmetro
    $password = substr(str_shuffle($characters), 0, $qtyCaraceters);

    //Retorna a senha
    return $password;
}

/**
 * Class Accounts
 * @package cPanel\Functions
 */

class Accounts extends Cwp7
{
    /**
     * Lista todas as contas
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function all()
    {
        $cwp = $this->query('account', ['action' => 'list']);
        if($cwp->status == "OK"){
            if(is_array($cwp->msj)){
                return $cwp->msj;
            }else{
                return [];
            }
        }else{
            return (object) [
                'status' => 'error',
                'message' => $cwp->msj
            ];
        }
    }

    /**
     * Criar uma nova conta
     *
     * @param array $param
     * @return array|object|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function create($param = array())
    {
        try{
            $errors = array('error' => array());

            if(!isset($param['user']) || empty($param['user']))
            {
                array_push($errors['error'], "O Usuário é obrigatório. [user]");
            }

            if(!isset($param['domain']) || empty($param['domain']))
            {
                array_push($errors['error'], "O Domínio é obrigatório. [domain]");
            }

            if(!isset($param['email']))
            {
                array_push($errors['error'], "O E-mail de contato é obrigatório. [email]");
            }

            if(!isset($param['plan']))
            {
                array_push($errors['error'], "O Pacote é obrigatório. [plan]");
            }

            if(count($errors['error']) > 0)
            {
                return $errors;
            }

            $senha = gerarSenha(10);

            $packages = self::packages()->all();
            $pkg_id = null;

            foreach ($packages as $pkg){
                if($pkg->package_name == $param['plan']){
                    $pkg_id = $pkg->id;
                }
            }

            $args = [
                'action' => 'add',
                'user' => $param['user'],
                'domain' => $param['domain'],
                'email' => $param['email'],
                'pass' => $senha,
                'package' => $pkg_id,
                'server_ips' => self::$host
            ];

            $cwp = $this->query('account', $args);
            if($cwp->status == "OK"){
                return (object) [
                    'status' => 'success',
                    'message' => 'A conta "'.$param['domain'].'" foi criada com sucesso.',
                    'data' => (object) [
                        'domain' => $param['domain'],
                        'ip' => self::$host,
                        'username' => $param['user'],
                        'password' => $senha,
                        'package' => $param['plan'],
                        'email_contact' => $param['email']
                    ]
                ];
            }else{
                return (object) [
                    'status' => 'error',
                    'message' => $cwp->msj
                ];
            }

        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * Suspender uma conta
     *
     * @param string $username
     * @param string $reason
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function suspend($username = '')
    {
        if(empty($username)){
            throw new \Exception("O Usuário é obrigatório.");
        }

        $cwp = $this->query('account', ['action' => 'susp', 'user' => $username]);

        if($cwp->status == "OK"){
            return (object) [
                'status' => 'success',
                'message' => 'A conta "'.$username.'" foi suspensa.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => $cwp->msj
            ];
        }
    }

    /**
     * Reativar uma conta que estava suspensa
     *
     * @param string $username
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function unsuspend($username = '')
    {
        if(empty($username)){
            throw new \Exception("O Usuário é obrigatório.");
        }

        $cwp = $this->query('account', ['action' => 'unsp', 'user' => $username]);
        if($cwp->status == "OK"){
            return (object) [
                'status' => 'success',
                'message' => 'A conta "'.$username.'" foi reativada.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => $cwp->msj
            ];
        }
    }

    /**
     * Alterar senha de uma conta
     *
     * @param string $username
     * @param string $password
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function password($username = '', $password = '')
    {
        if(!isset($username) || empty($username))
        {
            throw new \Exception("O Usuário é obrigatório.");
        }

        if(!isset($password) || empty($password))
        {
            throw new \Exception("A nova senha é obrigatória.");
        }

        $args = [
            'action' => 'udp',
            'user' => $username,
            'pass' => $password,
        ];

        $cwp = $this->query('changepass', $args);
        if($cwp->status == "OK"){
            return (object) [
                'status' => 'success',
                'message' => 'A senha da conta "'.$username.'" foi alterada.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => $cwp
            ];
        }
    }

    /**
     * Definir limite de tráfego / banda para uma conta
     *
     * @param string $username
     * @param string $limit
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function bw_limit($username = '', $limit = '')
    {
        if(!isset($username) || empty($username))
        {
            throw new \Exception("O Usuário é obrigatório.");
        }

        if(!isset($limit) || empty($limit))
        {
            throw new \Exception("O Limite de Tráfego é obrigatória.");
        }

        $args = [
            'api.version' => 1,
            'user' => $username,
            'bwlimit' => $limit,
        ];

        $whm = $this->query('limitbw', $args);
        if(isset($whm->metadata->result) && $whm->metadata->result == 1){
            return (object) [
                'status' => 'success',
                'message' => 'Limite de banda alterada com sucesso.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => 'Você não tem acesso a conta "'.$username.'".'
            ];
        }
    }

    /**
     * Definir limite de espaço em disco para uma conta
     *
     * @param string $username
     * @param string $limit
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function quota($username = '', $limit = '')
    {
        if(!isset($username) || empty($username))
        {
            throw new \Exception("O Usuário é obrigatório.");
        }

        if(!isset($limit) || empty($limit))
        {
            throw new \Exception("O espaço em disco é obrigatório.");
        }

        $args = [
            'api.version' => 1,
            'user' => $username,
            'quota' => $limit,
        ];

        $whm = $this->query('editquota', $args);
        if(isset($whm->metadata->result) && $whm->metadata->result == 1){
            return (object) [
                'status' => 'success',
                'message' => 'Espaço em disco alterado com sucesso.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => 'Você não tem acesso a conta "'.$username.'".'
            ];
        }
    }

    /**
     * Altera as configurações de uma conta (personalizada, requer permissões)
     *
     * @param string $username
     * @param array $param
     * @return mixed|object|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update($username = '', $param = array())
    {
        try{

            if(empty($username)){
                throw new \Exception('O Usuário é obrigatório');
            }

            $args['api.version'] = 1;

            if(isset($param['bwlimit']) && !empty($param['bwlimit'])){
                $args['DWLIMIT'] = $param['bwlimit'];
            }

            if(isset($param['email_contact']) && !empty($param['email_contact'])){
                $args['contactemail'] = $param['email_contact'];
            }

            if(isset($param['domain']) && !empty($param['domain'])){
                $args['DNS'] = $param['domain'];
            }

            if(isset($param['sqls']) && !empty($param['sqls'])){
                $args['MAXSQL'] = $param['sqls'];
            }

            if(isset($param['emails']) && !empty($param['emails'])){
                $args['MAXPOP'] = $param['emails'];
            }

            if(isset($param['new_user']) && !empty($param['new_user'])){
                $args['newuser'] = $param['new_user'];
            }

            if(isset($param['disk']) && !empty($param['disk'])){
                $args['QUOTA'] = $param['disk'];
            }

            $whm = $this->query('modifyacct', $args);

            //if(isset($whm->metadata->result) && $whm->metadata->result == 1){
            return $whm;

        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function info($username = ''){
        $cwp = $this->query('accountdetail', ['action' => 'list', 'user' => $username]);
        return $cwp->result;
    }

    /**
     * Remover uma conta no WHM
     *
     * @param string $username
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function remove($username = '')
    {
        if(empty($username)){
            throw new \Exception("Usuário é obrigatório");
        }

        $args['action'] = 'del';
        $args['user'] = $username;

        $cwp = $this->query('account', $args, true);

        if($cwp->status == "OK"){
            return (object) [
                'status' => 'success',
                'message' => 'A conta "'.$username.'" foi removida.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => $cwp
            ];
        }

    }

    /**
     * Exibir o uso de espaço em disco
     *
     * @param string $username
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function disk_usage($username = '')
    {
        if(empty($username)){
            throw new \Exception("Usuário é obrigatório");
        }

        $args = [
            'api.version' => 1,
            'api.filter.enable' => 1,
            'api.filter.a.field' => 'user',
            'api.filter.a.type' => 'eq',
            'api.filter.a.arg0' => $username
        ];

        $whm = $this->query('get_disk_usage', $args);
        if(isset($whm->metadata->result) && $whm->metadata->result == 1){
            return (object) [
                'status' => 'success',
                'data' => (object) [
                    'disk_limit' => $whm->data->accounts[0]->blocks_limit,
                    'disk_used'  => $whm->data->accounts[0]->blocks_used
                ]
            ];
        }
    }

    /**
     * Exibir o uso de tréfego / banda
     *
     * @param $username
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function bw_usage($username)
    {
        if(empty($username)){
            throw new \Exception("Usuário é obrigatório");
        }

        $args = [
            'api.version' => 1,
            'month' => date('m'),
            'searchtype' => 'user',
            'search' => $username
        ];

        $whm = $this->query('showbw', $args);
        if(isset($whm->metadata->result) && $whm->metadata->result == 1){
            return (object) [
                'status' => 'success',
                'data' => (object) [
                    'bw_limit' => $whm->data->acct[0]->limit,
                    'bw_used'  => $whm->data->acct[0]->bwusage[0]->usage
                ]
            ];
        }
    }
}