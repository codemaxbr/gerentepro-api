<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 16/08/19 17:58
 * Projeto: API cPanel/WHM
 */

namespace cPanel;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * @method static accounts()
 * @method static packages()
 */
class cPanel
{
    public static $whm_user;
    public static $api_token;
    public static $whm_pass;

    public static $host;

    private static $instance;

    private $args;
    private $options;

    public function __construct(){

    }

    /**
     * @param $method
     * @param null $arguments
     * @return mixed
     */
    public static function __callStatic($method, $arguments = null)
    {
        $class = '\\cPanel\Functions\\'.ucfirst($method);
        if(!class_exists($class, true)){
            die("{$class} não existe.\n");
        }

        return new $class();
    }

    /**
     * @param $token
     * @throws \Exception
     */
    public static function setToken($user_whm, $token)
    {
        if(empty($user_whm)) {
            throw new \Exception('Usuário WHM é obrigatório.');
        }

        if(empty($token)) {
            throw new \Exception('API Token é obrigatório.');
        }

        self::$whm_user = $user_whm;
        self::$api_token = $token;
    }

    private function getHost()
    {
        if (empty(self::$host)){
            throw new \Exception('IP do servidor WHM é obrigatório.');
        }

        return self::$host;
    }

    /**
     * Define a conexão com o servidor ao chamar a classe
     * @param $ip [obrigatório]
     * @throws \Exception
     */
    public static function setHost($hostname)
    {
        if(empty($hostname)) {
            throw new \Exception('Hostname do servidor WHM é obrigatório.');
        }

        self::$host = $hostname;
    }

    /**
     * @param string $uri
     * @param array $args
     * @return mixed|object
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    protected function query($uri = '', $args = array())
    {
        try
        {
            $client = new Client();
            $response = $client->request('GET', 'https://'.$this->getHost().':2087/json-api/'.$uri, [
                'headers' => [
                    'Authorization' => 'whm '. self::$whm_user.':'.self::$api_token
                ],
                'timeout' => 30,
                'query' => $args
            ]);

            return json_decode($response->getBody()->getContents());

        } catch (RequestException $e) {
            if($e->hasResponse())
            {
                $http_code = $e->getResponse()->getStatusCode();
                $error = json_decode($e->getResponse()->getBody()->getContents());

                if($http_code == 403){
                    throw new \Exception("Acesso negado", $http_code);
                }

                return (object) [
                    'code' => $http_code,
                    'error' => $error
                ];
            }else{
                throw new \Exception("Não foi possível se conectar ao Servidor: ".self::$host);
            }
        }
    }
}
