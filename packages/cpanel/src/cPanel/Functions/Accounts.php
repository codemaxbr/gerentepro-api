<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 16/08/19 19:05
 * Projeto: API cPanel/WHM
 */

namespace cPanel\Functions;
use cPanel\cPanel;

/**
 * Gera senhas aleatórias
 *
 * @param int $qtyCaraceters quantidade de caracteres na senha, por padrão 8
 * @return String
 */
function gerarSenha($qtyCaraceters = 8)
{
    //Letras minúsculas embaralhadas
    $smallLetters = str_shuffle('abcdefghijklmnopqrstuvwxyz');

    //Letras maiúsculas embaralhadas
    $capitalLetters = str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ');

    //Números aleatórios
    $numbers = (((date('Ymd') / 12) * 24) + mt_rand(800, 9999));
    $numbers .= 1234567890;

    //Caracteres Especiais
    $specialCharacters = str_shuffle('!@#$*-');

    //Junta tudo
    $characters = $capitalLetters.$smallLetters.$numbers.$specialCharacters;

    //Embaralha e pega apenas a quantidade de caracteres informada no parâmetro
    $password = substr(str_shuffle($characters), 0, $qtyCaraceters);

    //Retorna a senha
    return $password;
}

/**
 * Class Accounts
 * @package cPanel\Functions
 */

class Accounts extends cPanel
{
    /**
     * Lista todas as contas no WHM
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function all()
    {
        $whm = $this->query('listaccts', ['api.version' => 1]);

        if(isset($whm->metadata->result) && $whm->metadata->result == 1){
            return (object) [
                'status' => 'success',
                'accounts' => $whm->data->acct
            ];
        }
    }

    /**
     * Lista todas as contas suspensas no WHM
     *
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function suspended()
    {
        $whm = $this->query('listsuspended', ['api.version' => 1]);

        if(isset($whm->metadata->result) && $whm->metadata->result == 1){
            return (object) [
                'status' => 'success',
                'accounts' => $whm->data->account
            ];
        }
    }

    /**
     * Criar uma nova conta
     *
     * @param array $param
     * @return array|object|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function create($param = array())
    {
        try{
            $errors = array('error' => array());

            if(!isset($param['user']) || empty($param['user']))
            {
                array_push($errors['error'], "O Usuário é obrigatório. [user]");
            }

            if(!isset($param['domain']) || empty($param['domain']))
            {
                array_push($errors['error'], "O Domínio é obrigatório. [domain]");
            }

            if(!isset($param['email']))
            {
                array_push($errors['error'], "O E-mail de contato é obrigatório. [email]");
            }

            if(count($errors['error']) > 0)
            {
                return $errors;
            }

            $senha = gerarSenha(10);

            $args = [
                'api.version' => 1,
                'username' => $param['user'],
                'domain' => $param['domain'],
                'email' => $param['email'],
                'password' => $senha,
                'plan' => self::$whm_user.'_'.$param['plan'],
            ];

            $whm = $this->query('createacct', $args);

            if(isset($whm->metadata->result) && $whm->metadata->result == 1){
                return (object) [
                    'status' => 'success',
                    'message' => 'A conta "'.$param['domain'].'" foi criada com sucesso.',
                    'data' => (object) [
                        'domain' => $param['domain'],
                        'ip' => $whm->data->ip,
                        'username' => $param['user'],
                        'password' => $senha,
                        'cpmod' => 'paper_lantern',
                        'root_path' => '/home',
                        'nameserver1' => $whm->data->nameserver,
                        'nameserver2' => $whm->data->nameserver2,
                        'nameserver3' => $whm->data->nameserver3,
                        'nameserver4' => $whm->data->nameserver4,
                        'package' => $whm->data->package,
                        'email_contact' => $param['email'],
                        'language' => 'pt_br'
                    ]
                ];
            }else{
                return (object) [
                    'status' => 'error',
                    'message' => 'A conta "'.$param['domain'].'" já existe / não pode ser criada.',
                    'data' => $whm
                ];
            }

        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * Suspender uma conta
     *
     * @param string $username
     * @param string $reason
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function suspend($username = '', $reason = '')
    {
        if(empty($username)){
            throw new \Exception("Usuário é obrigatório");
        }

        $args = [
            'api.version' => 1,
            'user' => $username,
            'reason' => $reason,
        ];

        $whm = $this->query('suspendacct', $args);
        if(isset($whm->metadata->result) && $whm->metadata->result == 1){
            return (object) [
                'status' => 'success',
                'message' => 'A conta "'.$username.'" foi suspensa.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => 'A conta "'.$username.'" não existe.',
                'data' => $whm
            ];
        }
    }

    /**
     * Informações de uma conta
     */
    public function info()
    {
        $args = [
            'api.version' => 1,
            //'user' => $username,
        ];

        $whm = $this->query('getfeaturelist', $args);
        return $whm;
    }

    /**
     * Reativar uma conta que estava suspensa
     *
     * @param string $username
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function unsuspend($username = '')
    {
        if(empty($username)){
            throw new \Exception("Usuário é obrigatório");
        }

        $args = [
            'api.version' => 1,
            'user' => $username,
        ];

        $whm = $this->query('unsuspendacct', $args);
        if(isset($whm->metadata->result) && $whm->metadata->result == 1){
            return (object) [
                'status' => 'success',
                'message' => 'A conta "'.$username.'" foi reativada.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => 'A conta "'.$username.'" não existe.',
                'data' => $whm
            ];
        }
    }

    /**
     * Alterar senha de uma conta
     *
     * @param string $username
     * @param string $password
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function password($username = '', $password = '')
    {
        if(!isset($username) || empty($username))
        {
            throw new \Exception("O Usuário é obrigatório.");
        }

        if(!isset($password) || empty($password))
        {
            throw new \Exception("A nova senha é obrigatória.");
        }

        $args = [
            'api.version' => 1,
            'user' => $username,
            'password' => $password,
        ];

        $whm = $this->query('passwd', $args);
        if($whm->metadata->result == 0){
            return (object) [
                'status' => 'success',
                'message' => 'Lamento, mas a senha que você escolheu é muito fraca.'
            ];
        }else{
            return (object) [
                'status' => 'success',
                'message' => 'A senha da conta "'.$username.'" foi alterada.'
            ];
        }
    }

    /**
     * Definir limite de tráfego / banda para uma conta
     *
     * @param string $username
     * @param string $limit
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function bw_limit($username = '', $limit = '')
    {
        if(!isset($username) || empty($username))
        {
            throw new \Exception("O Usuário é obrigatório.");
        }

        if(!isset($limit) || empty($limit))
        {
            throw new \Exception("O Limite de Tráfego é obrigatória.");
        }

        $args = [
            'api.version' => 1,
            'user' => $username,
            'bwlimit' => $limit,
        ];

        $whm = $this->query('limitbw', $args);
        if(isset($whm->metadata->result) && $whm->metadata->result == 1){
            return (object) [
                'status' => 'success',
                'message' => 'Limite de banda alterada com sucesso.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => 'Você não tem acesso a conta "'.$username.'".'
            ];
        }
    }

    /**
     * Definir limite de espaço em disco para uma conta
     *
     * @param string $username
     * @param string $limit
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function quota($username = '', $limit = '')
    {
        if(!isset($username) || empty($username))
        {
            throw new \Exception("O Usuário é obrigatório.");
        }

        if(!isset($limit) || empty($limit))
        {
            throw new \Exception("O espaço em disco é obrigatório.");
        }

        $args = [
            'api.version' => 1,
            'user' => $username,
            'quota' => $limit,
        ];

        $whm = $this->query('editquota', $args);
        if(isset($whm->metadata->result) && $whm->metadata->result == 1){
            return (object) [
                'status' => 'success',
                'message' => 'Espaço em disco alterado com sucesso.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => 'Você não tem acesso a conta "'.$username.'".'
            ];
        }
    }

    /**
     * Altera as configurações de uma conta (personalizada, requer permissões)
     *
     * @param string $username
     * @param array $param
     * @return mixed|object|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update($username = '', $param = array())
    {
        try{

            if(empty($username)){
                throw new \Exception('O Usuário é obrigatório');
            }

            $args['api.version'] = 1;

            if(isset($param['bwlimit']) && !empty($param['bwlimit'])){
                $args['DWLIMIT'] = $param['bwlimit'];
            }

            if(isset($param['email_contact']) && !empty($param['email_contact'])){
                $args['contactemail'] = $param['email_contact'];
            }

            if(isset($param['domain']) && !empty($param['domain'])){
                $args['DNS'] = $param['domain'];
            }

            if(isset($param['sqls']) && !empty($param['sqls'])){
                $args['MAXSQL'] = $param['sqls'];
            }

            if(isset($param['emails']) && !empty($param['emails'])){
                $args['MAXPOP'] = $param['emails'];
            }

            if(isset($param['new_user']) && !empty($param['new_user'])){
                $args['newuser'] = $param['new_user'];
            }

            if(isset($param['disk']) && !empty($param['disk'])){
                $args['QUOTA'] = $param['disk'];
            }

            $whm = $this->query('modifyacct', $args);

            //if(isset($whm->metadata->result) && $whm->metadata->result == 1){
            return $whm;

        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * Remover uma conta no WHM
     *
     * @param string $username
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function remove($username = '')
    {
        if(empty($username)){
            throw new \Exception("Usuário é obrigatório");
        }

        $args = [
            'api.version' => 1,
            'user' => $username,
        ];

        $whm = $this->query('removeacct', $args);

        if(isset($whm->metadata->result) && $whm->metadata->result == 1){
            return (object) [
                'status' => 'success',
                'message' => 'A conta "'.$username.'" foi removida.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => 'A conta "'.$username.'" não existe.',
                'data' => $whm
            ];
        }
    }

    /**
     * Exibir o uso de espaço em disco
     *
     * @param string $username
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function disk_usage($username = '')
    {
        if(empty($username)){
            throw new \Exception("Usuário é obrigatório");
        }

        $args = [
            'api.version' => 1,
            'api.filter.enable' => 1,
            'api.filter.a.field' => 'user',
            'api.filter.a.type' => 'eq',
            'api.filter.a.arg0' => $username
        ];

        $whm = $this->query('get_disk_usage', $args);
        if(isset($whm->metadata->result) && $whm->metadata->result == 1){
            return (object) [
                'status' => 'success',
                'data' => (object) [
                    'disk_limit' => $whm->data->accounts[0]->blocks_limit,
                    'disk_used'  => $whm->data->accounts[0]->blocks_used
                ]
            ];
        }
    }

    /**
     * Exibir o uso de tréfego / banda
     *
     * @param $username
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function bw_usage($username)
    {
        if(empty($username)){
            throw new \Exception("Usuário é obrigatório");
        }

        $args = [
            'api.version' => 1,
            'month' => date('m'),
            'searchtype' => 'user',
            'search' => $username
        ];

        $whm = $this->query('showbw', $args);
        if(isset($whm->metadata->result) && $whm->metadata->result == 1){
            return (object) [
                'status' => 'success',
                'data' => (object) [
                    'bw_limit' => $whm->data->acct[0]->limit,
                    'bw_used'  => $whm->data->acct[0]->bwusage[0]->usage
                ]
            ];
        }
    }
}