<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 17/08/19 21:20
 * Projeto: API cPanel/WHM
 */
namespace cPanel\Functions;
use cPanel\cPanel;

class Packages extends cPanel
{
    public function all()
    {
        $whm = $this->query('listpkgs', ['api.version' => 1]);
        return $whm;
        //return (object) [
        //    'status' => 'success',
        //    'packages' => $whm->data->pkg
        //];
    }

    /**
     * Criar um pacote
     *
     * @param array $param
     * @return object|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function create($param = array())
    {
        try{
            $errors = array('error' => array());

            if(!isset($param['name']) || empty($param['name']))
            {
                array_push($errors['error'], "Nome é obrigatório. [name]");
            }

            if(!isset($param['disk']) || empty($param['disk']))
            {
                array_push($errors['error'], "O Espaço em Disco é obrigatório. [disk]");
            }

            if(!isset($param['bwlimit']) || empty($param['bwlimit']))
            {
                array_push($errors['error'], "O Limite de Tráfego é obrigatório. [bwlimit]");
            }

            if(isset($param['ip'])){
                $args['ip'] = $param['ip'];
            }

            if(isset($param['cgi'])){
                $args['cgi'] = $param['cgi'];
            }

            if(isset($param['frontpage'])){
                $args['frontpage'] = $param['frontpage'];
            }

            if(isset($param['theme'])){
                $args['cpmod'] = $param['theme'];
            }

            if(isset($param['maxpop'])){
                $args['maxpop'] = $param['maxpop'];
            }

            if(isset($param['maxsql'])){
                $args['maxsql'] = $param['maxsql'];
            }

            if(isset($param['maxaddon'])){
                $args['maxaddon'] = $param['maxaddon'];
            }

            if(isset($param['maxpark'])){
                $args['maxpark'] = $param['maxpark'];
            }

            if(isset($param['hasshell'])){
                $args['hasshell'] = $param['hasshell'];
            }

            if(count($errors['error']) > 0)
            {
                return $errors;
            }

            $args['api.version'] = 1;
            $args['name'] = $param['name'];
            $args['quota'] = $param['disk'];
            $args['bwlimit'] = $param['bwlimit'];
            $args['language'] = 'pt_br';

            $whm = $this->query('addpkg', $args);

            if(isset($whm->metadata->result) && $whm->metadata->result == 1){
                return (object) [
                    'status' => 'success',
                    'message' => 'O plano "'.$param['name'].'" foi configurado.'
                ];
            }else{
                return (object) [
                    'status' => 'error',
                    'verbose' => 'O plano "'.$param['name'].'" já existe.',
                    'data' => $whm
                ];
            }
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * Remover um pacote
     *
     * @param string $package
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function remove($package = '')
    {
        $whm = $this->query('killpkg', ['api.version' => 1, 'pkgname' => self::$whm_user.'_'.$package]);
        if(isset($whm->metadata->result) && $whm->metadata->result == 1){
            return (object) [
                'status' => 'success',
                'message' => 'O plano "'.$package.'" foi removido.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => 'O plano "'.$package.'" não existe.',
                'data' => $whm
            ];
        }
    }

    /**
     * Detalhes de um pacote
     *
     * @param string $package
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function info($package = '')
    {
        if(empty($package)){
            throw new \Exception("Plano é obrigatório");
        }

        $whm = $this->query('getpkginfo', ['api.version' => 1, 'pkg' => self::$whm_user.'_'.$package]);
        if(isset($whm->metadata->result) && $whm->metadata->result == 1){

            return (object) [
                'status' => 'success',
                'data' => (object) [
                    'disk' => $whm->data->pkg->QUOTA,
                    'bwlimit' => $whm->data->pkg->BWLIMIT,
                    'email_accounts' => ($whm->data->pkg->MAXPOP == NULL) ? 'unlimited' : $whm->data->pkg->MAXPOP,
                    'domains_addons' => ($whm->data->pkg->MAXADDON == NULL) ? 'unlimited' : $whm->data->pkg->MAXADDON,
                    'theme' => $whm->data->pkg->CPMOD,
                    'sqls' => ($whm->data->pkg->MAXSQL == NULL) ? 'unlimited' : $whm->data->pkg->MAXSQL,
                    'domains_park' => ($whm->data->pkg->MAXPARK == NULL) ? 'unlimited' : $whm->data->pkg->MAXPARK,
                    'acess_shell' => ($whm->data->pkg->HASSHELL == 1) ? 1 : 0,
                    'cgi' => ($whm->data->pkg->CGI == 1) ? 1 : 0,
                    'ip' => ($whm->data->pkg->IP == 1) ? 1 : 0,
                    'frontpage' => ($whm->data->pkg->FRONTPAGE == 1) ? 1 : 0,
                    'language' => $whm->data->pkg->LANG,
                    'subdomains' => ($whm->data->pkg->MAXSUB == NULL) ? 'unlimited' : $whm->data->pkg->MAXSUB,
                ],
                'message' => 'OK'
            ];
        }
        else{
            return (object) [
                'status' => 'error',
                'message' => 'O plano "'.$package.'" não existe.'
            ];
        }
    }

    /**
     * Editar configuração de um pacote
     *
     * @param string $package
     * @param array $param
     * @return object|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update($package = '', $param = array())
    {
        try{
            $errors = array('error' => array());

            if(!isset($package) || empty($package))
            {
                array_push($errors['error'], "O nome do plano precisa ser preenchido.");
            }

            if(!isset($param['disk']) || empty($param['disk']))
            {
                array_push($errors['error'], "O Espaço em Disco é obrigatório. [disk]");
            }

            if(!isset($param['bwlimit']) || empty($param['bwlimit']))
            {
                array_push($errors['error'], "O Limite de Tráfego é obrigatório. [bwlimit]");
            }

            if(isset($param['ip'])){
                $args['ip'] = $param['ip'];
            }

            if(isset($param['cgi'])){
                $args['cgi'] = $param['cgi'];
            }

            if(isset($param['frontpage'])){
                $args['frontpage'] = $param['frontpage'];
            }

            if(isset($param['theme'])){
                $args['cpmod'] = $param['theme'];
            }

            if(isset($param['maxpop'])){
                $args['maxpop'] = $param['maxpop'];
            }

            if(isset($param['maxsql'])){
                $args['maxsql'] = $param['maxsql'];
            }

            if(isset($param['maxaddon'])){
                $args['maxaddon'] = $param['maxaddon'];
            }

            if(isset($param['maxpark'])){
                $args['maxpark'] = $param['maxpark'];
            }

            if(isset($param['hasshell'])){
                $args['hasshell'] = $param['hasshell'];
            }

            if(count($errors['error']) > 0)
            {
                return $errors;
            }

            $args['api.version'] = 1;
            $args['name'] = $package;
            $args['quota'] = $param['disk'];
            $args['bwlimit'] = $param['bwlimit'];
            $args['language'] = 'pt_br';

            $whm = $this->query('editpkg', $args);

            if(isset($whm->metadata->result) && $whm->metadata->result == 1){
                return (object) [
                    'status' => 'success',
                    'message' => 'O plano "'.$package.'" foi re-configurado.'
                ];
            }else{
                return (object) [
                    'status' => 'error',
                    'message' => 'Não foi possível fazer as alterações do plano "'.$package.'".',
                    'data' => $whm
                ];
            }

        }catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Fazer Downgrade / Upgrade de pacote para um usuário
     *
     * @param string $username
     * @param string $package
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function change($username = '', $package = '')
    {
        if(empty($username)){
            throw new \Exception('O campo "Usuário" é obrigatório');
        }

        if(empty($package)){
            throw new \Exception('O campo "Plano" é obrigatório');
        }

        $whm = $this->query('changepackage', ['api.version' => 1, 'user' => $username, 'pkg' => self::$whm_user.'_'.$package]);
        if(isset($whm->metadata->result) && $whm->metadata->result == 1){
            return (object) [
                'status' => 'success',
                'message' => 'Upgrade/Downgrade Completo para "'.$username.'"'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => 'O Plano "'.$package.'" ou Usuário "'.$username.'" não existe.',
                'data' => $whm
            ];
        }
    }
}