<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 20/08/19 17:21
 * Projeto: API VestaCP
 */

namespace VestaCP;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * @method static accounts()
 * @method static packages()
 */
class VestaCP
{
    public static $username;
    public static $password;
    public static $return_code;

    public static $hash;

    public static $host;

    public function __construct(){

    }

    /**
     * @param $method
     * @param null $arguments
     * @return mixed
     */
    public static function __callStatic($method, $arguments = null)
    {
        $class = '\\VestaCP\Functions\\'.ucfirst($method);
        if(!class_exists($class, true)){
            die("{$class} não existe.\n");
        }

        return new $class();
    }

    /**
     * @param $token
     * @throws \Exception
     */
    public static function setAuth($username, $password)
    {
        if(empty($username)) {
            throw new \Exception('Usuário é obrigatório.');
        }

        if(empty($password)) {
            throw new \Exception('Senha é obrigatória.');
        }

        self::$username = $username;
        self::$password = $password;
    }

    private function getHost()
    {
        if (empty(self::$host)){
            throw new \Exception('IP do servidor é obrigatório.');
        }

        return self::$host;
    }

    /**
     * Define a conexão com o servidor ao chamar a classe
     * @param $ip [obrigatório]
     * @throws \Exception
     */
    public static function setHost($hostname)
    {
        if(empty($hostname)) {
            throw new \Exception('IP do servidor é obrigatório.');
        }

        self::$host = $hostname;
    }

    public static function generateKey()
    {
        $params['user'] = self::$username;
        $params['password'] = self::$password;
        $params['cmd'] = 'v-generate-api-key';
        $params['returncode'] = 'no';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://'.self::$host.':8083/api/');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);

        return curl_exec($curl);
    }

    /**
     * @param string $uri
     * @param array $args
     * @return mixed|object
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    protected function query($cmd, $args = [])
    {
        $params['user'] = self::$username;
        $params['password'] = self::$password;
        $params['cmd'] = $cmd;
        $params['returncode'] = self::$return_code;

        for ($i = 1; $i < count($args); $i++){
            $params['arg'.$i] = $args[$i];
        }

        $postdata = http_build_query(array_filter($params));

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://'.self::$host.':8083/api/');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);

        return json_decode(curl_exec($curl));
    }
}
