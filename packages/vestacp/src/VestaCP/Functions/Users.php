<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 20/08/19 19:08
 * Projeto: API VestaCP
 */

namespace VestaCP\Functions;
use VestaCP\VestaCP;

class Users extends VestaCP
{
    public function create($params = array())
    {
        $args = [
            'user'      => self::$username,
            'password'  => self::$password,
            'cmd'       => 'v-add-user',
            'returncode' => 'no',
            'arg1'      => $params['user'],
            'arg2'      => $params['password'],
            'arg3'      => $params['email'],
            'arg4'      => $params['plan'],
            'arg5'      => $params['first_name'],
            'arg6'      => $params['last_name']
        ];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://'.self::$host.':8083/api/');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $args);

        $response = curl_exec($curl);
        if($response == "OK"){
            return (object) [
                'status' => 'success',
                'message' => 'Usuário "'.$params['user'].'" criado com sucesso.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => $response
            ];
        }
    }

    public function enable_ssh($username)
    {
        $args = [
            'user'      => self::$username,
            'password'  => self::$password,
            'cmd'       => 'v-change-user-shell',
            'returncode' => 'no',
            'arg1'      => $username,
            'arg2'      => 'bash'
        ];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://'.self::$host.':8083/api/');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $args);

        $response = curl_exec($curl);
        if($response == "OK"){
            return (object) [
                'status' => 'success',
                'message' => 'Acesso SSH liberado para "'.$username.'".'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => $response
            ];
        }
    }

    public function suspend($username)
    {
        $args = [
            'user'      => self::$username,
            'password'  => self::$password,
            'cmd'       => 'v-suspend-user',
            'returncode' => 'no',
            'arg1'      => $username,
        ];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://'.self::$host.':8083/api/');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $args);

        $response = curl_exec($curl);
        if($response == "OK"){
            return (object) [
                'status' => 'success',
                'message' => 'Acesso suspenso para "'.$username.'".'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => $response
            ];
        }
    }

    public function unsuspend($username)
    {
        $args = [
            'user'      => self::$username,
            'password'  => self::$password,
            'cmd'       => 'v-unsuspend-user',
            'returncode' => 'no',
            'arg1'      => $username,
        ];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://'.self::$host.':8083/api/');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $args);

        $response = curl_exec($curl);
        if($response == "OK"){
            return (object) [
                'status' => 'success',
                'message' => 'Acesso liberado para "'.$username.'".'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => $response
            ];
        }
    }

    public function remove($username)
    {
        $args = [
            'user'      => self::$username,
            'password'  => self::$password,
            'cmd'       => 'v-delete-user',
            'returncode' => 'no',
            'arg1'      => $username,
        ];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://'.self::$host.':8083/api/');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $args);

        $response = curl_exec($curl);
        if($response == "OK"){
            return (object) [
                'status' => 'success',
                'message' => 'A conta de "'.$username.'" foi removida.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => $response
            ];
        }
    }

    public function password($username, $pass)
    {
        $args = [
            'user'      => self::$username,
            'password'  => self::$password,
            'cmd'       => 'v-change-user-password',
            'returncode' => 'no',
            'arg1'      => $username,
            'arg2'      => $pass
        ];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://'.self::$host.':8083/api/');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $args);

        $response = curl_exec($curl);
        if($response == "OK"){
            return (object) [
                'status' => 'success',
                'message' => 'Senha alterada para "'.$username.'".'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => $response
            ];
        }
    }
}