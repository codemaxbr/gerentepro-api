<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 20/08/19 20:42
 * Projeto: API VestaCP
 */

namespace VestaCP\Functions;
use VestaCP\VestaCP;

class Packages extends VestaCP
{
    public function all()
    {
        $args = [
            'user'      => self::$username,
            'password'  => self::$password,
            'cmd'       => 'v-list-user-packages',
            'returncode' => 'no',
            'arg1'      => 'json'
        ];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://'.self::$host.':8083/api/');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $args);

        $response = json_decode(curl_exec($curl));
        $plans = array();

        foreach ($response as $name => $plan){
            array_push($plans, (object) [
                'name' => $name,
                'web_template' => $plan->WEB_TEMPLATE,
                'proxy_template' => $plan->PROXY_TEMPLATE,
                'dns_template' => $plan->DNS_TEMPLATE,
                'domains' => $plan->WEB_DOMAINS,
                'aliases' => $plan->WEB_ALIASES,
                'dns_domains' => $plan->DNS_DOMAINS,
                'dns_records' => $plan->DNS_RECORDS,
                'mails' => $plan->MAIL_ACCOUNTS,
                'databases' => $plan->DATABASES,
                'cron_jobs' => $plan->CRON_JOBS,
                'disk_quota' => $plan->DISK_QUOTA,
                'bw_limit' => $plan->BANDWIDTH,
                'ns' => explode(',', $plan->NS),
                'shell' => $plan->SHELL,
                'backups' => $plan->BACKUPS,
                'created_at' => $plan->DATE.' '.$plan->TIME
            ]);
        }

        return $plans;
    }

    public function change($username, $plan)
    {
        $args = [
            'user'      => self::$username,
            'password'  => self::$password,
            'cmd'       => 'v-change-user-package',
            'returncode' => 'no',
            'arg1'      => $username,
            'arg2'      => $plan
        ];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://'.self::$host.':8083/api/');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $args);

        $response = curl_exec($curl);
        if($response == "OK"){
            return (object) [
                'status' => 'success',
                'message' => 'Plano alterado para "'.$plan.'", usuário: "'.$username.'".'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => $response
            ];
        }
    }
}