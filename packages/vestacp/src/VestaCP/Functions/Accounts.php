<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 20/08/19 18:00
 * Projeto: API VestaCP
 */

namespace VestaCP\Functions;
use VestaCP\VestaCP;

class Accounts extends VestaCP
{
    public function all()
    {
        self::$return_code = 'no';

        $vesta = $this->query('v-list-sys-users', [self::$username, 'json']);
        return $vesta;
    }

    public function create($params = array())
    {
        $args = [
            'user'      => self::$username,
            'password'  => self::$password,
            'cmd'       => 'v-add-domain',
            'returncode' => 'no',
            'arg1'      => $params['user'],
            'arg2'      => $params['domain'],
            'arg3'      => self::$host,
        ];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://'.self::$host.':8083/api/');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $args);

        $response = curl_exec($curl);

        if($response == "OK"){
            return (object) [
                'status' => 'success',
                'message' => 'Conta "'.$params['domain'].'" criado com sucesso.'
            ];
        }else{
            return (object) [
                'status' => 'error',
                'message' => $response
            ];
        }
    }
}