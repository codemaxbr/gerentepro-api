<?php
namespace CodemaxBR\Iugu;

class Iugu
{
    public $apiKey;
    public $accountId;

    /**
     * Iugu constructor.
     * @param $apiKey
     * @param $accountId
     */

    public function setCredentials($apiKey, $accountId)
    {
        $this->apiKey = $apiKey;
        $this->accountId = $accountId;
    }

    public function __call($method, $arguments = null)
    {
        $class = '\\CodemaxBR\Iugu\Functions\\'.ucfirst($method);
        if(!class_exists($class, true)){
            throw new \Exception("{$class} não existe.");
        }

        return new $class($this->apiKey);
    }
}