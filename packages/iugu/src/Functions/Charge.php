<?php

namespace CodemaxBR\Iugu\Functions;

class Charge extends API
{
    public function boleto(array $params)
    {
        return $this->postRequest('charge', $params);
    }
}