<?php
namespace CodemaxBR\Iugu\Functions;

class Customers extends API
{
    public function all(array $query = [])
    {
        return $this->getRequest("customers", $query);
    }
}