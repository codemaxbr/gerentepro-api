<?php
namespace CodemaxBR\Iugu\Functions;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Middleware;

class API
{
    protected $endpoint = "https://api.iugu.com/v1/";
    private $client;
    private $headers = array();
    private $token = '';

    /**
     * API constructor.
     * @param string $endpoint
     * @param string $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    public function getRequest($uri, $args = [])
    {
        try{
            $client = new Client();
            $credentials = base64_encode($this->token);

            $response = $client->request('GET', $this->endpoint.$uri, [
                'auth' => [$this->token, null],
                'json' => $args,
            ]);

            return json_decode($response->getBody()->getContents());

        }catch (RequestException $e){
            if ($e->hasResponse()){
                $http_code = $e->getResponse()->getStatusCode();
                $error = json_decode($e->getResponse()->getBody()->getContents());

                if($http_code == 401){
                    throw new \Exception($error->errors, $http_code);
                }else{
                    return $error;
                }
            }
        }
    }
    public function postRequest($uri, array $args)
    {
        try{
            $client = new Client();
            $credentials = base64_encode($this->token);

            $response = $client->request('POST', $this->endpoint.$uri, [
                'auth' => [$this->token, null],
                'json' => $args,
            ]);

            return json_decode($response->getBody()->getContents());

        }catch (RequestException $e){
            if ($e->hasResponse()){
                $http_code = $e->getResponse()->getStatusCode();
                $error = json_decode($e->getResponse()->getBody()->getContents());

                if($http_code == 401){
                    throw new \Exception($error->errors, $http_code);
                }else{
                    return $error;
                }
            }
        }
    }
    public function putRequest($uri, array $args)
    {
        try{
            $client = new Client();
            $credentials = base64_encode($this->token);

            $response = $client->request('PUT', $this->endpoint.$uri, [
                'auth' => [$this->token, null],
                'json' => $args,
            ]);

            return json_decode($response->getBody()->getContents());

        }catch (RequestException $e){
            if ($e->hasResponse()){
                $http_code = $e->getResponse()->getStatusCode();
                $error = json_decode($e->getResponse()->getBody()->getContents());

                if($http_code == 401){
                    throw new \Exception($error->errors, $http_code);
                }else{
                    return $error;
                }
            }
        }
    }
    public function deleteRequest($uri, array $args)
    {
        try{
            $client = new Client();
            $credentials = base64_encode($this->token);

            $response = $client->request('DELETE', $this->endpoint.$uri, [
                'auth' => [$this->token, null],
                'json' => $args,
            ]);

            return json_decode($response->getBody()->getContents());

        }catch (RequestException $e){
            if ($e->hasResponse()){
                $http_code = $e->getResponse()->getStatusCode();
                $error = json_decode($e->getResponse()->getBody()->getContents());

                if($http_code == 401){
                    throw new \Exception($error->errors, $http_code);
                }else{
                    return $error;
                }
            }
        }
    }

    private function response($response){
        dd($response);

        if($response->getStatusCode() == 200)
        {
            return (object) ['response' => json_decode($response->getBody()->getContents())];
        }

        if($response->getStatusCode() == 401){
            $error = json_decode($response->getBody()->getContents());
            throw new \Exception($error->errors, $response->getStatusCode());
        }

        else{
            return (object) [
                'status' => $response->getStatusCode(),
                'response' => json_decode($response->getBody()->getContents())
            ];
        }
    }
}