<?php
namespace CodemaxBR\Iugu\Providers;

use Illuminate\Support\ServiceProvider;
use CodemaxBR\Iugu\Iugu;

class IuguServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/iugu.php', 'iugu');

        $this->publishes([
            __DIR__ . '/../config/iugu.php' => config_path('iugu.php'),
        ]);
    }

    public function register()
    {
        $this->app->bind('CodemaxBR\Iugu', function(){
            return new Iugu(config()->get('iugu.token'), config()->get('iugu.account_id'));
        });
    }
}