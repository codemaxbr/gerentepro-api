<?php
namespace CodemaxBR\F2b\Facades;

use Illuminate\Support\Facades\Facade;

class F2b extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'CodemaxBR\F2b';
    }
}