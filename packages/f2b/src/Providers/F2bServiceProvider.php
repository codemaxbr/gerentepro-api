<?php
namespace CodemaxBR\F2b\Providers;

use CodemaxBR\F2b\F2b;
use Illuminate\Support\ServiceProvider;

class F2bServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/f2b.php', 'f2b');

        $this->publishes([
            __DIR__ . '/../config/f2b.php' => config_path('f2b.php'),
        ]);
    }

    public function register()
    {
        $this->app->bind('CodemaxBR\F2b', function(){
            return new F2b(config()->get('f2b.token'));
        });
    }
}