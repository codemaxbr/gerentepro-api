<?php
namespace CodemaxBR\F2b;

class F2b
{
    public $token;

    public function setCredentials($token)
    {
        $this->token = $token;
    }

    public function __call($method, $arguments = null)
    {
        $class = '\\CodemaxBR\F2b\Functions\\'.ucfirst($method);
        if(!class_exists($class, true)){
            throw new \Exception("{$class} não existe.");
        }

        return new $class($this->token);
    }
}