<?php
namespace CodemaxBR\F2b\Functions;

class Cobrancas extends API
{
    public function listar(array $query = [])
    {
        return $this->getRequest("cobrancas", $query);
    }

    public function registrar(array $query = [])
    {
        return $this->postRequest("cobrancas", $query);
    }
}