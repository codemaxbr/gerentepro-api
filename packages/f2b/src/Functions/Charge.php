<?php

namespace CodemaxBR\F2b\Functions;

class Charge extends API
{
    public function boleto(array $params)
    {
        return $this->postRequest('charge', $params);
    }
}