<?php
namespace CodemaxBR\Padmoney\Providers;

use CodemaxBR\Padmoney\Padmoney;
use Illuminate\Support\ServiceProvider;

class PadmoneyServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/padmoney.php', 'padmoney');

        $this->publishes([
            __DIR__ . '/../config/padmoney.php' => config_path('padmoney.php'),
        ]);
    }

    public function register()
    {
        $this->app->bind('CodemaxBR\Padmoney', function(){
            return new Padmoney(config()->get('padmoney.token'), config()->get('padmoney.account_id'));
        });
    }
}