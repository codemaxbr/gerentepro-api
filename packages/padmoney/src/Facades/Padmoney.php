<?php
namespace CodemaxBR\Padmoney\Facades;

use Illuminate\Support\Facades\Facade;

class Padmoney extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'CodemaxBR\Padmoney';
    }
}