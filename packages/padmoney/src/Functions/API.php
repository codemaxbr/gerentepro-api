<?php
namespace CodemaxBR\Padmoney\Functions;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Middleware;

class API
{
    protected $endpoint = "https://api.padmoney.com/v2/";
    private $client;
    private $headers = array();
    private $token = '';
    private $chave = '';

    /**
     * API constructor.
     * @param string $endpoint
     * @param string $token
     */
    public function __construct($token, $chave)
    {
        $this->token = $token;
        $this->chave = $chave;
    }

    public function getRequest($uri, $args = [])
    {
        try{
            $client = new Client();
            $response = $client->request('GET', $this->endpoint.$uri, [
                'headers' => [
                    'Padmoney-Token' => $this->token,
                    'Padmoney-Token-Secret' => $this->chave,
                ],
                'json' => $args,
            ]);

            return json_decode($response->getBody()->getContents());

        }catch (RequestException $e){
            if ($e->hasResponse()){
                $http_code = $e->getResponse()->getStatusCode();
                $error = json_decode($e->getResponse()->getBody()->getContents());

                if($http_code == 401){
                    throw new \Exception("Não autorizado", $http_code);
                }else{
                    return $http_code;
                }
            }
        }
    }
    public function postRequest($uri, array $args)
    {
        try{
            $client = new Client();
            $response = $client->request('POST', $this->endpoint.$uri, [
                'headers' => [
                    'Padmoney-Token' => $this->token,
                    'Padmoney-Token-Secret' => $this->chave,
                ],
                'json' => $args,
            ]);

            return json_decode($response->getBody()->getContents());

        }catch (RequestException $e){
            if ($e->hasResponse()){
                $http_code = $e->getResponse()->getStatusCode();
                $error = json_decode($e->getResponse()->getBody()->getContents());

                if($http_code == 401){
                    throw new \Exception("Não autorizado", $http_code);
                }else{
                    return $http_code;
                }
            }
        }
    }
    public function putRequest($uri, array $args)
    {
        try{
            $client = new Client();
            $response = $client->request('PUT', $this->endpoint.$uri, [
                'headers' => [
                    'Padmoney-Token' => $this->token,
                    'Padmoney-Token-Secret' => $this->chave,
                ],
                'json' => $args,
            ]);

            return json_decode($response->getBody()->getContents());

        }catch (RequestException $e){
            if ($e->hasResponse()){
                $http_code = $e->getResponse()->getStatusCode();
                $error = json_decode($e->getResponse()->getBody()->getContents());

                if($http_code == 401){
                    throw new \Exception("Não autorizado", $http_code);
                }else{
                    return $http_code;
                }
            }
        }
    }
    public function deleteRequest($uri, array $args)
    {
        try{
            $client = new Client();
            $response = $client->request('DELETE', $this->endpoint.$uri, [
                'headers' => [
                    'Padmoney-Token' => $this->token,
                    'Padmoney-Token-Secret' => $this->chave,
                ],
                'json' => $args,
            ]);

            return json_decode($response->getBody()->getContents());

        }catch (RequestException $e){
            if ($e->hasResponse()){
                $http_code = $e->getResponse()->getStatusCode();
                $error = json_decode($e->getResponse()->getBody()->getContents());

                if($http_code == 401){
                    throw new \Exception("Não autorizado", $http_code);
                }else{
                    return $http_code;
                }
            }
        }
    }
}