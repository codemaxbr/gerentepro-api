<?php

namespace CodemaxBR\Padmoney\Functions;

class Boletos extends API
{
    public function all(array $params = [])
    {
        return $this->getRequest('bank_billets', $params);
    }

    public function create(array $params)
    {
        return $this->postRequest('bank_billets', $params);
    }

    public function status($id)
    {
        return $this->getRequest('bank_billets/'.$id);
    }
}