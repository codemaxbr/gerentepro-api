<?php
namespace CodemaxBR\Padmoney;

class Padmoney
{
    public $token;
    public $chave;

    /**
     * Iugu constructor.
     * @param $apiKey
     * @param $accountId
     */

    public function setCredentials($token, $chave)
    {
        $this->token = $token;
        $this->chave = $chave;
    }

    public function __call($method, $arguments = null)
    {
        $class = '\\CodemaxBR\Padmoney\Functions\\'.ucfirst($method);
        if(!class_exists($class, true)){
            throw new \Exception("{$class} não existe.");
        }

        return new $class($this->token, $this->chave);
    }
}