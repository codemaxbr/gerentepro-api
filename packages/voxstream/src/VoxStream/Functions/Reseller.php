<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 17/08/19 02:02
 * Projeto: API CentovaCast
 */

namespace VoxStream\Functions;
use VoxStream\VoxStream;

class Reseller extends VoxStream
{
    /**
     * Criar uma Nova Conta de Revenda
     *
     * @param $args
     */
    public function create($args = array())
    {
        try{
            $errors = array('error' => array());

            if (empty($args['ouvintes']) || !isset($args['ouvintes'])) {
                array_push($errors['error'], 'Quantidade de Ouvintes não definido. [ouvintes]');
            }

            if (empty($args['bitrate']) || !isset($args['bitrate'])) {
                array_push($errors['error'], 'Qualidade Bitrate não definido. [bitrate]');
            }

            if (empty($args['disco']) || !isset($args['disco'])) {
                array_push($errors['error'], 'Espaço em Disco não definido. [disco]');
            }

            if (empty($args['senha']) || !isset($args['senha'])) {
                array_push($errors['error'], 'Senha não definida. [senha]');
            }

            if (empty($args['android']) || !isset($args['android'])) {
                array_push($errors['error'], 'Aplicativo Android não definido. [android]');
            }

            if (empty($args['streamings']) || !isset($args['streamings'])) {
                array_push($errors['error'], 'Limite de Streamings não definido. [streamings]');
            }

            if (empty($args['email']) || !isset($args['email'])) {
                array_push($errors['error'], 'E-mail principal não definido. [email]');
            }

            if (empty($args['subrevendas']) || !isset($args['subrevendas'])) {
                array_push($errors['error'], 'Quantidade de Sub-Revendas não definido. [subrevendas]');
            }

            if(count($errors['error']) > 0)
            {
                return $errors;
            }

            $array = array(
                'streamings' => @$args['streamings'],
                'ouvintes' => @$args['ouvintes'],
                'bitrate' => @$args['bitrate'],
                'disco' => @$args['disco'],
                'senha' => @$args['senha'],
                'aacplus' => 'sim',
                'lingua' => 'pt-br',
                'android' => @$args['android'],
                'encoder' => 'sim',
                'enc_aacplus' => 'sim',
                'subrevendas' => @$args['subrevendas'],
                'email_subrevenda' => @$args['email'],
            );

            $params = implode('/', $array);
            return $this->query('cadastrar_subrevenda', $params);

        }catch (\Exception $e)
        {
            return $e->getMessage();
        }
    }

    /**
     * Suspende uma Conta
     *
     * @param $username
     * @return mixed
     */
    public function suspend($id_revenda = '')
    {
        if (empty($id_revenda) || !isset($id_revenda)) {
            throw new \Exception('ID Revenda é obrigatório.');
        }

        return $this->query('bloquear_subrevenda', $id_revenda);
    }

    /**
     * Reativa uma Conta
     *
     * @param $username
     * @return mixed
     */
    public function unsuspend($id_revenda = '')
    {
        if (empty($id_revenda) || !isset($id_revenda)) {
            throw new \Exception('ID Revenda é obrigatório.');
        }

        return $this->query('desbloquear_subrevenda', $id_revenda);
    }

    /**
     * Remove uma Conta Permanentemente
     *
     * @param $username
     * @return mixed
     */
    public function remove($id_revenda = '')
    {
        if (empty($id_revenda) || !isset($id_revenda)) {
            throw new \Exception('ID Revenda é obrigatório.');
        }

        return $this->query('remover_subrevenda', $id_revenda);
    }
}