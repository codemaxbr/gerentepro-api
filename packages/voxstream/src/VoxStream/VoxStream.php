<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 16/08/19 17:58
 * Projeto: API VoxStream
 */

namespace VoxStream;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use mysql_xdevapi\Exception;

/**
 * @method static accounts()
 */
class VoxStream
{
    public static $chave_api;

    public static $host;
    public static $porta;

    public function __construct(){

    }

    /**
     * @param $method
     * @param null $arguments
     * @return mixed
     */
    public static function __callStatic($method, $arguments = null)
    {
        $class = '\\VoxStream\Functions\\'.ucfirst($method);
        if(!class_exists($class, true)){
            die("{$class} não existe.\n");
        }

        return new $class();
    }

    /**
     * @param $token
     * @throws \Exception
     */
    public static function setAuth($host, $chave_api)
    {
        if(empty($host)) {
            throw new \Exception('Hostname do servidor VoxStream é obrigatório');
        }

        if(empty($chave_api)) {
            throw new \Exception('Chave API é obrigatória.');
        }

        self::$host = $host;
        self::$chave_api = $chave_api;
    }

    private function getHost()
    {
        if (empty(self::$host)){
            throw new \Exception('Hostname do servidor VoxStream é obrigatório.');
        }

        return self::$host;
    }

    private function getChaveAPI()
    {
        if (empty(self::$chave_api)){
            throw new \Exception('Chave API é obrigatória.');
        }

        return self::$chave_api;
    }

    /**
     * Define a conexão com o servidor ao chamar a classe
     * @param $ip [obrigatório]
     * @throws \Exception
     */
    public static function setHost($hostname)
    {
        if(empty($hostname)) {
            throw new \Exception('Hostname do servidor VoxStream é obrigatório.');
        }

        self::$host = $hostname;
    }

    public static function getCredencials()
    {
        $retorno = self::accounts()->suspend('99999');

        if($retorno->message == "Permissao negada."){
            return TRUE;
        }else{
            throw new \Exception('Chave da API inválida.');
        }
    }

    /**
     * @param string $uri
     * @param array $args
     * @return mixed|object
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    protected function query($action = '', $arguments = array())
    {
        try
        {
            $host = $this->getHost();
            $chave = $this->getChaveAPI();

            $client = new Client(['base_uri' => $host.'/admin/api/'.$chave.'/']);
            $response = $client->request('GET', $action.'/'.$arguments);

            $retorno = $response->getBody()->getContents();
            list($status, $dados, $msg) = explode('|', $retorno);

            if($status == 1){
                return (object)[
                    'status' => 'success',
                    'data' => $dados,
                    'message' => $msg
                ];
            }else{
                return (object)[
                    'status' => 'false',
                    'data' => $dados,
                    'message' => $msg
                ];
            }

        } catch (RequestException $e) {
            if($e->hasResponse())
            {
                $http_code = $e->getResponse()->getStatusCode();
                $error = json_decode($e->getResponse()->getBody()->getContents());

                if($http_code == 403){
                    throw new \Exception("Acesso negado", $http_code);
                }

                return (object) [
                    'code' => $http_code,
                    'error' => $error
                ];
            }else{
                throw new \Exception("Não foi possível se conectar ao Servidor: ".self::$host);
            }
        }
    }
}