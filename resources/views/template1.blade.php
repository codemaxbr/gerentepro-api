<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,900&display=swap" rel="stylesheet">
    <style type="text/css" media="all">
        .body {
            background: #eee;
            font-family: 'Roboto', sans-serif !important;
            letter-spacing: -0.4px;
            text-align: center;
            display: block;
            height: 100%;
            padding-top: 50px;
            padding-bottom: 50px;
            font-size: 14px;
        }

        p {margin: 0px;}
        .copyright a {color: #555; text-decoration: none;}
        .copyright a:hover {color: #000; text-decoration: underline;}

        table {
            width: 700px;
            margin: 0 auto;
            background: #fff;
            box-shadow: 0px 0px 40px #ccc;
            border-radius: 5px;
        }

        table td{
            background: #fff;
            padding: 20px 25px;
            text-align: left;
        }

        .logo{ max-height: 50px; }

        .copyright{
            margin-top: 30px;
            font-size: 13px;
            color: #888;
        }

    </style>
</head>
<body>
    <div class="body">
        <table>
            <tr>
                <td>
                    <img src="{{ $logo }}" class="logo" alt="" />
                </td>
            </tr>
            <tr>
                <td>{!! $content !!}</td>
            </tr>
        </table>

        <div class="copyright">
            <a href="#">Não consigo visualizar este e-mail</a><br />
            aaaaaaa
        </div>
    </div>
</body>
</html>