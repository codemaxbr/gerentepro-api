<?php

/**
 * Servidores
 */

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => '/servers', 'middleware' => 'jwt.auth'], function(){
    Route::get('/',              ['uses' => 'ServerController@index', 'as' => 'servers.list']);
    Route::post('/',             ['uses' => 'ServerController@store', 'as' => 'server.create']);
    Route::put('/{id}/update',   ['uses' => 'ServerController@update', 'as' => 'server.update']);
    Route::get('/{id}/detalhes', ['uses' => 'ServerController@show', 'as' => 'server.info']);
    Route::delete('/{id}',       ['uses' => 'ServerController@destroy', 'as' => 'server.destroy']);
});