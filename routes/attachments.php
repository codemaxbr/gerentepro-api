<?php

/**
 * Anexos e Documentos
 */

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => '/attachments', 'middleware' => 'jwt.auth'], function(){
    Route::get('/',                 ['uses' => 'AttachmentController@index', 'as' => 'attachments.list']);
    Route::get('/{uuid}',           ['uses' => 'AttachmentController@show',  'as' => 'attachments.show']);
    Route::post('/',                ['uses' => 'AttachmentController@store', 'as' => 'attachments.store']);
    Route::post('/{uuid}/update',   ['uses' => 'AttachmentController@update', 'as' => 'attachments.update']);
    Route::delete('/{uuid}',        ['uses' => 'AttachmentController@destroy', 'as' => 'attachments.destroy']);
});