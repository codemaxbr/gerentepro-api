<?php
/**
 * Cupons de desconto
 */

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => '/promotions', 'middleware' => 'jwt.auth'], function(){
    Route::get('/',                         ['uses' => 'PromotionController@index', 'as' => 'promotions.list']);
    Route::get('/{id}/info',                ['uses' => 'PromotionController@show', 'as' => 'promotion.info']);
    Route::post('/',                        ['uses' => 'PromotionController@store', 'as' => 'promotion.store']);
    Route::put('/{id}/update',              ['uses' => 'PromotionController@update', 'as' => 'promotion.update']);

    Route::get('/{id}/vouchers',            ['uses' => 'PromotionController@vouchers', 'as' => 'promotion.vouchers']);
    Route::delete('/{id}/vouchers',         ['uses' => 'PromotionController@removeVouchers', 'as' => 'promotion.vouchers.destroy']);
    Route::post('/{code}/validate/voucher', ['uses' => 'PromotionController@useVoucher', 'as' => 'promotion.voucher.validate']); //todo falta terminar esse
    Route::delete('/{id}/destroy',          ['uses' => 'PromotionController@destroy', 'as' => 'promotion.destroy']);
});