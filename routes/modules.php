<?php
use Illuminate\Support\Facades\Route;

/**
 * Módulos Config
 */
Route::group(['prefix' => '/modules', 'middleware' => 'jwt.auth'], function(){
    Route::get('/',              ['uses' => 'ModuleController@index', 'as' => 'modules.list']);
    Route::get('/info/{plugin}', ['uses' => 'ModuleController@info', 'as' => 'module.info']);
    Route::post('/',             ['uses' => 'ModuleController@store', 'as' => 'modules.store']);
    Route::get('/configs/{id}',  ['uses' => 'ModuleController@getModule', 'as' => 'module.config']);
    Route::put('/{id}/update',   ['uses' => 'ModuleController@update', 'as' => 'modules.update']);
    Route::delete('/{id}',       ['uses' => 'ModuleController@destroy', 'as' => 'modules.destroy']);
    Route::get('/{id}',          ['uses' => 'ModuleController@show', 'as' => 'modules.show']);
});