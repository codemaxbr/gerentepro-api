<?php

/**
 * Usuários
 */

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => '/users', 'middleware' => 'jwt.auth'], function(){
    Route::get('/',             ['uses' => 'UserController@index',      'as' => 'users.list']);
    Route::get('/{id}',         ['uses' => 'UserController@show',       'as' => 'user.info']);
    Route::put('/{id}/update',  ['uses' => 'UserController@update',     'as' => 'user.update']);
    Route::post('/',            ['uses' => 'UserController@store',      'as' => 'user.create']);
    Route::delete('/{id}',      ['uses' => 'UserController@destroy',    'as' => 'user.destroy']);
});