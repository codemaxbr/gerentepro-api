<?php
/**
 * Contas a Pagar
 */

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => '/debits', 'middleware' => 'jwt.auth'], function(){
    Route::get('/',              ['uses' => 'DebitController@index', 'as' => 'debits.list']);
    Route::get('/{id}/detalhes', ['uses' => 'DebitController@show', 'as' => 'debit.info']);
    Route::get('/count',         ['uses' => 'DebitController@count', 'as' => 'debits.count']);
    Route::put('/{id}/update',   ['uses' => 'DebitController@update', 'as' => 'debit.update']);
    Route::delete('/{id}',       ['uses' => 'DebitController@destroy', 'as' => 'debit.destroy']);
    Route::post('/',             ['uses' => 'DebitController@store', 'as' => 'debit.create']);
});