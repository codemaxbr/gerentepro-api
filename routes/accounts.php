<?php

/**
 * Meu GerentePRO
 */

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => '/accounts'], function(){

    Route::group(['middleware' => 'jwt.auth'], function(){
        Route::get('/',                     ['uses' => 'AccountController@index',       'as' => 'accounts.list', 'middleware' => 'reseller.auth']);
        Route::get('/{uuid}/info',          ['uses' => 'AccountController@info',        'as' => 'account.info']);
        Route::post('/{uuid}/update',       ['uses' => 'AccountController@update',      'as' => 'account.update']);
        Route::delete('/{uuid}',            ['uses' => 'AccountController@destroy',     'as' => 'account.destroy', 'middleware' => 'reseller.auth']);
    });

    Route::post('/register',                ['uses' => 'AccountController@store',       'as' => 'account.register']);
    Route::get('/activation/{token}',       ['uses' => 'AccountController@activation',  'as' => 'account.activation']);

    Route::get('/find/{subdomain}',         ['uses' => 'AccountController@getAccount',  'as' => 'account.find-domain']);
    Route::get('/{uuid}/scripts',           ['uses' => 'AccountController@getScripts',  'as' => 'account.scripts']);

    /**
     * Contas bancárias
     */
    Route::group(['prefix' => '/banks'], function(){
        Route::get('/',             ['uses' => 'BankController@index', 'as' => 'banks.list']);
        Route::post('/',            ['uses' => 'BankController@store', 'as' => 'banks.create']);
        Route::put('/{id}/update',  ['uses' => 'BankController@update', 'as' => 'banks.update']);
        Route::get('/{id}',         ['uses' => 'BankController@show', 'as' => 'banks.info']);
        Route::delete('/{id}',      ['uses' => 'BankController@destroy', 'as' => 'banks.destroy']);
    });
});