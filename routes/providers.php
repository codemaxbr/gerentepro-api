<?php

/**
 * Fornecedores
 */

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => '/providers', 'middleware' => 'jwt.auth'], function(){
    Route::get('/',                         ['uses' => 'ProviderController@index', 'as' => 'providers.list']);
    Route::get('/{provider_uuid}/perfil',   ['uses' => 'ProviderController@show', 'as' => 'providers.info']);
    Route::get('/count',                    ['uses' => 'ProviderController@count', 'as' => 'providers.count']);
    Route::put('/{id}/update',              ['uses' => 'ProviderController@update', 'as' => 'provider.update']);
    Route::delete('/{id}',                  ['uses' => 'ProviderController@destroy', 'as' => 'provider.destroy']);
    Route::post('/',                        ['uses' => 'ProviderController@store', 'as' => 'provider.create']);
});