<?php

/**
 * Templates de E-mail
 */

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => '/templates-emails', 'middleware' => 'jwt.auth'], function(){
    Route::get('/',                 ['uses' => 'TemplateEmailController@index',     'as' => 'template.emails.list']);
    Route::get('/signature',        ['uses' => 'TemplateEmailController@signature', 'as' => 'template.emails.signature']);
    Route::post('/signature',       ['uses' => 'TemplateEmailController@updateSignature', 'as' => 'template.emails.signature.update']);
    Route::get('/{id}',             ['uses' => 'TemplateEmailController@show',      'as' => 'template.emails.show']);
    Route::post('/{id}/update',     ['uses' => 'TemplateEmailController@update',    'as' => 'template.emails.update']);

});