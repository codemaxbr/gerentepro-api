<?php
/**
 * Revendedores
 */

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => '/resellers', 'middleware' => 'root'], function(){
    Route::get('/',         ['uses' => 'ResellerController@index', 'as' => 'resellers.list']);
    Route::get('/{id}',     ['uses' => 'ResellerController@show', 'as' => 'reseller.info']);
    Route::delete('/{id}',  ['uses' => 'ResellerController@destroy', 'as' => 'reseller.destroy']);
    Route::post('/assign',  ['uses' => 'ResellerController@store', 'as' => 'reseller.assign']);
});