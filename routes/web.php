<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function (){
    return "API GerentePRO v1";
});

/**
 * Helpers abertos
 */
Route::post('/checkout',            ['uses' => 'Front\CheckoutController@processaPagamento', 'as' => 'checkout.payment']);
Route::post('/check-register',      ['uses' => 'Front\CheckoutController@checkField', 'as' => 'check.valid.field']);
Route::get('/test-email',           ['uses' => 'ConfigController@testeEmail']);

/*
|-----------------------------------------------------------------------------------------------------------------------
| Rotas importadas
|-----------------------------------------------------------------------------------------------------------------------
*/
require 'accounts.php';
require 'auth.php';
require 'helpers.php';
require 'customers.php';
require 'plans.php';
require 'modules.php';
require 'domains.php';
require 'users.php';
require 'servers.php';
require 'providers.php';
require 'promotions.php';
require 'invoices.php';
require 'subscriptions.php';
require 'attachments.php';
require 'statements.php';
require 'templates-emails.php';
require 'resellers.php';
require 'cancellations.php';
require 'transactions.php';
require 'abandoned-carts.php';
require 'reports.php';
require 'config.php';
require 'optionals.php';
require 'logs.php';