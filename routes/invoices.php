<?php

/**
 * Faturas
 */

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => '/invoices', 'middleware' => 'jwt.auth'], function(){
    Route::get('/',                  ['uses' => 'InvoiceController@index', 'as' => 'invoices.list']); //OK
    Route::get('/export',            ['uses' => 'InvoiceController@export', 'as' => 'invoices.export']);
    Route::get('/{uuid}/details',    ['uses' => 'InvoiceController@show', 'as' => 'invoices.show']); //OK
    Route::get('/{id}/transactions', ['uses' => 'InvoiceController@transactions', 'as' => 'invoices.transactions']); //OK
    Route::post('/',                 ['uses' => 'InvoiceController@create', 'as' => 'invoices.create']); //OK
    Route::post('/{uuid}/paid',      ['uses' => 'InvoiceController@changePaid', 'as' => 'invoices.changePaid']);
    Route::post('/{uuid}/remember',  ['uses' => 'InvoiceController@remember', 'as' => 'invoices.emailRemember']);
    Route::post('/{uuid}/due',       ['uses' => 'InvoiceController@changeDue', 'as' => 'invoices.changeDue']);
    Route::get('/{uuid}/cancel',     ['uses' => 'InvoiceController@cancel', 'as' => 'invoices.cancel']);
    Route::post('/{uuid}/nulled',    ['uses' => 'InvoiceController@nulled', 'as' => 'invoices.changeNull']);
    Route::delete('/{uuid}',         ['uses' => 'InvoiceController@destroy', 'as' => 'invoices.destroy']);
    Route::put('/{uuid}/update',     ['uses' => 'InvoiceController@update', 'as' => 'invoices.update']);
});