<?php
use Illuminate\Support\Facades\Route;

/**
 * Clientes
 */
Route::group(['prefix' => '/customers', 'middleware' => 'jwt.auth'], function(){

    Route::get('/',                     ['uses' => 'CustomerController@index', 'as' => 'customers.list']);
    Route::get('/{uuid}/perfil',        ['uses' => 'CustomerController@show', 'as' => 'customer.info']);
    Route::get('/search',               ['uses' => 'CustomerController@search', 'as' => 'customers.search']);
    Route::get('/export',               ['uses' => 'CustomerController@export', 'as' => 'customers.export']);
    Route::get('{id}/subscriptions',    ['uses' => 'CustomerController@subscriptions', 'as' => 'customer.subscriptions']);
    Route::get('/count',                ['uses' => 'CustomerController@count', 'as' => 'customers.count']);
    Route::put('/{id}/update',          ['uses' => 'CustomerController@update', 'as' => 'customer.update']);
    Route::put('/{id}/address',         ['uses' => 'CustomerController@updateAddress', 'as' => 'customer.update.address']);
    Route::delete('/{uuid}/destroy',    ['uses' => 'CustomerController@destroy', 'as' => 'customer.destroy']);
    Route::post('/',                    ['uses' => 'CustomerController@store', 'as' => 'customer.create']);
    Route::post('/validate-email',      ['uses' => 'CustomerController@validaUnique', 'as' => 'customer.validate.email']);
    Route::post('/validate-cpf',        ['uses' => 'CustomerController@validaUniqueCPF', 'as' => 'customer.validate.cpf']);

    /**
     * Cartões de Crédito
     */
    Route::group(['prefix' => '/credit-cards'], function(){
        Route::get('/{customer_uuid}',          ['uses' => 'CreditCardController@index', 'as' => 'creditcards.list']); //@todo terminar de montar
        Route::post('/{customer_uuid}',         ['uses' => 'CreditCardController@store', 'as' => 'creditcards.store']); //@todo terminar de montar
        Route::get('/{customer_uuid}/{id}',     ['uses' => 'CreditCardController@show', 'as' => 'creditcards.show']); //@todo terminar de montar
        Route::delete('/{customer_uuid}/{id}',  ['uses' => 'CreditCardController@destroy', 'as' => 'creditcards.destroy']); //@todo terminar de montar
        Route::put('/{customer_uuid}/{id}',     ['uses' => 'CreditCardController@update', 'as' => 'creditcards.update']); //@todo terminar de montar
    });
});