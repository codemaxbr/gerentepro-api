<?php
use Illuminate\Support\Facades\Route;

/**
 * Autenticação e Login
 */
Route::group(['prefix' => 'auth'], function(){
    Route::get('/user',                     ['uses' => 'AuthController@validateToken', 'as' => 'auth.validate']);
    Route::post('/user',                    ['uses' => 'AuthController@authenticate', 'as' => 'auth.admin']);
    Route::post('/customer',                ['uses' => 'AuthController@authCustomer', 'as' => 'auth.front']);
    Route::post('/customer/register',       ['uses' => 'AuthController@registerLoginCustomer', 'as' => 'auth.register.front']);

    Route::get('/customer',                 ['uses' => 'AuthController@customerLogged', 'as' => 'customer.logged']);
    Route::get('/customer/confirm/{uuid}',  ['uses' => 'AuthController@activateCustomer', 'as' => 'activate.customer']);
});