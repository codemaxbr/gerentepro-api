<?php
/**
 * Helpers
 */

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => '/helpers'], function(){

    Route::group(['middleware' => 'jwt.auth'], function(){
        Route::get('/main',                 ['uses' => 'MainController@filters', 'as' => 'helpers.main']);
        Route::post('/receita-federal',     ['uses' => 'MainController@consultaReceita', 'as' => 'helpers.receita-federal']);

        Route::get('/emails',               ['uses' => 'ConfigController@emails', 'as' => 'config.emails']);
        Route::get('/emails/{tag}',         ['uses' => 'ConfigController@getTemplate', 'as' => 'config.emails.template']);

        Route::get('/modules-available',    ['uses' => 'MainController@modulesAvailable', 'as' => 'modules.available']);
    });
});