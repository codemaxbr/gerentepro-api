<?php
use Illuminate\Support\Facades\Route;

/**
 * Planos de Assinatura
 */
Route::group(['prefix' => '/plans'], function(){
    Route::get('/{uuid}/info',      ['uses' => 'Front\PlanController@show', 'as' => 'front.plan.show']);
});

Route::group(['prefix' => '/plans', 'middleware' => 'jwt.auth'], function(){
    Route::get('/',                 ['uses' => 'PlanController@index',          'as' => 'plans.list']);
    Route::post('/',                ['uses' => 'PlanController@store',          'as' => 'plan.create']);
    Route::post('/grid',            ['uses' => 'PlanController@storeGrid',      'as' => 'plan.grid.create']);
    Route::put('/grid/{id}',        ['uses' => 'PlanController@updateGrid',     'as' => 'plan.grid.update']);
    Route::delete('/grid/{id}',     ['uses' => 'PlanController@removeGrid',     'as' => 'plan.grid.destroy']);
    Route::get('/grids',            ['uses' => 'PlanController@grids',          'as' => 'plan.grids.list']);
    Route::get('/{id}/detalhes',    ['uses' => 'PlanController@show',           'as' => 'plan.info']);
    Route::put('/{id}/update',      ['uses' => 'PlanController@update',         'as' => 'plan.update']);
    Route::delete('/{id}',          ['uses' => 'PlanController@destroy',        'as' => 'plan.destroy']);

    Route::put('/prices/{id}',      ['uses' => 'PlanController@updatePrice',    'as' => 'plan.price.update']);
    Route::delete('/prices/{id}',   ['uses' => 'PlanController@removePrice',    'as' => 'plan.price.destroy']);
    Route::post('/prices',          ['uses' => 'PlanController@storePrice',     'as' => 'plan.price.store']);

    Route::post('/{id}/metadata',   ['uses' => 'PlanController@setMetadata',    'as' => 'plan.metadata.store']);
    Route::delete('/{id}/metadata', ['uses' => 'PlanController@deleteMetadata', 'as' => 'plan.metadata.destroy']);
});