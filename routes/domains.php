<?php
use Illuminate\Support\Facades\Route;

/**
 * Domínios
 */
Route::post('/whois',                ['uses' => 'WhoisController@whois',         'as' => 'whois']); //TODO: botar pra funcionar com o preço do sistema

Route::group(['prefix' => '/domains', 'middleware' => 'jwt.auth'], function()
{
    Route::get('/',                  ['uses' => 'DomainController@index',        'as' => 'domains.list']);
    Route::post('/',                 ['uses' => 'DomainController@store',        'as' => 'domain.create']);
    Route::get('/{id}/detalhes',     ['uses' => 'DomainController@show',         'as' => 'domain.info']);
    Route::put('/{id}/update',       ['uses' => 'DomainController@update',       'as' => 'domain.update']);
    Route::delete('/{id}',           ['uses' => 'DomainController@destroy',      'as' => 'domain.destroy']);
    Route::post('/whois',            ['uses' => 'DomainController@whois',        'as' => 'domain.whois']);

    Route::group(['prefix' => '/prices'], function(){
        Route::get('/',              ['uses' => 'DomainController@prices',       'as' => 'domains.prices.list']);
        Route::post('/',             ['uses' => 'DomainController@storePrice',   'as' => 'domains.price.create']);
        Route::put('/{id}/update',   ['uses' => 'DomainController@updatePrice',  'as' => 'domains.price.update']);
        Route::get('/{id}/detalhes', ['uses' => 'DomainController@showPrice',    'as' => 'domains.price.info']);
        Route::delete('/{id}',       ['uses' => 'DomainController@destroyPrice', 'as' => 'domains.price.destroy']);
    });
});