<?php
/**
 * Configurações todo falta fazer essa api
 */

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => '/config'], function(){
    Route::get('/my-account',       ['uses' => 'ConfigController@businessProfile', 'as' => 'config.business-profile']);
    Route::post('/update-account',  ['uses' => 'ConfigController@updateProfile', 'as' => 'config.update.business-profile']); //todo remover isso
});