<?php

/**
 * Assinaturas
 */

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => '/subscriptions', 'middleware' => 'jwt.auth'], function(){
    Route::get('/',                 ['uses' => 'SubscriptionController@index', 'as' => 'subscriptions.list']);
    Route::get('/{id}',             ['uses' => 'SubscriptionController@show', 'as' => 'subscriptions.show']);
    Route::put('/{id}/update',      ['uses' => 'SubscriptionController@update', 'as' => 'subscriptions.update']);
    Route::delete('/{id}',          ['uses' => 'SubscriptionController@destroy', 'as' => 'subscriptions.destroy']);
});