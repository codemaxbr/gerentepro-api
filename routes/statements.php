<?php

/**
 * Lançamentos financeiros todo falta fazer essa api
 */

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => '/statements', 'middleware' => 'jwt.auth'], function(){
    Route::get('/',             ['uses' => 'StatementController@index', 'as' => 'statements.list']);
    Route::get('/{id}',         ['uses' => 'StatementController@show', 'as' => 'statements.show']);
    Route::delete('/{id}',      ['uses' => 'StatementController@remove', 'as' => 'statements.remove']);
});