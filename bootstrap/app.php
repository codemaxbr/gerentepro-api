<?php

require_once __DIR__.'/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(dirname(__DIR__)))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/
$app = new Laravel\Lumen\Application(
    realpath(__DIR__ . '/../')
);

$app->instance('path.config', app()->basePath() . DIRECTORY_SEPARATOR . 'config');
$app->instance('path.storage', env('APP_STORAGE', app()->basePath() . DIRECTORY_SEPARATOR . 'storage'));
$app->instance('path.public', app()->basePath() . DIRECTORY_SEPARATOR . 'public');

$app->configure('cache');
$app->configure('broadcasting');
$app->configure('filesystems');
$app->configure('queue');
$app->configure('mail');
$app->configure('models');
$app->configure('database');
$app->configure('sentry');
$app->configure('auth');
$app->configure('permission');
$app->configure('cors');
$app->configure('session');
$app->configure('laravel-model-caching');

$app->alias('mailer', Illuminate\Mail\Mailer::class);
$app->alias('mailer', Illuminate\Contracts\Mail\Mailer::class);
$app->alias('mailer', Illuminate\Contracts\Mail\MailQueue::class);
$app->alias('cache', Illuminate\Cache\CacheManager::class);  // if you don't have this already

$app->register(Jenssegers\Mongodb\MongodbServiceProvider::class);
$app->register(Illuminate\Notifications\NotificationServiceProvider::class);

$app->register(CodemaxBR\Vindi\Providers\VindiServiceProvider::class);
$app->register(CodemaxBR\Iugu\Providers\IuguServiceProvider::class);
$app->register(CodemaxBR\F2b\Providers\F2bServiceProvider::class);
$app->register(CodemaxBR\Padmoney\Providers\PadmoneyServiceProvider::class);

$app->register(Illuminate\Cookie\CookieServiceProvider::class);
$app->register(Illuminate\Session\SessionServiceProvider::class);

$app->register(\GeneaLabs\LaravelModelCaching\Providers\Service::class);

//$app->register();

$app->register(Barryvdh\Cors\ServiceProvider::class);

$app->withEloquent();
$app->withFacades(true, [
    'Illuminate\Support\Facades\Notification' => 'Notification',

    'CodemaxBR\Vindi\Facades\Vindi' => 'Vindi',
    'CodemaxBR\Iugu\Facades\Iugu' => 'Iugu',
    'CodemaxBR\F2b\Facades\F2b' => 'F2b',
    'CodemaxBR\Padmoney\Facades\Padmoney' => 'Padmoney',
]);

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton('mailer', function ($app) {
    $app->configure('services');
    return $app->loadComponent('mail', Illuminate\Mail\MailServiceProvider::class, 'mailer');
});

$app->singleton('filesystem', function ($app) {
    return $app->loadComponent(
        'filesystems',
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        'filesystem'
    );
});


/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

$app->middleware([
    //\Barryvdh\Cors\HandleCors::class,
    App\Http\Middleware\CorsMiddleware::class,
]);

// $app->middleware([
//     App\Http\Middleware\ExampleMiddleware::class
// ]);

// $app->routeMiddleware([
//     'auth' => App\Http\Middleware\Authenticate::class,
// ]);

$app->routeMiddleware([
    'cors'          => \Barryvdh\Cors\HandleCors::class,

    'auth'          => App\Http\Middleware\Authenticate::class,
    'permission'    => Spatie\Permission\Middlewares\PermissionMiddleware::class,
    'role'          => Spatie\Permission\Middlewares\RoleMiddleware::class,
    'role_or_permission' => \Spatie\Permission\Middlewares\RoleOrPermissionMiddleware::class,

    'jwt.auth'      => App\Http\Middleware\JwtMiddleware::class,
    'reseller.auth' => App\Http\Middleware\ResellerMiddleware::class,
    'root'          => App\Http\Middleware\RootMiddleware::class,
    'front.auth'    => App\Http\Middleware\CustomerMiddleware::class,
    'check.domain'  => App\Http\Middleware\CheckDomain::class,
    'server.only'   => App\Http\Middleware\ServerOnly::class,
]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

$app->register(App\Providers\AppServiceProvider::class);
$app->register(App\Providers\AuthServiceProvider::class);
$app->register(App\Providers\EventServiceProvider::class);
$app->register(Flipbox\LumenGenerator\LumenGeneratorServiceProvider::class);
$app->register(Prettus\Repository\Providers\LumenRepositoryServiceProvider::class);
$app->register(Webpatser\Uuid\UuidServiceProvider::class);
$app->register(Illuminate\Mail\MailServiceProvider::class);
$app->register(Reliese\Coders\CodersServiceProvider::class);
$app->register(Illuminate\Redis\RedisServiceProvider::class);
$app->register(Sentry\Laravel\ServiceProvider::class);
$app->register(Spatie\Permission\PermissionServiceProvider::class);
$app->register(Illuminate\Filesystem\FilesystemServiceProvider::class);

$app->register(Superbalist\LaravelGoogleCloudStorage\GoogleCloudStorageServiceProvider::class);

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->router->group([
    'namespace' => 'App\Http\Controllers',
], function ($router) {
    require __DIR__.'/../routes/web.php';
});

//$app->router->group([
//    'namespace' => 'App\Http\Controllers',
//], function ($router) {
//    require __DIR__.'/../routes/front.php';
//});

return $app;
