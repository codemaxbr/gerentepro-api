<?php
namespace App\Observers;

use App\Models\ModulesAccount;

class ModuleAccountObserver
{
    public function updated(ModulesAccount $modulesAccount)
    {
        //dump('atualizou o módulo.');
        //dump($modulesAccount);
    }

    public function created(ModulesAccount $modulesAccount)
    {
        //dump('criou o módulo.');
        //dump($modulesAccount);
    }

    public function deleted(ModulesAccount $modulesAccount)
    {
        //dump('removeu o módulo.');
        //dump($modulesAccount);
    }
}