<?php
namespace App\Observers;

use App\Models\User;

class UserObserver
{
    /**
     * @-- Gravar log de atividade
     * @-- Faz a sincronização com o Módulo externo (Ex: criar plano no WHM, etc)
     * @-- Webhook personalizado
     */
    public function created(User $user)
    {
        //event(new LogActivity($customer, __FUNCTION__, customerLogged(), adminLogged()));
        //dump('Evento criado.');
    }

    public function updated(User $user)
    {
        //event(new LogActivity($customer, __FUNCTION__, customerLogged(), adminLogged()));
        //dump('Evento atualizado.');
    }

    public function deleted(User $user)
    {
        //event(new LogActivity($customer, __FUNCTION__, customerLogged(), adminLogged()));
        //dump('Evento removido.');
    }
}