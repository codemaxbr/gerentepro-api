<?php
namespace App\Observers;

use App\Models\Metadata;

class MetadataObserver
{
    /**
     * todo fazer isso funcionar, pra finalizar a integração dos plugins de Painel de controle
     * @-- Gravar log de atividade
     * @-- Faz a sincronização com o Módulo externo (Ex: criar plano no WHM, etc)
     * @-- Webhook personalizado
     */
    public function created(Metadata $metadata)
    {
        //event(new LogActivity($customer, __FUNCTION__, customerLogged(), adminLogged()));
        //dump('Evento criado.');
    }

    public function updated(Metadata $metadata)
    {
        //event(new LogActivity($customer, __FUNCTION__, customerLogged(), adminLogged()));
        //dump('Evento atualizado.');
    }

    public function deleted(Metadata $metadata)
    {
        //event(new LogActivity($customer, __FUNCTION__, customerLogged(), adminLogged()));
        //dump('Evento removido.');
    }
}