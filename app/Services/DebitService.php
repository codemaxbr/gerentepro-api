<?php
namespace App\Services;

use App\Models\Debit;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class DebitService
{

    private $debitRepository;

    /**
     * DebitService constructor.
     */
    public function __construct()
    {

    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getAll()
    {
        return Debit::with(['provider', 'payment_cycle'])->where(['account_id' => AccountId()])->paginate(20);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createDebit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description' => 'required',
            'provider_id' => 'required | integer | exists:providers,id',
            'total' => 'required | numeric',
            'due' => 'required'
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{

            try{
                $request['due'] = Carbon::createFromFormat('d/m/Y', $request->due)->toDateString();
                $request['account_id'] = AccountId();
                $debit = Debit::create($request->all());

                return \response()->json([
                    'status' => 'success',
                    'message' => 'Despesa adicionada com sucesso!',
                    'data' => $debit
                ], Response::HTTP_OK);

            }catch (\Exception $e){

                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDebit($id)
    {
        $debit = Debit::find($id);

        if($debit == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Esta conta não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        return $debit;
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|\Illuminate\Http\JsonResponse|null
     */
    public function updateDebit($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description' => 'required',
            'provider_id' => 'required | integer | exists:providers,id',
            'payment_cycle_id' => 'required | integer',
            'total' => 'required | numeric | between:0,99.99',
            'due' => 'required | date'
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{

            try{

                $debit = Debit::query()->where(['account_id' => AccountId()])->find($id);

                if(!empty($request->birthdate) && $request->has('birthdate'))
                {
                    $date = explode('/', $request->birthdate);
                    $request->birthdate = $date[2].'-'.$date[1].'-'.$date[0];
                }

                foreach ($request->all() as $input => $value)
                {
                    $debit->$input = $value;
                }

                $debit->update($request->all());
                return response()->json([
                    'status' => 'success',
                    'message' => 'Dados atualizados com sucesso.',
                    'data' => $debit
                ]);

            }catch (\Exception $e){

                if(QueryException::class){

                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }

    public function deleteDebit($id)
    {
        try{
            $debit = Debit::query()->where(['account_id' => AccountId()])->find($id);
            if($debit == null)
            {
                return response()->json([
                    'error' => [
                        'message' => 'Esta despesa não existe na base de dados.'
                    ]
                ], Response::HTTP_NOT_FOUND);
            }

            $debit->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Despesa removida com sucesso.'
            ]);

        }catch (\Exception $e){
            if(QueryException::class){

                ErrorReport(serialize($debit), $e, __FUNCTION__, __FILE__, 500);

                return response()->json([
                    'error' => [
                        'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                    ]
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            return ['error' => $e->getMessage()];
        }
    }
}