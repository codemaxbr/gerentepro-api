<?php
/**
 * Desenvolvido por Lucas Maia - lucas.codemax@gmail.com
 * Empresa: Codemax Sistemas Ltda
 *
 * Criado em: 24/08/19 00:31
 * Projeto: gerentepro-api
 */

namespace App\Services;

class WhoisService
{
    public $m_status = 0;
    public $m_domain = '';
    public $m_servers = array();
    public $m_data = array();
    public $m_connectiontimeout = 5;
    public $m_sockettimeout = 30;
    public $m_redirectauth = true;
    public $m_usetlds = array();
    public $m_tlds = array();
    public $m_supportedtlds = array();
    public $m_serversettings = array();

    public static $domain;
    public static $extension;

    public static $whoisExtensions = array(
        'com',
        'net',
    );

    public static $isBr;
    public static $url;
    public static $referer;
    public static $postFields;

    public function __construct(){
        $this->readconfig();
    }

    public function readconfig(){

        $this->m_serversettings = array();
        $this->m_tlds = array();
        $this->m_usetlds = array();


        $servers = array(

            "whois.crsnic.net#domain |No match for |Whois Server:|>NOTICE: The expiration date |Registrar:#Status:#Expiration Date:",
            "whois.afilias.net|NOT FOUND||<you agree to abide by this policy.|Expiration Date:#Status:#Registrant Email:#Admin Name:#Billing Name:#Billing Email#Tech Name:#Tech Email:#Registrant Name:#Admin Email:#Name Server:",
            "whois.nic.us|Not found:||>NeuStar, Inc., the Registry Administrator|Domain Expiration Date:#Domain Status:#Sponsoring Registrar:#Registrant Name:#Registrant Email:#Administrative Contact Name:#Administrative Contact Email:#Billing Contact Name:#Billing Contact Email:#Technical Contact Name:#Technical Contact Email:#Name Server:",
            "whois.internic.net|No match for |Whois Server:",
            "whois.publicinterestregistry.net|NOT FOUND||<you agree to abide by this policy.|Expiration Date:#Status:#Name Server:#Registrant Name:#Registrant Email:#Admin Name:#Admin Email:#Tech Name:#Tech Email:#Billing Name:#Billing Email:",
            "whois.neulevel.biz|Not found:||>NeuLevel, Inc., the Registry|Domain Expiration Date:#Domain Status:#Sponsoring Registrar:#Registrant Name:#Registrant Email:#Administrative Contact Name:#Administrative Contact Email:#Billing Contact Name:#Billing Contact Email:#Technical Contact Name:#Technical Contact Email:#Name Server:",
            "whois.nic.uk|No match for|||Registration Status:#Registrant:#Registrant's Address:#Renewal Date:#Name servers",
            "rs.domainbank.net|||<of the foregoing policies.|Administrative Contact:#Record expires on #Technical Contact:#Registrant:#Zone Contact:#Domain servers in ",
            "whois.moniker.com|||<you agree to abide by this policy.|Administrative Contact:#Registrant:#Domain Servers#Billing Contact:#Technical Contact:#Domain Expires on",
            "whois.networksolutions.com|||<right to modify these terms at any time.|Registrant:#Administrative Contact:#Record expires on #Domain servers in listed order:",
            "whois.enom.com|||>The data in this whois database |Registrant Contact:#Technical Contact:#Billing Contact:#Administrative Contact:#Status:#Name Servers:#Expiration date:",
            "whois.opensrs.net|||>The Data in the Tucows Registrar|Registrant:#Administrative Contact:#Technical Contact:#Record expires on#Domain servers in listed order:",
            "whois.godaddy.com|||<domain names listed in this database.|Registrant:#Expires On:#Administrative Contact:#Technical Contact:#Domain servers in listed order:",
            "whois.aunic.net|No Data Found|||Status:#Registrant Contact Name:#Registrant Email:#Name Server:#Tech Name:#Tech Email:",
            "whois.denic.de|free",
            "whois.worldsite.ws|No match for|||Registrant:#Name Servers:",
            "whois.nic.tv|",
            "whois.nic.tm|No match for",
            "whois.cira.ca|AVAIL",
            "whois.nic.cc|No match|Whois Server:|>The Data in eNIC Corporation|Whois Server:#Updated:",
            "whois.domainzoo.com|||<you agree to abide by these terms.",
            "whois.domaindiscover.com|||<you agree to abide by this policy.",
            "whois.markmonitor.com|||<you agree to abide by this policy.",
            "whois2.afilias-grs.net|NOT FOUND||<abide by this policy.",
            "whois.inregistry.net|NOT FOUND",
            "whois.safenames.net|No match for",
            "whois.nic.tv|",
            "whois.nic.co|",
            "whois.nic.me|NOT FOUND",
            "whois.nic.name|",
            "whois.dotmobiregistry.net|",
            "whois.nic.xxx|",
            "whois.centralnic.com|DOMAIN NOT FOUND",
            "whois.dns.pt|",
            "whois.ripe.net|",
            "whois.registro.br|"
        );

        $tlds = array(
            "com=whois.crsnic.net",
            "net=whois.crsnic.net",
            "org=whois.publicinterestregistry.net",
            "info=whois.afilias.net",
            "biz=whois.neulevel.biz",
            "us=whois.nic.us",
            "co.uk=whois.nic.uk",
            "org.uk=whois.nic.uk",
            "ltd.uk=whois.nic.uk",
            "ca=whois.cira.ca",
            "cc=whois.nic.cc",
            "edu=whois.crsnic.net",
            "com.au=whois.aunic.net",
            "net.au=whois.aunic.net",
            "de=whois.denic.de",
            "ws=whois.worldsite.ws",
            "sc=whois2.afilias-grs.net",
            "in=whois.inregistry.net",
            "tv=whois.nic.tv",
            "me=whois.nic.me",
            "co=whois.nic.co",
            "name=whois.nic.name",
            "mobi=whois.dotmobiregistry.net",
            "xxx=whois.nic.xxx",

            //Dominios Nacionais (Brasileiros) NIC.br
            "com.br=whois.registro.br",
            "net.br=whois.registro.br",
            "eco.br=whois.registro.br",
            "emp.br=whois.registro.br",
            "edu.br=whois.registro.br",
            "blog.br=whois.registro.br",
            "flog.br=whois.registro.br",
            "nom.br=whois.registro.br",
            "vlog.br=whois.registro.br",
            "wiki.br=whois.registro.br",
            "adm.br=whois.registro.br",
            "adv.br=whois.registro.br",
            "arq.br=whois.registro.br",
            "ato.br=whois.registro.br",
            "bio.br=whois.registro.br",
            "bmd.br=whois.registro.br",
            "cim.br=whois.registro.br",
            "cng.br=whois.registro.br",
            "cnt.br=whois.registro.br",
            "ecn.br=whois.registro.br",
            "eng.br=whois.registro.br",
            "eti.br=whois.registro.br",
            "fnd.br=whois.registro.br",
            "fot.br=whois.registro.br",
            "fst.br=whois.registro.br",
            "cgf.br=whois.registro.br",
            "jor.br=whois.registro.br",
            "lel.br=whois.registro.br",
            "mat.br=whois.registro.br",
            "med.br=whois.registro.br",
            "mus.br=whois.registro.br",
            "not.br=whois.registro.br",
            "ntr.br=whois.registro.br",
            "odo.br=whois.registro.br",
            "ppg.br=whois.registro.br",
            "pro.br=whois.registro.br",
            "psc.br=whois.registro.br",
            "qsl.br=whois.registro.br",
            "slg.br=whois.registro.br",
            "taxi.br=whois.registro.br",
            "teo.br=whois.registro.br",
            "trd.br=whois.registro.br",
            "vet.br=whois.registro.br",
            "zlg.br=whois.registro.br",
            "agr.br=whois.registro.br",
            "art.br=whois.registro.br",
            "esp.br=whois.registro.br",
            "etc.br=whois.registro.br",
            "far.br=whois.registro.br",
            "imb.br=whois.registro.br",
            "ind.br=whois.registro.br",
            "inf.br=whois.registro.br",
            "radio.br=whois.registro.br",
            "rec.br=whois.registro.br",
            "srv.br=whois.registro.br",
            "tmp.br=whois.registro.br",
            "tur.br=whois.registro.br",
            "tv.br=whois.registro.br",
            "am.br=whois.registro.br",
            "coop.br=whois.registro.br",
            "fm.br=whois.registro.br",
            "g12.br=whois.registro.br",
            "gov.br=whois.registro.br",
            "mil.br=whois.registro.br",
            "org.br=whois.registro.br",
            "psi.br=whois.registro.br",
            "b.br=whois.registro.br",
            "jus.br=whois.registro.br",
            "leg.br=whois.registro.br",
            "mp.br=whois.registro.br",

            "br.com=whois.centralnic.com"
        );

        $cnt = count($servers);

        foreach( $servers as $server){
            $server = trim($server);
            $bits = explode('|', $server);
            if( count($bits) > 1 ){
                for( $i = count($bits); $i < 5; $i++){
                    if( !isset($bits[$i]) ) $bits[$i] = '';
                }
                $server = explode("#", $bits[0]);
                if( !isset($server[1]) ) $server[1] = '';

                $this->m_serversettings[$server[0]] = array('server'=>$server[0], 'available'=>$bits[1], 'auth'=>$bits[2], 'clean'=>$bits[3], 'hilite'=>$bits[4], 'extra'=>$server[1]);
            }
        }
        foreach( $tlds as $tld ){
            $tld = trim($tld);
            $bits = explode('=', $tld);
            if( count($bits) == 2 && $bits[0] != '' && isset($this->m_serversettings[$bits[1]])){
                $this->m_usetlds[$bits[0]] = true;
                $this->m_tlds[$bits[0]] = $bits[1];
            }
        }
    }

    public function SetTlds($tlds = 'com.br,com,net,org,info,biz,us,co.uk,org.uk,in,org.br'){
        $tlds = strtolower($tlds);
        $tlds = explode(',',$tlds);
        $this->m_usetlds = array();
        foreach( $tlds as $t ){
            $t = trim($t);
            if( isset($this->m_tlds[$t]) ) $this->m_usetlds[$t] = true;
        }
        return count($this->m_usetlds);
    }

    public function Lookup($domain){
        $domain = strtolower($domain);
        $this->m_servers = array();
        $this->m_data = array();
        $this->m_tld = $this->m_sld = '';
        $this->m_domain = $domain;
        if( $this->splitdomain($this->m_domain, $this->m_sld, $this->m_tld) ){
            $this->m_servers[0] = $this->m_tlds[$this->m_tld];
            $this->m_data[0] = $this->dolookup($this->m_serversettings[$this->m_servers[0]]['extra'].$domain, $this->m_servers[0]);
            if( $this->m_data[0] != '' ){
                if( $this->m_serversettings[$this->m_servers[0]]['auth'] != '' && $this->m_redirectauth && $this->m_status == "STATUS_UNAVAILABLE"){
                    if( preg_match('/'.$this->m_serversettings[$this->m_servers[0]]['auth'].'(.*)/i', $this->m_data[0], $match) ){
                        $server = trim($match[1]);
                        if( $server != '' ){
                            $this->m_servers[1] = $server;
                            $command = isset($this->m_serversettings[$this->m_servers[1]]['extra']) ? $this->m_serversettings[$this->m_servers[1]]['extra'] : '';
                            $dt = $this->dolookup($command.$this->m_domain, $this->m_servers[1]);
                            $this->m_data[1] = $dt;
                        }
                    }
                }
                return true;
            }else{
                return false;
            }
        }
        return false;
    }

    public function ValidDomain($domain){
        $domain = strtolower($domain);
        return $this->splitdomain($domain, $sld, $tld);
    }

    public function GetDomain(){
        return $this->m_domain;
    }

    public function GetServer($i = 0){
        return isset($this->m_servers[$i]) ? $this->m_servers[$i] : '';
    }

    public function GetData($i = -1){
        if( $i != -1 && isset($this->m_data[$i])){
            $dt = htmlspecialchars(trim($this->m_data[$i]));
            $this->cleandata($this->m_servers[$i], $dt);
            return $dt;
        }else{
            return trim(join("\n", $this->m_data));
        }
        return '';
    }

    public function splitdomain($domain, &$sld, &$tld){
        $domain = strtolower($domain);
        $sld = $tld = '';
        $domain = trim($domain);
        $pos = strpos($domain, '.');
        if( $pos != -1){
            $sld = substr($domain, 0, $pos);
            $tld = substr($domain, $pos+1);
            if( isset($this->m_usetlds[$tld]) && $sld != '' ) return true;
        }else{
            $tld = $domain;
        }
        return false;
    }

    public function whatserver($domain){
        $sld = $tld = '';
        $this->splitdomain($domain, $sld, $tld);
        $server = isset($this->m_usetlds[$tld]) ? $this->m_tlds[$tld] : '';
        return $server;
    }

    public function dolookup($domain, $server){
        $domain = strtolower($domain);
        $server = strtolower($server);
        if( $domain == '' || $server == '' ) return false;

        $data = "";
        $fp = @fsockopen($server, 43,$errno, $errstr, $this->m_connectiontimeout);
        if( $fp ){
            @fputs($fp, $domain."\r\n");
            @socket_set_timeout($fp, $this->m_sockettimeout);
            while( !@feof($fp) ){
                $data .= @fread($fp, 4096);
            }
            @fclose($fp);

            return $data;
        }else{
            return "\nError - could not open a connection to $server\n\n";
        }
    }

    public function cleandata($server, &$data){
        if( isset($this->m_serversettings[$server]) ){
            $clean = $this->m_serversettings[$server]['clean'];
            if( $clean != '' ){
                $from = $clean[0];
                if( $from == '>' || $from == '<' ){
                    $clean = substr($clean,1);
                    $pos = strpos(strtolower($data), strtolower($clean));
                    if( $pos !== false ){
                        if( $from == '>' ){
                            $data = trim(substr($data, 0, $pos));
                        }else{
                            $data = trim(substr($data, $pos+strlen($clean)));
                        }
                    }
                }
            }
        }
    }

    public static function check( $domain, $extension ) {

        self::$domain = $domain;
        self::$extension = $extension;

        $hasExtension = ( !empty ( $extension ) ) ? true : false;
        $hasDomain = ( !empty ( $domain ) ) ? true : false;

        if( $hasDomain == false && $hasExtension == false ) {
            return 'You must inform the domain and extension';
        } else {
            if( in_array( $extension, self::$whoisExtensions ) ) {
                self::$isBr = false;
            } else {
                self::$isBr = true;
            }
        }
    }

    public static function isAvailable( $domain, $extension ) {

        self::check( $domain, $extension );
        if( self::$isBr == true ) {
            $result = self::checkFromRegistroBR();
        } else {
            $result = self::checkFromInternic();
        }
        return $result;
    }

    public static function isWhois( $domain, $extension ) {

        self::check( $domain, $extension );
        if( self::$isBr == true ) {
            $result = self::whoisRegistroBR();
        } else {
            $result = self::checkFromWhois();
        }
        return $result;
    }

    public static function isOnline() {}

    public static function useCurl() {

        $userAgent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_USERAGENT, $userAgent);
        curl_setopt( $ch, CURLOPT_URL, self::$url );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
        curl_setopt( $ch, CURLOPT_REFERER, self::$referer );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, self::$postFields );

        $content = curl_exec($ch);
        curl_close( $ch );

        return $content;
    }

    public static function checkFromRegistroBR() {

        self::$postFields = null;
        self::$postFields = "qr=" . self::$domain . '.' . self::$extension;
        self::$url = "http://whois.locaweb.com.br/";
        self::$referer = "Referer: http://whois.locaweb.com.br/";
        $string = self::useCurl();

        return self::grabInformationFromString( $string );
    }

    public static function whoisRegistroBR() {

        self::$postFields = null;
        self::$postFields = "qr=" . self::$domain . '.' . self::$extension;
        self::$url = "http://whois.locaweb.com.br/";
        self::$referer = "Referer: http://whois.locaweb.com.br/";
        $string = self::useCurl();

        preg_match_all('/<pre style="font-size:14px">(.+)<\/pre>/s', $string, $conteudo);
        //print_r($conteudo);
        $pattern = array();
        $pattern[] = '@<a href="[^"]+">(.+?)</a>@';
        $pattern[] = '@<A HREF="[^"]+">(.+?)</A>@';
        $pattern[] = '/<pre style="font-size:14px">(.+)<\/pre>/s';
        $replace = array();
        $replace[] = '$1';
        $replace[] = '$1';
        $replace[] = '$1';

        $text = preg_replace($pattern,$replace,$conteudo[0][0]);


        return (string) $text;
    }

    public function checkFromInternic($domain, $tld){
        //self::$domain . '.' . self::$extension;
        return 'to fazendo alguma coisa...';
        /*
        $array = explode("\n", $whois);

        //--------------------------------------------  Até aqui ta maneiro... agora que o bicho pega! ------------------------

        $regra = '';
        switch($extensao){
            case 'edu':
            case 'net':
            case 'com': $regra = substr($array[6], 0, 12) == "No match for"; break;
            case 'net.br':
            case 'com.br': $regra = substr($array[11], 0, 21) == "% No match for domain"
                                 or substr($array[8], 0, 21) == "% No match for domain"; break;
        }

        if($regra){ // aqui verifica o tipo (.net|.com)
            echo 'Disponivel';
        }else{
            echo 'Indisponivel';
        }
        */
    }/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function getWhois($domain, $tld){

        if( self::Lookup($domain.'.'.$tld)){

            $array = explode("\n", self::GetData(1));
            //--------------------------------------------  Até aqui ta maneiro... agora que o bicho pega! ------------------------

            $regra = '';
            switch($tld){
                case 'cc':
                case 'tv': $regra = substr($array[5], 0, 12) == "No match for"; break;

                case 'edu':
                case 'net':

                case 'com': $regra = substr($array[6], 0, 12) == "No match for"; break;
                case 'ws': $regra = substr($array[7], 0, 12) == "No match for"; break;
                case 'org':
                case 'in':
                case 'info':
                case 'mobi':
                case 'me':
                case 'xxx': $regra = substr($array[0], 0, 9) == "NOT FOUND"; break;
                case 'us':
                case 'co':
                case 'biz': $regra = substr($array[0], 0, 9) == "Not found"; break;
                case 'name': $regra = substr($array[22], 0, 8) == "No match"; break;

                case 'com.br':
                case 'emp.br':
                case 'net.br':
                case 'eco.br':
                case 'edu.br':
                case 'blog.br':
                case 'flog.br':
                case 'nom.br':
                case 'vlog.br':
                case 'wiki.br':
                case 'arg.br':
                case 'art.br':
                case 'esp.br':
                case 'etc.br':
                case 'far.br':
                case 'imb.br':
                case 'ind.br':
                case 'inf.br':
                case 'radio.br':
                case 'rec.br':
                case 'srv.br':
                case 'tmp.br':
                case 'tur.br':
                case 'tv.br':
                case 'am.br':
                case 'coop.br':
                case 'fm.br':
                case 'g12.br':
                case 'gov.br':
                case 'mil.br':
                case 'org.br':
                case 'psi.br':
                case 'b.br':
                case 'jus.br':
                case 'leg.br':
                case 'mp.br':
                case 'adm.br':
                case 'adv.br':
                case 'arq.br':
                case 'ato.br':
                case 'bio.br':
                case 'bmd.br':
                case 'cim.br':
                case 'cng.br':
                case 'cnt.br':
                case 'ecn.br':
                case 'eng.br':
                case 'eti.br':
                case 'fnd.br':
                case 'fot.br':
                case 'fst.br':
                case 'cgf.br':
                case 'jor.br':
                case 'lel.br':
                case 'mat.br':
                case 'med.br':
                case 'mus.br':
                case 'not.br':
                case 'ntr.br':
                case 'odo.br':
                case 'ppg.br':
                case 'pro.br':
                case 'psc.br':
                case 'qsl.br':
                case 'slg.br':
                case 'taxi.br':
                case 'teo.br':
                case 'trd.br':
                case 'vet.br':
                case 'zlg.br':
                    $regra = (substr($array[11], 0, 21) == "% No match for domain") ? substr($array[11], 0, 21) == "% No match for domain" : substr($array[8], 0, 21) == "% No match for domain";
                    break;
            }


            if($regra){ // aqui verifica o tipo (.net|.com)
                return TRUE;
            }else{
                return FALSE;
            }

        }else{
            // se ele não conseguiu com dominios internacionais, ele tentará com brasileiros;
            return self::isAvailable($domain, $tld);
        }
    }
}