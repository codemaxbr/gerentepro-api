<?php
/**
 * Created by PhpStorm.
 * User: Lucas e Nathalia
 * Date: 11/08/2018
 * Time: 16:30
 */

namespace App\Services;


use App\Events\TerminateSubscription;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use App\Models\Subscription;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class SubscriptionService
{

    private $subscriptionRepository;
    private $subscription_id;
    private $accountId;

    public function getSubscriptions()
    {
        $result = Subscription::with([
            'customer:id,name,email,cpf_cnpj,type', 'plan'
            ])
            ->whereHas('account', function ($q){
                $q->where('id', AccountId());

            });

        return $result->paginate(20);
    }

    public function getAll_search($search)
    {
        return Subscription::with('customer')
            ->where(['account_id' => AccountId()])
            ->whereHas('customer', function ($q) use ($search){
                $q->where('name', 'LIKE', "%{$search}%");
            })
            ->paginate(20);
    }

    public function update($id, Request $request)
    {
        $subscription = Subscription::query()->where(['account_id' => AccountId(), 'id' => $id])->first();

        if($subscription == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Esta assinatura não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        $validator = Validator::make($request->all(), [
            'customer_id' => 'required | integer | exists:customers,id',
            'plan_id' => 'required | integer | exists:plans,id',
            'due' => 'required | date',
            'status' => 'required | integer'
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{

            try{
                $subscription = Subscription::find($id);
                foreach ($request->all() as $index => $update)
                {
                    $subscription->$index = $update;
                }

                $subscription->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Dados da Assinatura Nº '.$subscription->id.' foram atualizados.',
                    'data' => [
                        'subscription' => $subscription
                    ]
                ], Response::HTTP_OK);
            }
            catch (\Exception $e) {

                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }

    public function remove($id)
    {
        $subscription = Subscription::query()->where(['account_id' => AccountId(), 'id' => $id])->first();

        if($subscription == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Esta assinatura não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        $plan = $subscription->plan;

        /** @var Subscription $subscription */
        Event::dispatch(new TerminateSubscription($subscription, $plan));

        $subscription->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'A Assinatura Nº '.$subscription->id.' foi removida.'
        ]);
    }

    public function getAllSubscriptionsStatus($status)
    {
        return Subscription::query()
            ->where('status', $status)
            ->orderBy('id', 'desc')
            ->paginate(20);
    }

    public function getSubscription($id)
    {
        $subscription = Subscription::with([
            'customer', 'type_payment', 'server', 'plan', 'plan.type_term', 'plan.type_plan', 'plan.payment_cycle', 'plan.module']
        )->find($id);

        if($subscription == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Esta assinatura não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }else{

            return $subscription;
        }
    }

    public function newSubscription($data)
    {
        $subscription = Subscription::create($data);

        if($subscription)
        {
           try{
               return response()->json([
                   'status' => 'success',
                   'msg'   =>  'Uma nova assinatura do plano '.$subscription->plan->name.' Foi criada com sucesso',
                   'data'  =>  $subscription
               ], 201);
           }catch (\Exception $e)
           {
               if(QueryException::class){

                   ErrorReport(serialize($data), $e, __FUNCTION__, __FILE__, 500);

                   return response()->json([
                       'error' => [
                           'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                       ]
                   ], Response::HTTP_INTERNAL_SERVER_ERROR);
               }

               return ['error' => $e->getMessage()];
           }
        }
    }
}