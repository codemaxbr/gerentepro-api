<?php
/**
 * Created by PhpStorm.
 * User: Lucas Maia
 * Date: 08/03/2019
 * Time: 12:25
 */

namespace App\Services;

use App\Models\Email;
use App\Models\EmailSignature;
use App\Models\TemplateEmail;
use App\Models\TypeEmail;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class EmailService
{

    /**
     * EmailService constructor.
     */
    public function __construct()
    {
    }

    public function getFields($tag)
    {
        switch ($tag){
            case 'customer_register': return \App\Mail\CustomerRegistered::$fields; break;

            default: return []; break;
        }
    }

    public function getTypes()
    {
        return TypeEmail::all();
    }

    public function getTemplate($tag, $account = '')
    {
        if(empty($account)){
           $account = AccountId();
        }

        $email    = Email::query()->where(['tag' => $tag])->first();
        $storage  = Storage::disk('local');
        $template = TemplateEmail::query()->where(['account_id' => $account, 'email_id' => $email->id])->first();

        if($email != null)
        {
            if($template != null)
            {
                return (object) [
                    'subject' => $template->subject,
                    'template' => $template->message
                ];

            }else{
                $file = storage_path('emails/'.$email->tag.'.xml');
                if(file_exists($file)){
                    $template = (array) simplexml_load_file($file);

                    return (object) [
                        'subject' => $template['subject'],
                        'template' => htmlspecialchars_decode($template['template'])
                    ];
                }else{
                    return (object) [
                        'subject' => null,
                        'template' => null
                    ];
                }
            }
        }else{
            return response()->json([
                'error' => [
                    'message' => 'Este Template de e-mail não existe na base de dados.'
                ]
            ]);
        }
    }

    public function getTemplates()
    {
        return TypeEmail::with(['emails'])->get()->all();
    }

    public function updateTemplate($id, Request $request)
    {
        $email    = Email::find($id);
        $storage  = Storage::disk('local');
        $template = TemplateEmail::query()->where(['account_id' => AccountId(), 'email_id' => $id])->first();

        $validator = Validator::make($request->all(), [
            'subject' => 'required',
            'message' => 'required',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else {
            if($template == null){
                // se não tiver um template salvo, deve criar um
                try{
                    $template = TemplateEmail::create([
                        'email_id' => $id,
                        'subject'  => $request->subject,
                        'message'  => $request->message,
                        'account_id' => AccountId()
                    ]);

                    return response()->json([
                        'status' => 'success',
                        'message' => 'Template de E-mail "'.$template->email->name.'" salvo com sucesso.',
                        'data' => $template
                    ], Response::HTTP_OK);
                }catch (\Exception $e)
                {
                    if(QueryException::class){
                        ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                        return response()->json([
                            'error' => [
                                'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                            ]
                        ], Response::HTTP_INTERNAL_SERVER_ERROR);
                    }

                    return response()->json(['error' => $e->getMessage()]);
                }
            }else {
                // se sim, atualiza
                try{
                    foreach ($request->all() as $index => $update)
                    {
                        $template->$index = $update;
                    }

                    $template->save();

                    return response()->json([
                        'status' => 'success',
                        'message' => 'Template de E-mail "'.$template->email->name.'" atualizado com sucesso.',
                        'data' => $template
                    ], Response::HTTP_OK);
                }catch (\Exception $e)
                {
                    if(QueryException::class){
                        ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                        return response()->json([
                            'error' => [
                                'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                            ]
                        ], Response::HTTP_INTERNAL_SERVER_ERROR);
                    }

                    return response()->json(['error' => $e->getMessage()]);
                }
            }
        }
    }

    public function getSignature()
    {
        $signature = EmailSignature::query()->where(['account_id' => AccountId()])->first();
        if($signature == null)
        {
            return response()->json([
                'info' => [
                    'message' => 'Esta conta ainda não possui modelo de e-mail.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        return $signature;
    }

    public function updateSignature(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'header' => 'required',
            'footer' => 'required',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else {
            $signature = EmailSignature::query()->where(['account_id' => AccountId()])->first();
            if($signature == null)
            {
                try{
                    $new_signature = EmailSignature::create([
                        'header' => $request->header,
                        'footer' => $request->footer,
                        'signature' => ($request->has('signature')) ? $request->signature : null,
                        'account_id' => AccountId()
                    ]);

                    return response()->json([
                        'status' => 'success',
                        'message' => 'Assinatura de e-mail salvo com sucesso.',
                        'data' => [
                            'signature' => $new_signature
                        ]
                    ], Response::HTTP_OK);

                }catch (\Exception $e)
                {
                    if(QueryException::class){
                        ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                        return response()->json([
                            'error' => [
                                'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                            ]
                        ], Response::HTTP_INTERNAL_SERVER_ERROR);
                    }

                    return response()->json(['error' => $e->getMessage()]);
                }
            }else{
                try{
                    foreach ($request->all() as $index => $update)
                    {
                        $signature->$index = $update;
                    }

                    $signature->save();

                    return response()->json([
                        'status' => 'success',
                        'message' => 'Assinatura de e-mail salvo com sucesso.',
                        'data' => [
                            'signature' => $signature
                        ]
                    ], Response::HTTP_OK);
                }catch (\Exception $e)
                {
                    if(QueryException::class){
                        ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                        return response()->json([
                            'error' => [
                                'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                            ]
                        ], Response::HTTP_INTERNAL_SERVER_ERROR);
                    }

                    return response()->json(['error' => $e->getMessage()]);
                }
            }
        }
    }
}