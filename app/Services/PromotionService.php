<?php
/**
 * Created by PhpStorm.
 * User: Lucas Maia
 * Date: 27/02/2019
 * Time: 00:12
 */

namespace App\Services;


use App\Jobs\GenerateVouchers;
use App\Models\Promotion;
use App\Models\Voucher;
use Carbon\Carbon;
use DateTimeZone;
use Firebase\JWT\JWT;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Webpatser\Uuid\Uuid;

class PromotionService
{
    /**
     * PromotionService constructor.
     */
    public function __construct()
    {

    }

    public function getPromotions()
    {
        return Promotion::with(['status_promotion', 'type_promotion'])->withCount('vouchers')
            ->where(['account_id' => AccountId()])
            ->paginate(20);
    }

    public function getAll_search($search)
    {
        return Promotion::with(['status_promotion', 'type_promotion'])
            ->whereHas('account', function ($q){
                $q->where('id', AccountId());
            })
            ->where('name', 'LIKE', "%{$search}%")
            ->paginate(20);
    }

    public function getPromotion($id)
    {
        $promotion = Promotion::with(['status_promotion', 'type_promotion'])->find($id);

        if($promotion == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Esta promoção não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        return $promotion;
    }

    public function updatePromotion($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'type_promotion_id' => 'required | integer | exists:type_promotions,id',
            'status_promotion_id' => 'required | integer | exists:status_promotions,id',
            'account_id' => 'required | integer | exists:accounts,id',
            'type_value' => 'required',
            'value' => 'required | numeric | between:0,99.99',
            'unique' => 'boolean',
            'qty' => 'required_if:unique,1',
            'code' => 'required_without:unique'
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{
            if (!$request->has('unique') || $request->unique == 0){
                $request['multi_uses'] = true;
                $request['qty'] = 1;
            }

            if(!$request->has('code')){
                $request['code'] = null;
            }

            $promotion = Promotion::find($id);
            $vouchers = $promotion->vouchers;

            foreach ($request->all() as $index => $update)
            {
                $edit[$index] = $update;
                $old[$index] = $promotion->getOriginal($index);
            }

            if($request->qty > $old['qty'] && $request->unique == 1)
            {
                $diferenca = intval($request->qty - $old['qty']);
                $vouchers = $this->generateVouchers($diferenca, $promotion);
                $obs = "(Mais $diferenca cupons foram adicionados)";
            }

            if($request->has('code') && $request->code != null)
            {
                Voucher::query()->where(['promotion_id' => $id])->delete();
                $vouchers = $this->generateVouchers($request->qty, $promotion, $request->code);
                $obs = "(Um novo cupom foi atruído para esta promoção)";
            }

            try{
                $promotion->update($edit);

                return response()->json([
                    'status' => 'success',
                    'message' => 'Promoção "'.$promotion->name.'" foi alterada. '.$obs,
                    'data' => [
                        'promotion' => $promotion,
                        'vouchers' => $vouchers
                    ]
                ]);
            }catch (\Exception $e){

                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }

    public function deletePromotion($id)
    {
        try{
            $promotion = Promotion::query()->where(['account_id' => AccountId()])->find($id);
            if($promotion == null)
            {
                return response()->json([
                    'error' => [
                        'message' => 'Esta promoção não existe na base de dados.'
                    ]
                ], Response::HTTP_NOT_FOUND);
            }

            $promotion->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Promoção removida com sucesso.'
            ]);

        }catch (\Exception $e){
            if(QueryException::class){

                ErrorReport(serialize($id), $e, __FUNCTION__, __FILE__, 500);

                return response()->json([
                    'error' => [
                        'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                    ]
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            return ['error' => $e->getMessage()];
        }
    }

    /**
     * @param $request
     * @return \App\Models\Promotion
     */
    public function createPromotion($request)
    {
        if (!isset($request['unique']) || $request['unique'] == 0)
        {
            $request['multi_uses'] = true;
            $request['qty'] = 1;
        }

        if(!isset($request['code'])){
            $request['code'] = null;
        }

        try{
            $request['account_id'] = AccountId();
            $request['uuid'] = Uuid::generate(4)->string;

            $promotion = Promotion::create($request);

            //TODO: Incluir relacionamento dos planos selecionados

            if($request['qty'] > 1000)
            {
                $limite = 5000;
                $partes = intval(ceil($request['qty'] / $limite));
                $resto = $request['qty'] % $limite;

                for ($i = 0; $i < $partes; $i++)
                {
                    if($resto != 0 && $i == ($partes - 1))
                    {
                        dispatch((new GenerateVouchers($resto, $promotion, $request['code'], $partes, $request['qty']))->onQueue('low'));
                    }else{
                        dispatch((new GenerateVouchers($limite, $promotion, $request['code'], $partes, $request['qty']))->onQueue('low'));
                    }
                }
            }else{
                $this->generateVouchers($request['qty'], $promotion, $request['code']);
            }

            return $promotion;

        }catch (\Exception $e)
        {
            return $e->getMessage();
        }

    }

    /**
     * Atribui um voucher à um cliente (reserva)
     *
     * @param $promotion_id
     * @param Request $request
     */
    public function assignVoucher($promotion_id, Request $request)
    {
        dump($request->customer);
    }

    public function validateVoucher($code, Request $request)
    {
        $token = $request->bearerToken();
        $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);

        $customer_id = $credentials->sub;
        $voucher = Voucher::query()->where(['code' => $code])->first();

        if($voucher == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Cupom inválido.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }
        else{

            $promotion = $voucher->promotion;
            dump($promotion); //todo continuar aqui quando fizer o Middleware de login de cliente (Front)
        }
    }

    public function getVouchers($promotion_id)
    {
        return Voucher::query()->where(['promotion_id' => $promotion_id])->paginate(20);
    }

    public function deleteVouchers($promotion_id)
    {
        try{
            $promotion = Promotion::query()->where(['account_id' => AccountId()])->find($promotion_id);
            if($promotion == null)
            {
                return response()->json([
                    'error' => [
                        'message' => 'Esta promoção não existe na base de dados.'
                    ]
                ], Response::HTTP_NOT_FOUND);
            }

            Voucher::query()->where(['promotion_id' => $promotion_id])->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Todos os cupons desta promoção foram removidos.'
            ]);

        }catch (\Exception $e){
            if(QueryException::class){

                ErrorReport(serialize($promotion_id), $e, __FUNCTION__, __FILE__, 500);

                return response()->json([
                    'error' => [
                        'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                    ]
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            return ['error' => $e->getMessage()];
        }
    }

    protected function randomStr($string)
    {
        return masked($string, "#####-####-#######");
    }

    /**
     * @param $qty
     * @param Promotion $promotion
     * @param null $code
     */
    private function generateVouchers($qty, Promotion $promotion, $code = null)
    {
        $create = array();
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < $qty; $i++)
        {
            $generated = $this->randomStr(strtoupper($faker->unique()->regexify('[A-Z0-9][A-Z0-9][A-Z]{16,16}')));

            $add = [
                'promotion_id' => $promotion->id,
                'code' => ($code != null) ? $code : $generated,
                'used' => false,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ];

            array_push($create, $add);
        }

        DB::table('vouchers')->insert($create);
        return $create;
    }
}