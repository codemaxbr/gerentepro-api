<?php
/**
 * Created by PhpStorm.
 * User: Lucas Maia
 * Date: 25/09/2018
 * Time: 22:11
 */

namespace App\Services;

use App\Models\Transaction;
use App\Repositories\TransactionRepository;

class TransactionService
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * TransactionService constructor.
     */
    public function __construct()
    {

    }

    public function getTransactionsInvoice($invoice_id)
    {
        return Transaction::with(['type_payment', 'status', 'module'])->where(['invoice_id' => $invoice_id, 'account_id' => AccountId()])->orderBy('id', 'desc')->paginate(20);
    }

    public function getTransactions()
    {
        return Transaction::with(['type_payment', 'status', 'module', 'customer', 'invoice'])->where(['account_id' => AccountId()])->paginate(20);
    }

    public function createTransaction($dados)
    {
        return $this->transactionRepository->create($dados);
    }

    public function getExternalId($id)
    {
        return $this->transactionRepository->findWhere(['external_id' => $id])->first();
    }

    public function getTransaction($id)
    {
        return $this->transactionRepository->find($id);
    }

    public function updateTransaction($id, $dados)
    {
        return $this->transactionRepository->update($dados, $id);
    }
}