<?php

namespace App\Services;

use App\Events\ValidateModule;
use App\Models\Account;
use App\Models\Module;
use App\Models\ModulesAccount;
use App\Models\Option;
use App\Models\Server;
use App\Models\TypeModule;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Validator;
use Webpatser\Uuid\Uuid;

class ModuleService
{
    /**
     * ModuleService constructor.
     */
    public function __construct(){

    }

    // Lista todos os módulos disponíveis no GerentePRO (Global)
    public function getModules($type = '*')
    {
        if(!Cache::has('modules'))
        {
            Cache::rememberForever('modules', function ()
            {
                return Module::with('type_module')->get()->toJson();
            });
        }

        $modules = cache('modules');

        if($type != '*') {
            $lista = [];

            foreach (json_decode($modules) as $module){
                if($type == $module->type_module->slug){
                    array_push($lista, $module);
                }
            }

            return $lista;
        }else{
            return json_decode($modules);
        }
    }

    public function getTypes()
    {
        return TypeModule::with('modules')->select('id', 'name', 'slug')->get()->all();
    }

    public function createConfig(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'module_id' => 'required | integer | exists:modules,id',
            'config' => 'required | array',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{

            try{

                $module = Module::find($request->module_id);

                /**
                 * Se o tipo do módulo for diferente de "Painel de Controle" (que é o único que permite multiplas configurações para o mesmo módulo)
                 */
                if($module->type_module_id != 2){

                    $configModule = ModulesAccount::query()->where(['account_id' => AccountId(), 'module_id' => $module->id])->first();

                    if($configModule == null){
                        $config = ModulesAccount::create([
                            'uuid' => Uuid::generate(4)->string,
                            'module_id' => $module->id,
                            'host' => ($request->has('host')) ? $request->host : null,
                            'config' => serialize($request->config),
                            'account_id' => AccountId()
                        ]);

                        return response()->json([
                            'status' => 'success',
                            'message' => 'Configurações salvas para o módulo = '.$module->name.'.',
                            'module'=> $module,
                            'data' => [
                                'config' => $config
                            ]
                        ]);
                    }else{
                        return response()->json([
                            'error' => [
                                'message' => 'Você está tentando criar uma configuração que já existe no módulo desejado.'
                            ]
                        ], Response::HTTP_BAD_REQUEST);
                    }
                }else{
                    $config = ModulesAccount::create([
                        'uuid' => Uuid::generate(4)->string,
                        'module_id' => $module->id,
                        'host' => ($request->has('host')) ? $request->host : null,
                        'config' => serialize($request->config),
                        'account_id' => AccountId()
                    ]);

                    return response()->json([
                        'status' => 'success',
                        'message' => 'Configurações salvas para o módulo = '.$module->name.'.',
                        'module'=> $module,
                        'data' => [
                            'config' => $config
                        ]
                    ]);
                }

            }catch (\Exception $e)
            {
                //if(QueryException::class){
                //    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);
//
                //    return response()->json([
                //        'error' => [
                //            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                //        ]
                //    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                //}

                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }

    public function removeConfig($id)
    {
        $config = ModulesAccount::query()->where(['account_id' => AccountId(), 'id' => $id])->first();
        if($config == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Este módulo não possui uma configuração existente na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        $config->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Configuração de módulo removida com sucesso.'
        ]);
    }

    public function updateConfig($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'module_id' => 'required | integer | exists:modules,id',
            'config' => 'required | array',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{
            try{
                $module = Module::find($request->module_id);

                $config = ModulesAccount::with(['module'])->find($id);
                $config->update([
                    'module_id' => $module->id,
                    'host' => ($request->has('host')) ? $request->host : null,
                    'config' => serialize($request->config),
                ]);

                return response()->json([
                    'status' => 'success',
                    'message' => 'Configurações salvas para o módulo = '.$module->name.'.',
                    'data' => [
                        'config' => $config
                    ]
                ]);

            }catch (\Exception $e)
            {
                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }
        }
        //return ModulesAccount::query()->where(['account_id' => $account->id, 'module_id' => $module->id])->update(['config' => $json]);
    }

    public function getConfigs()
    {
        $modules = $this->getModules();
        $configs = ModulesAccount::whereAccountId(AccountId())->get()->toArray();

        foreach ($modules as $module){

            $module->multi_configs = ($module->type_module_id == 2) ? true : false;
            $module->configs = [];

            foreach ($configs as $config){
                if ($module->id == $config['module_id']){
                    array_push($module->configs, $config);
                }
            }
        }

        return $modules;
    }

    public function hasGateway()
    {
        $configs = ModulesAccount::query()->where(['account_id' => AccountId()])->whereHas('module', function ($q){
            $q->where('type_module_id', 3);
        });

        if($configs->count() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function getConfigsByModule($module_id, $account_id = ''){
        if($account_id == ''){
            $account_id = AccountId();
        }

        $configs = ModulesAccount::query()->where(['account_id' => $account_id, 'module_id' => $module_id])->get()->all();
        $array = array();

        foreach ($configs as $config){
            $add_config = [
                "id" => $config->id,
                "uuid" => $config->uuid,
                "module_id" => $config->module_id,
                "host" => $config->host,
                "account_id" => $config->account_id,
                "created_at" => $config->created_at->toDateTimeString(),
                "updated_at" => $config->updated_at->toDateTimeString(),
                "config" => $config->config
            ];

            array_push($array, $add_config);
        }

        return $array;
    }

    public function hasConfig()
    {
        $options = Option::query()->whereIn('name', ['boleto_gateway', 'cartao_gateway'])->where(['account_id' => AccountId()])->get();

        if($options->count() > 0){
            return true;
        }else{
            return false;
        }

    }

    /**
     * @param Module $module
     * @param Account $account
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public function getConfig($id)
    {
        $config = ModulesAccount::query()->where(['account_id' => AccountId(), 'id' => $id])->first();
        if($config == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Este módulo não possui uma configuração existente na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        return $config;
    }

    public function getGateways(Account $account)
    {
        return ModulesAccount::query()->with('module')->whereHas('module', function ($q){
            $q->where(['type_module_id'=> 3]);
        })->where(['account_id' => $account->id])->get()->all();
    }

    public function getModuleBySlug($slug, $account_id = '')
    {
        if($account_id == ''){
            $account_id = AccountId();
        }

        $module = Module::with('type_module')->where(['slug' => $slug])->first();
        return $module;
    }
}