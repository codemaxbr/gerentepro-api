<?php
/**
 * Created by PhpStorm.
 * User: Codemax Sistemas
 * Date: 11/12/2017
 * Time: 19:56
 */

namespace App\Services;

use App\Events\ConfirmPayment;
use App\Events\InvoiceChangePaid;
use App\Events\LogActivity;
use App\Events\SendEmail;
use App\Models\Invoice;
use App\Models\Plan;
use App\Models\TypeInvoice;
use Carbon\Carbon;
use DateTimeZone;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use phpDocumentor\Reflection\Types\Integer;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Webpatser\Uuid\Uuid;


class InvoiceService
{
    private $account_id;
    private $invoice_id;
    private $invoice_uuid;
    private $invoicesCustomer;
    /**
     * @var Invoice
     */
    private $invoice;

    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }


    /**
     * Mostra todas os registros
     * @param null $paginate
     * @return mixed
     */
    public function getInvoices()
    {
        return Invoice::with(['customer:id,name,email,uuid', 'status_invoice', 'type_invoice'])
                ->where(['account_id' => AccountId()])
                ->orderBy('id', 'desc')
                ->paginate(20);
    }

    public function allInvoices()
    {
        return Invoice::with(['customer:id,name,email,uuid', 'status_invoice', 'type_invoice'])
            ->where(['account_id' => AccountId()])
            ->orderBy('id', 'desc')
            ->get()->all();
    }

    public function getResume()
    {
        $pending = Invoice::query()->select([DB::raw('SUM(total) as total'), DB::raw('COUNT(id) as count')])
            ->where(['status_id' => 1, 'account_id' => AccountId()])->first();

        $paid = Invoice::query()->select([DB::raw('SUM(total) as total'), DB::raw('COUNT(id) as count')])
            ->whereIn('status_id', ['5', '6'])
            ->where(['account_id' => AccountId()])->first();

        $overdue = Invoice::query()->select([DB::raw('SUM(total) as total'), DB::raw('COUNT(id) as count')])
            ->where(['status_id' => 2, 'account_id' => AccountId()])->first();

        $all = Invoice::query()->select([DB::raw('SUM(total) as total'), DB::raw('COUNT(id) as count')])
            ->whereNotIn('status_id', ['3', '4', '7', '8'])
            ->where(['account_id' => AccountId()])->first();

        return ['resume' => [
            'pending' => [
                'total' => (is_null($pending->total)) ? 0.00 : $pending->total,
                'count' => $pending->count
            ],

            'paid' => [
                'total' => (is_null($paid->total)) ? 0.00 : $paid->total,
                'count' => $paid->count
            ],

            'overdue' => [
                'total' => (is_null($overdue->total)) ? 0.00 : $overdue->total,
                'count' => $overdue->count
            ],

            'all' => [
                'total' => (is_null($all->total)) ? 0.00 : $all->total,
                'count' => $all->count
            ],
        ]];
    }

    public function setId($id)
    {
        $this->invoice_id = $id;
    }

    public function setUuid($uuid)
    {
        $this->invoice_uuid = $uuid;
    }

    /**
     * Mostra detalhes de uma fatura
     * @return mixed
     */
    public function getInvoice($uuid)
    {
        if(!Cache::has('invoice_'.$uuid))
        {
            $invoice = Invoice::with([
                'customer:id,uuid,name,email,cpf_cnpj,type',
                'user',
                'status_invoice',
                'type_invoice',
                'invoice_histories',
                'invoice_items',
                'invoice_items.plan',
                'invoice_items.plan.grids',
                'statement',
                'statement.type_payment',
                'statement.user',
                'attachment',
                'logs' => function($q){
                    $q->orderBy('id', 'DESC');
                },
                'logs.user',
            ])->where(['uuid' => $uuid, 'account_id' => AccountId()])->first();

            if($invoice != null){
                Cache::rememberForever('invoice_'.$uuid, function() use ($invoice){
                    return $invoice;
                });
            }
        }

        $invoice = Cache::get('invoice_'.$uuid);

        return $invoice;
    }

    public function getInvoicesCustomer($id)
    {
        return Invoice::all()->where('customer_id' , $id);
    }

    /**
     * Exite total "Pendente"
     * @param $year
     * @param $monthy
     * @return float|mixed
     */
    public function totalPending($year, $monthy)
    {
        $count = DB::table('invoices')->select(DB::raw('sum(total) as total_pending'))
            ->where('status', 0)
            ->whereYear('due', '=', $year)->whereMonth('due', '=', $monthy)
            ->first();

        return ($count->total_pending != null) ? $count->total_pending : 0.00;
    }

    public function totalInvoiceCustomer($id)
    {
        $count = DB::table('invoices')->select(DB::raw('sum(total) as total_pending'))
            ->where('status', 0)
            ->where('customer_id', $id)
            ->first();

        return ($count->total_pending != null) ? $count->total_pending : 0.00;
    }

    public function totalPendingCustomer($dataAtual, $id)
    {
        $count = DB::table('invoices')->select(DB::raw('sum(total) as total_pending'))
            ->where('status', 0)
            ->where('customer_id', $id)
            ->where('due', '<=', $dataAtual)
            ->first();

        return ($count->total_pending != null) ? $count->total_pending : 0.00;
    }

    /**
     * Exibe total "Pago"
     * @param $year
     * @param $monthy
     * @return float|mixed
     */
    public function totalPaid($year, $monthy)
    {
        $count = DB::table('invoices')->select(DB::raw('sum(total) as total_paid'))
            ->where('status', 1)
            ->whereYear('due', '=', $year)->whereMonth('due', '=', $monthy)
            ->first();

        return ($count->total_paid != null) ? $count->total_paid : 0.00;
    }

    /**
     * Exibe total "Não pago / Em atraso"
     * @param $year
     * @param $monthy
     * @return float|mixed
     */
    public function totalOverdue($year, $monthy)
    {
        $count = DB::table('invoices')->select(DB::raw('sum(total) as total_overdue'))
            ->where('status', 2)
            ->whereYear('due', '=', $year)->whereMonth('due', '=', $monthy)
            ->first();

        return ($count->total_overdue != null) ? $count->total_overdue : 0.00;
    }

    /**
     * Exibe total "A receber do mês"
     * @param $year
     * @param $monthy
     * @return float|mixed
     */
    public function totalIncome($year, $monthy)
    {
        $count = DB::table('invoices')->select(DB::raw('sum(total) as total_income'))
            ->whereYear('due', '=', $year)->whereMonth('due', '=', $monthy)
            ->first();

        return ($count->total_income != null) ? $count->total_income : 0.00;
    }

    public function sendRemember($uuid, Request $request)
    {
        try{
            $invoice = Invoice::with(['customer'])->where(['uuid' => $uuid, 'account_id' => AccountId()])->first();

            if($request->from == "old"){
                $to = $invoice->customer->email;
            }else{
                $to = $request->to;
            }

            $event = Event::dispatch(new SendEmail('invoice_remember', $invoice, $to));

            return response()->json([
                'status' => 'success',
                'message' => 'Notificação enviada para: '.$to,
                'response' => $event[0]
            ]);
        }

        catch (\Exception $e) {
            if(QueryException::class){

                ErrorReport($uuid, $e, __FUNCTION__, __FILE__, 500);

                return response()->json([
                    'error' => [
                        'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                    ]
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            return response()->json([
                'error' => [
                    'message' => $e->getMessage(),
                    'line' => $e->getLine(),
                    'file' => $e->getFile()
                ]
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function cancelInvoice($uuid)
    {
        try{
            $invoice = Invoice::query()->where(['uuid' => $uuid, 'account_id' => AccountId()])->first();
            $invoice->status_id = 4;
            $invoice->save();

            event(new LogActivity($invoice, 'cancel', customerLogged(), adminLogged()));

            return response()->json([
                'status' => 'success',
                'message' => 'A cobrança Nº '.$invoice->id.' foi cancelada.'
            ]);

        }catch (\Exception $e){
            if(QueryException::class){

                ErrorReport($uuid, $e, __FUNCTION__, __FILE__, 500);

                return response()->json([
                    'error' => [
                        'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                    ]
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            return response()->json([
                'error' => [
                    'message' => $e->getMessage(),
                    'line' => $e->getLine(),
                    'file' => $e->getFile()
                ]
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function changeDue(Request $request, $uuid)
    {
        $validator = Validator::make($request->all(), [
            'due'   => 'required | date',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else {
            try{
                $invoice = Invoice::query()->where(['uuid' => $uuid, 'account_id' => AccountId()])->first();
                $invoice->due = $request->due;
                $invoice->save();

                event(new LogActivity($invoice, 'change_due', customerLogged(), adminLogged()));

                return response()->json([
                    'status' => 'success',
                    'message' => 'O vencimento da cobrança Nº '.$invoice->id.' foi alterada.'
                ]);


            }catch (\Exception $e){

                if(QueryException::class){

                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json([
                    'error' => [
                        'message' => $e->getMessage(),
                        'line' => $e->getLine(),
                        'file' => $e->getFile()
                    ]
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Altera o status da fatura
     * @param Integer $status_code
     * @param null $obs
     * @return mixed
     */
    public function setStatus($status_code, $obs)
    {
        return $this->invoiceRepository->update(['status' => (int) $status_code, 'obs' => $obs], $this->invoice_id);
    }

    public function remove($uuid)
    {
        $invoice = Invoice::query()->where('uuid', $uuid)->first();

        if($invoice == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Esta fatura não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        $invoice->delete();

        event(new LogActivity($invoice, 'deleted', customerLogged(), adminLogged()));

        return response()->json([
            'status' => 'success',
            'message' => 'A Fatura Nº '.$invoice->id.' foi excluída.'
        ]);
    }

    ####################################################################################################################

    public function getAll_items($invoice_id)
    {
        $invoices = DB::table('invoice_items as ivt')
            ->where('ivt.invoice_id', $invoice_id)
            ->orderBy('id', 'asc')
            ->get();

        return ($invoices->isNotEmpty()) ? $invoices : $invoices;
    }

    public function addItems($items)
    {
        if(!empty($items)){
            return DB::table('invoice_items')->insertGetId($items);
        }
    }

    public function getTypes()
    {
        return TypeInvoice::all();
    }

    /**
     * @param Request $request
     * @param $uuid
     * @throws \Exception
     */
    public function changePaid(Invoice $invoice, $request, $parent, $module = null)
    {
        DB::beginTransaction();

        try{
            // Se houver um arquivo de anexo (comprovante)
            if($request instanceof Request && $request->has('receipt'))
            {
                $event = Event::dispatch(new InvoiceChangePaid($invoice, $request, $parent,null));
            }
            else{
                if(is_object($request) && isset($request->type_payment_id))
                {
                    $event = Event::dispatch(new ConfirmPayment($invoice, $request, null));
                }else{
                    throw new Exception("O parâmetro 'type_payment_id' deve ser informado.", 500);
                }
            }

            /**
             * [0] = \App\Models\Statement
             * [1] = \App\Models\Transaction
             * [2] = \App\Models\Subscription
             * [3] = Anexo de Comprovante
             */

            if($event[0] != null && $event[1] != null)
            {
                DB::commit();
                Cache::forget('invoice_'.$invoice->uuid);

            }else{
                DB::rollBack();
            }

            return (object)[
                'invoice'       => $invoice,
                'statement'     => $event[0],
                'attachment'    => @$event[3],
                'transaction'   => $event[2],
            ];

        }catch (\Exception $e)
        {
            DB::rollBack();

            if(QueryException::class){

                ErrorReport(serialize(null), $e, __FUNCTION__, __FILE__, 500);
            }

            return $e->getMessage();
        }

    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function createInvoice(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id'   => 'required | integer | exists:customers,id',
            'type_invoice_id'  => 'required | integer | exists:type_invoices,id',
            'due'           => 'required | date | date_format:Y-m-d',
            'items'         => 'required | array'
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else {
            //status_id, account_id, uuid
            $request['uuid'] = Uuid::generate(4)->string;
            $request['account_id'] = AccountId();
            $request['tax'] = $request->tax;
            $request['discount'] = $request->discount;
            $request['status_id'] = 1;

            if(isset($request->installment) && $request->installment == 1){
                $request['in_installment'] = true;
                $request['installments'] = $request->installments['qty'];

                $request['total'] = $request->total_geral / $request['installments'];
                $request['part'] = 1;
            }

            try
            {
                DB::beginTransaction();
                $invoice = Invoice::create($request->except('items'));

                foreach ($request->items as $item){

                    if($item['type'] == "plan")
                    {
                        $add_item = [
                            'plan_id'   => $item['plan']['id'],
                            'price_id'  => $item['payment_cycle']['price_id'],
                            'domain'    => @$item['domain'],
                            'price'     => $item['price'],
                            'type'      => $item['type'],
                            'months'    => $item['payment_cycle']['months']
                        ];
                    }

                    if($item['type'] == "nulled")
                    {
                        $add_item = [
                            'description'   => $item['description'],
                            'domain'        => @$item['domain'],
                            'price'         => $item['price'],
                            'type'          => $item['type'],
                            'months'        => $item['payment_cycle']['months']
                        ];
                    }

                    $invoice->invoice_items()->create($add_item);
                }

                DB::commit();

                // Isso é apenas para ser exibido no response (Não excluir);
                $invoice->invoice_items;

                return response()->json([
                    'status' => 'success',
                    'message' => 'Fatura Nº '.$invoice->id.' criada com sucesso',
                    'data' => [
                        'invoice' => $invoice
                    ]
                ]);

            } catch (\Exception $e) {

                DB::rollBack();

                if(QueryException::class){

                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return ['error' => $e->getMessage()];
            }
        }
    }

    /* Cria uma nova invoice vinda do evento boleto e cartao payment*/
    public function newInvoice($dados)
    {
        $items = $dados['invoice_items'];
        $invoice = Invoice::create($dados);

        foreach ($items as $item){
            $invoice->invoice_items()->create($item);
        }

        return $invoice;
    }

    public function searchSimples($firstday, $lastday)
    {
        return Invoice::query()->whereBetween('due', [$firstday, $lastday])->where(['account_id'    =>  AccountId()])->paginate(20);
    }

    public function searchAdvanced($busca)
    {
        $invoices = Invoice::query();
        if($busca['status']  != null)
        {
            $invoices->where('status', $busca['status']);
        }

        if($busca['customer'] != null)
        {
            $invoices->with(['customer'])->whereHas('customer', function ($query) use ($busca){
                $query->where('name', 'LIKE', "%{$busca['customer']}%");
            });
        }

        if($busca['due'] !=null)
        {
            $invoices->where('due', $busca['due']);
        }

        $invoices->where(['account_id'  =>  AccountId()])->orderBy('id', 'desc');

        return($invoices->paginate(20)->total() != 0) ? $invoices->paginate(20) : NULL;
    }

    public function getAll_search($search)
    {
        return Invoice::with('customer')
            ->whereHas('account', function ($q){
                $q->where('id', AccountId());
            })
            ->where(['id' => $search])
            ->paginate(20);
    }

    public function changeNull($uuid, Request $request)
    {
        $invoice = Invoice::query()->where('uuid', $uuid)->first();

        if($invoice == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Esta fatura não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        $validator = Validator::make($request->all(), [
            'obs'               => 'required',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else {
            if($invoice->status_id == 5)
            {
                return response()->json([
                    'error' => [
                        'message' => 'Você não pode anular uma fatura que já foi paga.'
                    ]
                ]);
            }else{
                try{
                    $request['account_id'] = AccountId();
                    $request['status_id'] = 3;
                    $invoice->update($request->all());

                    return \response()->json([
                        'status' => 'success',
                        'message' => 'A Fatura Nº '.$invoice->id.' foi marcada como NULA',
                        'data' => [
                            'invoice' => $invoice
                        ]
                    ], Response::HTTP_OK);

                }catch (\Exception $e)
                {
                    if(QueryException::class){

                        ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                        return response()->json([
                            'error' => [
                                'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                            ]
                        ], Response::HTTP_INTERNAL_SERVER_ERROR);
                    }

                    return response()->json([
                        'error' => [
                            'message' => $e->getMessage(),
                            'line' => $e->getLine(),
                            'file' => $e->getFile()
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }
            }
        }
    }

    public function update($uuid, Request $request)
    {
        $invoice = Invoice::query()->where('uuid', $uuid)->first();

        if($invoice == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Esta fatura não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        $validator = Validator::make($request->all(), [
            'customer_id'   => 'required | integer | exists:customers,id',
            'total'         => 'required | numeric | between:0,99.99',
            'type_invoice_id'  => 'required | integer | exists:type_invoices,id',
            'due'           => 'required | date | date_format:Y-m-d',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else {
            try{

                $invoice = Invoice::query()->where('uuid', $uuid)->first();
                foreach ($request->all() as $index => $update)
                {
                    $invoice->$index = $update;
                }

                $invoice->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Dados da Fatura Nº '.$invoice->id.' foram atualizados.',
                    'data' => [
                        'invoice' => $invoice
                    ]
                ], Response::HTTP_OK);

            }catch (\Exception $e)
            {
                if(QueryException::class){

                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json([
                    'error' => [
                        'message' => $e->getMessage(),
                        'line' => $e->getLine(),
                        'file' => $e->getFile()
                    ]
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
    }
}