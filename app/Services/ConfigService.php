<?php
/**
 * Created by PhpStorm.
 * User: Lucas e Nathalia
 * Date: 09/07/2018
 * Time: 22:17
 */

namespace App\Services;

use App\Models\Account;
use App\Models\Bank;
use App\Models\Email;
use App\Models\Option;
use App\Models\TemplateEmail;
use App\Models\TypeEmail;
use App\Models\TypeInvoice;
use App\Models\TypeModule;
use App\Models\TypePayment;
use App\Models\TypePlan;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ConfigService
{
    /**
     * ConfigService constructor.
     */
    public function __construct()
    {

    }

    public function getAccount()
    {
        return Account::with(['account_plan', 'reseller'])->where(['id' => AccountId()])->firstOrFail();
    }

    public function saveOption(Request $request)
    {
        foreach ($request->all() as $key => $option)
        {
            $get_option = $this->getOption($key);

            if(is_null($get_option))
            {
                Option::create([
                    'account_id' => AccountId(),
                    'name' => $key,
                    'value' => $option
                ]);
            }else{
                $get_option->value = $option;
                $get_option->save();
            }
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Configurações atualizadas com sucesso.',
            'data' => $request->all(),
        ], Response::HTTP_OK);
    }

    public function getOptions()
    {
        try{
            $result = [];

            $options = Option::query()->where(['account_id' => AccountId()])->get();
            foreach ($options as $option)
            {
                $result[$option->name] = $option->value;
            }

            return $result;

        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function getOption($name)
    {
        try{
            return Option::query()->find(['account_id' => AccountId(), 'name' => $name])->first();
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function getBanks()
    {
        try{
            return Bank::query()->where(['account_id' => AccountId()])->get()->all();
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function newBank($data)
    {
        try{
            return Bank::create($data);
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function getTypePayments()
    {
        try{
            return TypePayment::query()->orderBy('name', 'ASC')->get()->all();
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function getPaymentsCycles()
    {
        try{
            return DB::table('payment_cycles')->get()->all();
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function createPaymentCycle($name, $months)
    {
        try{
            return DB::table('payment_cycles')->insert([
                'name' => $name,
                'slug' => str_slug($name),
                'months' => $months
            ]);
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function updatePaymentCycle($id, $name, $months)
    {
        try{
            return DB::table('payment_cycles')->where('id', $id)->update([
                'name' => $name,
                'slug' => str_slug($name),
                'months' => $months
            ]);
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function deletePaymentCycle($id)
    {
        try{
            return DB::table('payment_cycles')->where('id', $id)->delete();
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function getTypesPlan()
    {
        try{
            return TypePlan::all();
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function getTypesInvoice()
    {
        try{
            return TypeInvoice::query()->orderBy('name', 'ASC')->get()->all();
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function createTypePlan($name, $slug = null)
    {
        try{
            return DB::table('type_plans')->insert([
                'name' => $name,
                'slug' => (!is_null($slug)) ? $slug : str_slug($name)
            ]);
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function updateTypePlan($id, $name)
    {
        try{
            return DB::table('type_plans')->where('id', $id)->update([
                'name' => $name,
                'slug' => str_slug($name)
            ]);
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function deleteTypePlan($id)
    {
        try{
            return DB::table('type_plans')->where('id', $id)->delete();
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function getTypesTerm()
    {
        try{
            return DB::table('type_terms')->orderBy('name', 'asc')->get()->all();
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function createTypeTerm($name)
    {
        try{
            return DB::table('type_terms')->insert([
                'name' => $name
            ]);
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function updateTypeTerm($id, $name)
    {
        try{
            return DB::table('type_terms')->where('id', $id)->update([
                'name' => $name
            ]);
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function deleteTypeTerm($id)
    {
        try{
            return DB::table('type_terms')->where('id', $id)->delete();
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function getTypesModules()
    {
        try{
            return TypeModule::with(['modules', 'modules.accounts'])->get()->all();
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function createTypeModule($name, $slug = null)
    {
        try{
            return DB::table('type_modules')->insert([
                'name' => $name,
                'slug' => (!is_null($slug)) ? $slug : str_slug($name)
            ]);
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function updateTypeModule($id, $name)
    {
        try{
            return DB::table('type_modules')->where('id', $id)->update([
                'name' => $name
            ]);
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * @param $id
     * @return int|string
     */
    public function deleteTypeModule($id)
    {
        try{
            return DB::table('type_modules')->where('id', $id)->delete();
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function createTypeItem($name)
    {
        try{
            return DB::table('type_invoice_items')->insert(['name' => $name]);
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function updateTypeItem($id, $name)
    {
        try{
            return DB::table('type_invoice_items')->where('id', $id)->update([
                'name' => $name
            ]);
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function deleteTypeItem($id)
    {
        try{
            return DB::table('type_invoice_items')->where('id', $id)->delete();
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function getTypesItem()
    {
        try{
            return DB::table('type_invoice_items')->get()->all();
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function createTypeAddress($name)
    {
        try{
            return DB::table('type_addresses')->insert([
                'name' => $name,
                'slug' => str_slug($name)
            ]);
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function updateTypeAddress($id, $name)
    {
        try{
            return DB::table('type_addresses')->where('id', $id)->update([
                'name' => $name,
                'slug' => str_slug($name)
            ]);
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function deleteTypeAddress($id)
    {
        try{
            return DB::table('type_addresses')->where('id', $id)->delete();
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function getTypesAddress()
    {
        try{
            return DB::table('type_addresses')->get()->all();
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function getEmails()
    {
        return TypeEmail::with(['emails', 'emails.templates'])->has('emails', '>', 0)->get()->all();
    }

    public function getEmailbyTag($tag)
    {
        $emailService = new EmailService();

        return [
            'config'     => Email::with(['type'])->where(['tag' => $tag])->first(),
            'content'    => $emailService->getTemplate($tag),
            'shortcodes' => $emailService->getFields($tag)
        ];
    }

    public function getTemplateEmail($id)
    {
        return TemplateEmail::with(['email'])->where(['email_id' => $id, 'account_id' => AccountId()])->first();
    }

    public function saveAccount(Request $request, $logo_url)
    {
        $account = $this->getAccount();
        $fields = [
            'name_business',
            'logo',
            'domain',
            'email_contact',
            'phone_contact',
            'url_terms',
        ];
        foreach ($request->except('logo') as $field => $value){
            if(in_array($field, $fields)){
                $account->$field = $value;
            }
        }

        $account->logo = $logo_url.'?v='.randomNumber(4);
        $account->save();

        return \response()->json([
            'status' => 'success',
            'message' => 'Configurações da conta atualizada com sucesso.',
            'data' => $account
        ]);
    }
}