<?php
/**
 * Created by PhpStorm.
 * User: codemaxbr
 * Date: 29/01/19
 * Time: 18:32
 */

namespace App\Services;

use App\Models\Account;
use App\Models\Provider;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Webpatser\Uuid\Uuid;

class ProviderService
{
    /**
     * ProviderService constructor.
     */
    public function __construct()
    {

    }

    public function allProviders()
    {
        try{
            $providers = Provider::withCount(['servers', 'debits' => function($query){
                    $query->whereMonth('due', '=', Carbon::now()->format('m')); // Contar apenas os que tiver Conta para pagar no mês atual
                    $query->where('paid', '=', null);
                }])
                ->whereHas('account', function ($q){
                    $q->where('id', AccountId());
                })
                ->paginate(20);

            return $providers;
        }catch (\Exception $e){
            if(QueryException::class){

                ErrorReport(null, $e, __FUNCTION__, __FILE__, 500);

                return response()->json([
                    'error' => [
                        'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                    ]
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            return ['error' => $e->getMessage()];
        }
    }

    public function getAll_search($search)
    {
        return Provider::withCount(['servers', 'debits' => function($query){
                $query->whereMonth('due', '=', Carbon::now()->format('m')); // Contar apenas os que tiver Conta para pagar no mês atual
                $query->where('paid', '=', null);
            }])
            ->whereHas('account', function ($q){
                $q->where('id', AccountId());
            })
            ->where('name', 'LIKE', "%{$search}%")
            ->paginate(20);
    }

    public function createProvider(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'cpf_cnpj' => 'required',
            'email' => 'required | email',
            'phone' => 'required',
            'type' => 'required'
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{

            try{
                $request['uuid'] = Uuid::generate(4)->string;
                $request['account_id'] = AccountId();

                if($request->has('birthdate')){
                    if($request->birthdate != '' || $request->birthdate != null){
                        $request['birthdate'] = Carbon::createFromFormat('d/m/Y', $request->birthdate)->toDateString();
                    }else{
                        unset($request['birthdate']);
                    }
                }

                $provider = Provider::create($request->all());

                return response()->json([
                    'status' => 'success',
                    'message' => 'Fornecedor cadastrado com sucesso.',
                    'data' => [
                        'provider' => $provider
                    ]
                ], Response::HTTP_OK);
            }
            catch (\Exception $e){

                if(QueryException::class){

                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return ['error' => $e->getMessage()];
            }
        }
    }

    public function getProvider($uuid)
    {
        $provider = Provider::query()->where(['uuid' => $uuid])->get()->first();

        if($provider == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Este fornecedor não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        return $provider;
    }

    public function updateProvider($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'cpf_cnpj' => 'required',
            'email' => 'required | email',
            'phone' => 'required',
            'type' => 'required'
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{
            try{
                $provider = Provider::find($id);

                if($request->has('birthdate')){
                    if($request->birthdate != '' || $request->birthdate != null){
                        $request['birthdate'] = Carbon::createFromFormat('d/m/Y', $request->birthdate)->toDateString();
                    }else{
                        unset($request['birthdate']);
                    }
                }

                foreach ($request->all() as $input => $value)
                {
                    $provider->$input = $value;
                }

                $provider->update($request->all());
                return $provider;

            }catch (\Exception $e){

                if(QueryException::class){

                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return ['error' => $e->getMessage()];
            }
        }
    }

    public function deleteProvider($id)
    {
        try{
            $provider = Provider::query()->where(['account_id' => AccountId()])->find($id);
            if($provider == null)
            {
                return response()->json([
                    'error' => [
                        'message' => 'Este fornecedor não existe na base de dados.'
                    ]
                ], Response::HTTP_NOT_FOUND);
            }

            $provider->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Fornecedor removido com sucesso.'
            ], Response::HTTP_OK);

        }catch (\Exception $e){
            if(QueryException::class){

                ErrorReport(serialize($provider), $e, __FUNCTION__, __FILE__, 500);

                return response()->json([
                    'error' => [
                        'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                    ]
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            return ['error' => $e->getMessage()];
        }
    }

    public function getTotal()
    {
        return Provider::query()->where(['account_id' => AccountId()])->count('id');
    }
}