<?php
namespace App\Services;

use App\Events\CustomerRegistered;
use App\Models\Account;
use App\Models\Subscription;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Models\Customer;
use Carbon\Carbon;
use DateTimeZone;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Webpatser\Uuid\Uuid;

class CustomerService
{

    private $customerRepository;
    private $account_id;

    /**
     * CustomerService constructor.
     */
    public function __construct()
    {
        //$this->account_id = AccountId();
    }

    public function total($account)
    {
        return DB::table('customers as c')
            ->where('account_id', $account)
            ->count();
    }

    public function getCustomers()
    {
        $result = Customer::with('subscriptions')->where(['account_id' => AccountId()]);
        return $result->paginate(20);
    }

    public function getAll_search($search)
    {
        return Customer::with('account')
            ->whereHas('account', function ($q){
                $q->where('id', AccountId());
            })
            ->where('name', 'LIKE', "%{$search}%")
            ->paginate(20);
    }

    //todo remover essa função (sem uso)
    public function getAjax_search($account, $search)
    {
        $this->account_id = $account;

        return Customer::with('account')
            ->select('id', 'uuid', 'name', 'email')
            ->whereHas('account', function ($q){
                $q->where('id', $this->account_id);
            })
            ->where('name', 'LIKE', "%{$search}%")
            ->paginate(20);
    }

    //todo remover essa função (sem uso)
    public function getAdvanced_search(Request $request)
    {


        //return $customers->paginate(20);
        /*

        $customers = DB::table('customers as c');
        $customers->where('c.account_id', $account);
        if($search['type'] != NULL){
            $customers->where('c.type', $search['type']);
        }

        if($search['cpf_cnpj'] != NULL){
            $customers->where('c.cpf_cnpj', $search['cpf_cnpj']);
        }

        if($search['email'] != NULL){
            $customers->where('c.email', $search['email']);
        }

        if($search['phone'] != NULL){
            $customers->where('c.phone', $search['phone']);
        }

        $customers->orderBy('name', 'asc');

        return ($customers->paginate(15)->total() != 0) ? $customers->paginate(15) : NULL;
        */
    }

    //todo remover essa função (sem uso)
    public function getCPF_search($account, $search)
    {
        $customers = DB::table('customers as c')
            ->where('c.account_id', $account)
            ->where('c.cpf_cnpj', $search)
            ->get();

        return ($customers->isNotEmpty()) ? $customers : NULL;
    }

    public function uniqueEmail($email)
    {
        $find = DB::table('customers')->select(['id', 'email', 'account_id'])->where(['email' => $email, 'account_id' => AccountId()])->first();
        if($find == null){
            return \response()->json(true);
        }else{
            return \response()->json(false);
        }
    }

    public function uniqueCPF($cpf_cnpj)
    {
        $find = DB::table('customers')->select(['id', 'cpf_cnpj', 'account_id'])->where(['cpf_cnpj' => $cpf_cnpj, 'account_id' => AccountId()])->first();
        if($find == null){
            return \response()->json(true);
        }else{
            return \response()->json(false);
        }
    }

    public function createCustomer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => [
                'required',
                'email',
                Rule::unique('customers')->where(function ($query) {
                    $query->where(['account_id' => AccountId()]);
                }),
            ],
            'type' => 'required',
            'cpf_cnpj' => [
                'required',
                Rule::unique('customers')->where(function ($query) {
                    $query->where(['account_id' => AccountId()]);
                }),
            ],
            'name' => 'required',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{

            try{
                $request['account_id'] = AccountId();
                $request['uuid'] = Uuid::generate(4)->string;

                if($request->has('birthdate')){
                    if($request->birthdate != '' || $request->birthdate != null){
                        $request['birthdate'] = Carbon::createFromFormat('d/m/Y', $request->birthdate)->toDateString();
                    }else{
                        unset($request['birthdate']);
                    }
                }

                $customer = Customer::create($request->all());
                Event::dispatch(new CustomerRegistered($customer));

                return response()->json([
                    'status' => 'success',
                    'message' => 'Cliente cadastrado com sucesso.',
                    'data' => [
                        'customer' => $customer
                    ]
                ], Response::HTTP_OK);

            }catch (\Exception $e){

                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }

    //todo remover essa função (sem uso)
    public function newCustomer($dados)
    {
        if(!empty($dados)){

            unset($dados['_token']);

            $dados['created_at'] = Carbon::now(new DateTimeZone('America/Sao_Paulo'));
            $dados['updated_at'] = Carbon::now(new DateTimeZone('America/Sao_Paulo'));
            $dados['uuid'] = Uuid::generate()->string;

            $customer = Customer::create($dados);

            Event::dispatch(new CustomerRegistered($customer));
            return $customer;
        }
    }

    public function newCustomer_multiple($dados)
    {
        if(!empty($dados)){
            $customer = DB::table('customers')->insert($dados);
            return $customer;
        }
    }

    public function updateAddress($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'address' => 'required',
            'number' => 'required',
            'zipcode' => 'required',
            'city' => 'required',
            'uf' => 'required',
        ], [
            'required' => 'Este campo é obrigatório.',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{
            $customer = Customer::query()->where(['uuid' => $id, 'account_id' => AccountId()])->first();

            if($customer == null)
            {
                return response()->json([
                    'error' => [
                        'message' => 'Este cliente não existe na base de dados.'
                    ]
                ], Response::HTTP_NOT_FOUND);
            }

            try{
                foreach ($request->all() as $input => $value)
                {
                    $customer->$input = $value;
                }

                $customer->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'O dados de endereço de "'.$customer->name.'" foram atualizados.',
                    'data' => [
                        'customer' => $customer
                    ]
                ]);
            }catch (\Exception $e){

                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }


                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }

    public function updateCustomer($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => [
                'required',
                'email',
                Rule::unique('customers')->where(function ($query) use ($id) {
                    $query->where(['account_id' => AccountId()]);
                })->ignore($id, 'uuid'),
            ],
            'type' => 'required',
            'cpf_cnpj' => [
                'required',
                Rule::unique('customers')->where(function ($query) use ($id) {
                    $query->where(['account_id' => AccountId()]);
                })->ignore($id, 'uuid'),
            ],
            'name' => 'required',
        ], [

            'required' => 'Este campo é obrigatório.',
            'cpf_cnpj.unique' => 'Já existe um cadastro com este CPF/CNPJ.',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{
            $customer = Customer::query()->where(['uuid' => $id, 'account_id' => AccountId()])->first();

            if($customer == null)
            {
                return response()->json([
                    'error' => [
                        'message' => 'Este cliente não existe na base de dados.'
                    ]
                ], Response::HTTP_NOT_FOUND);
            }

            try{
                if($request->has('birthdate'))
                {
                    if($request->birthdate != '' || $request->birthdate != null){
                        $request['birthdate'] = Carbon::createFromFormat('d/m/Y', $request->birthdate)->toDateString();
                    }else{
                        unset($request['birthdate']);
                    }
                }

                foreach ($request->all() as $input => $value)
                {
                    $customer->$input = $value;
                }

                $customer->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Dados do cliente "'.$customer->name.'" foram atualizados.',
                    'data' => [
                        'customer' => $customer
                    ]
                ]);
            }catch (\Exception $e){

                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }


                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }

    public function deleteCustomer($uuid)
    {
        try{
            $customer = Customer::query()->where(['account_id' => AccountId(), 'uuid' => $uuid])->first();
            if($customer == null)
            {
                return response()->json([
                    'error' => [
                        'message' => 'Este cliente não existe na base de dados.'
                    ]
                ], Response::HTTP_NOT_FOUND);
            }

            $customer->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Cliente removido com sucesso.'
            ]);

        }catch (\Exception $e){
            if(QueryException::class){

                ErrorReport(serialize($uuid), $e, __FUNCTION__, __FILE__, 500);

                return response()->json([
                    'error' => [
                        'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                    ]
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            return ['error' => $e->getMessage()];
        }
    }

    public function getCustomer($uuid, $coluna = 'uuid')
    {

        $customer = Customer::with('statements', 'subscriptions', 'subscriptions.plan', 'subscriptions.type_payment', 'credit_cards')->where($coluna, $uuid)->first();

        if($customer == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Este cliente não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        return $customer;
    }

    public function getFields()
    {
        //$temp = $this->newQuery()->fromQuery("SHOW FIELDS FROM ".$this->getTable());
        $temp = DB::select(DB::raw("SHOW FULL FIELDS FROM customers"));
        $fields = array();
        foreach ($temp as $val){
            $fields[$val->Field] = $val->Comment;
        }

        return array_filter($fields);
    }

    public function getTotal()
    {
        $clientes = Customer::query()->where(['account_id' => AccountId()])->count('id');
        return response()->json(['total' => $clientes]);
    }

    /*******************************************************************************************************************
     * Cartões de Créditos
     ******************************************************************************************************************/

    public function getCreditCards($customer_uuid)
    {
        return $customer_uuid;
    }

    public function createCreditCard($customer_uuid, Request $request)
    {
        return [$customer_uuid, $request->all()];
    }

    public function getCreditCard($customer_uuid, $id)
    {
        return [$customer_uuid, $id];
    }

    public function updateCreditCard($customer_uuid, $id, Request $request)
    {
        return [$customer_uuid, $id, $request->all()];
    }

    public function deleteCreditCard($customer_uuid, $id)
    {
        return [$customer_uuid, $id];
    }

    public function getSubscriptions($customer_id)
    {
        return Subscription::with(['plan', 'plan.prices', 'plan.prices.payment_cycle', 'plan.grids', 'type_payment', 'invoices'])
            ->where(['customer_id' => $customer_id])
            ->paginate(20);
    }

    public function getAll_export()
    {
        return Customer::query()->where(['account_id' => AccountId()])->get()->all();
    }
}