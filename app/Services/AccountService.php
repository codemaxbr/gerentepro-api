<?php

namespace App\Services;

use App\Events\AccountCreated;
use App\Models\Account;
use App\Models\Module;
use App\Models\ModulesAccount;
use App\Models\Option;
use App\Models\User;
use Carbon\Carbon;
use DateTimeZone;
use Gerencianet\Gerencianet;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Webpatser\Uuid\Uuid;

use CodemaxBR\F2b\Facades\F2b;
use CodemaxBR\Iugu\Facades\Iugu;
use CodemaxBR\Padmoney\Facades\Padmoney;

class AccountService
{
    private $find = array();
    /**
     * @var UserService
     */
    private $userService;
    public $js_load = [];
    private $model;

    /**
     * AccountService constructor.
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
        $this->model = new Account;
    }

    /**
     * Verifica se já existe uma conta por um domínio específico
     *
     * @param $domain
     * @return bool
     */
    public function existsDomain($domain)
    {
        $count = DB::table('accounts as acc')
            ->select('domain')
            ->where('domain', $domain)
            ->count();

        return ($count > 0) ? TRUE : FALSE;
    }

    public function getAccount($uuid)
    {
        if(!Cache::has('account_'.$uuid))
        {
            $account = Account::with(['reseller', 'account_plan', 'account_plan.payment_cycle', 'account_plan.type_term'])->where(['uuid' => $uuid])->get();

            if($account->isNotEmpty())
            {
                Cache::remember('account_'.$uuid, Carbon::now()->addDays(2), function() use ($account){
                    return $account->first();
                });
            }else{
                throw new \Exception('Esta conta não existe na base de dados.', '4041');
            }
        }

        return Cache::get('account_'.$uuid);
    }

    public function getAccounts()
    {
        return Account::with(['reseller'])->paginate(20);
    }

    public function newAccount($data)
    {
        DB::beginTransaction();

        try{
            $account = [
                'uuid'              => Uuid::generate(4)->string,
                'name_business'     => $data['name_business'],
                'domain'            => $data['domain'],
                'reseller_id'       => (!isset($data['reseller_id'])) ? 1 : $data['reseller_id'],
                'account_plan_id'   => (!isset($data['account_plan_id'])) ? 1 : $data['account_plan_id'],
            ];

            /** @var Account $nova_conta */
            $nova_conta = Account::create($account);
            $token      = str_random(35);
            $senha      = randomPassword();

            $user = $nova_conta->users()->create([
                'name'      => $data['name'],
                'email'     => $data['email'],
                'password'  => Hash::make($senha),
                'token'     => $token,
                'is_reseller' => 1
            ]);
            
            $role = $nova_conta->roles()->create([
                'name' => 'root', 
                'description' => 'Administrador'
            ]);

            $role->givePermissionTo(\App\Models\Permission::all());
            $user->assignRole('root');

            //todo Criar Job para enviar e-mails, invés de Event
            //Event::dispatch(new AccountCreated($nova_conta, $user));

            DB::commit();
            return $nova_conta;

        }catch (\Exception $e){
            DB::rollBack();

            if(QueryException::class){
                ErrorReport(serialize($data), $e, __FUNCTION__, __FILE__, 500);
            }

            throw new \Exception($e->getMessage());
        }
    }

    public function updateAccount($uuid, $dados)
    {
        DB::beginTransaction();

        /** @var Account $account */
        $account = Account::query()->where(['uuid' => $uuid])->first();

        $fields = [
            'name_business',
            'logo',
            'domain',
            'email_contact',
            'phone_contact',
            'url_terms',
        ];

        try{
            foreach ($dados as $field => $value){
                if(in_array($field, $fields)){
                    $account->$field = $value;
                }
            }

            $account->save();
            if($account->wasChanged())
            {
                DB::commit();
            }

            return $account;

        }catch (\Exception $e)
        {
            if(QueryException::class){
                ErrorReport(serialize($dados), $e, __FUNCTION__, __FILE__, 500);
            }
            throw new \Exception($e->getMessage());
        }
    }

    public function activate($token)
    {
        $user = User::query()->where(['token' => $token])->first();
        if ($user != null)
        {
            $user->confirmed = 1;
            $user->token = '';
            $user->save();

            $account = Account::find($user->account_id);
            $account->status = 1;
            $account->save();

            return response()->json([
                'status' => 'success',
                'message' => 'Parabéns! Sua conta está ativa. Vamos começar?',
                'data' => [
                    'user' => $user,
                ]
            ]);
        }else{
            return response()->json([
                'error' => [
                    'message' => 'Desculpe, token inválido.'
                ]
            ]);
        }
    }

    public function removeAccount($uuid)
    {
        try{
            $account = Account::query()->where(['uuid' => $uuid])->first();
            if($account == null)
            {
                return response()->json([
                    'error' => [
                        'message' => 'Esta conta não existe na base de dados.'
                    ]
                ], Response::HTTP_NOT_FOUND);
            }

            $account->delete();

            //todo colocar pra disparar um evento quando uma conta for removida.

            return response()->json([
                'status' => 'success',
                'message' => 'Conta removida com sucesso.'
            ]);

        }catch (\Exception $e){

            if(QueryException::class){

                ErrorReport(serialize($uuid), $e, __FUNCTION__, __FILE__, 500);

                return response()->json([
                    'error' => [
                        'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                    ]
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            return ['error' => $e->getMessage()];
        }
    }

    public function getAccountByDomain($subdomain)
    {
        if(!Cache::has('account_'.$subdomain))
        {
            $account = DB::table($this->model->getTable())
                ->select(['id', 'name_business', 'uuid', 'logo', 'email_contact', 'phone_contact', 'url_terms'])
                ->where(['domain' => $subdomain])
                ->get();

            if($account->isNotEmpty())
            {
                Cache::remember('account_'.$subdomain, Carbon::now()->addDays(2), function() use ($subdomain, $account){
                    return $account[0];
                });
            }
        }

        return Cache::get('account_'.$subdomain);
    }

    public function getPaymentDefault(Account $account)
    {
        /** @var Option $options */
        $options = $account->options()->whereIn('name', ['boleto_gateway', 'cartao_gateway'])->get();

        if($options->isNotEmpty())
        {
            foreach ($options as $option)
            {
                if($option->name == "boleto_gateway") {
                    $boleto_module = $account->modules()->where(['module_id' => intval($option->value)])->first();
                }

                if($option->name == "cartao_gateway"){
                    $cartao_module = $account->modules()->where(['module_id' => intval($option->value)])->first();
                }
            }

            if((isset($boleto_module) && !is_null($boleto_module)) || (isset($cartao_module) && !is_null($cartao_module))){
                return (object) [
                    'boleto' => $boleto_module,
                    'cartao' => $cartao_module
                ];
            }else{
                throw new \Exception('Você definiu uma forma de pagamento padrão que não está configurado.');
            }
        }else{
            throw new \Exception('Não existe configurações de pagamento padrão.');
        }
    }

    public function getScriptsJS($uuid)
    {
        try{
            $account = $this->getAccount($uuid);
            $modules = $this->getPaymentDefault($account);
            $scripts = [];

            /** @var Module $gateway */
            foreach ($modules as $gateway)
            {
                if (!is_null($gateway) && !in_array($gateway->slug, $this->js_load))
                {

                    $account_conf = $gateway->pivot;
                    $config = (object) unserialize($account_conf->config);

                    switch ($gateway->slug):
                        /**
                         * PagSeguro
                         */
                        case 'pagseguro':
                            try{
                                \PagSeguro\Library::initialize();
                                \PagSeguro\Library::cmsVersion()->setName("Nome")->setRelease("1.0.0");
                                \PagSeguro\Library::moduleVersion()->setName("Nome")->setRelease("1.0.0");

                                \PagSeguro\Configuration\Configure::setEnvironment('production');//production or sandbox
                                \PagSeguro\Configuration\Configure::setAccountCredentials($config->email, $config->token);

                                \PagSeguro\Configuration\Configure::setCharset('UTF-8');// UTF-8 or ISO-8859-1
                                \PagSeguro\Configuration\Configure::setLog(false, '/logpath/logFilename.log');

                                $sessionCode = \PagSeguro\Services\Session::create(
                                    \PagSeguro\Configuration\Configure::getAccountCredentials()
                                );

                                array_push($this->js_load, 'pagseguro');
                                array_push($scripts, (object) [
                                    'type' => 'text/javascript',
                                    'src'  => 'https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js',
                                    'body' => true
                                ]);

                                array_push($scripts, (object) [
                                    'type' => 'text/javascript',
                                    'inner'  => 'var sessionId = "'.$sessionCode->getResult().'";',
                                    'body' => true
                                ]);
                            }catch (\Exception $e){
                                throw new \Exception('(PagSeguro) Configurações de integração estão incorretas. ['.$e->getMessage().']');
                            }
                        break;

                        /**
                         * Gerencianet
                         */
                        case 'gerencianet':
                            try{
                                $payee_code = $config->payee_code;
                                $options = [
                                    'client_id' => $config->client_id,
                                    'client_secret' => $config->client_secret,
                                    'sandbox' => true // altere conforme o ambiente (true = desenvolvimento e false = producao)
                                ];

                                $params = [
                                    'total' => '20000',
                                    'brand' => 'visa'
                                ];

                                $api = new Gerencianet($options);
                                $installments = $api->getInstallments($params, []);

                                array_push($this->js_load, 'gerencianet');
                                array_push($scripts, (object) [
                                    'type' => 'text/javascript',
                                    'inner'  => 'var s=document.createElement("script");s.type="text/javascript";var v=parseInt(Math.random()*1000000);s.src="https://sandbox.gerencianet.com.br/v1/cdn/'.$payee_code.'/"+v;s.async=false;s.id="'.$payee_code.'";if(!document.getElementById("'.$payee_code.'")){document.getElementsByTagName("head")[0].appendChild(s);};$gn={validForm:true,processed:false,done:{},ready:function(fn){$gn.done=fn;}};',
                                    'body' => true
                                ]);
                                array_push($scripts, (object) [
                                    'type' => 'text/javascript',
                                    'inner'  => '$gn.ready(function(checkoutGn) {});',
                                    'body' => true
                                ]);

                            }catch (\Exception $e) {
                                throw new \Exception('(Gerencianet) Configurações de integração estão incorretas. ['.$e->getMessage().']');
                            }
                        break;

                        /**
                         * PagHiper
                         */
                        case 'paghiper':
                            try{
                                \App\Services\Plugins\paghiperService::checkConfig($config->token, $config->apiKey);
                            }catch (\Exception $e){
                                throw new \Exception('(PagHiper) Configurações de integração estão incorretas. ['.$e->getMessage().']');
                            }
                        break;

                        /**
                         * Iugu
                         */
                        case 'iugu':
                            try{
                                $key  = $config->token;
                                $account_id = $config->account_id;

                                Iugu::setCredentials($key, $account_id);
                                Iugu::customers()->all();

                                array_push($this->js_load, 'iugu');
                                array_push($scripts, (object) [
                                    'type' => 'text/javascript',
                                    'src'  => 'https://js.iugu.com/v2',
                                    'body' => true
                                ]);
                                array_push($scripts, (object) [
                                    'type' => 'text/javascript',
                                    'inner'  => 'var accountId = "'.$account_id.'"',
                                    'body' => true
                                ]);

                            }catch (\Exception $e){
                                throw new \Exception('(Iugu) Configurações de integração estão incorretas. ['.$e->getMessage().']');
                            }
                        break;

                        /**
                         * F2b
                         */
                        case 'f2b':
                            try{
                                $token = $config->token;

                                F2b::setCredentials($token);
                                F2b::cobrancas()->listar();

                            }catch (\Exception $e){
                                throw new \Exception('(F2b) Configurações de integração estão incorretas. ['.$e->getMessage().']');
                            }
                        break;

                        /**
                         * Boleto
                         */
                        case 'boleto':
                            try{
                                $dados = (object) $config->config;

                                if(is_null($dados->banco) || is_null($dados->conta) || is_null($dados->agencia) || is_null($dados->carteira)){
                                    throw new \Exception("Módulo não configurado corretamente.");
                                }
                            }catch (\Exception $e){
                                throw new \Exception('(Boleto) Configurações de integração estão incorretas. ['.$e->getMessage().']');
                            }
                        break;

                        /**
                         * Padmoney
                         */
                        case 'padmoney':
                            try{
                                $token  = $config->token;
                                $chave  = $config->chave;

                                Padmoney::setCredentials($token, $chave);
                                Padmoney::boletos()->all();

                            }catch (\Exception $e){
                                throw new \Exception('(Padmoney) Configurações de integração estão incorretas. ['.$e->getMessage().']');
                            }
                        break;

                        /**
                         * Juno
                         */
                        case 'juno':
                            try{
                                $token  = $config->token;
                                $public  = $config->public;

                                $juno = new \App\Services\Plugins\junoService;
                                $juno->checkConfig($token);

                                array_push($this->js_load, 'juno');
                                array_push($scripts, (object) [
                                    'type' => 'text/javascript',
                                    'src'  => 'https://sandbox.boletobancario.com/boletofacil/wro/direct-checkout.min.js',
                                    'body' => true
                                ]);
                                array_push($scripts, (object) [
                                    'type' => 'text/javascript',
                                    'inner'  => 'var public_key = "'.$public.'";',
                                    'body' => true
                                ]);

                            }catch (\Exception $e){
                                throw new \Exception('(Juno) Configurações de integração estão incorretas. ['.$e->getMessage().']');
                            }
                        break;

                    endswitch;
                }
            }

            return $scripts;
        }catch (\Exception $e){
            /*
            if(QueryException::class){

                ErrorReport(serialize($subdomain), $e, __FUNCTION__, __FILE__, 500);

                return response()->json([
                    'error' => [
                        'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                    ]
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }
            */

            return [
                'error' => [
                    'message' => 'Não foi possível carregar os scripts de integração.',
                    'reason' => $e->getMessage()
                ]
            ];
        }
    }
}