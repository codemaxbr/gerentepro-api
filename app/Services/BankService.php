<?php
/**
 * Created by PhpStorm.
 * User: Lucas Maia
 * Date: 01/03/2019
 * Time: 02:35
 */

namespace App\Services;


use App\Models\Bank;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class BankService
{

    /**
     * BankService constructor.
     */
    public function __construct()
    {
    }

    public function getBanks()
    {
        $banks = Bank::with(['bank_house', 'type_bank'])->where(['account_id' => AccountId()])->get();

        if($banks->isEmpty()){
            return response()->json([
                'message' => 'Nenhuma conta bancária encontrada na seu GerentePRO',
                'data' => $banks->all()
            ]);
        }

        return $banks;
    }

    public function createBank(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'owner' => 'required | string',
            'wallet' => 'required',
            'type_bank_id' => 'required | integer | exists:type_banks,id',
            'bank_house_id' => 'required | integer | exists:bank_houses,id',
            'agency' => 'required | numeric',
            'account' => 'required | numeric',
            'digit' => 'required | integer',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{
            try{
                $request['account_id'] = AccountId();
                $bank = Bank::create($request->all());

                return response()->json([
                    'status' => 'success',
                    'message' => 'Conta bancária cadastrada com sucesso.',
                    'data' => [
                        'bank' => $bank
                    ]
                ], Response::HTTP_OK);

            }catch (\Exception $e){

                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }

    public function updateBank($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'owner' => 'required | string',
            'wallet' => 'required',
            'type_bank_id' => 'required | integer | exists:type_banks,id',
            'bank_house_id' => 'required | integer | exists:bank_houses,id',
            'agency' => 'required | numeric',
            'account' => 'required | numeric',
            'digit' => 'required | integer',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{

            try{
                $bank = Bank::find($id);
                foreach ($request->all() as $index => $update)
                {
                    $bank->$index = $update;
                }

                $bank->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Conta bancária atualizada com sucesso.',
                    'data' => $bank
                ], Response::HTTP_OK);
            }
            catch (\Exception $e) {

                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }

    public function getBank($id)
    {
        $bank = Bank::with(['bank_house', 'type_bank'])->find($id);

        if($bank == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Esta conta bancária não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        return $bank;
    }

    public function destroyBank($id)
    {
        try{
            $bank = Bank::query()->where(['account_id' => AccountId()])->find($id);
            if($bank == null)
            {
                return response()->json([
                    'error' => [
                        'message' => 'Esta conta bancária não existe na base de dados.'
                    ]
                ], Response::HTTP_NOT_FOUND);
            }

            $bank->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Conta bancária removida com sucesso.'
            ]);

        }catch (\Exception $e){
            if(QueryException::class){

                ErrorReport(serialize($id), $e, __FUNCTION__, __FILE__, 500);

                return response()->json([
                    'error' => [
                        'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                    ]
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            return ['error' => $e->getMessage()];
        }
    }
}