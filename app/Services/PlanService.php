<?php


namespace App\Services;

use App\Events\CreatePlanModule;
use App\Events\RemovePlanModule;
use App\Events\UpdatePlanModule;
use App\Jobs\CreatePlan;
use App\Jobs\ExampleJob;
use App\Models\Grid;
use App\Models\ModulesAccount;
use App\Models\PaymentCycle;
use App\Models\Plan;
use App\Models\PlanGrid;
use App\Models\PlanPrice;
use App\Models\Price;
use App\Models\TypePlan;
use App\Models\TypeTerm;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\UnauthorizedException;
use Webpatser\Uuid\Uuid;

class PlanService
{

    private $planRepository;
    private $account_id;

    /**
     * PlanService constructor.
     */
    public function __construct()
    {

    }

    public function createGrid(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description' => 'required',
            'type_plan_id' => 'required | integer',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else {
            try{
                $request['account_id'] = AccountId();

                $plan_grid = Grid::create($request->all());
                return \response()->json([
                    'status' => 'success',
                    'message' => 'Grade "'.$plan_grid->description.'" criada com sucesso',
                    'data' => [
                        'plan_grid' => $plan_grid
                    ]
                ], Response::HTTP_OK);
            }catch (\Exception $e){
                if(QueryException::class){

                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return ['error' => $e->getMessage()];
            }
        }
    }

    public function getGrids()
    {
        return Grid::with('type_plan')->where(['account_id' => AccountId()])->get()->all();
    }

    public function updateGrid($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description' => 'required',
            'type_plan_id' => 'required | integer',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else {
            try{
                $grid = Grid::query()->where(['account_id' => AccountId(), 'id' => $id])->first();

                if($grid == null)
                {
                    return response()->json([
                        'error' => [
                            'message' => 'Este plano não existe na base de dados.'
                        ]
                    ], Response::HTTP_NOT_FOUND);
                }

                foreach ($request->all() as $input => $value)
                {
                    $grid->$input = $value;
                }
                $grid->update($request->all());

                return response()->json([
                    'status' => 'success',
                    'message' => 'Grade atualizada com sucesso.',
                    'data' => $grid
                ], Response::HTTP_OK);

            }catch (\Exception $e){

            }
        }
    }

    public function deleteGrid($id)
    {
        $grid = Grid::query()->where(['account_id' => AccountId(), 'id' => $id])->first();

        if($grid == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Esta grade não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }else{
            $grid->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'A Grade "'.$grid->description.'" removida com sucesso.',
            ], Response::HTTP_OK);
        }
    }

    public function updatePrice($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'payment_cycle_id' => 'required | integer | exists:payment_cycles,id',
            'price' => 'required | numeric',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else {
            $price = Price::query()->where(['id' => $id, 'account_id' => AccountId()])->first();

            try{
                $price->update($request->all());

                return response()->json([
                    'status' => 'success',
                    'message' => 'Preço atualizado com sucesso.'
                ], Response::HTTP_OK);

            }catch (\Exception $e){
                if(QueryException::class){

                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return ['error' => $e->getMessage()];
            }
        }
    }

    public function removePrice($id)
    {
        $price = Price::find($id);

        if($price == null){
            return response()->json([
                'error' => [
                    'message' => 'Este preço não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }else{
            $price->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'O preço "'.$price->payment_cycle->name.'" removido com sucesso.',
            ], Response::HTTP_OK);
        }
    }

    public function storePrice(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'payment_cycle_id' => 'required | integer | exists:payment_cycles,id',
            'price' => 'required | numeric',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else {

            try{
                DB::beginTransaction();

                $price = Price::create([
                    'payment_cycle_id' => $request->payment_cycle_id,
                    'price' => $request->price,
                    'account_id' => AccountId()
                ]);

                PlanPrice::create([
                    'price_id' => $price->id,
                    'plan_id' => $request->plan_id,
                    'account_id' => AccountId()
                ]);

                DB::commit();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Preço criado com sucesso.'
                ], Response::HTTP_OK);

            }catch (\Exception $e){
                /*
                if(QueryException::class){

                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }
                */

                return ['error' => $e->getMessage()];
            }
        }
    }

    protected function setAccount($id)
    {
        $this->account_id = $id;
    }

    public function getPlan($uuid)
    {
        $plan = Plan::with('grids', 'prices', 'prices.payment_cycle', 'grids.type_plan', 'subscriptions', 'metadata', 'metadata.module', 'metadata.modules_account')
                ->withCount('subscriptions')
                ->where(['uuid' => $uuid])
                ->first();

        if($plan == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Este plano não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        return $plan;
    }

    public function getTypes()
    {
        return TypePlan::query()->select('id', 'name')->get()->all();
    }

    public function getPaymentCycles()
    {
        return PaymentCycle::query()->select('id', 'name', 'months')->get()->all();
    }

    public function getTypeTerms()
    {
        return TypeTerm::all();
    }

    public function getAll_search($search)
    {
        return Plan::with('account')
            ->whereHas('account', function ($q){
                $q->where('id', AccountId());
            })
            ->where('name', 'LIKE', "%{$search}%")
            ->paginate(20);
    }

    public function getPlans_byType($type_id)
    {
        return Plan::with('payment_cycle:id,name')->where(['type_plan_id' => $type_id, 'account_id' => AccountId()])->paginate(20);
    }

    public function allPlans()
    {
        return Grid::with(['plans', 'type_plan', 'plans.prices', 'plans.prices.payment_cycle', 'plans.subscriptions'])->where(['account_id' => AccountId()])->get();
    }

    public function updatePlan($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'grid_id' => 'required | integer | exists:grids,id',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else {
            try{
                $plan = Plan::query()->where(['account_id' => AccountId()])->find($id);
                if($plan == null)
                {
                    return response()->json([
                        'error' => [
                            'message' => 'Este plano não existe na base de dados.'
                        ]
                    ], Response::HTTP_NOT_FOUND);
                }

                $plan->update([
                    'name' => $request->name,
                    'trial' => $request->trial,
                    'domain' => $request->domain
                ]);

                $plan_grid = PlanGrid::query()->where(['plan_id' => $plan->id, 'account_id' => AccountId()])->first();
                $plan_grid->update([
                    'plan_id' => $plan->id,
                    'grid_id' => $request->grid_id
                ]);

                return response()->json([
                    'status' => 'success',
                    'message' => 'Plano atualizado com sucesso.',
                    'data' => $plan
                ], Response::HTTP_OK);


            }catch (\Exception $e){
                if(QueryException::class){

                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return ['error' => $e->getMessage()];
            }
        }
    }

    public function deletePlan($id)
    {
        try{
            $plan = Plan::query()->where(['account_id' => AccountId()])->find($id);
            if($plan == null)
            {
                return response()->json([
                    'error' => [
                        'message' => 'Este plano não existe na base de dados.'
                    ]
                ], Response::HTTP_NOT_FOUND);
            }

            if($plan->subscriptions()->count() == 0)
            {
                if($plan->module != null && $plan->config != null)
                {
                    DB::beginTransaction();

                    $config = ModulesAccount::query()->where(['module_id' => $plan->module_id, 'server_id' => $plan->server_id])->first();
                    $result = Event::dispatch(new RemovePlanModule($plan, $config));
                    $response = json_decode(json_encode($result[0]));

                    $plan->delete();

                    if($result[0]->status == 1)
                    {
                        DB::commit();
                        return \response()->json([
                            'status' => 'success',
                            'message' => 'Plano "'.$plan->name.'" removido com sucesso.',
                            'data' => [
                                'plan' => $plan,
                                'response_module' => $result[0]
                            ]
                        ], Response::HTTP_OK);
                    }

                    if($result[0]->status == 0)
                    {
                        DB::rollBack();
                        return \response()->json([
                            'error' => [
                                'message' => 'Ocorreu um erro na integração do módulo.',
                                'response_module' => $result[0]
                            ]
                        ], Response::HTTP_INTERNAL_SERVER_ERROR);
                    }
                }
                else{
                    $prices = PlanPrice::query()->where(['plan_id' => $plan->id, 'account_id' => AccountId()])->get();
                    if($prices->isNotEmpty()){
                        foreach ($prices->all() as $price){
                            $price_delete = Price::find($price->price_id);
                            $price_delete->delete();
                        }
                    }

                    $plan->delete();

                    return \response()->json([
                        'status' => 'success',
                        'message' => 'Plano "'.$plan->name.'" removido com sucesso.',
                        'data' => [
                            'plan' => $plan,
                        ]
                    ], Response::HTTP_OK);
                }
            }else{
                return response()->json([
                    'error' => [
                        'message' => 'Você não pode excluir este plano, por que possui assinaturas ativas.'
                    ]
                ], Response::HTTP_BAD_REQUEST);
            }

        }catch (\Exception $e){
            
            if(QueryException::class){

                ErrorReport($id, $e, __FUNCTION__, __FILE__, 500);

                return response()->json([
                    'error' => [
                        'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                    ]
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            return ['error' => $e->getMessage()];
        }
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function createPlan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'grid_id' => 'required | integer | exists:grids,id',
            'price' => 'required | numeric',
            'payment_cycle_id' => 'required | integer | exists:payment_cycles,id',
        ], [

            'required' => 'Este campo é obrigatório.',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{

            try{
                $request['uuid'] = Uuid::generate(4)->string;
                $request['account_id'] = AccountId();

                DB::beginTransaction();
                // Cria o plano
                $plan = Plan::create($request->all());

                // Adiciona o plano ao grupo
                PlanGrid::create([
                    'plan_id' => $plan->id,
                    'grid_id' => $request->grid_id,
                    'account_id' => AccountId()
                ]);

                // Cria o preço
                $price = Price::create([
                    'payment_cycle_id' => $request->payment_cycle_id,
                    'price' => $request->price,
                    'account_id' => AccountId()
                ]);

                PlanPrice::create([
                    'plan_id' => $plan->id,
                    'price_id' => $price->id,
                    'account_id' => AccountId()
                ]);

                DB::commit();

                return \response()->json([
                    'status' => 'success',
                    'message' => 'Plano "'.$plan->name.'" cadastrado com sucesso.',
                    'data' => [
                        'plan' => $plan,
                    ]
                ]);
            }
            catch (\Exception $e){

                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);
                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return ['error' => $e->getMessage()];
            }
        }
    }
}