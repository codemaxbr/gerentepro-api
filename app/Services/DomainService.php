<?php
namespace App\Services;

use App\Events\DomainRegistered;
use App\Models\Domain;
use App\Models\PriceDomain;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Iodev\Whois\Exceptions\ConnectionException;
use Iodev\Whois\Exceptions\ServerMismatchException;
use Iodev\Whois\Exceptions\WhoisException;
use Iodev\Whois\Whois;

class DomainService
{
    /**
     * DomainService constructor.
     */
    public function __construct()
    {

    }

    public function getDomains()
    {
        return Domain::with([
            'price_domain.module:id,name,logo',
            'price_domain:id,price_register',
            'customer:id,name,email,uuid',
            'payment_cycle:id,name'
        ])->where(['account_id' => AccountId()])->paginate(20);
    }

    public function getPriceDomains()
    {
        return PriceDomain::with(['module:id,name,logo'])->where(['account_id' => AccountId()])->paginate(20);
    }

    public function createDomain(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'domain' => [
                'required',
                Rule::unique('domains')->where(function ($query) {
                    $query->where(['account_id' => AccountId()]);
                }),
            ],
            'customer_id' => 'required | integer | exists:customers,id',
            'price_domain_id' => 'required | integer | exists:price_domains,id',
            'payment_cycle_id' => 'required | integer | exists:payment_cycles,id',
            'due' => 'required | date',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{
            try{
                $request['account_id'] = AccountId();
                $domain = Domain::create($request->all());

                $queue = false;

                if($domain->price_domain->module != null){
                    $queue = true;
                    Event::dispatch(new DomainRegistered($domain));
                }

                return response()->json([
                    'status' => 'success',
                    'message' => 'Domínio cadastrado com sucesso.',
                    'data' => $domain,
                    'queue' => $queue
                ], Response::HTTP_OK);

            }catch (\Exception $e){

                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }

    public function updateDomain($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'domain' => [
                'required',
                Rule::unique('domains')->where(function ($query) {
                    $query->where(['account_id' => AccountId()]);
                })->ignore($id),
            ],
            'customer_id' => 'required | integer | exists:customers,id',
            'price_domain_id' => 'required | integer | exists:price_domains,id',
            'payment_cycle_id' => 'required | integer | exists:payment_cycles,id',
            'due' => 'required | date',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{

            try{
                $domain = Domain::find($id);
                foreach ($request->all() as $index => $update)
                {
                    $domain->$index = $update;
                }

                $domain->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Registro de domínio atualizado.',
                    'data' => $domain
                ], Response::HTTP_OK);
            }
            catch (\Exception $e) {

                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }

    public function getDomain($id)
    {
        $domain = Domain::with(['customer:id,name,email,uuid', 'price_domain', 'payment_cycle:id,name', 'price_domain.module'])->find($id);

        if($domain == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Este domínio não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        return $domain;
    }

    public function removeDomain($id)
    {
        try{
            $domain = Domain::query()->where(['account_id' => AccountId()])->find($id);
            if($domain == null)
            {
                return response()->json([
                    'error' => [
                        'message' => 'Este domínio não existe na base de dados.'
                    ]
                ], Response::HTTP_NOT_FOUND);
            }

            $domain->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Domínio removido com sucesso.'
            ]);

        }catch (\Exception $e){
            if(QueryException::class){

                ErrorReport(serialize($id), $e, __FUNCTION__, __FILE__, 500);

                return response()->json([
                    'error' => [
                        'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                    ]
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            return ['error' => $e->getMessage()];
        }
    }

    public function createPrice(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'extension' => [
                'required',
                Rule::unique('price_domains')->where(function ($query) {
                    $query->where(['account_id' => AccountId()]);
                }),
            ],
            'cost' => 'required | numeric',
            'price_register' => 'required | numeric',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{

            try{
                $request['account_id'] = AccountId();
                $price_domain = PriceDomain::create($request->all());

                return response()->json([
                    'status' => 'success',
                    'message' => 'Preço de domínio cadastrado com sucesso.',
                    'data' => $price_domain
                ], Response::HTTP_OK);

            }catch (\Exception $e){

                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }

    public function updatePrice($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'extension' => [
                'required',
                Rule::unique('price_domains')->where(function ($query) {
                    $query->where(['account_id' => AccountId()]);
                })->ignore($id),
            ],
            'cost' => 'required | numeric',
            'price_register' => 'required | numeric',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{

            try{
                $priceDomain = PriceDomain::find($id);
                foreach ($request->all() as $index => $update)
                {
                    $priceDomain->$index = $update;
                }

                $priceDomain->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Preço de domínio atualizado.',
                    'data' => $priceDomain
                ], Response::HTTP_OK);

            }catch (\Exception $e){

                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }

    public function getPrice($id)
    {
        $priceDomain = PriceDomain::with('module:id,name')->find($id);

        if($priceDomain == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Este preço de domínio não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        return $priceDomain;
    }

    public function removePrice($id)
    {
        try{
            $priceDomain = PriceDomain::query()->where(['account_id' => AccountId()])->find($id);
            if($priceDomain == null)
            {
                return response()->json([
                    'error' => [
                        'message' => 'Este preço de domínio não existe na base de dados.'
                    ]
                ], Response::HTTP_NOT_FOUND);
            }

            $priceDomain->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Preço de domínio removido com sucesso.'
            ]);

        }catch (\Exception $e){
            if(QueryException::class){

                ErrorReport(serialize($id), $e, __FUNCTION__, __FILE__, 500);

                return response()->json([
                    'error' => [
                        'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                    ]
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            return ['error' => $e->getMessage()];
        }
    }

    public function getAll_search($search)
    {
        return Domain::with([
                'price_domain.module:name,logo',
                'price_domain:id,price_register',
                'customer:id,name,email,uuid',
                'payment_cycle:id,name'
            ])
            ->whereHas('account', function ($q){
                $q->where('id', AccountId());
            })
            ->where('domain', 'LIKE', "%{$search}%")
            ->paginate(20);
    }

    public function whois($domain)
    {
        // Checking availability
        try {
            $search = Whois::create()->isDomainAvailable($domain);

            if($search)
            {
                return response()->json([
                    'domain' => $domain,
                    'availability' => $search
                ]);
            }else {
                $info = Whois::create()->loadDomainInfo($domain);

                return response()->json([
                    'domain' => $domain,
                    'availability' => $search,
                    'info' => [
                        'created_at' => date("d/m/Y", $info->getCreationDate()),
                        'expires' => date("d/m/Y", $info->getExpirationDate()),
                        'status' => $info->getStates()[0],
                        'owner' => $info->getOwner(),
                        'registrar' => $info->getRegistrar(),
                        'nameservers' => $info->getNameServers(),
                        'whois_server' => $info->getResponse()->getHost(),
                        'query' => $info->getResponse()->getText()
                    ]
                ]);
            }

        } catch (ConnectionException $e) {
            return response()->json([
                'error' => [
                    'message' => $e->getMessage()
                ]
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (ServerMismatchException $e) {
            return response()->json([
                'error' => [
                    'message' => $e->getMessage()
                ]
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (WhoisException $e) {
            return response()->json([
                'error' => [
                    'message' => $e->getMessage()
                ]
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}