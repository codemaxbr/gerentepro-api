<?php
/**
 * Created by PhpStorm.
 * User: rafaellessa
 * Date: 20/09/2018
 * Time: 14:33
 */

namespace App\Services;


use App\Models\Statement;
use App\Models\TypePayment;
use App\Repositories\StatementRepository;
use Firebase\JWT\JWT;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class StatementService
{
    private $statementRepository;


    /**
     * StatementService constructor.
     * @param $statementRepository
     */
    public function __construct(Statement $statementRepository)
    {
        $this->statementRepository = $statementRepository;
    }

    /**
     * @param $dados
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function createStatement($dados)
    {
        $statement =  Statement::create($dados);
        if ($statement)
        {
            try{
                return response()->json([
                    'status' => 'success',
                    'msg'   =>  'Uma nova transação foi criada com sucesso',
                    'data'  =>  $statement
                ], 201);

            }catch (\Exception $e)
            {
                if(QueryException::class){

                    ErrorReport(serialize($dados), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return ['error' => $e->getMessage()];
            }
        }
    }

    public function getSalesByPlan($first_day, $last_day)
    {
        return Statement::with('invoice', 'totalPedido')
            ->whereBetween('created_at', [$first_day, $last_day])
            ->where(['account_id' => AccountId()])
            ->groupBy('plan_id')
            ->get();
    }

    public function getStatementsCustomer($id)
    {
        $statemtents = DB::table('statements as s')
            ->where('s.customer_id', $id)
            ->get();

        return ($statemtents->isNotEmpty()) ? $statemtents : NULL;
    }

    public function getStatetementCustomerMes($id, $month, $year)
    {
        $statemtents = DB::table('statements as s')
            ->where('s.customer_id', $id)
            ->whereYear('created_at', '=', $year)
            ->whereMonth('created_at','=', $month)
            ->get();

        return ($statemtents->isNotEmpty()) ? $statemtents : NULL;
    }

    public function getTypes()
    {
        return TypePayment::query()->select('id', 'name')->get()->all();
    }

    public function totalGeral($first_day, $last_day)
    {
        $count = Statement::query()
            ->select(
                DB::raw('sum(total) as total'),
                DB::raw('count(id) as qty')
            )
            ->whereBetween('created_at', [$first_day, $last_day])
            ->whereIn('type_invoice_id', [3, 2])
            ->first();

        return $count;
    }

    public function getStatetementsMes($month, $year)
    {
        return Statement::with('customer:id,name,email,cpf_cnpj,type', 'invoice:id,total,uuid', 'type_payment', 'user:id,name,email', 'type_invoice')
            ->whereYear('created_at','=', $year)
            ->whereMonth('created_at', '=', $month)
            ->where(['account_id' => AccountId()])
            ->paginate(20);
    }

    public function getStatement($id)
    {
        $statement = Statement::with(['customer', 'invoice', 'type_payment', 'user', 'invoice.type_invoice'])
            ->where(['account_id' => AccountId()])
            ->find($id);

        if($statement == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Não existe este lançamento na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        return $statement;
    }

    public function removeStatement($id, Request $request)
    {
        $statement = Statement::find($id);
        $token = $request->bearerToken();
        $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);

        if($statement == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Não existe este lançamento na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        $statement->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Lançamento removido com sucesso.',
            'user' => $credentials->sub
        ]);
    }

    public function makeStatement(Request $request)
    {

    }
}