<?php
namespace App\Services;

use App\Models\Attachment;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Webpatser\Uuid\Uuid;

class AttachmentService
{
    public function getAttachments()
    {
        return Attachment::query()->where(['account_id' => AccountId()])->paginate(20);
    }

    public function getAttachment($uuid)
    {
        $attachment = Attachment::query()->where(['uuid' => $uuid, 'account_id' => AccountId()])->first();
        if($attachment == null){
            return response()->json([
                'error' => [
                    'message' => 'Este anexo / documento não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        return $attachment;
    }

    public function createAttachment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'    => 'required',
            'receipt' => 'required | file'
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{
            try{
                $request['account_id'] = AccountId();
                $request['uuid'] = Uuid::generate(4)->string;

                $anexo = $request->file('receipt');
                $imageFileName = 'doc_'.$request['uuid'].'.'.$anexo->getClientOriginalExtension();

                $s3 = Storage::disk('s3');
                if(!$s3->exists('/uploads/anexos')){
                    $s3->makeDirectory('/uploads/anexos/');
                }
                $filePath = '/uploads/anexos/'.$imageFileName;
                $s3->put($filePath, fopen($anexo, 'r+'), 'public');

                $attachment = Attachment::create([
                    'name' => $request->name,
                    'uuid' => $request->uuid,
                    'account_id' => $request->account_id,
                    'file_url' => env('S3_URL').$filePath
                ]);

                return response()->json([
                    'status' => 'success',
                    'message' => 'Anexo ou Documento enviado com sucesso.',
                    'data' => [
                        'attachment' => $attachment
                    ]
                ], Response::HTTP_OK);

            }catch (\Exception $e){

                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }

    public function updateAttachment($uuid, Request $request)
    {
        $attachment = Attachment::query()->where(['uuid' => $uuid, 'account_id' => AccountId()])->first();
        if($attachment == null){
            return response()->json([
                'error' => [
                    'message' => 'Este anexo / documento não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        $validator = Validator::make($request->all(), [
            'name'    => 'required',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{

            try{
                $attachment = Attachment::query()->where(['uuid' => $uuid, 'account_id' => AccountId()])->first();
                $attachment->name = $request->name;

                if($request->hasFile('receipt'))
                {
                    $anexo = $request->file('receipt');
                    $imageFileName = 'doc_'.$uuid.'.'.$anexo->getClientOriginalExtension();

                    $s3 = Storage::disk('s3');
                    $array = explode('/', $attachment->file_url);
                    $ext = explode('.', $array[6]);
                    $s3->delete('/uploads/anexos/'.'doc_'.$uuid.'.'.$ext[1]);

                    if(!$s3->exists('/uploads/anexos')){
                        $s3->makeDirectory('/uploads/anexos/');
                    }
                    $filePath = '/uploads/anexos/'.$imageFileName;
                    $s3->put($filePath, fopen($anexo, 'r+'), 'public');

                    $attachment->file_url = env('S3_URL').$filePath;
                }

                $attachment->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'O Anexo Nº '.$attachment->id.' foi atualizado.',
                    'data' => [
                        'attachment' => $attachment
                    ]
                ], Response::HTTP_OK);
            }catch (\Exception $e){

                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }

    public function deleteAttachment($uuid)
    {
        $attachment = Attachment::query()->where(['uuid' => $uuid, 'account_id' => AccountId()])->first();
        if($attachment == null){
            return response()->json([
                'error' => [
                    'message' => 'Este anexo / documento não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        if($attachment->file_url != null){
            $s3 = Storage::disk('s3');
            $array = explode('/', $attachment->file_url);
            $s3->delete('/uploads/anexos/'.$array[6]);
        }

        $attachment->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Anexo Nº '.$attachment->id.' foi removido.'
        ]);
    }

    public function getAll_search($search)
    {
        return Attachment::query()
            ->where(['account_id' => AccountId()])
            ->where('name', 'LIKE', "%{$search}%")
            ->paginate(20);
    }
}