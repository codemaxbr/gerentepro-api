<?php
namespace App\Services\Plugins;

use App\Models\AccountDetail;
use App\Models\PlanGrid;
use Carbon\Carbon;

class boletoService
{
    private $customer;
    private $invoice;
    private $cart;
    private $total;
    private $request;
    private $plugin;

    public function __construct($customer, $invoice, $cart, $total, $request, $plugin)
    {
        $this->customer = $customer;
        $this->invoice = $invoice;
        $this->cart = $cart;
        $this->total = $total;
        $this->request = $request;
        $this->plugin = $plugin;
    }

    public function registerBoleto()
    {
        $request    = $this->request;
        $invoice    = $this->invoice;
        $customer   = $this->customer;
        $checkout   = $this->cart;
        $config     = (object) $this->plugin->config;
        $items      = array();

        $empresa    = AccountDetail::query()->where(['account_id' => front_AccountId()])->firstOrFail();

        $beneficiario = new \Eduardokum\LaravelBoleto\Pessoa();
        $beneficiario->setDocumento($empresa->cpf_cnpj);
        $beneficiario->setNome($empresa->name);
        $beneficiario->setCep($empresa->zipcode);
        $beneficiario->setEndereco($empresa->address.', '.$empresa->number);
        $beneficiario->setBairro($empresa->district);
        $beneficiario->setCidade($empresa->city);
        $beneficiario->setUf($empresa->uf);;

        $pagador = new \Eduardokum\LaravelBoleto\Pessoa();
        $pagador->setDocumento($customer->cpf_cnpj);
        $pagador->setNome($customer->name);
        $pagador->setCep($customer->zipcode);
        $pagador->setEndereco($customer->address.', '.$customer->number);
        $pagador->setBairro($customer->district);
        $pagador->setUf($customer->uf);
        $pagador->setCidade($customer->city);

        foreach ($invoice->invoice_items as $cart) {
            switch ($cart->type) {
                case 'plan':
                    $grid = PlanGrid::query()->where(['plan_id' => $cart->plan_id])->first();
                    $description = $grid->grid->description . ' - ' . $grid->plan->name;
                    break;

                case 'domain':
                    $description = $cart->description;
                    break;

                case 'nulled':
                    $description = $cart->description;
                    break;
            }

            array_push($items, $description.' - R$ '.numFormat($cart->price));
        }

        $now = now()->addDays(3);
        $vencimento = \Carbon\Carbon::instance($now);

        //$vencimento = \Carbon\Carbon::now()->addDays(3)->format('Y-m-d');

        $banco_class = '\Eduardokum\LaravelBoleto\Boleto\Banco\\'.$config->banco;
        $boleto = new $banco_class;

        $boleto->setLogo('http://demo.gerentepro.com.br/uploads/logos/codemax.png');
        $boleto->setDataVencimento($vencimento);
        $boleto->setValor($invoice->total);
        $boleto->setNumero($invoice->id);
        $boleto->setNumeroDocumento($invoice->id);
        $boleto->setPagador($pagador);
        $boleto->setBeneficiario($beneficiario);
        $boleto->setCarteira($config->carteira);
        $boleto->setAgencia($config->agencia);
        $boleto->setConta($config->conta);
        $boleto->setDescricaoDemonstrativo($items);
        $boleto->setInstrucoes([$config->instrucao1, $config->instrucao2, $config->instrucao3]);

        return (object) [
            'status' => 'success',
            'type' => 'invoice',
            'transaction' => (object) [
                'gateway' => 'boleto',
                'type_payment_id'  => 1,
                'url' => url('/boleto/'.$invoice->uuid),
                'id'  => $invoice->id,
                'status' => 1,
                'data' => $boleto
            ]
        ];
    }
}