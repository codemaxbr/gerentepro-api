<?php

namespace App\Services\Plugins;
use App\Models\PlanGrid;
use Gerencianet\Exception\GerencianetException;
use Gerencianet\Gerencianet;

class gerencianetService
{
    private $customer;
    private $invoice;
    private $cart;
    private $total;
    private $request;
    private $plugin;

    /**
     * pagseguroService constructor.
     */
    public function __construct($customer, $invoice, $cart, $total, $request, $plugin)
    {
        $this->customer = $customer;
        $this->invoice = $invoice;
        $this->cart = $cart;
        $this->total = $total;
        $this->request = $request;
        $this->plugin = $plugin;
    }

    public function createCharge()
    {
        $request    = $this->request;
        $invoice    = $this->invoice;
        $customer   = $this->customer;
        $checkout   = $this->cart;
        $config     = (object) $this->plugin->config;
        $items      = array();

        $options = [
            'client_id' => $config->client_id,
            'client_secret' => $config->client_secret,
            'sandbox' => false // altere conforme o ambiente (true = desenvolvimento e false = producao)
        ];

        foreach ($invoice->invoice_items as $cart) {
            switch ($cart->type) {
                case 'plan':
                    $grid = PlanGrid::query()->where(['plan_id' => $cart->plan_id])->first();
                    $description = $grid->grid->description . ' - ' . $grid->plan->name;
                    break;

                case 'domain':
                    $description = $cart->description;
                    break;

                case 'nulled':
                    $description = $cart->description;
                    break;
            }

            array_push($items, [
                'name' => $description, // nome do item, produto ou serviço
                'amount' => 1, // quantidade
                'value' => intval(limpaNumeros(number_format($cart->price, 2))) // valor (2000 = R$ 20,00)
            ]);
        }

        $metadata = [
            'custom_id' => 'GPRO'.$invoice->id
        ];

        $body  =  [
            'items' => $items,
            'metadata' => $metadata
        ];

        try {
            $api = new Gerencianet($options);
            $charge = $api->createCharge([], $body);

            return $charge['data']['charge_id'];

        } catch (\Exception $e) {
            dump($e);
        }
    }

    public function registerBoleto()
    {
        $request    = $this->request;
        $invoice    = $this->invoice;
        $customer   = $this->customer;
        $checkout   = $this->cart;
        $config     = (object) $this->plugin->config;
        $charge_id  = $this->createCharge();

        $options = [
            'client_id' => $config->client_id,
            'client_secret' => $config->client_secret,
            'sandbox' => false // altere conforme o ambiente (true = desenvolvimento e false = producao)
        ];

        // $charge_id refere-se ao ID da transação gerada anteriormente
        $params = [
            'id' => $charge_id
        ];

        if($customer->type == "fisica") {
            $cliente = [
                'name' => $customer->name, // nome do cliente
                'cpf' => limpaNumeros($customer->cpf_cnpj) , // cpf válido do cliente
                'phone_number' => limpaNumeros($customer->phone) // telefone do cliente
            ];
        }else{
            $cliente = [
                'juridical_person' => [
                    'corporate_name' => $customer->name,
                    'cnpj' => limpaNumeros($customer->cpf_cnpj)
                ], // nome do cliente
                'phone_number' => limpaNumeros($customer->phone) // telefone do cliente
            ];
        }

        $bankingBillet = [
            'expire_at' => now()->addDays(3)->format('Y-m-d'), // data de vencimento do boleto (formato: YYYY-MM-DD)
            'customer' => $cliente
        ];

        $payment = [
            'banking_billet' => $bankingBillet // forma de pagamento (banking_billet = boleto)
        ];

        $body = [
            'payment' => $payment
        ];

        try {
            $api = new Gerencianet($options);
            $charge = $api->payCharge($params, $body);

            $charge = (object) $charge['data'];

            switch ($charge->status){
                case 'new':
                case 'waiting': $status_id = 1; break;
                case 'paid': $status_id = 5; break;
                case 'unpaid': $status_id = 2; break;
                case 'canceled': $status_id = 4; break;
            }

            /**
             * Tipos de pagamentos
             *
             * 1. Boleto Bancário
             * 2. Cartão de crédito
             * 3. Transferência bancária
             * 4. Dinheiro
             * 5. Débito Automático
             * 6. Paypal
             */

            return (object) [
                'status' => 'success',
                'type' => 'invoice',
                'transaction' => (object) [
                    'gateway' => 'gerencianet',
                    'type_payment_id'  => 1,
                    'url' => $charge->link,
                    'id'  => $charge->charge_id,
                    'status' => $status_id,
                    'data' => $charge
                ]
            ];

        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function registerCartao()
    {
        $request    = $this->request;
        $invoice    = $this->invoice;
        $customer   = $this->customer;
        $checkout   = $this->cart;
        $config     = (object) $this->plugin->config;

        $charge_id  = $this->createCharge();

        $options = [
            'client_id' => $config->client_id,
            'client_secret' => $config->client_secret,
            'sandbox' => false // altere conforme o ambiente (true = desenvolvimento e false = producao)
        ];

        // $charge_id refere-se ao ID da transação gerada anteriormente
        $params = [
            'id' => $charge_id
        ];

        if($customer->type == "fisica") {
            $cliente = [
                'email' => 'no-reply@gerentepro.com.br',
                'name' => $customer->name, // nome do cliente
                'cpf' => limpaNumeros($customer->cpf_cnpj) , // cpf válido do cliente
                'phone_number' => limpaNumeros($customer->phone), // telefone do cliente
                'birth' => '1990-05-20' // data de nascimento do cliente
            ];
        }else{
            $cliente = [
                'email' => 'no-reply@gerentepro.com.br',
                'juridical_person' => [
                    'corporate_name' => $customer->name,
                    'cnpj' => limpaNumeros($customer->cpf_cnpj)
                ], // nome do cliente
                'phone_number' => limpaNumeros($customer->phone), // telefone do cliente
                'birth' => '1990-05-20' // data de nascimento do cliente
            ];
        }

        $paymentToken = $request->card_token;

        $billingAddress = [
            'street' => $customer->address,
            'number' => $customer->number,
            'neighborhood' => $customer->district,
            'zipcode' => limpaNumeros($customer->zipcode),
            'city' => $customer->city,
            'state' => $customer->uf,
        ];

        $creditCard = [
            'installments' => 1, // número de parcelas em que o pagamento deve ser dividido
            'billing_address' => $billingAddress,
            'payment_token' => $paymentToken,
            'customer' => $cliente
        ];

        $payment = [
            'credit_card' => $creditCard // forma de pagamento (credit_card = cartão)
        ];

        $body = [
            'payment' => $payment
        ];

        try{
            $api = new Gerencianet($options);
            $charge = $api->payCharge($params, $body);

            $charge = (object) $charge['data'];

            switch ($charge->status){
                case 'new':
                case 'waiting': $status_id = 1; break;
                case 'paid': $status_id = 5; break;
                case 'unpaid': $status_id = 2; break;
                case 'canceled': $status_id = 4; break;
            }

            /**
             * Tipos de pagamentos
             *
             * 1. Boleto Bancário
             * 2. Cartão de crédito
             * 3. Transferência bancária
             * 4. Dinheiro
             * 5. Débito Automático
             * 6. Paypal
             */

            return (object) [
                'status' => 'success',
                'type' => 'invoice',
                'transaction' => (object) [
                    'gateway' => 'gerencianet',
                    'type_payment_id'  => 2,
                    'url' => null,
                    'brand' => $request->card_brand,
                    'card_number' => substr($request->card_number, -4),
                    'id'  => $charge->charge_id,
                    'status' => $status_id,
                    'data' => $charge
                ]
            ];
        }catch (\Exception $e){
            return $e;
        }
    }
}