<?php
namespace App\Services\Plugins;

use App\Models\PlanGrid;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class junoService
{
    private $url = "https://sandbox.boletobancario.com/boletofacil/integration/api/v1";
    private $customer;
    private $invoice;
    private $cart;
    private $total;
    private $request;
    private $plugin;

    public function __construct($customer = null, $invoice = null, $cart = null, $total = null, $request = null, $plugin = null)
    {
        $this->customer = $customer;
        $this->invoice = $invoice;
        $this->cart = $cart;
        $this->total = $total;
        $this->request = $request;
        $this->plugin = $plugin;
    }

    private function request($uri, $params = array())
    {
        try{
            $client = new Client();
            $response = $client->request('POST', $this->url.$uri, ['form_params' => $params]);

            return json_decode($response->getBody()->getContents());
        }catch (RequestException $e){
            if($e->hasResponse()){
                $http_code = $e->getResponse()->getStatusCode();
                $error = json_decode($e->getResponse()->getBody()->getContents());

                if($error->errorMessage == "Token inválido"){
                    throw new \Exception("Não autorizado", $http_code);
                }else{
                    return $error;
                }
            }
        }
    }

    public function checkConfig($token)
    {
        return $this->request('/fetch-balance', ['token' => $token]);
    }

    public function registerBoleto()
    {
        $request    = $this->request;
        $invoice    = $this->invoice;
        $customer   = $this->customer;
        $checkout   = $this->cart;
        $config     = (object) $this->plugin->config;
        $items      = array();
        $description = '';

        $cobranca = [
            'token' => $config->token,
            'description' => 'Fatura #'.$invoice->id,
            'reference' => 'GPRO'.$invoice->id,
            'amount' => number_format($invoice->total, 2),
            'dueDate' => now()->addDays(3)->format('d/m/Y'),
            'maxOverdueDays' => 10,
            'payerName' => $customer->name,
            'payerCpfCnpj' => $customer->cpf_cnpj,
            'payerEmail' => $customer->email,
            'notifyPayer' => false,
            //'notificationUrl' => '',
            'paymentTypes' => 'BOLETO',
        ];

        $juno = $this->request('/issue-charge', $cobranca);
        $response = $juno->data->charges[0];

        return (object) [
            'status' => 'success',
            'type' => 'invoice',
            'transaction' => (object) [
                'gateway' => 'juno',
                'type_payment_id'  => 1,
                'url' => $response->link,
                'id'  => $response->code,
                'status' => 1,
                'data' => $response
            ]
        ];
    }

    public function registerCartao()
    {
        $request    = $this->request;
        $invoice    = $this->invoice;
        $customer   = $this->customer;
        $checkout   = $this->cart;
        $config     = (object) $this->plugin->config;
        $items      = array();

        dd($request->all());
    }

}