<?php
/**
 * Created by PhpStorm.
 * User: codemaxbr
 * Date: 11/08/19
 * Time: 16:33
 */

namespace App\Services\Plugins;
use App\Models\Plan;
use App\Models\PlanGrid;
use Artistas\PagSeguro\PagSeguroConfig;
use Artistas\PagSeguro\PagSeguroException;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use \PagSeguro;

class pagseguroService
{
    private $customer;
    private $invoice;
    private $cart;
    private $total;
    private $request;
    private $plugin;

    /**
     * pagseguroService constructor.
     */
    public function __construct($customer, $invoice, $cart, $total, $request, $plugin)
    {
        $this->customer = $customer;
        $this->invoice = $invoice;
        $this->cart = $cart;
        $this->total = $total;
        $this->request = $request;
        $this->plugin = $plugin;

        $this->setConfig($plugin);
    }

    public function setConfig($config)
    {
        $config = (object) $config->config;
        $accountEmail = $config->email;
        $accountToken = $config->token;

        try {
            \PagSeguro\Library::initialize();
        } catch (\Exception $e) {
            die($e);
        }

        \PagSeguro\Library::initialize();
        \PagSeguro\Library::cmsVersion()->setName("Nome")->setRelease("1.0.0");
        \PagSeguro\Library::moduleVersion()->setName("Nome")->setRelease("1.0.0");

        \PagSeguro\Configuration\Configure::setEnvironment('production');//production or sandbox
        \PagSeguro\Configuration\Configure::setAccountCredentials(
            $accountEmail,
            $accountToken
        );

        \PagSeguro\Configuration\Configure::setCharset('UTF-8');// UTF-8 or ISO-8859-1
        \PagSeguro\Configuration\Configure::setLog(false, '/logpath/logFilename.log');
    }

    public function registerCartao()
    {
        $request    = $this->request;
        $invoice    = $this->invoice;
        $customer   = $this->customer;
        $checkout   = $this->cart;
        $config     = (object) $this->plugin->config;
        $items      = array();
        $accountEmail = $config->email;
        $accountToken = $config->token;

        $creditCard = new \PagSeguro\Domains\Requests\DirectPayment\CreditCard();
        $creditCard->setMode('default');
        $creditCard->setReceiverEmail($config->email);

        // Set the currency
        $creditCard->setCurrency("BRL");

        foreach ($invoice->invoice_items as $cart) {
            switch ($cart->type) {
                case 'plan':
                    $grid = PlanGrid::query()->where(['plan_id' => $cart->plan_id])->first();
                    $description = $grid->grid->description . ' - ' . $grid->plan->name;
                    break;

                case 'domain':
                    $description = $cart->description;
                    break;

                case 'nulled':
                    $description = $cart->description;
                    break;
            }

            $creditCard->addItems()->withParameters(
                ($cart->plan_id) ? $cart->plan_id : 1,
                $description,
                1,
                number_format($cart->price, 2)
            );
        }

        $creditCard->setReference('GPRO'.$invoice->id);
        $creditCard->setExtraAmount(0.00);

        $creditCard->setSender()->setName($customer->name);
        $creditCard->setSender()->setEmail($customer->email); //

        $creditCard->setSender()->setPhone()->withParameters(
            substr(limpaNumeros($customer->phone), 0, 2),
            substr(limpaNumeros($customer->phone), 2)
        );

        $tipo = ($customer->type == 'fisica') ? 'CPF' : 'CNPJ';
        $creditCard->setSender()->setDocument()->withParameters(
            $tipo,
            limpaNumeros($customer->cpf_cnpj)
        );

        $creditCard->setSender()->setHash($request->sender_hash);
        $creditCard->setToken($request->card_token);

        $creditCard->setInstallment()->withParameters(1, $invoice->total);

        $creditCard->setHolder()->setBirthdate('01/10/1979');
        $creditCard->setHolder()->setName($request->card_owner); // Equals in Credit Card

        // If your payment request don't need shipping information use:
        $creditCard->setShipping()->setAddressRequired()->withParameters('FALSE');

        $address = new PagSeguro\Domains\Address();
        $address->setCountry("BRA");
        $address->setState($customer->uf);
        $address->setDistrict($customer->district);
        $address->setComplement($customer->additional);
        $address->setPostalCode(limpaNumeros($customer->zipcode));
        $address->setNumber($customer->number);
        $address->setCity($customer->city);
        $address->setStreet($customer->address);

        $creditCard->setBilling()->setAddress()->instance($address);

        $creditCard->setHolder()->setPhone()->withParameters(
            substr(limpaNumeros($customer->phone), 0, 2),
            substr(limpaNumeros($customer->phone), 2)
        );

        $creditCard->setHolder()->setDocument()->withParameters(
            $tipo,
            limpaNumeros($customer->cpf_cnpj)
        );

        try{
            $result = $creditCard->register(
                \PagSeguro\Configuration\Configure::getAccountCredentials()
            );
            // You can use methods like getCode() to get the transaction code and getPaymentLink() for the Payment's URL.

            /**
             * Tipos de pagamentos
             *
             * 1. Boleto Bancário
             * 2. Cartão de crédito
             * 3. Transferência bancária
             * 4. Dinheiro
             * 5. Débito Automático
             * 6. Paypal
             */

            return (object) [
                'status' => 'success',
                'type' => 'invoice',
                'transaction' => (object) [
                    'gateway' => 'pagseguro',
                    'type_payment_id'  => 2,
                    'url' => null,
                    'brand' => $request->card_brand,
                    'card_number' => substr($request->card_number, -4),
                    'id'  => $result->getCode(),
                    'status' => $result->getStatus(),
                    'data' => $result
                ]
            ];
        }catch (\Exception $e) {
            dd($e);
        }
    }

    public function registerBoleto()
    {
        $request    = $this->request;
        $invoice    = $this->invoice;
        $customer   = $this->customer;
        $cart       = $this->cart;
        $config     = (object) $this->plugin->config;
        $items      = array();
        $accountEmail = $config->email;
        $accountToken = $config->token;

        $boleto = new \PagSeguro\Domains\Requests\DirectPayment\Boleto();
        $boleto->setMode('default');
        $boleto->setReceiverEmail($config->email);

        // Set the currency
        $boleto->setCurrency("BRL");

        foreach ($invoice->invoice_items as $cart) {
            switch ($cart->type) {
                case 'plan':
                    $grid = PlanGrid::query()->where(['plan_id' => $cart->plan_id])->first();
                    $description = $grid->grid->description . ' - ' . $grid->plan->name;
                    break;

                case 'domain':
                    $description = $cart->description;
                    break;

                case 'nulled':
                    $description = $cart->description;
                    break;
            }

            $boleto->addItems()->withParameters(
                ($cart->plan_id) ? $cart->plan_id : 1,
                $description,
                1,
                number_format($cart->price, 2)
            );
        }

        $boleto->setReference('GPRO'.$invoice->id);
        $boleto->setExtraAmount(0.00);

        $boleto->setSender()->setName($customer->name);
        $boleto->setSender()->setEmail($customer->email); // $customer->name

        $boleto->setSender()->setPhone()->withParameters(
            substr(limpaNumeros($customer->phone), 0, 2),
            substr(limpaNumeros($customer->phone), 2)
        );

        $tipo = ($customer->type == 'fisica') ? 'CPF' : 'CNPJ';
        $boleto->setSender()->setDocument()->withParameters(
            $tipo,
            limpaNumeros($customer->cpf_cnpj)
        );

        $boleto->setSender()->setHash($request->sender_hash);

        // If your payment request don't need shipping information use:
        $boleto->setShipping()->setAddressRequired()->withParameters('FALSE');

        try{
            $result = $boleto->register(
                \PagSeguro\Configuration\Configure::getAccountCredentials()
            );
            // You can use methods like getCode() to get the transaction code and getPaymentLink() for the Payment's URL.

            /**
             * Tipos de pagamentos
             *
             * 1. Boleto Bancário
             * 2. Cartão de crédito
             * 3. Transferência bancária
             * 4. Dinheiro
             * 5. Débito Automático
             * 6. Paypal
             */

            return (object) [
                'status' => 'success',
                'type' => 'invoice',
                'transaction' => (object) [
                    'gateway' => 'pagseguro',
                    'type_payment_id'  => 1,
                    'url' => $result->getPaymentLink(),
                    'id'  => $result->getCode(),
                    'status' => $result->getStatus(),
                    'data' => $result
                ]
            ];
        }catch (\Exception $e) {
            dd($e);
        }
        // =============================================================================================================
    }
}