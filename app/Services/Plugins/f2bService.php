<?php
namespace App\Services\Plugins;

use App\Models\PlanGrid;
use CodemaxBR\F2b\Facades\F2b;

class f2bService
{
    private $customer;
    private $invoice;
    private $cart;
    private $total;
    private $request;
    private $plugin;

    public function __construct($customer, $invoice, $cart, $total, $request, $plugin)
    {
        $this->customer = $customer;
        $this->invoice = $invoice;
        $this->cart = $cart;
        $this->total = $total;
        $this->request = $request;
        $this->plugin = $plugin;

        $this->setConfig($this->plugin);
    }

    private function setConfig($config)
    {
        $config = (object) $this->plugin->config;
        F2b::setCredentials($config->token);
    }

    public function registerBoleto()
    {
        $request    = $this->request;
        $invoice    = $this->invoice;
        $customer   = $this->customer;
        $checkout   = $this->cart;
        $config     = (object) $this->plugin->config;
        $items      = array();

        foreach ($invoice->invoice_items as $cart) {
            switch ($cart->type) {
                case 'plan':
                    $grid = PlanGrid::query()->where(['plan_id' => $cart->plan_id])->first();
                    $description = $grid->grid->description . ' - ' . $grid->plan->name;
                    break;

                case 'domain':
                    $description = $cart->description;
                    break;

                case 'nulled':
                    $description = $cart->description;
                    break;
            }

            array_push($items, (object) [
                '@type' => "item",
                'texto_demonstrativo' => $description,
                'qtde_item' => intval(1),
                'valor_item' => number_format($cart->price, 2)
            ]);
        }

        $sacado = [
            'nome'      => $customer->name,
            'email'     => $customer->email,
            'logradouro_endereco'   => $customer->address,
            'numero_endereco'       => $customer->number,
            'complemento_endereco'  => $customer->additional,
            'bairro'    => $customer->district,
            'cidade'    => $customer->city,
            'estado'    => $customer->uf,
            'cep'       => $customer->zipcode,
            'ddd_celuar'        => substr(limpaNumeros($customer->phone), 0, 2),
            'number_celular'    => substr(limpaNumeros($customer->phone), 2),
            'observacao'        => "Cadastrado pelo GerentePRO",
            'tipo_envio'        => "N"
        ];

        if($customer->type == "fisica"){
            $sacado['cpf'] = limpaNumeros($customer->cpf_cnpj);
        }else{
            $sacado['cnpj'] = limpaNumeros($customer->cpf_cnpj);
        }

        $cobranca = [
            'objeto_sacado'     => [$sacado],
            'valor_cobranca'    => number_format($invoice->total, 2),
            'data_vencimento'   => now()->addDays(3)->format('d/m/Y'),
            'demonstrativo'     => $items,
            'tipo_cobranca'     => 'B',
            'tipo_ws'           => 'GPRO'.$invoice->id,
            'maximo_dias_pagamento' => 15
        ];

        $f2b = F2b::cobrancas()->registrar($cobranca);
        $f2b = $f2b[0];

        return (object) [
            'status' => 'success',
            'type' => 'invoice',
            'transaction' => (object) [
                'gateway' => 'f2b',
                'type_payment_id'  => 1,
                'url' => $f2b->url_cobranca,
                'id'  => $f2b->numero_cobranca,
                'status' => 1,
                'data' => $f2b
            ]
        ];
    }
}