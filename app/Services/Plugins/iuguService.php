<?php
namespace App\Services\Plugins;

use App\Models\PlanGrid;
use CodemaxBR\Iugu\Facades\Iugu;

class iuguService
{
    private $customer;
    private $invoice;
    private $cart;
    private $total;
    private $request;
    private $plugin;

    public function __construct($customer, $invoice, $cart, $total, $request, $plugin)
    {
        $this->customer = $customer;
        $this->invoice = $invoice;
        $this->cart = $cart;
        $this->total = $total;
        $this->request = $request;
        $this->plugin = $plugin;

        $this->setConfig($this->plugin);
    }

    private function setConfig($config)
    {
        $config = (object) $this->plugin->config;
        Iugu::setCredentials($config->token, $config->account_id);
    }

    public function registerBoleto()
    {
        $request    = $this->request;
        $invoice    = $this->invoice;
        $customer   = $this->customer;
        $checkout   = $this->cart;
        $config     = (object) $this->plugin->config;
        $items      = array();

        foreach ($invoice->invoice_items as $cart) {
            switch ($cart->type) {
                case 'plan':
                    $grid = PlanGrid::query()->where(['plan_id' => $cart->plan_id])->first();
                    $description = $grid->grid->description . ' - ' . $grid->plan->name;
                    break;

                case 'domain':
                    $description = $cart->description;
                    break;

                case 'nulled':
                    $description = $cart->description;
                    break;
            }

            array_push($items, (object) [
                'description' => $description, // nome do item, produto ou serviço
                'quantity' => intval(1), // quantidade
                'price_cents' => intval(limpaNumeros(number_format($cart->price, 2))) // valor (2000 = R$ 20,00)
            ]);
        }

        $endereco = [
            'street'        => $customer->address,
            'number'        => $customer->number,
            'district'      => $customer->district,
            'city'          => $customer->city,
            'state'         => $customer->uf,
            'zip_code'      => limpaNumeros($customer->zipcode),
            'complement'    => $customer->additional
        ];

        $comprador = [
            'name'          => $customer->name,
            'cpf_cnpj'      => $customer->cpf_cnpj,
            'phone_prefix'  => substr(limpaNumeros($customer->phone), 0, 2),
            'phone'         => substr(limpaNumeros($customer->phone), 2),
            'email'         => $customer->email,
            'address'       => $endereco
        ];

        $iugu = Iugu::charge()->boleto([
            'method'    => 'bank_slip',
            'email'     => $customer->email,
            'items'     => $items,
            'payer'     => $comprador,
            'order_id'  => 'GPRO'.$invoice->id,
            'bank_slip_extra_days' => 3,
        ]);

        return (object) [
            'status' => 'success',
            'type' => 'invoice',
            'transaction' => (object) [
                'gateway' => 'iugu',
                'type_payment_id'  => 1,
                'url' => $iugu->url,
                'id'  => $iugu->invoice_id,
                'status' => 1,
                'data' => $iugu
            ]
        ];
    }

    public function registerCartao()
    {
        $request    = $this->request;
        $invoice    = $this->invoice;
        $customer   = $this->customer;
        $checkout   = $this->cart;
        $config     = (object) $this->plugin->config;
        $items      = array();

        foreach ($invoice->invoice_items as $cart) {
            switch ($cart->type) {
                case 'plan':
                    $grid = PlanGrid::query()->where(['plan_id' => $cart->plan_id])->first();
                    $description = $grid->grid->description . ' - ' . $grid->plan->name;
                    break;

                case 'domain':
                    $description = $cart->description;
                    break;

                case 'nulled':
                    $description = $cart->description;
                    break;
            }

            array_push($items, (object) [
                'description' => $description, // nome do item, produto ou serviço
                'quantity' => intval(1), // quantidade
                'price_cents' => intval(limpaNumeros(number_format($cart->price, 2))) // valor (2000 = R$ 20,00)
            ]);
        }

        $endereco = [
            'street'        => $customer->address,
            'number'        => $customer->number,
            'district'      => $customer->district,
            'city'          => $customer->city,
            'state'         => $customer->uf,
            'zip_code'      => limpaNumeros($customer->zipcode),
            'complement'    => $customer->additional
        ];

        $comprador = [
            'name'          => $customer->name,
            'cpf_cnpj'      => $customer->cpf_cnpj,
            'phone_prefix'  => substr(limpaNumeros($customer->phone), 0, 2),
            'phone'         => substr(limpaNumeros($customer->phone), 2),
            'email'         => $customer->email,
            'address'       => $endereco
        ];

        $iugu = Iugu::charge()->boleto([
            'token'     => $request->card_token,
            'email'     => $customer->email,
            'items'     => $items,
            'payer'     => $comprador,
            'order_id'  => 'GPRO'.$invoice->id,
            'bank_slip_extra_days' => 3,
        ]);

        $status_id = 1;
        if($iugu->message == "Autorizado"){
            $status_id = 5;

            $invoice->status_id = 5;
            $invoice->save();
        }

        return (object) [
            'status' => 'success',
            'type' => 'invoice',
            'transaction' => (object) [
                'gateway' => 'iugu',
                'type_payment_id'  => 2,
                'brand' => $request->card_brand,
                'card_number' => substr($request->card_number, -4),
                'url' => $iugu->url,
                'id'  => $iugu->invoice_id,
                'status' => $status_id,
                'data' => $iugu
            ]
        ];
    }
}