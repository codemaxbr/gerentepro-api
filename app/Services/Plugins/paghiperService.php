<?php

namespace App\Services\Plugins;

use App\Models\PlanGrid;

class paghiperService
{
    private $customer;
    private $invoice;
    private $cart;
    private $total;
    private $request;
    private $plugin;

    private $apiKey;
    private $token;

    public function __construct($customer, $invoice, $cart, $total, $request, $plugin)
    {
        $this->customer = $customer;
        $this->invoice = $invoice;
        $this->cart = $cart;
        $this->total = $total;
        $this->request = $request;
        $this->plugin = $plugin;
    }

    public static function checkConfig($token, $apiKey)
    {
        $data_post = [
            'token'     => $token,
            'apiKey'    => $apiKey,
        ];

        $headers = array();
        $headers[] = "Accept: application/json";
        $headers[] = "Accept-Charset: UTF-8";
        $headers[] = "Accept-Encoding: application/json";
        $headers[] = "Content-Type: application/json;charset=UTF-8";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.paghiper.com/bank_accounts/list/");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data_post));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);

        $json = json_decode($result, true);

        if($json['bank_account_list_request']['result'] != "success"){
            throw new \Exception("token ou apiKey inválidos");
        }

        return true;
    }

    public function registerBoleto()
    {
        $request    = $this->request;
        $invoice    = $this->invoice;
        $customer   = $this->customer;
        $checkout   = $this->cart;
        $config     = (object) $this->plugin->config;
        $items      = array();

        foreach ($invoice->invoice_items as $cart) {
            switch ($cart->type) {
                case 'plan':
                    $grid = PlanGrid::query()->where(['plan_id' => $cart->plan_id])->first();
                    $description = $grid->grid->description . ' - ' . $grid->plan->name;
                    break;

                case 'domain':
                    $description = $cart->description;
                    break;

                case 'nulled':
                    $description = $cart->description;
                    break;
            }

            array_push($items, [
                'description' => removeSpecial($description), // nome do item, produto ou serviço
                'quantity' => '1', // quantidade
                'item_id' => ($cart->plan_id) ? $cart->plan_id : 1,
                'price_cents' => intval(limpaNumeros(number_format($cart->price, 2))) // valor (2000 = R$ 20,00)
            ]);
        }

        $boleto = [
            'apiKey'            => $config->apiKey,
            'order_id'          => 'GPRO'.$invoice->id,
            'payer_email'       => $customer->email,
            'payer_name'        => $customer->name,
            'payer_cpf_cnpj'    => $customer->cpf_cnpj,
            'payer_phone'       => limpaNumeros($customer->phone),
            'payer_street'      => $customer->address,
            'payer_number'      => $customer->number,
            'payer_complement'  => $customer->additional,
            'payer_district'    => $customer->district,
            'payer_city'        => $customer->city,
            'payer_state'       => $customer->uf,
            'payer_zip_code'    => $customer->zipcode,
            'days_due_date'     => 3,
            'type_bank_slip'    => 'boletoA4',
            'notification_url'  => '', // todo colocar a URL de retorno automático
            'items'             => $items
        ];

        $headers = array();
        $headers[] = "Accept: application/json";
        $headers[] = "Accept-Charset: UTF-8";
        $headers[] = "Accept-Encoding: application/json";
        $headers[] = "Content-Type: application/json;charset=UTF-8";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://api.paghiper.com/transaction/create/");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($boleto));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);

        $json = json_decode($result);
        $response = $json->create_request;

        return (object) [
            'status' => 'success',
            'type' => 'invoice',
            'transaction' => (object) [
                'gateway' => 'paghiper',
                'type_payment_id'  => 1,
                'url' => $response->bank_slip->url_slip,
                'id'  => $response->transaction_id,
                'status' => 1,
                'data' => $response
            ]
        ];
    }
}