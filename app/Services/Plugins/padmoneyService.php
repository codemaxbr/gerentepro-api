<?php
namespace App\Services\Plugins;

use CodemaxBR\Padmoney\Facades\Padmoney;

class padmoneyService
{
    private $customer;
    private $invoice;
    private $cart;
    private $total;
    private $request;
    private $plugin;

    public function __construct($customer, $invoice, $cart, $total, $request, $plugin)
    {
        $this->customer = $customer;
        $this->invoice = $invoice;
        $this->cart = $cart;
        $this->total = $total;
        $this->request = $request;
        $this->plugin = $plugin;

        $this->setConfig($this->plugin);
    }

    private function setConfig($config)
    {
        $config = (object) $this->plugin->config;
        Padmoney::setCredentials($config->token, $config->chave);
    }

    public function registerBoleto()
    {
        $request = $this->request;
        $invoice = $this->invoice;
        $customer = $this->customer;
        $checkout = $this->cart;
        $config = (object)$this->plugin->config;
        $items = array();

        $type_person = '';
        if($customer->type == "fisica"){
            $type_person = 'individual';
        }else{
            $type_person = 'juridical';
        }

        $padmoney = Padmoney::boletos()->create([
            'amount' => $invoice->total,
            'due_date' => now()->addDays(3)->format('Y-m-d'),
            'customer_document_number' => (string) $invoice->id,
            'description' => 'Fatura #'.$invoice->id,
            'payer' => [
                'person_type' => $type_person,
                'name' => $customer->name,
                'cnpj_cpf' => limpaNumeros($customer->cpf_cnpj),
                'email' => $customer->email,
                'phone_number' => $customer->phone,
                'zipcode' => limpaNumeros($request->sender_zipcode),
                'address' => $request->sender_address,
                'neighborhood' => $request->sender_district,
                'address_number' => $request->sender_number,
                'address_complement' => $request->sender_additional,
                'city_name' => $request->sender_city,
                'state' => $request->sender_uf
            ]
        ]);

        $padmoney = $padmoney[0];

        return (object) [
            'status' => 'success',
            'type' => 'invoice',
            'transaction' => (object) [
                'gateway' => 'padmoney',
                'type_payment_id'  => 1,
                'url' => $padmoney->url,
                'id'  => $padmoney->id,
                'status' => 1,
                'data' => $padmoney
            ]
        ];
    }
}