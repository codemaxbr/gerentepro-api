<?php
/**
 * Created by PhpStorm.
 * User: codemaxbr
 * Date: 26/04/19
 * Time: 01:53
 */

namespace App\Services\Plugins;


use Illuminate\Support\Facades\Validator;

class NfeService
{
    protected $apiKey = 'wLl3jqTLzuVl2ltOpiehkkmETsS6W18CLoZV4wNsqYD7aWvlqKlhJXeMVVsHMwIOhD1';

    /**
     * @var string https://naturalperson.api.nfe.io/v1/naturalperson/status/{0000000000}/{yyyy-mm-dd}?apikey=$apiKey
     */
    protected $host_cpf = 'https://naturalperson.api.nfe.io/v1/naturalperson';

    /**
     * @var string https://legalentity.api.nfe.io/v1/legalentities/basicInfo/{000000000000000}?apikey=$apiKey
     */
    protected $host_cnpj = 'https://legalentity.api.nfe.io/v1/legalentities';

    /**
     * NfeService constructor.
     */
    public function __construct()
    {

    }

    public function consultaCPF($cpf, $data_nascimento)
    {
        //Configuracao do cabecalho da requisicao
        $headers = array();
        $headers[] = "Accept: " . "application/json";
        $headers[] = "Accept-Charset: " . "UTF-8";
        $headers[] = "Accept-Encoding: " . "*";
        $headers[] = "Content-Type: " . "application/json" . ";charset=" . "UTF-8";

        $ch = curl_init();

        $url = $this->host_cpf.'/status/'.$cpf.'/'.$data_nascimento.'?apikey='.$this->apiKey;

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


        return json_decode(curl_exec($ch));
    }

    public function consultaCNPJ($cnpj)
    {
        //Configuracao do cabecalho da requisicao
        $headers = array();
        $headers[] = "Accept: " . "application/json";
        $headers[] = "Accept-Charset: " . "UTF-8";
        $headers[] = "Accept-Encoding: " . "*";
        $headers[] = "Content-Type: " . "application/json" . ";charset=" . "UTF-8";

        $ch = curl_init();

        $url = $this->host_cnpj.'/basicInfo/'.$cnpj.'?apikey='.$this->apiKey;

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


        return json_decode(curl_exec($ch));
    }
}