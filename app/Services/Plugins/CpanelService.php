<?php
/**
 * Created by PhpStorm.
 * User: Lucas Maia
 * Date: 18/10/2018
 * Time: 02:28
 */
namespace App\Services\Plugins;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Validator;

class CpanelService
{
    private $plan;
    private $server;
    private $params;

    /**
     * CpanelService constructor.
     */
    public function __construct($plan, $server, $config_server, $params)
    {
        $this->config_server = $config_server;
        $this->plan = $plan;
        $this->server = $server;
        $this->params = $params;
    }

    public function createAccount()
    {
        $config_server = (object)unserialize($this->server->config);
        $cpanel = new \cPanel\API($this->server->ip, $this->config_server->username, $this->config_server->hash);

        $item = $this->params;
        $customer = $item->invoice->customer_id;

        $response = $cpanel->createAccount([
            'plan' => $this->config_server->username . '_' . limpaChars($this->plan->name),
            'domain' => $item->domain,
            'user' => trim(substr(limpaNumeros($item->domain), 0, 8)),
            'password' => 'himura08,123'
        ]);

        if ($response->status == 1) {
            return true;
        }else{
            return $response;
        }
    }

    public function suspendAccount($reason = null)
    {
        $config_server = (object)unserialize($this->server->config);
        $cpanel = new \cPanel\API($this->server->ip, $config_server->user_cpanel, $config_server->hash_cpanel);

        $response = $cpanel->suspendAccount([
            'user' => trim(substr(limpaNumeros($this->params->domain), 0, 16)),
            'reason' => (!is_null($reason)) ? $reason : 'Bloqueio manual via GerentePRO.'
        ]);

        if ($response->status == 1) {
            return true;
        }else{
            return $response;
        }
    }

    public function unsuspendAccount()
    {
        try{
            $config_server = (object)unserialize($this->server->config);
            $cpanel = new \cPanel\API($this->server->ip, $config_server->user_cpanel, $config_server->hash_cpanel);

            $response = $cpanel->unsuspendAccount(trim(substr(limpaNumeros($this->params->domain), 0, 16)));

            if ($response->status == 1) {
                return (object)[
                    'status' => true,
                    'domain' => $this->params->domain,
                    'user' => trim(substr(limpaNumeros($this->params->domain), 0, 16)),
                ];
            }else{
                return $response;
            }

        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function terminateAccount()
    {
        try{
            $config_server = (object) unserialize($this->server->config);
            $cpanel = new \cPanel\API($this->server->ip, $config_server->user_cpanel, $config_server->hash_cpanel);

            $username = trim(substr(limpaNumeros($this->params->domain), 0, 16));
            $plan = limpaChars($this->plan->name);

            $response = $cpanel->terminateAccount($username);
            if ($response->status == 1) {
                return (object)[
                    'status' => true,
                    'domain' => $this->params->domain,
                    'user' => trim(substr(limpaNumeros($this->params->domain), 0, 16)),
                ];
            }else{
                return $response;
            }

        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function changePackage(){
        try{
            $config_server = (object) unserialize($this->server->config);
            $cpanel = new \cPanel\API($this->server->ip, $config_server->user_cpanel, $config_server->hash_cpanel);

            $username = trim(substr(limpaNumeros($this->params->domain), 0, 16));
            $plan = limpaChars($this->plan->name);

            $response = $cpanel->changePackage($username, $plan);
            if ($response->status == 1) {
                return (object)[
                    'status' => true,
                    'domain' => $this->params->domain,
                    'user' => trim(substr(limpaNumeros($this->params->domain), 0, 16)),
                    'new_plan' => $this->plan,
                    'old_plan' => $this->params->plan
                ];
            }else{
                return $response;
            }

        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function makePlan()
    {
        $this->params = (array) $this->params;
        $validator = Validator::make($this->params, [
            'disk_limit' => 'required | numeric',
            'bw_limit' => 'required | numeric',
            'emails' => 'required | integer',
            'databases' => 'required | integer',
            'domains' => 'required | integer',
            'parks' => 'required | integer',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{

            try{

                $cpanel = new \cPanel\API($this->server->ip, $this->config_server->username, $this->config_server->hash);
                if($cpanel->checkConnection())
                {
                    return $cpanel->addPackage([
                        'name' => limpaChars($this->plan->name),
                        'disk' => $this->params['disk_limit'],
                        'bwlimit' => $this->params['bw_limit'],
                        'maxpop' => $this->params['emails'],
                        'maxsql' => $this->params['databases'],
                        'maxaddon' => $this->params['domains'],
                        'maxpark' => $this->params['parks']
                    ]);
                }

            }catch (\Exception $e)
            {
                return $e->getMessage();
            }
        }
    }

    public function updatePlan()
    {
        $this->params = (array) $this->params;
        $validator = Validator::make($this->params, [
            'disk_limit' => 'required | numeric',
            'bw_limit' => 'required | numeric',
            'emails' => 'required | integer',
            'databases' => 'required | integer',
            'domains' => 'required | integer',
            'parks' => 'required | integer',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{

            try{

                $cpanel = new \cPanel\API($this->server->ip, $this->config_server->username, $this->config_server->hash);
                if($cpanel->checkConnection())
                {
                    return $cpanel->editPackage([
                        'name' => limpaChars($this->plan->name),
                        'disk' => $this->params['disk_limit'],
                        'bwlimit' => $this->params['bw_limit'],
                        'maxpop' => $this->params['emails'],
                        'maxsql' => $this->params['databases'],
                        'maxaddon' => $this->params['domains'],
                        'maxpark' => $this->params['parks']
                    ]);
                }

            }catch (\Exception $e)
            {
                return $e->getMessage();
            }
        }
    }

    public function deletePlan()
    {
        try{
            $cpanel = new \cPanel\API($this->server->ip, $this->config_server->username, $this->config_server->hash);
            if($cpanel->checkConnection())
            {
                return $cpanel->deletePackage(limpaChars($this->plan->name));
            }
        }catch (\Exception $e)
        {
            return $e->getMessage();
        }
    }
}