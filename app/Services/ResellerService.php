<?php
namespace App\Services;

use App\Models\Account;
use App\Models\Reseller;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ResellerService
{
    public function getResellers()
    {
        return Reseller::query()->withCount(['accounts'])->paginate(20);
    }

    public function getReseller($id)
    {
        $reseller = Reseller::find($id);

        if($reseller == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Este revendedor não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        return Account::query()->whereHas('reseller', function ($q) use ($id){
            $q->where('id', $id);
        })->paginate(20);


    }

    public function removeReseller($id)
    {
        $reseller = Reseller::find($id);

        if($reseller == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Este revendedor não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }else{

            try{
                Account::query()->whereHas('reseller', function ($q) use ($id){
                    $q->where('id', $id);
                })->update(['reseller_id' => 1]);

                $reseller->delete();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Revendedor removido com sucesso!'
                ]);
            }catch (\Exception $e){

                if(QueryException::class){
                    ErrorReport(serialize($id), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }

        }
    }

    public function assignReseller(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'account_id' => 'required | integer | exists:accounts,id',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{

            try{
                $account = Account::find($request->account_id);
                $reseller = Reseller::create(['name' => $account->name_business]);

                $account->reseller_id = $reseller->id;
                $account->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'A conta "'.$account->name_business.'" foi atribuída como Revendedor.',
                    'data' => $account,
                ], Response::HTTP_OK);

            }catch (\Exception $e){
                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }
}