<?php

namespace App\Services;

use App\Models\Module;
use App\Models\Server;
use App\Models\UptimeKey;
use HttpException;
use HttpRequest;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use UptimeRobot\UptimeRobot;
use Webpatser\Uuid\Uuid;

class ServerService
{
    private $account_id;

    /**
     * ServerService constructor.
     */
    public function __construct()
    {

    }

    public function createServer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'provider_id' => 'required | integer | exists:providers,id',
            'ip' => 'required | string',
            'cost' => 'required | numeric | between:0,99.99',
            'type_server_id' => 'required | integer | exists:type_servers,id'
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{
            try{
                $request['account_id'] = AccountId();
                $request['uuid'] = Uuid::generate(4)->string;

                $uptime_key = UptimeKey::query()->where('used', '<=', 50)->first();
                $find = Server::query()->where('ip', '=', $request->ip)->where('monitor', '!=', null)->first();

                if($find == null)
                {
                    $uptime_key->api_key;
                    $upRobot = json_decode($this->newMonitor(['name' => $request->name, 'host' => $request->ip], $uptime_key->api_key));
                    $monitor = $upRobot->monitor->id;

                }else{
                    $monitor = $find->monitor;
                }

                $request['monitor'] = $monitor;
                $server = Server::create($request->all());

                return response()->json([
                    'status' => 'success',
                    'message' => 'Servidor vinculado com sucesso.',
                    'data' => $server
                ], Response::HTTP_OK);

            }catch (\Exception $e){

                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }

    public function allServers()
    {
        return Server::with(['provider', 'modules_accounts', 'type_server'])->where(['account_id' => AccountId()])->paginate(20);
    }

    public function getAll_search($search)
    {
        return Server::with(['module', 'provider', 'modules_accounts'])
            ->whereHas('account', function ($q){
                $q->where('id', AccountId());
            })
            ->where('name', 'LIKE', "%{$search}%")
            ->paginate(20);
    }

    public function updateServer($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'provider_id' => 'required | integer | exists:providers,id',
            'ip' => 'required | ip',
            'cost' => 'required | numeric | between:0,99.99',
            'module_id' => 'required | integer | exists:modules,id',
            'config' => 'required'
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{

            try{
                $server = Server::find($id);
                $request['config'] = serialize($request->config);
                foreach ($request->all() as $index => $update)
                {
                    $server->$index = $update;
                }

                $server->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Servidor atualizado.',
                    'data' => $server
                ], Response::HTTP_OK);

            }catch (\Exception $e){

                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }

    public function getServer($id)
    {
        $server = Server::with(['module', 'provider', 'modules_accounts'])->find($id);

        if($server == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Este servidor não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        return $server;
    }

    public function removeServer($id)
    {
        try{
            $server = Server::query()->where(['account_id' => AccountId()])->find($id);
            if($server == null)
            {
                return response()->json([
                    'error' => [
                        'message' => 'Este servidor não existe na base de dados.'
                    ]
                ], Response::HTTP_NOT_FOUND);
            }

            $server->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Servidor removido com sucesso.'
            ]);

        }catch (\Exception $e){
            if(QueryException::class){

                ErrorReport(serialize($id), $e, __FUNCTION__, __FILE__, 500);

                return response()->json([
                    'error' => [
                        'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                    ]
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            return ['error' => $e->getMessage()];
        }
    }

    public function newMonitor($params, $api_key)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.uptimerobot.com/v2/newMonitor",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "api_key=".$api_key."&format=json&type=1&url=".$params['host']."&friendly_name=".$params['name']."",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            $uptime_key = UptimeKey::query()->where(['api_key' => $api_key])->first();
            $uptime_key->used = $uptime_key->used + 1;
            $uptime_key->save();
            return $response;
        }
    }
}