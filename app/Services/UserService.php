<?php
namespace App\Services;

use App\Events\UserInvited;
use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserService
{
    /**
     * UserService constructor.
     */
    public function __construct()
    {

    }

    public function getUser($id)
    {
        $user = User::query()->where(['account_id' => AccountId()])->find($id);

        if($user == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Este usuário não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        return $user;
    }

    public function getUsers()
    {
        return User::query()->where(['account_id' => AccountId()])->paginate(20);
    }

    public function updateUser($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($id),
            ],
            'name' => 'required',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{

            try{
                $user = User::find($id);
                foreach ($request->all() as $index => $update)
                {
                    $user->$index = $update;
                }

                $user->save();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Usuário atualizado com sucesso.',
                    'data' => $user
                ], Response::HTTP_OK);
            }
            catch (\Exception $e) {

                if(QueryException::class){

                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }

    public function getAll_search($search)
    {
        return User::query()
            ->whereHas('account', function ($q){
                $q->where('id', AccountId());
            })
            ->where('name', 'LIKE', "%{$search}%")
            ->paginate(20);
    }

    public function createUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => [
                'required',
                'email',
                Rule::unique('users')->where(function ($query) {
                    $query->where(['account_id' => AccountId()]);
                })
            ],
            'name' => 'required',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{
            try{
                $request['account_id'] = AccountId();
                $code = randomDigit(8);
                $request['password'] = Hash::make($code);
                $request['token'] = str_random(25);
                $user = User::create($request->all());

                Event::dispatch(new UserInvited($user, $code));

                return response()->json([
                    'status' => 'success',
                    'message' => 'Usuário cadastrado com sucesso.',
                    'data' => $user
                ], Response::HTTP_OK);

            }catch (\Exception $e){

                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }

    public function removeUser($id)
    {
        try{
            $user = User::query()->where(['account_id' => AccountId()])->find($id);
            if($user == null)
            {
                return response()->json([
                    'error' => [
                        'message' => 'Este usuário não existe na base de dados.'
                    ]
                ], Response::HTTP_NOT_FOUND);
            }

            $user->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Usuário removido com sucesso.'
            ]);

        }catch (\Exception $e){
            if(QueryException::class){

                ErrorReport(serialize($id), $e, __FUNCTION__, __FILE__, 500);

                return response()->json([
                    'error' => [
                        'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                    ]
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            return ['error' => $e->getMessage()];
        }
    }
}