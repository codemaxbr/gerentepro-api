<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TemplateEmail
 * 
 * @property int $id
 * @property int $email_id
 * @property string $subject
 * @property string $message
 * @property int $account_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Account $account
 * @property \App\Models\Email $email
 *
 * @package App\Models
 */
class TemplateEmail extends Eloquent
{
	protected $casts = [
		'email_id' => 'int',
		'account_id' => 'int'
	];

	protected $fillable = [
		'email_id',
		'subject',
		'message',
		'account_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function email()
	{
		return $this->belongsTo(\App\Models\Email::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
