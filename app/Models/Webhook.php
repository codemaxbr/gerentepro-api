<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Webhook extends Model
{
    public $timestamps = ['update_at', 'created_at'];

    protected $fillable = [
        'account_id',
        'user_id',
        'name',
        'listener',
        'model',
        'url',
        'http',
        'payload',
    ];

    public function account()
    {
        return $this->belongsTo(\App\Models\Account::class);
    }

    public function event()
    {
        return new $this->listener;
    }

    public function user()
    {
return $this->belongsTo(\App\Models\User::class);
}
}
