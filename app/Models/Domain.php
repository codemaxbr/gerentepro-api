<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Domain
 * 
 * @property int $id
 * @property string $domain
 * @property int $customer_id
 * @property int $price_domain_id
 * @property string $ns1
 * @property string $ns2
 * @property string $ns3
 * @property string $ns4
 * @property string $ip1
 * @property string $ip2
 * @property string $ip3
 * @property string $ip4
 * @property \Carbon\Carbon $due
 * @property int $payment_cycle_id
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $account_id
 * 
 * @property \App\Models\Account $account
 * @property \App\Models\Customer $customer
 * @property \App\Models\PaymentCycle $payment_cycle
 * @property \App\Models\PriceDomain $price_domain
 *
 * @package App\Models
 */
class Domain extends Eloquent
{
	protected $casts = [
		'customer_id' => 'int',
		'price_domain_id' => 'int',
		'payment_cycle_id' => 'int',
		'status' => 'int',
		'account_id' => 'int'
	];

	protected $dates = [
		'due'
	];

	protected $fillable = [
		'domain',
		'customer_id',
		'price_domain_id',
		'ns1',
		'ns2',
		'ns3',
		'ns4',
		'ip1',
		'ip2',
		'ip3',
		'ip4',
		'due',
		'payment_cycle_id',
		'status',
		'account_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function customer()
	{
		return $this->belongsTo(\App\Models\Customer::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }

	public function payment_cycle()
	{
		return $this->belongsTo(\App\Models\PaymentCycle::class);
	}

	public function price_domain()
	{
		return $this->belongsTo(\App\Models\PriceDomain::class);
	}
}
