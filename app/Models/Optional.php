<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Optional
 * 
 * @property int $id
 * @property string $name
 * @property string $uuid
 * @property string $description
 * @property int $email_template_id
 * @property bool $suspend_principal
 * @property float $price
 * @property int $type_term_id
 * @property int $payment_cycle_id
 * @property int $visibility
 * @property string $plans
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $account_id
 * 
 * @property \App\Models\Account $account
 * @property \App\Models\PaymentCycle $payment_cycle
 * @property \App\Models\TypeTerm $type_term
 * @property \Illuminate\Database\Eloquent\Collection $subscriptions
 *
 * @package App\Models
 */
class Optional extends Eloquent
{
	protected $casts = [
		'email_template_id' => 'int',
		'suspend_principal' => 'bool',
		'price' => 'float',
		'type_term_id' => 'int',
		'payment_cycle_id' => 'int',
		'visibility' => 'int',
		'account_id' => 'int'
	];

	protected $fillable = [
		'name',
		'uuid',
		'description',
		'email_template_id',
		'suspend_principal',
		'price',
		'type_term_id',
		'payment_cycle_id',
		'visibility',
		'plans',
		'account_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function payment_cycle()
	{
		return $this->belongsTo(\App\Models\PaymentCycle::class);
	}

	public function type_term()
	{
		return $this->belongsTo(\App\Models\TypeTerm::class);
	}

	public function subscriptions()
	{
		return $this->hasMany(\App\Models\Subscription::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
