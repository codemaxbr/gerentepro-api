<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PriceDomain
 * 
 * @property int $id
 * @property string $extension
 * @property int $module_id
 * @property float $cost
 * @property float $price_register
 * @property float $price_renew
 * @property float $price_transfer
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $account_id
 * 
 * @property \App\Models\Account $account
 * @property \App\Models\Module $module
 * @property \Illuminate\Database\Eloquent\Collection $domains
 *
 * @package App\Models
 */
class PriceDomain extends Eloquent
{
	protected $casts = [
		'module_id' => 'int',
		'cost' => 'float',
		'price_register' => 'float',
		'price_renew' => 'float',
		'price_transfer' => 'float',
		'account_id' => 'int'
	];

	protected $fillable = [
		'extension',
		'module_id',
		'cost',
		'price_register',
		'price_renew',
		'price_transfer',
		'account_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function module()
	{
		return $this->belongsTo(\App\Models\Module::class);
	}

	public function domains()
	{
		return $this->hasMany(\App\Models\Domain::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
