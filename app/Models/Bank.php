<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Bank
 * 
 * @property int $id
 * @property string $owner
 * @property string $wallet
 * @property int $type_bank_id
 * @property int $bank_house_id
 * @property string $agency
 * @property string $account
 * @property int $digit
 * @property int $account_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\BankHouse $bank_house
 * @property \App\Models\TypeBank $type_bank
 *
 * @package App\Models
 */
class Bank extends Eloquent
{
	protected $casts = [
		'type_bank_id' => 'int',
		'bank_house_id' => 'int',
		'digit' => 'int',
		'account_id' => 'int'
	];

	protected $fillable = [
		'owner',
		'wallet',
		'type_bank_id',
		'bank_house_id',
		'agency',
		'account',
		'digit',
		'account_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function bank_house()
	{
		return $this->belongsTo(\App\Models\BankHouse::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }

	public function type_bank()
	{
		return $this->belongsTo(\App\Models\TypeBank::class);
	}
}
