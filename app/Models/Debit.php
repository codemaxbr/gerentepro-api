<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Debit
 * 
 * @property int $id
 * @property float $total
 * @property int $provider_id
 * @property int $account_id
 * @property int $n_document
 * @property \Carbon\Carbon $due
 * @property \Carbon\Carbon $paid
 * @property int $payment_cycle_id
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Account $account
 * @property \App\Models\PaymentCycle $payment_cycle
 * @property \App\Models\Provider $provider
 *
 * @package App\Models
 */
class Debit extends Eloquent
{
	protected $casts = [
		'total' => 'float',
		'provider_id' => 'int',
		'account_id' => 'int',
		'n_document' => 'int',
		'payment_cycle_id' => 'int'
	];

	protected $dates = [
		'due',
		'paid'
	];

	protected $fillable = [
		'total',
		'provider_id',
		'account_id',
		'n_document',
		'due',
		'paid',
		'payment_cycle_id',
		'description'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }

	public function payment_cycle()
	{
		return $this->belongsTo(\App\Models\PaymentCycle::class);
	}

	public function provider()
	{
		return $this->belongsTo(\App\Models\Provider::class);
	}
}
