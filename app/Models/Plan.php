<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Plan
 * 
 * @property int $id
 * @property string $uuid
 * @property string $name
 * @property bool $status
 * @property int $email_template_id
 * @property bool $domain
 * @property int $trial
 * @property int $visibility
 * @property float $tax
 * @property int $module_id
 * @property string $server
 * @property string $config
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $account_id
 * 
 * @property \App\Models\Account $account
 * @property \Illuminate\Database\Eloquent\Collection $abandoned_carts
 * @property \Illuminate\Database\Eloquent\Collection $invoice_items
 * @property \Illuminate\Database\Eloquent\Collection $grids
 * @property \Illuminate\Database\Eloquent\Collection $prices
 * @property \Illuminate\Database\Eloquent\Collection $promotions
 * @property \Illuminate\Database\Eloquent\Collection $subscriptions
 *
 * @package App\Models
 */
class Plan extends Eloquent
{
    //use Cachable;

	protected $casts = [
		'status' => 'bool',
		'email_template_id' => 'int',
		'domain' => 'bool',
		'trial' => 'int',
		'visibility' => 'int',
		'tax' => 'float',
		'module_id' => 'int',
		'account_id' => 'int'
	];

	protected $fillable = [
		'uuid',
		'name',
		'status',
		'email_template_id',
		'domain',
		'trial',
		'visibility',
		'tax',
		'module_id',
		'server',
		'config',
		'account_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function abandoned_carts()
	{
		return $this->hasMany(\App\Models\AbandonedCart::class);
	}

	public function invoice_items()
	{
		return $this->hasMany(\App\Models\InvoiceItem::class);
	}

	public function grids()
	{
		return $this->belongsToMany(\App\Models\Grid::class, 'plan_grids')
					->withPivot('id', 'account_id');
	}

	public function prices()
	{
		return $this->belongsToMany(\App\Models\Price::class, 'plan_prices')
					->withPivot('id', 'account_id');
	}

	public function promotions()
	{
		return $this->belongsToMany(\App\Models\Promotion::class, 'promotion_plans')
					->withPivot('id')
					->withTimestamps();
	}

	public function subscriptions()
	{
		return $this->hasMany(\App\Models\Subscription::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }

    public function metadata()
    {
        return $this->hasOne(\App\Models\Metadata::class);
    }
}
