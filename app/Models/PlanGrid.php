<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PlanGrid
 * 
 * @property int $id
 * @property int $plan_id
 * @property int $grid_id
 * @property int $account_id
 * 
 * @property \App\Models\Account $account
 * @property \App\Models\Grid $grid
 * @property \App\Models\Plan $plan
 *
 * @package App\Models
 */
class PlanGrid extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'plan_id' => 'int',
		'grid_id' => 'int',
		'account_id' => 'int'
	];

	protected $fillable = [
		'plan_id',
		'grid_id',
		'account_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function grid()
	{
		return $this->belongsTo(\App\Models\Grid::class);
	}

	public function plan()
	{
		return $this->belongsTo(\App\Models\Plan::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
