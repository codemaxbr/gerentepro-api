<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PaymentCycle
 * 
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int $months
 * 
 * @property \Illuminate\Database\Eloquent\Collection $account_details
 * @property \Illuminate\Database\Eloquent\Collection $account_plans
 * @property \Illuminate\Database\Eloquent\Collection $debits
 * @property \Illuminate\Database\Eloquent\Collection $domains
 * @property \Illuminate\Database\Eloquent\Collection $optionals
 * @property \Illuminate\Database\Eloquent\Collection $prices
 *
 * @package App\Models
 */
class PaymentCycle extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'months' => 'int'
	];

	protected $fillable = [
		'name',
		'slug',
		'months'
	];

	public function account_details()
	{
		return $this->hasMany(\App\Models\AccountDetail::class);
	}

	public function account_plans()
	{
		return $this->hasMany(\App\Models\AccountPlan::class);
	}

	public function debits()
	{
		return $this->hasMany(\App\Models\Debit::class);
	}

	public function domains()
	{
		return $this->hasMany(\App\Models\Domain::class);
	}

	public function optionals()
	{
		return $this->hasMany(\App\Models\Optional::class);
	}

	public function prices()
	{
		return $this->hasMany(\App\Models\Price::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
