<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TypeBank
 * 
 * @property int $id
 * @property string $name
 * 
 * @property \Illuminate\Database\Eloquent\Collection $banks
 *
 * @package App\Models
 */
class TypeBank extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name'
	];

	public function banks()
	{
		return $this->hasMany(\App\Models\Bank::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
