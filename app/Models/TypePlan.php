<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TypePlan
 * 
 * @property int $id
 * @property string $name
 * @property string $slug
 * 
 * @property \Illuminate\Database\Eloquent\Collection $grids
 *
 * @package App\Models
 */
class TypePlan extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name',
		'slug'
	];

	public function grids()
	{
		return $this->hasMany(\App\Models\Grid::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
