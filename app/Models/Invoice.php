<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Feb 2019 15:01:33 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Invoice
 * 
 * @property int $id
 * @property int $customer_id
 * @property float $total
 * @property float $fee
 * @property float $discount
 * @property int $type_invoice_id
 * @property string $uuid
 * @property \Carbon\Carbon $due
 * @property string $reason
 * @property string $obs
 * @property int $status_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $account_id
 * 
 * @property \App\Models\Account $account
 * @property \App\Models\Customer $customer
 * @property \App\Models\StatusInvoice $status_invoice
 * @property \App\Models\TypeInvoice $type_invoice
 * @property \Illuminate\Database\Eloquent\Collection $invoice_histories
 * @property \Illuminate\Database\Eloquent\Collection $invoice_items
 * @property \Illuminate\Database\Eloquent\Collection $statements
 * @property \Illuminate\Database\Eloquent\Collection $subscriptions
 * @property \Illuminate\Database\Eloquent\Collection $transactions
 *
 * @package App\Models
 */
class Invoice extends Eloquent
{
    use SoftDeletes;

	protected $casts = [
		'customer_id' => 'int',
		'total' => 'float',
		'tax' => 'float',
		'discount' => 'float',
		'type_invoice_id' => 'int',
		'status_id' => 'int',
		'account_id' => 'int',
		'user_id' => 'int',
	];

	protected $fillable = [
		'customer_id',
		'total',
		'tax',
		'discount',
		'type_invoice_id',
		'uuid',
        'in_installment',
        'installments',
        'part',
		'due',
		'reason',
		'obs',
		'status_id',
		'user_id',
		'account_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function customer()
	{
		return $this->belongsTo(\App\Models\Customer::class);
	}

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

	public function status_invoice()
	{
		return $this->belongsTo(\App\Models\StatusInvoice::class, 'status_id');
	}

	public function type_invoice()
	{
		return $this->belongsTo(\App\Models\TypeInvoice::class);
	}

	public function invoice_histories()
	{
		return $this->hasMany(\App\Models\InvoiceHistory::class);
	}

	public function invoice_items()
	{
		return $this->hasMany(\App\Models\InvoiceItem::class);
	}

	public function attachment()
    {
        return $this->morphOne(\App\Models\Attachment::class, 'attachmentable');
    }

	public function statement()
	{
		return $this->hasOne(\App\Models\Statement::class);
	}

	public function subscriptions()
	{
		return $this->belongsToMany(\App\Models\Subscription::class, 'subscriptions_invoices')
					->withPivot('id')
					->withTimestamps();
	}

	public function transactions()
	{
		return $this->hasMany(\App\Models\Transaction::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
