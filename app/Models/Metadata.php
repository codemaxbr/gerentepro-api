<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 25 Sep 2019 00:51:24 -0300.
 */

namespace App\Models;

use App\Observers\MetadataObserver;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Metadata
 *
 * @property int $id
 * @property string $from_type
 * @property int $from_id
 * @property int $module_id
 * @property string $meta
 * @property int $module_account_id
 * @property int $account_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Account $account
 * @property \App\Models\ModulesAccount $modules_account
 * @property \App\Models\Module $module
 *
 * @package App\Models
 * @method static created(\Closure $param)
 * @method static updated(\Closure $param)
 * @method static deleted(\Closure $param)
 */
class Metadata extends Eloquent
{
	protected $casts = [
		'plan_id' => 'int',
		'module_id' => 'int',
		'module_account_id' => 'int',
		'account_id' => 'int'
	];

	protected $fillable = [
		'plan_id',
		'module_id',
		'meta',
		'module_account_id',
		'account_id'
	];

	public function plan()
    {
        return $this->belongsTo(\App\Models\Plan::class);
    }

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function modules_account()
	{
		return $this->belongsTo(\App\Models\ModulesAccount::class, 'module_account_id');
	}

	public function module()
	{
		return $this->belongsTo(\App\Models\Module::class);
	}

    public function getMetaAttribute($value)
    {
        if(unserialize($value)){
            return unserialize($value);
        }

        return null;
    }
}
