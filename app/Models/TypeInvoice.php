<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TypeInvoice
 * 
 * @property int $id
 * @property string $name
 * 
 * @property \Illuminate\Database\Eloquent\Collection $invoice_accounts
 * @property \Illuminate\Database\Eloquent\Collection $invoices
 * @property \Illuminate\Database\Eloquent\Collection $statements
 *
 * @package App\Models
 */
class TypeInvoice extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name'
	];

	public function invoice_accounts()
	{
		return $this->hasMany(\App\Models\InvoiceAccount::class);
	}

	public function invoices()
	{
		return $this->hasMany(\App\Models\Invoice::class);
	}

	public function statements()
	{
		return $this->hasMany(\App\Models\Statement::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
