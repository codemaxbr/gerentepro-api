<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TypePayment
 * 
 * @property int $id
 * @property string $name
 * @property string $slug
 * 
 * @property \Illuminate\Database\Eloquent\Collection $account_details
 * @property \Illuminate\Database\Eloquent\Collection $statements
 * @property \Illuminate\Database\Eloquent\Collection $subscriptions
 *
 * @package App\Models
 */
class TypePayment extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name',
		'slug'
	];

	public function account_details()
	{
		return $this->hasMany(\App\Models\AccountDetail::class);
	}

    public function transactions()
    {
        return $this->hasMany(\App\Models\Transaction::class);
    }

	public function statements()
	{
		return $this->hasMany(\App\Models\Statement::class);
	}

	public function subscriptions()
	{
		return $this->hasMany(\App\Models\Subscription::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
