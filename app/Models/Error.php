<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Error
 * 
 * @property int $id
 * @property string $request
 * @property string $message
 * @property string $function
 * @property string $file
 * @property int $status_code
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Error extends Eloquent
{
	protected $casts = [
		'status_code' => 'int'
	];

	protected $fillable = [
		'request',
		'message',
		'function',
		'file',
		'status_code'
	];

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
