<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Import
 * 
 * @property int $id
 * @property string $uuid
 * @property string $file
 * @property int $account_id
 * @property string $status
 * @property string $importable_type
 * @property int $importable_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Account $account
 *
 * @package App\Models
 */
class Import extends Eloquent
{
	protected $casts = [
		'account_id' => 'int',
		'importable_id' => 'int'
	];

	protected $fillable = [
		'uuid',
		'file',
		'account_id',
		'status',
		'importable_type',
		'importable_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
