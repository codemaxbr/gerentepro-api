<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class InvoiceHistory
 * 
 * @property int $id
 * @property string $name
 * @property int $invoice_id
 * @property int $type_activity_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Invoice $invoice
 * @property \App\Models\TypeActivity $type_activity
 *
 * @package App\Models
 */
class InvoiceHistory extends Eloquent
{
	protected $casts = [
		'invoice_id' => 'int',
		'type_activity_id' => 'int'
	];

	protected $fillable = [
		'name',
		'invoice_id',
		'type_activity_id'
	];

	public function invoice()
	{
		return $this->belongsTo(\App\Models\Invoice::class);
	}

	public function type_activity()
	{
		return $this->belongsTo(\App\Models\TypeActivity::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
