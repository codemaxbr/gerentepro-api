<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Module
 * 
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $logo
 * @property string $description
 * @property int $type_module_id
 * 
 * @property \App\Models\TypeModule $type_module
 * @property \Illuminate\Database\Eloquent\Collection $accounts
 * @property \Illuminate\Database\Eloquent\Collection $price_domains
 * @property \Illuminate\Database\Eloquent\Collection $transactions
 *
 * @package App\Models
 */
class Module extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'type_module_id' => 'int'
	];

	protected $fillable = [
		'name',
		'slug',
		'logo',
		'description',
		'type_module_id'
	];

	public function type_module()
	{
		return $this->belongsTo(\App\Models\TypeModule::class);
	}

	public function accounts()
	{
		return $this->belongsToMany(\App\Models\Account::class, 'modules_account')
					->withPivot('id', 'uuid', 'host', 'config')
					->withTimestamps();
	}

	public function price_domains()
	{
		return $this->hasMany(\App\Models\PriceDomain::class);
	}

	public function transactions()
	{
		return $this->hasMany(\App\Models\Transaction::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
