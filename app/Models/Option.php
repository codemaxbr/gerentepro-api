<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Option
 * 
 * @property int $id
 * @property string $name
 * @property string $value
 * @property int $account_id
 * 
 * @property \App\Models\Account $account
 *
 * @package App\Models
 */
class Option extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'account_id' => 'int'
	];

	protected $fillable = [
		'name',
		'value',
		'account_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
