<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class StatusInvoice
 * 
 * @property int $id
 * @property string $name
 * @property int $account_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Account $account
 * @property \Illuminate\Database\Eloquent\Collection $invoices
 * @property \Illuminate\Database\Eloquent\Collection $transactions
 *
 * @package App\Models
 */
class StatusInvoice extends Eloquent
{
	protected $casts = [
		'account_id' => 'int'
	];

	protected $fillable = [
		'name',
		'account_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function invoices()
	{
		return $this->hasMany(\App\Models\Invoice::class, 'status_id');
	}

    public function transactions()
    {
        return $this->hasMany(\App\Models\Transaction::class, 'status_id');
    }

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
