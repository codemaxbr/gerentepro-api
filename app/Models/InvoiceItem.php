<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class InvoiceItem
 * 
 * @property int $id
 * @property int $invoice_id
 * @property int $plan_id
 * @property string $domain
 * @property string $description
 * @property float $price
 * @property float $discount
 * @property int $qty
 * @property \Carbon\Carbon $data
 * @property \Carbon\Carbon $data_end
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Invoice $invoice
 * @property \App\Models\Plan $plan
 *
 * @package App\Models
 */
class InvoiceItem extends Eloquent
{
	protected $casts = [
		'invoice_id' => 'int',
		'plan_id' => 'int',
		'price_id' => 'int',
		'price' => 'float',
		'discount' => 'float',
		'tax' => 'float',
	];

	protected $fillable = [
		'invoice_id',
		'plan_id',
		'price_id',
		'domain',
		'description',
		'price',
		'discount',
		'tax',
		'months',
		'type'
	];

	public function invoice()
	{
		return $this->belongsTo(\App\Models\Invoice::class);
	}

	public function plan()
	{
		return $this->belongsTo(\App\Models\Plan::class);
	}

    public function price()
    {
        return $this->belongsTo(\App\Models\Price::class);
    }

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
