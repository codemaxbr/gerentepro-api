<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/*
 * @property bool $available
 * @property int $limit
 * @property int $account_plan_id
 * @property string $model
 *
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\AccountPlan $account_plan
 * @property \Illuminate\Database\Eloquent\Collection $accounts
 */

class Feature extends Model
{
    protected $casts = [
        'available' => 'bool',
        'limit' => 'int',
        'account_plan_id' => 'int'
    ];

    protected $fillable = [
        'model',
        'available',
        'limit',
        'account_plan_id'
    ];

    public function account_plan()
    {
        return $this->belongsTo(\App\Models\AccountPlan::class);
    }

    public function accounts()
    {
        return $this->belongsToMany(\App\Models\Account::class);
    }
}
