<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TypeServer
 * 
 * @property int $id
 * @property string $name
 * @property string $logo
 * 
 * @property \Illuminate\Database\Eloquent\Collection $servers
 *
 * @package App\Models
 */
class TypeServer extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name',
		'logo'
	];

	public function servers()
	{
		return $this->hasMany(\App\Models\Server::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
