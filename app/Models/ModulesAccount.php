<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ModulesAccount
 * 
 * @property int $id
 * @property string $uuid
 * @property int $module_id
 * @property string $host
 * @property int $account_id
 * @property string $config
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Account $account
 * @property \App\Models\Module $module
 *
 * @package App\Models
 */
class ModulesAccount extends Eloquent
{
	protected $table = 'modules_account';

	protected $casts = [
		'module_id' => 'int',
		'account_id' => 'int'
	];

	protected $fillable = [
		'uuid',
		'module_id',
		'host',
		'account_id',
		'config'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function module()
	{
		return $this->belongsTo(\App\Models\Module::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }

    public function getConfigAttribute($value)
    {
        return unserialize($value);
    }
}
