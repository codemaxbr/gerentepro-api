<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UptimeKey
 * 
 * @property int $id
 * @property string $api_key
 * @property int $used
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class UptimeKey extends Eloquent
{
	protected $casts = [
		'used' => 'int'
	];

	protected $fillable = [
		'api_key',
		'used'
	];

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
