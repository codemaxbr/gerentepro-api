<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission as BasePermission;

class Permission extends BasePermission
{
    protected $hidden = ['id', 'guard_name', 'pivot', 'created_at', 'updated_at'];

    protected $fillable = [
        'name',
        'description',
        'guard_name',
    ];
}
