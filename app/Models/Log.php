<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Log
 * 
 * @property int $id
 * @property string $logable_type
 * @property int $logable_id
 * @property string $action
 * @property int $user_id
 * @property int $account_id
 * @property int $customer_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Account $account
 * @property \App\Models\Customer $customer
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Log extends Eloquent
{
	protected $casts = [
		'logable_id' => 'int',
		'user_id' => 'int',
		'account_id' => 'int',
		'customer_id' => 'int'
	];

	protected $fillable = [
		'logable_type',
		'logable_id',
		'action',
		'user_id',
		'account_id',
		'customer_id'
	];

    public function logable()
    {
        return $this->morphTo();
    }

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function customer()
	{
		return $this->belongsTo(\App\Models\Customer::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
