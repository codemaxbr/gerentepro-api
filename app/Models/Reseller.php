<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Reseller
 * 
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $accounts
 * @property \Illuminate\Database\Eloquent\Collection $invoice_accounts
 *
 * @package App\Models
 */
class Reseller extends Eloquent
{
	protected $fillable = [
		'name'
	];

	public function accounts()
	{
		return $this->hasMany(\App\Models\Account::class);
	}

	public function invoice_accounts()
	{
		return $this->hasMany(\App\Models\InvoiceAccount::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
