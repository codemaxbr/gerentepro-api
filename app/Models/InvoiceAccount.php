<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class InvoiceAccount
 * 
 * @property int $id
 * @property int $account_id
 * @property float $total
 * @property float $fee
 * @property float $discount
 * @property int $type_invoice_id
 * @property string $uuid
 * @property \Carbon\Carbon $due
 * @property string $obs
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $reseller_id
 * 
 * @property \App\Models\Account $account
 * @property \App\Models\Reseller $reseller
 * @property \App\Models\TypeInvoice $type_invoice
 *
 * @package App\Models
 */
class InvoiceAccount extends Eloquent
{
	protected $casts = [
		'account_id' => 'int',
		'total' => 'float',
		'fee' => 'float',
		'discount' => 'float',
		'type_invoice_id' => 'int',
		'status' => 'int',
		'reseller_id' => 'int'
	];

	protected $dates = [
		'due'
	];

	protected $fillable = [
		'account_id',
		'total',
		'fee',
		'discount',
		'type_invoice_id',
		'uuid',
		'due',
		'obs',
		'status',
		'reseller_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function reseller()
	{
		return $this->belongsTo(\App\Models\Reseller::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }

	public function type_invoice()
	{
		return $this->belongsTo(\App\Models\TypeInvoice::class);
	}
}
