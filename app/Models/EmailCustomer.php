<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EmailCustomer
 * 
 * @property int $id
 * @property int $customer_id
 * @property int $email_id
 * @property int $status_email_id
 * @property int $account_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Account $account
 * @property \App\Models\Customer $customer
 * @property \App\Models\Email $email
 * @property \App\Models\StatusEmail $status_email
 *
 * @package App\Models
 */
class EmailCustomer extends Eloquent
{
	protected $casts = [
		'customer_id' => 'int',
		'email_id' => 'int',
		'status_email_id' => 'int',
		'account_id' => 'int'
	];

	protected $fillable = [
		'customer_id',
		'email_id',
		'status_email_id',
		'account_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function customer()
	{
		return $this->belongsTo(\App\Models\Customer::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }

	public function email()
	{
		return $this->belongsTo(\App\Models\Email::class);
	}

	public function status_email()
	{
		return $this->belongsTo(\App\Models\StatusEmail::class);
	}
}
