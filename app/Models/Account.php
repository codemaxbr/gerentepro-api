<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Account
 * 
 * @property int $id
 * @property string $name_business
 * @property string $uuid
 * @property string $logo
 * @property string $domain
 * @property string $email_contact
 * @property string $phone_contact
 * @property string $url_terms
 * @property bool $status
 * @property int $reseller_id
 * @property int $account_plan_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\AccountPlan $account_plan
 * @property \App\Models\Reseller $reseller
 * @property \Illuminate\Database\Eloquent\Collection $abandoned_carts
 * @property \Illuminate\Database\Eloquent\Collection $account_details
 * @property \Illuminate\Database\Eloquent\Collection $attachments
 * @property \Illuminate\Database\Eloquent\Collection $banks
 * @property \Illuminate\Database\Eloquent\Collection $credit_cards
 * @property \Illuminate\Database\Eloquent\Collection $customers
 * @property \Illuminate\Database\Eloquent\Collection $debits
 * @property \Illuminate\Database\Eloquent\Collection $domains
 * @property \Illuminate\Database\Eloquent\Collection $email_customers
 * @property \Illuminate\Database\Eloquent\Collection $email_signatures
 * @property \Illuminate\Database\Eloquent\Collection $emails
 * @property \Illuminate\Database\Eloquent\Collection $grids
 * @property \Illuminate\Database\Eloquent\Collection $imports
 * @property \Illuminate\Database\Eloquent\Collection $invoice_accounts
 * @property \Illuminate\Database\Eloquent\Collection $invoices
 * @property \Illuminate\Database\Eloquent\Collection $logs
 * @property \Illuminate\Database\Eloquent\Collection $modules
 * @property \Illuminate\Database\Eloquent\Collection $optionals
 * @property \Illuminate\Database\Eloquent\Collection $options
 * @property \Illuminate\Database\Eloquent\Collection $plan_grids
 * @property \Illuminate\Database\Eloquent\Collection $plan_prices
 * @property \Illuminate\Database\Eloquent\Collection $plans
 * @property \Illuminate\Database\Eloquent\Collection $price_domains
 * @property \Illuminate\Database\Eloquent\Collection $prices
 * @property \Illuminate\Database\Eloquent\Collection $promotions
 * @property \Illuminate\Database\Eloquent\Collection $providers
 * @property \Illuminate\Database\Eloquent\Collection $servers
 * @property \Illuminate\Database\Eloquent\Collection $statements
 * @property \Illuminate\Database\Eloquent\Collection $status_invoices
 * @property \Illuminate\Database\Eloquent\Collection $subscriptions
 * @property \Illuminate\Database\Eloquent\Collection $template_emails
 * @property \Illuminate\Database\Eloquent\Collection $transactions
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Account extends Eloquent
{
	protected $casts = [
		'status' => 'bool',
		'reseller_id' => 'int',
		'account_plan_id' => 'int'
	];

	protected $hidden = ['id', 'reseller_id', 'account_plan_id'];

	protected $fillable = [
		'name_business',
		'uuid',
		'logo',
		'domain',
		'email_contact',
		'phone_contact',
		'url_terms',
		'status',
		'reseller_id',
		'account_plan_id',
        'payment_cycle_id',
        'type_payment_id',
        'expires'
	];

	public function account_plan()
	{
		return $this->belongsTo(\App\Models\AccountPlan::class);
	}

	public function type_payment()
    {
        return $this->belongsTo(\App\Models\TypePayment::class);
    }

    public function payment_cycle()
    {
        return $this->belongsTo(\App\Models\PaymentCycle::class);
    }

	public function reseller()
	{
		return $this->belongsTo(\App\Models\Reseller::class);
	}

	public function abandoned_carts()
	{
		return $this->hasMany(\App\Models\AbandonedCart::class);
	}

	public function account_details()
	{
		return $this->hasMany(\App\Models\AccountDetail::class);
	}

	public function roles()
	{
		return $this->hasMany(\App\Models\Role::class);
	}

	public function attachments()
	{
		return $this->hasMany(\App\Models\Attachment::class);
	}

	public function banks()
	{
		return $this->hasMany(\App\Models\Bank::class);
	}

	public function credit_cards()
	{
		return $this->hasMany(\App\Models\CreditCard::class);
	}

	public function customers()
	{
		return $this->hasMany(\App\Models\Customer::class);
	}

	public function debits()
	{
		return $this->hasMany(\App\Models\Debit::class);
	}

	public function domains()
	{
		return $this->hasMany(\App\Models\Domain::class);
	}

	public function email_customers()
	{
		return $this->hasMany(\App\Models\EmailCustomer::class);
	}

	public function email_signatures()
	{
		return $this->hasMany(\App\Models\EmailSignature::class);
	}

	public function emails()
	{
		return $this->hasMany(\App\Models\Email::class);
	}

	public function grids()
	{
		return $this->hasMany(\App\Models\Grid::class);
	}

	public function imports()
	{
		return $this->hasMany(\App\Models\Import::class);
	}

	public function invoice_accounts()
	{
		return $this->hasMany(\App\Models\InvoiceAccount::class);
	}

	public function invoices()
	{
		return $this->hasMany(\App\Models\Invoice::class);
	}

	public function logs()
	{
		return $this->hasMany(\App\Models\Log::class);
	}

	public function modules()
	{
		return $this->belongsToMany(\App\Models\Module::class, 'modules_account')
					->withPivot('id', 'uuid', 'host', 'config')
					->withTimestamps();
	}

	public function optionals()
	{
		return $this->hasMany(\App\Models\Optional::class);
	}

	public function options()
	{
		return $this->hasMany(\App\Models\Option::class);
	}

	public function plan_grids()
	{
		return $this->hasMany(\App\Models\PlanGrid::class);
	}

	public function plan_prices()
	{
		return $this->hasMany(\App\Models\PlanPrice::class);
	}

	public function plans()
	{
		return $this->hasMany(\App\Models\Plan::class);
	}

	public function price_domains()
	{
		return $this->hasMany(\App\Models\PriceDomain::class);
	}

	public function prices()
	{
		return $this->hasMany(\App\Models\Price::class);
	}

	public function promotions()
	{
		return $this->hasMany(\App\Models\Promotion::class);
	}

	public function providers()
	{
		return $this->hasMany(\App\Models\Provider::class);
	}

	public function servers()
	{
		return $this->hasMany(\App\Models\Server::class);
	}

	public function statements()
	{
		return $this->hasMany(\App\Models\Statement::class);
	}

	public function status_invoices()
	{
		return $this->hasMany(\App\Models\StatusInvoice::class);
	}

	public function subscriptions()
	{
		return $this->hasMany(\App\Models\Subscription::class);
	}

	public function template_emails()
	{
		return $this->hasMany(\App\Models\TemplateEmail::class);
	}

	public function transactions()
	{
		return $this->hasMany(\App\Models\Transaction::class);
	}

	public function users()
	{
		return $this->hasMany(\App\Models\User::class);
	}
}
