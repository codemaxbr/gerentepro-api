<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Promotion
 * 
 * @property int $id
 * @property string $name
 * @property string $uuid
 * @property int $type_promotion_id
 * @property \Carbon\Carbon $start
 * @property \Carbon\Carbon $expires
 * @property string $type_value
 * @property float $value
 * @property int $qty
 * @property bool $multi_uses
 * @property bool $unique
 * @property bool $only_registers
 * @property string $prefix
 * @property int $status_promotion_id
 * @property int $account_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Account $account
 * @property \App\Models\StatusPromotion $status_promotion
 * @property \App\Models\TypePromotion $type_promotion
 * @property \Illuminate\Database\Eloquent\Collection $plans
 * @property \Illuminate\Database\Eloquent\Collection $vouchers
 *
 * @package App\Models
 */
class Promotion extends Eloquent
{
	protected $casts = [
		'type_promotion_id' => 'int',
		'value' => 'float',
		'total_min' => 'float',
		'qty' => 'int',
		'multi_uses' => 'bool',
		'unique' => 'bool',
		'only_registers' => 'bool',
		'upgrade_plan' => 'bool',
		'status_promotion_id' => 'int',
		'account_id' => 'int'
	];

	protected $dates = [
		'start',
		'expires'
	];

	protected $fillable = [
		'name',
		'uuid',
		'type_promotion_id',
		'start',
		'expires',
		'type_value',
		'value',
		'total_min',
		'qty',
		'multi_uses',
		'unique',
		'upgrade_plan',
		'only_registers',
		'prefix',
		'status_promotion_id',
		'account_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function status_promotion()
	{
		return $this->belongsTo(\App\Models\StatusPromotion::class);
	}

	public function type_promotion()
	{
		return $this->belongsTo(\App\Models\TypePromotion::class);
	}

	public function plans()
	{
		return $this->belongsToMany(\App\Models\Plan::class, 'promotion_plans')
					->withPivot('id')
					->withTimestamps();
	}

	public function vouchers()
	{
		return $this->hasMany(\App\Models\Voucher::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }

    public function tasks()
    {
        return $this->morphMany(\App\Models\Task::class, 'target');
    }
}
