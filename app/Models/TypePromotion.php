<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TypePromotion
 * 
 * @property int $id
 * @property string $name
 * 
 * @property \Illuminate\Database\Eloquent\Collection $promotions
 *
 * @package App\Models
 */
class TypePromotion extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name',
        'field'
	];

	public function promotions()
	{
		return $this->hasMany(\App\Models\Promotion::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
