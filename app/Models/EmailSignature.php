<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EmailSignature
 * 
 * @property int $id
 * @property string $header
 * @property string $footer
 * @property string $signature
 * @property int $account_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Account $account
 *
 * @package App\Models
 */
class EmailSignature extends Eloquent
{
	protected $casts = [
		'account_id' => 'int'
	];

	protected $fillable = [
		'header',
		'footer',
		'signature',
		'account_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
