<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Voucher
 * 
 * @property int $id
 * @property int $promotion_id
 * @property bool $used
 * @property string $code
 * @property int $customer_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Customer $customer
 * @property \App\Models\Promotion $promotion
 *
 * @package App\Models
 */
class Voucher extends Eloquent
{
	protected $casts = [
		'promotion_id' => 'int',
		'used' => 'bool',
		'customer_id' => 'int'
	];

	protected $fillable = [
		'promotion_id',
		'used',
		'code',
		'customer_id'
	];

	public function customer()
	{
		return $this->belongsTo(\App\Models\Customer::class);
	}

	public function promotion()
	{
		return $this->belongsTo(\App\Models\Promotion::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
