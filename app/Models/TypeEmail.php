<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TypeEmail
 * 
 * @property int $id
 * @property string $name
 *
 * @package App\Models
 */
class TypeEmail extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name'
	];

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }

    public function emails()
    {
        return $this->hasMany(\App\Models\Email::class);
    }
}
