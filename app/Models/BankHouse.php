<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BankHouse
 * 
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $logo
 * 
 * @property \Illuminate\Database\Eloquent\Collection $banks
 *
 * @package App\Models
 */
class BankHouse extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name',
		'code',
		'logo'
	];

	public function banks()
	{
		return $this->hasMany(\App\Models\Bank::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
