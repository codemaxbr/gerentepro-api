<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PlanPrice
 * 
 * @property int $id
 * @property int $price_id
 * @property int $plan_id
 * @property int $account_id
 * 
 * @property \App\Models\Account $account
 * @property \App\Models\Plan $plan
 * @property \App\Models\Price $price
 *
 * @package App\Models
 */
class PlanPrice extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'price_id' => 'int',
		'plan_id' => 'int',
		'account_id' => 'int'
	];

	protected $fillable = [
		'price_id',
		'plan_id',
		'account_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function plan()
	{
		return $this->belongsTo(\App\Models\Plan::class);
	}

	public function price()
	{
		return $this->belongsTo(\App\Models\Price::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
