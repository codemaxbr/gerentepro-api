<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class InvoiceAccountItem
 * 
 * @property int $id
 * @property int $invoice_account_id
 * @property int $account_plan_id
 * @property string $domain
 * @property string $description
 * @property float $price
 * @property float $discount
 * @property int $qty
 * @property \Carbon\Carbon $data
 * @property \Carbon\Carbon $data_end
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class InvoiceAccountItem extends Eloquent
{
	protected $casts = [
		'invoice_account_id' => 'int',
		'account_plan_id' => 'int',
		'price' => 'float',
		'discount' => 'float',
		'qty' => 'int'
	];

	protected $dates = [
		'data',
		'data_end'
	];

	protected $fillable = [
		'invoice_account_id',
		'account_plan_id',
		'domain',
		'description',
		'price',
		'discount',
		'qty',
		'data',
		'data_end'
	];

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
