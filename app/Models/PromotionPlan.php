<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PromotionPlan
 * 
 * @property int $id
 * @property int $promotion_id
 * @property int $plan_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Plan $plan
 * @property \App\Models\Promotion $promotion
 *
 * @package App\Models
 */
class PromotionPlan extends Eloquent
{
	protected $casts = [
		'promotion_id' => 'int',
		'plan_id' => 'int'
	];

	protected $fillable = [
		'promotion_id',
		'plan_id'
	];

	public function plan()
	{
		return $this->belongsTo(\App\Models\Plan::class);
	}

	public function promotion()
	{
		return $this->belongsTo(\App\Models\Promotion::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
