<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role as BaseRole;

class Role extends BaseRole
{
    protected $fillable = [
        'name',
        'description',
        'guard_name',
        'account_id'
    ];

    public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}
}
