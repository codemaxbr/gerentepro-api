<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CreditCard
 * 
 * @property int $id
 * @property string $flag
 * @property string $final_number
 * @property string $owner
 * @property string $expires
 * @property int $customer_id
 * @property string $payment_token
 * @property int $account_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Account $account
 * @property \App\Models\Customer $customer
 *
 * @package App\Models
 */
class CreditCard extends Eloquent
{
	protected $casts = [
		'customer_id' => 'int',
		'account_id' => 'int'
	];

	protected $hidden = [
		'payment_token'
	];

	protected $fillable = [
		'flag',
		'final_number',
		'owner',
		'expires',
		'customer_id',
		'payment_token',
		'account_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }

	public function customer()
	{
		return $this->belongsTo(\App\Models\Customer::class);
	}
}
