<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class StatusEmail
 * 
 * @property int $id
 * @property string $name
 * 
 * @property \Illuminate\Database\Eloquent\Collection $email_customers
 *
 * @package App\Models
 */
class StatusEmail extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name'
	];

	public function email_customers()
	{
		return $this->hasMany(\App\Models\EmailCustomer::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
