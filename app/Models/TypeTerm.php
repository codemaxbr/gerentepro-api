<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TypeTerm
 * 
 * @property int $id
 * @property string $name
 * 
 * @property \Illuminate\Database\Eloquent\Collection $account_plans
 * @property \Illuminate\Database\Eloquent\Collection $optionals
 *
 * @package App\Models
 */
class TypeTerm extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name'
	];

	public function account_plans()
	{
		return $this->hasMany(\App\Models\AccountPlan::class);
	}

	public function optionals()
	{
		return $this->hasMany(\App\Models\Optional::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
