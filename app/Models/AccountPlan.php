<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AccountPlan
 * 
 * @property int $id
 * @property string $name
 * @property float $price_month
 * @property float $price_year
 * @property bool $status
 * @property int $type_term_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\TypeTerm $type_term
 * @property \Illuminate\Database\Eloquent\Collection $account_details
 * @property \Illuminate\Database\Eloquent\Collection $accounts
 * @property \Illuminate\Database\Eloquent\Collection $features
 *
 * @package App\Models
 */
class AccountPlan extends Eloquent
{
	protected $casts = [
		'price_month' => 'float',
		'price_year' => 'float',
		'status' => 'bool',
		'type_term_id' => 'int'
	];

	protected $fillable = [
		'name',
		'price_month',
		'price_year',
		'status',
		'type_term_id'
	];

	public function type_term()
	{
		return $this->belongsTo(\App\Models\TypeTerm::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }

    public function features()
    {
        return $this->hasMany(\App\Models\Feature::class);
    }

	public function account_details()
	{
		return $this->hasMany(\App\Models\AccountDetail::class);
	}

	public function accounts()
	{
		return $this->hasMany(\App\Models\Account::class);
	}
}
