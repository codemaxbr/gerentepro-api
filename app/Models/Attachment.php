<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 27 Feb 2019 15:01:33 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Attachment
 * 
 * @property int $id
 * @property string $name
 * @property string $uuid
 * @property int $account_id
 * @property string $attachmentable_type
 * @property int $attachmentable_id
 * @property string $file_url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Account $account
 *
 * @package App\Models
 */
class Attachment extends Eloquent
{
	protected $casts = [
		'account_id' => 'int',
		'attachmentable_id' => 'int'
	];

	protected $fillable = [
		'name',
		'uuid',
		'account_id',
		'attachmentable_type',
		'attachmentable_id',
		'file_url'
	];

	public function attachmentable()
    {
        return $this->morphTo();
    }

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }

	public function invoice()
    {
        return $this->belongsTo(\App\Models\Invoice::class);
    }
}
