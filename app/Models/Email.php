<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Email
 * 
 * @property int $id
 * @property string $subject
 * @property string $content
 * @property int $account_id
 * 
 * @property \App\Models\Account $account
 * @property \Illuminate\Database\Eloquent\Collection $customers
 * @property \Illuminate\Database\Eloquent\Collection $template_emails
 *
 * @package App\Models
 */
class Email extends Eloquent
{
    public $timestamps = false;

	protected $fillable = [
		'title',
		'tag',
		'subject',
        'description',
        'type_email_id'
	];

	public function type()
    {
        return $this->belongsTo(TypeEmail::class, 'type_email_id', 'id');
    }

    public function template()
    {
        return $this->hasOne(\App\Models\TemplateEmail::class);
    }

    public function templates()
    {
        return $this->hasMany(\App\Models\TemplateEmail::class);
    }

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
