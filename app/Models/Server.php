<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Server
 * 
 * @property int $id
 * @property string $uuid
 * @property string $monitor
 * @property string $name
 * @property string $datacenter
 * @property string $ip
 * @property int $limit_accounts
 * @property float $cost
 * @property int $provider_id
 * @property int $type_server_id
 * @property string $ns1
 * @property string $ns1_ip
 * @property string $ns2
 * @property string $ns2_ip
 * @property string $ns3
 * @property string $ns3_ip
 * @property string $ns4
 * @property string $ns4_ip
 * @property int $account_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Account $account
 * @property \App\Models\Provider $provider
 * @property \App\Models\TypeServer $type_server
 *
 * @package App\Models
 */
class Server extends Eloquent
{
	protected $casts = [
		'limit_accounts' => 'int',
		'cost' => 'float',
		'provider_id' => 'int',
		'type_server_id' => 'int',
		'account_id' => 'int'
	];

	protected $fillable = [
		'uuid',
		'monitor',
		'name',
		'datacenter',
		'ip',
		'limit_accounts',
		'cost',
		'provider_id',
		'type_server_id',
		'ns1',
		'ns1_ip',
		'ns2',
		'ns2_ip',
		'ns3',
		'ns3_ip',
		'ns4',
		'ns4_ip',
		'account_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function provider()
	{
		return $this->belongsTo(\App\Models\Provider::class);
	}

	public function type_server()
	{
		return $this->belongsTo(\App\Models\TypeServer::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
