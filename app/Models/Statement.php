<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Statement
 * 
 * @property int $id
 * @property string $name
 * @property int $customer_id
 * @property float $total
 * @property string $type
 * @property string $obs
 * @property int $type_payment_id
 * @property int $invoice_id
 * @property int $account_id
 * @property int $user_id
 * @property int $type_invoice_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Account $account
 * @property \App\Models\Customer $customer
 * @property \App\Models\Invoice $invoice
 * @property \App\Models\TypeInvoice $type_invoice
 * @property \App\Models\TypePayment $type_payment
 *
 * @package App\Models
 */
class Statement extends Eloquent
{
	protected $casts = [
		'customer_id' => 'int',
		'total' => 'float',
		'type_payment_id' => 'int',
		'invoice_id' => 'int',
		'account_id' => 'int',
		'user_id' => 'int',
		'type_invoice_id' => 'int'
	];

	protected $fillable = [
		'name',
		'customer_id',
		'total',
		'type',
		'obs',
		'type_payment_id',
		'invoice_id',
		'account_id',
		'user_id',
		'type_invoice_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function customer()
	{
		return $this->belongsTo(\App\Models\Customer::class);
	}

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

	public function invoice()
	{
		return $this->belongsTo(\App\Models\Invoice::class);
	}

	public function type_invoice()
	{
		return $this->belongsTo(\App\Models\TypeInvoice::class);
	}

	public function type_payment()
	{
		return $this->belongsTo(\App\Models\TypePayment::class);
	}
}
