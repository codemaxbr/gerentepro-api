<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Price
 * 
 * @property int $id
 * @property int $payment_cycle_id
 * @property float $price
 * @property int $account_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Account $account
 * @property \App\Models\PaymentCycle $payment_cycle
 * @property \Illuminate\Database\Eloquent\Collection $plans
 *
 * @package App\Models
 */
class Price extends Eloquent
{
	protected $casts = [
		'payment_cycle_id' => 'int',
		'price' => 'float',
		'account_id' => 'int'
	];

	protected $fillable = [
		'payment_cycle_id',
		'price',
		'account_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function payment_cycle()
	{
		return $this->belongsTo(\App\Models\PaymentCycle::class);
	}

	public function plans()
	{
		return $this->belongsToMany(\App\Models\Plan::class, 'plan_prices')
					->withPivot('id', 'account_id');
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
