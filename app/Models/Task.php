<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Task
 *
 * @property int $id
 * @property string $description
 * @property int $attempts
 * @property int $account_id
 * @property int $module_id
 * @property int $parts
 * @property int $completed
 * @property text $response
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */

class Task extends Model
{
    protected $fillable = [
        'description',
        'attempts',
        'status',
        'response',
        'module_id',
        'target',
        'failed_job_id',
        'account_id',
        'completed',
        'parts'
    ];

    public function target()
    {
        return $this->morphTo();
    }

    public function job()
    {
        return $this->belongsTo(\App\Models\Job::class);
    }

    public function failed_job()
    {
        return $this->belongsTo(\App\Models\FailedJob::class);
    }

    public function account()
    {
        return $this->belongsTo(\App\Models\Account::class);
    }

    public function module()
    {
        return $this->belongsTo(\App\Models\Module::class);
    }
}
