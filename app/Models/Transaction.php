<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Transaction
 * 
 * @property int $id
 * @property int $invoice_id
 * @property int $customer_id
 * @property float $total
 * @property int $module_id
 * @property string $external_id
 * @property string $status
 * @property string $url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $account_id
 * 
 * @property \App\Models\Account $account
 * @property \App\Models\Customer $customer
 * @property \App\Models\Invoice $invoice
 * @property \App\Models\Module $module
 *
 * @package App\Models
 */
class Transaction extends Eloquent
{
	protected $casts = [
		'invoice_id' => 'int',
		'customer_id' => 'int',
		'total' => 'float',
		'module_id' => 'int',
		'account_id' => 'int',
		'status_id' => 'int',
	];

	protected $fillable = [
		'invoice_id',
		'customer_id',
		'total',
		'module_id',
		'external_id',
        'type_payment_id',
		'status_id',
		'url',
		'account_id',
        'response',
        'brand',
        'final_card'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

    public function type_payment()
    {
        return $this->belongsTo(\App\Models\TypePayment::class);
    }

	public function customer()
	{
		return $this->belongsTo(\App\Models\Customer::class);
	}

    public function status()
    {
        return $this->belongsTo(\App\Models\StatusInvoice::class);
    }

	public function invoice()
	{
		return $this->belongsTo(\App\Models\Invoice::class);
	}

	public function module()
	{
		return $this->belongsTo(\App\Models\Module::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
