<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SubscriptionsInvoice
 * 
 * @property int $id
 * @property int $invoice_id
 * @property int $subscription_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Invoice $invoice
 * @property \App\Models\Subscription $subscription
 *
 * @package App\Models
 */
class SubscriptionsInvoice extends Eloquent
{
	protected $casts = [
		'invoice_id' => 'int',
		'subscription_id' => 'int'
	];

	protected $fillable = [
		'invoice_id',
		'subscription_id'
	];

	public function invoice()
	{
		return $this->belongsTo(\App\Models\Invoice::class);
	}

	public function subscription()
	{
		return $this->belongsTo(\App\Models\Subscription::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
