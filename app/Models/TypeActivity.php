<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TypeActivity
 * 
 * @property int $id
 * @property string $name
 * 
 * @property \Illuminate\Database\Eloquent\Collection $invoice_histories
 *
 * @package App\Models
 */
class TypeActivity extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name'
	];

	public function invoice_histories()
	{
		return $this->hasMany(\App\Models\InvoiceHistory::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
