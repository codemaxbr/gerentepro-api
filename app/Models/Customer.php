<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class Customer
 * 
 * @property int $id
 * @property string $name
 * @property string $uuid
 * @property string $type
 * @property string $cpf_cnpj
 * @property string $password
 * @property string $photo
 * @property string $email
 * @property string $email_nfe
 * @property string $business
 * @property string $phone
 * @property string $mobile
 * @property string $ins_municipal
 * @property string $ins_estadual
 * @property string $skype
 * @property string $whatsapp
 * @property string $rg
 * @property \Carbon\Carbon $birthdate
 * @property string $genre
 * @property bool $status
 * @property \Carbon\Carbon $last_login
 * @property string $obs
 * @property string $zipcode
 * @property string $address
 * @property string $number
 * @property string $uf
 * @property string $city
 * @property string $district
 * @property string $additional
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $account_id
 * @property int $vindi_id
 * @property string $remember_token
 * @property string $provider
 * @property string $provider_id
 * 
 * @property \App\Models\Account $account
 * @property \Illuminate\Database\Eloquent\Collection $abandoned_carts
 * @property \Illuminate\Database\Eloquent\Collection $credit_cards
 * @property \Illuminate\Database\Eloquent\Collection $domains
 * @property \Illuminate\Database\Eloquent\Collection $emails
 * @property \Illuminate\Database\Eloquent\Collection $invoices
 * @property \Illuminate\Database\Eloquent\Collection $logs
 * @property \Illuminate\Database\Eloquent\Collection $statements
 * @property \Illuminate\Database\Eloquent\Collection $subscriptions
 * @property \Illuminate\Database\Eloquent\Collection $transactions
 * @property \Illuminate\Database\Eloquent\Collection $vouchers
 *
 * @package App\Models
 */
class Customer extends Eloquent
{
	public static $permissions = [
		'create.customer' 	=> 'Criar um novo cliente',
		'edit.customer' 	=> 'Alterar dados de clientes',
		'delete.customer' 	=> 'Excluir um cliente',
		'login.customer' 	=> 'Simular acesso de clientes',
		'message.customer' 	=> 'Enviar mensagem para clientes',
	];

	protected $casts = [
		'status' => 'bool',
		'account_id' => 'int',
		'vindi_id' => 'int'
	];

	protected $dates = [
		'birthdate',
		'last_login'
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'name',
		'uuid',
		'type',
		'cpf_cnpj',
		'password',
		'photo',
		'email',
		'email_nfe',
		'business',
		'phone',
		'mobile',
		'ins_municipal',
		'ins_estadual',
		'skype',
		'whatsapp',
		'rg',
		'birthdate',
		'genre',
		'status',
		'last_login',
		'obs',
		'zipcode',
		'address',
		'number',
		'uf',
		'city',
		'district',
		'additional',
		'account_id',
		'vindi_id',
		'remember_token',
		'provider',
		'provider_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function abandoned_carts()
	{
		return $this->hasMany(\App\Models\AbandonedCart::class);
	}

	public function credit_cards()
	{
		return $this->hasMany(\App\Models\CreditCard::class);
	}

	public function domains()
	{
		return $this->hasMany(\App\Models\Domain::class);
	}

	public function emails()
	{
		return $this->belongsToMany(\App\Models\Email::class, 'email_customers')
					->withPivot('id', 'status_email_id', 'account_id')
					->withTimestamps();
	}

	public function invoices()
	{
		return $this->hasMany(\App\Models\Invoice::class);
	}

	public function logs()
	{
        return $this->morphMany(\App\Models\Log::class, 'logable');
	}

	public function statements()
	{
		return $this->hasMany(\App\Models\Statement::class);
	}

	public function subscriptions()
	{
		return $this->hasMany(\App\Models\Subscription::class);
	}

	public function transactions()
	{
		return $this->hasMany(\App\Models\Transaction::class);
	}

	public function vouchers()
	{
		return $this->hasMany(\App\Models\Voucher::class);
	}
}
