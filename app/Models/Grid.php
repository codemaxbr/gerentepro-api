<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Grid
 * 
 * @property int $id
 * @property string $description
 * @property int $type_plan_id
 * @property int $account_id
 * 
 * @property \App\Models\Account $account
 * @property \App\Models\TypePlan $type_plan
 * @property \Illuminate\Database\Eloquent\Collection $plans
 *
 * @package App\Models
 */
class Grid extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'type_plan_id' => 'int',
		'account_id' => 'int'
	];

	protected $fillable = [
		'description',
		'type_plan_id',
		'account_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function type_plan()
	{
		return $this->belongsTo(\App\Models\TypePlan::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }

	public function plans()
	{
		return $this->belongsToMany(\App\Models\Plan::class, 'plan_grids')
					->withPivot('id', 'account_id');
	}
}
