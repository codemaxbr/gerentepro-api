<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Provider
 * 
 * @property int $id
 * @property string $name
 * @property string $fantasia
 * @property string $uuid
 * @property string $type
 * @property string $cpf_cnpj
 * @property string $email
 * @property string $phone
 * @property string $mobile
 * @property string $insc_municipal
 * @property string $insc_estadual
 * @property \Carbon\Carbon $birthdate
 * @property bool $status
 * @property string $obs
 * @property string $zipcode
 * @property string $address
 * @property string $number
 * @property string $uf
 * @property string $city
 * @property string $district
 * @property string $additional
 * @property int $account_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Account $account
 * @property \Illuminate\Database\Eloquent\Collection $debits
 * @property \Illuminate\Database\Eloquent\Collection $servers
 *
 * @package App\Models
 */
class Provider extends Eloquent
{
	protected $casts = [
		'status' => 'bool',
		'account_id' => 'int'
	];

	protected $dates = [
		'birthdate'
	];

	protected $fillable = [
		'name',
		'fantasia',
		'uuid',
		'type',
		'cpf_cnpj',
		'email',
		'phone',
		'mobile',
		'insc_municipal',
		'insc_estadual',
		'birthdate',
		'status',
		'obs',
		'zipcode',
		'address',
		'number',
		'uf',
		'city',
		'district',
		'additional',
		'account_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

	public function debits()
	{
		return $this->hasMany(\App\Models\Debit::class);
	}

	public function servers()
	{
		return $this->hasMany(\App\Models\Server::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
