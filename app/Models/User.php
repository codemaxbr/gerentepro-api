<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 22 Apr 2019 18:08:45 +0000.
 */

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class User
 * 
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property boolean $photo
 * @property bool $confirmed
 * @property bool $is_reseller
 * @property string $token
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $last_login
 * @property int $account_id
 * 
 * @property \App\Models\Account $account
 * @property \Illuminate\Database\Eloquent\Collection $logs
 * @property \Illuminate\Database\Eloquent\Collection $roles
 *
 * @package App\Models
 */
class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

	protected $casts = [
		'photo' => 'boolean',
		'confirmed' => 'bool',
		'is_reseller' => 'bool',
		'account_id' => 'int'
	];

	protected $dates = [
		'last_login'
	];

	protected $hidden = [
		'password',
		'token',
		'remember_token'
	];

	protected $fillable = [
		'name',
		'email',
		'password',
		'photo',
        'phone',
		'confirmed',
		'is_reseller',
		'token',
		'remember_token',
		'last_login',
		'account_id'
	];

	public function account()
	{
		return $this->belongsTo(\App\Models\Account::class);
	}

    public function logs()
    {
        return $this->morphMany(\App\Models\Log::class, 'logable');
    }
}
