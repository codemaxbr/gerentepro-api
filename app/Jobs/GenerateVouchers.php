<?php

namespace App\Jobs;

use App\Models\Promotion;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;

ini_set('memory_limit', '1G');//1 GIGABYTE
ini_set('max_execution_time', '0');

class GenerateVouchers extends Job
{
    use \Illuminate\Queue\InteractsWithQueue;

    public $qty;
    /**
     * @var Promotion
     */
    public $promotion;
    /**
     * @var null
     */
    public $code;
    public $parts;
    public $total;
    public $task;
    protected $job;
    public $tries = 3;
    public $timeout = 120;

    public $limit = 5000;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($qty, Promotion $promotion, $code = null, $parts, $total)
    {
        $this->qty = $qty;
        $this->promotion = $promotion;
        $this->code = $code;
        $this->parts = $parts;
        $this->total = $total;

        $this->task = $promotion->tasks()->firstOrCreate(['description' => 'GenerateVouchers'], [
            'target_id' => $promotion->id,
            'module_id' => null,
            'status' => 'waiting',
            'parts' => $parts,
            'account_id' => $promotion->account_id,
        ]);
    }

    protected function randomStr($string)
    {
        return masked($string, "#####-####-#######");
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /** @var Task $task */
        $task = $this->task;

        /** @var Promotion $promotion */
        $promotion = $this->promotion;
        $code = $this->code;

        /*
         * Gravando histórico de Tarefas
         */
        $task->attempts++;
        $task->status = 'running';
        $task->save();

        DB::beginTransaction();

        try{
            $create = array();
            $faker = \Faker\Factory::create();
            $error = false;

            for ($i = 0; $i < $this->qty; $i++)
            {
                $generated = $this->randomStr(strtoupper($faker->unique()->regexify('[A-Z0-9][A-Z0-9][A-Z]{16,16}')));

                $add = [
                    'promotion_id' => $promotion->id,
                    'code' => ($code != null) ? $code : $generated,
                    'used' => false,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString()
                ];

                //array_push($create, $add);
                $create = DB::table('vouchers')->insert($add);
                if(!$create){
                    $error = true;
                }
            }

            if(!$error)
            {
                $task->completed++;

                if($task->completed == $this->parts)
                {
                    $task->status = 'completed';
                    $task->response = '('.$this->total.') vouchers gerados com sucesso!';
                    $task->failed_job_id = null;
                }
                $task->save();

                DB::commit();

                return true;
            }else{
                $task->status = 'failed';
                $task->response = 'Ocorreu algum erro..';
                $task->save();
                DB::rollBack();

                $this->fail('Ocorreu algum erro..');
            }

        }catch (Exception $e){
            $task->status = 'failed';
            $task->response = $e->getMessage();
            $task->save();
            DB::rollBack();

            $this->fail($e->getMessage());
        }
    }
}
