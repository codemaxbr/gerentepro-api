<?php

namespace App\Jobs;

use App\Models\Customer;
use App\Models\Invoice;
use App\Models\InvoiceItem;
use App\Models\Module;
use App\Models\ModulesAccount;
use App\Models\Plan;
use App\Models\Metadata;
use App\Models\Subscription;
use App\Models\Task;
use cPanel\cPanel;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class CreateAccount extends Job
{
    use \Illuminate\Queue\InteractsWithQueue;
    /**
     * @var Subscription
     */
    public $item;
    /**
     * @var Plan
     */
    public $plan;
    public $module_account;
    public $metadata;
    public $subscription;
    protected $job;

    public $task;

    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Subscription $subscription, Metadata $metadata, $item)
    {
        $this->metadata = $metadata;
        $this->item = $item;
        $this->subscription = $subscription;
        $module = $metadata->module;

        $this->task = $subscription->tasks()->firstOrCreate(['description' => 'CreateAccount'], [
            'target_id' => $subscription->id,
            'module_id' => $module->id,
            'status' => 'waiting',
            'account_id' => $subscription->account_id,
        ]);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /** @var Metadata $metadata */
        $metadata = $this->metadata;
        $meta = $metadata->meta;

        /** @var Task $task */
        $task = $this->task;

        /** @var InvoiceItem $item */
        $item = $this->item;

        /** @var Customer $customer */
        $customer = $item->invoice->customer;

        /** @var Module $module */
        $module = $metadata->module;

        /** @var ModulesAccount $config_server */
        $config_server = $metadata->modules_account;

        $subscription = $this->subscription;

        /*
         * Gravando histórico de Tarefas
         */
        $task->attempts++;
        $task->status = 'running';
        $task->save();

        switch($module->slug)
        {
            /***********************************************************************************************************
             * cPanel / WHM
             */
            case 'cpanel':
                try {
                    cPanel::setHost($config_server->host);
                    cPanel::setToken($config_server->config['username'], $config_server->config['token']);

                    $cpanel = cPanel::accounts()->create([
                        'user' => substr(userDomain($item->domain), 0, 8),
                        'domain' => $item->domain,
                        'plan' => $meta['name'],
                        'email' => $customer->email
                    ]);

                    if($cpanel->status == 'success'){
                        $data = $cpanel->data;

                        $subscription->login_user = $data->username;
                        $subscription->login_password = $data->password;
                        $subscription->save();

                        $task->status = 'completed';
                        $task->response = json_encode($data);
                        $task->failed_job_id = null;
                        $task->save();

                        return true;
                    }else{
                        $task->status = 'failed';
                        $task->response = $cpanel->data->metadata->reason;
                        $task->save();

                        throw new Exception($cpanel->data->metadata->reason);
                        $this->fail();
                    }
                } catch (\Exception $e) {
                    $this->fail($e->getMessage());
                }

            break;

            /***********************************************************************************************************
             * Plesk
             */
            case 'plesk':

            break;

            case 'centovacast':

            break;

            case 'whmsonic':

            break;

            case 'vestacp':

            break;
        }
    }
}
