<?php

namespace App\Jobs;

use App\Models\Metadata;
use App\Models\Module;
use App\Models\Subscription;

use cPanel\cPanel;
use Exception;

class SuspendAccount extends Job
{
    public $subscription;
    public $metadata;
    public $task;
    public $reason;

    protected $job;
    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @param Subscription $subscription
     * @param Metadata $metadata
     */
    public function __construct(Subscription $subscription, Metadata $metadata, $reason = '')
    {
        $this->metadata = $metadata;
        $this->subscription = $subscription;
        $module = $metadata->module;
        $this->reason = $reason;

        $this->task = $subscription->tasks()->firstOrCreate(['description' => 'SuspendAccount'], [
            'target_id' => $subscription->id,
            'module_id' => $module->id,
            'status' => 'waiting',
            'account_id' => $subscription->account_id,
        ]);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /** @var Metadata $metadata */
        $metadata = $this->metadata;
        $meta = $metadata->meta;
        $subscription = $this->subscription;

        /** @var Task $task */
        $task = $this->task;

        /** @var Customer $customer */
        $customer = $subscription->customer;

        /** @var Module $module */
        $module = $metadata->module;

        /** @var ModulesAccount $config_server */
        $config_server = $metadata->modules_account;

        /*
         * Gravando histórico de Tarefas
         */
        $task->attempts++;
        $task->status = 'running';
        $task->save();

        switch($module->slug)
        {
            /***********************************************************************************************************
             * cPanel / WHM
             */
            case 'cpanel':
                try {
                    cPanel::setHost($config_server->host);
                    cPanel::setToken($config_server->config['username'], $config_server->config['token']);

                    $cpanel = cPanel::accounts()->suspend($subscription->login_user, $this->reason);

                    if($cpanel->status == 'success'){
                        $task->status = 'completed';
                        $task->response = $cpanel->message;
                        $task->failed_job_id = null;
                        $task->save();

                        return true;
                    }else{
                        $task->status = 'failed';
                        $task->response = $cpanel->data->metadata->reason;
                        $task->save();

                        throw new Exception($cpanel->data->metadata->reason);
                        $this->fail();
                    }
                } catch (\Exception $e) {
                    $this->fail($e->getMessage());
                }

                break;

            /***********************************************************************************************************
             * Plesk
             */
            case 'plesk':

                break;

            case 'centovacast':

                break;

            case 'whmsonic':

                break;

            case 'vestacp':

                break;
        }
    }

}
