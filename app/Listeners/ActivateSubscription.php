<?php

namespace App\Listeners;

use App\Events\ConfirmPayment;
use App\Jobs\ActiveAccount;
use App\Jobs\CreateAccount;
use App\Models\ModulesAccount;
use App\Models\Subscription;
use App\Models\SubscriptionsInvoice;
use App\Services\SubscriptionService;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Database\QueryException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Queue;

class ActivateSubscription
{
    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(SubscriptionService $subscriptionService)
    {
        $this->subscriptionService = $subscriptionService;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $user = $event->user;
        $invoice = $event->invoice;
        $request = $event->request;
        $customer = $invoice->customer;
        $parent = $event->parent;

        // Se a Fatura for do tipo PEDIDO...
        if($invoice->type_invoice->name == 'Pedido' || $invoice->type_invoice->name == 'Recorrência')
        {
            foreach($invoice->invoice_items as $item)
            {
                switch($item->type){
                    /*************************************************************
                     * Se o item for: PLANO DE ASSINATURA
                     *************************************************************/
                    case 'plan': 
                        // Plano gravado no cache da Fatura (qualquer alteração futura do planos, após a fatura ser gerada.. não irá mudar)
                        $plan = $item->plan;
                        $where['plan_id'] = $item->plan_id;
                        if($plan->domain) $where['domain'] = $item->domain;
                        
                        // Verifica se o cliente já possui uma assinatura com estes parâmetros
                        $subscription   = $customer->subscriptions()->where($where)->get();
                        $metadata       = $plan->metadata;
                        $price          = $plan->prices()->find($item->price_id);
                        $months         = $price->payment_cycle->months;

                        if($subscription->isEmpty())
                        {
                            // @var App\Models\Subscription $subscribe
                            $subscribe = $customer->subscriptions()->create([
                                'plan_id'           => $plan->id,
                                'domain'            => @$item->domain,
                                'due'               => ($months > 0) ? Carbon::now()->addMonths($months) : null,
                                'activated_at'      => Carbon::now(),
                                'total'             => $price->price, // <-- TODO Resolver isso "total"
                                'recurrence'        => ($months > 0) ? true : false,
                                'status'            => 1,
                                'type_payment_id'   => $request->type_payment_id,
                                'account_id'        => $plan->account_id
                            ]);

                            $invoice_subscription = SubscriptionsInvoice::create([
                                'invoice_id'        =>  $invoice->id,
                                'subscription_id'   =>  $subscribe->id
                            ]);

                            // Se o plano possuir integração com algum Plugin
                            if($metadata != null){
                                dispatch(new CreateAccount($subscribe, $metadata, $item));
                            }

                            return $subscribe;
                        }
                        
                        // Se o cliente já possuir uma assinatura
                        else{
                            /* todo precisa especificar um retorno de erro caso haja uma assinatura ativa do cliente*/
                            $subscriber = $subscription->first();

                            if($subscriber->status != 1 && !$subscriber->cancelled)
                            {
                                $subscriber->status = 1;

                                // Roda o job para desbloquear a assinatura do cliente
                                if($metadata != null){
                                    dispatch(new ActiveAccount($subscriber, $metadata));
                                }
                            }

                            $subscriber->due = $subscriber->due->addMonths($months);
                            $subscriber->save();

                            return $subscriber;
                        }
                    break;
                }// Fim da verificação do TIPO DE ITEM
            }
        }

        if($invoice->type_invoice->name == 'Avulsa')
        {
            return [
                'status' => 'warning',
                'message' => 'Fatura avulsa não tem ação com Assinaturas / Recorrências.'
            ];
        }
        
    }
}
