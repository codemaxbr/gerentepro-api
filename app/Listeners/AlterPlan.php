<?php

namespace App\Listeners;

use App\Events\UpdatePlanModule;

class alterPlan
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdatePlanModule  $event
     * @return void
     */
    public function handle(UpdatePlanModule $event)
    {
        $plan = $event->plan;
        $plugin = $event->module_account->module;
        $config_server = (object) unserialize($event->module_account->config);
        $server = $event->module_account->server;
        $params = $event->params;

        if($plugin != null){
            switch ($plugin->slug){
                case 'cpanel':      $serviceName = '\App\Services\Plugins\CpanelService'; break;
                case 'plesk':       $serviceName = '\App\Services\Plugins\PleskService'; break;
                case 'centovacast': $serviceName = '\App\Services\Plugins\CentovaCastService'; break;
                case 'whmsonic':    $serviceName = '\App\Services\Plugins\WhmsonicService'; break;
                case 'vestacp':     $serviceName = '\App\Services\Plugins\VestacpService'; break;

                default: $serviceName = '\App\Services\Plugins\\'.$plugin->name.'Service'; break;
            }

            $this->service = new $serviceName($plan, $server, $config_server, $params);
            $response = $this->service->updatePlan();

            return $response;

        }else{
            return (object) [
                'status' => 'success',
                'response' => null
            ];
        }
    }
}
