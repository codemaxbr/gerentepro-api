<?php
/**
 * Created by PhpStorm.
 * User: Lucas Maia
 * Date: 12/09/2018
 * Time: 12:31
 */

namespace App\Listeners;
use App\Events\CustomerRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class NotifyCustomerConfirmation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CustomerRegistered $event)
    {
        $customer = $event->customer;
        $account  = $customer->account;

        $email = new \App\Mail\CustomerRegistered($customer, $account);
        Mail::to($customer->email)->send($email);
    }
}