<?php

namespace App\Listeners;

use App\Events\ConfirmPayment;
use App\Events\LogActivity;
use App\Models\Statement;
use App\Models\Transaction;
use App\Models\User;
use App\Services\StatementService;
use Firebase\JWT\JWT;
use Illuminate\Database\QueryException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class CreateStatement
{


    /**
     * @var StatementService
     */
    private $statementRepository;
    /**
     * @var StatementService
     */
    private $statementService;

    public function __construct(StatementService $statementService)
    {
        $this->statementService = $statementService;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $invoice = $event->invoice;
        $request = $event->request;
        $user = null;

        if(boolval($token = app('request')->bearerToken())){

            //Verifica se existe um cache de Autenticação de Usuário
            if(Cache::has($token)){
                $user = Cache::get($token);
            }else{
                try{
                    $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
                    if($credentials->iss == "lumen-jwt"){
                        $user = User::find($credentials->sub);
                    }

                }catch (\Exception $e){
                    $user = null;
                }
            }
        }

        $event->user = $user;
        $description = 'Pagamento da Cobrança Nº '.$invoice->id;

        switch ($invoice->type_invoice->name)
        {
            case 'Pedido':
                $description = 'Pagamento da Cobrança Nº '.$invoice->id.' (Assinatura)';
                $type = 'credito';
            break;

            case 'Recorrência':
                $description = 'Pagamento da Cobrança Nº '.$invoice->id.' (Recorrência)';
                $type = 'credito';
            break;

            case 'Pagamento':
                $description = 'Pagamento da Cobrança Nº '.$invoice->id.' (Pagamento / Estorno)';
                $type = 'debito';
            break;

            case 'Avulsa':
                $description = 'Pagamento da Cobrança Nº '.$invoice->id.' (Avulso)';
                $type = 'credito';
            break;
        }

        if($invoice->statement == null)
        {
            $create = [
                'name'            => $description,
                'customer_id'     => $invoice->customer_id,
                'total'           => $invoice->total,
                'type'            => $type,
                'type_payment_id' => $request->type_payment_id,
                'invoice_id'      => $invoice->id,
                'type_invoice_id' => $invoice->type_invoice->id,
                'account_id'      => $invoice->account_id,
                'user_id'         => (!is_null($user)) ? $user->id : null
            ];

            try{

                $invoice->status_id = (is_null($user)) ? 5 : 6;
                $invoice->user_id   = (!is_null($user)) ? $user->id : null;

                if(isset($request->obs)){
                    $invoice->obs = $request->obs;
                }
                $invoice->save();

                /** @noinspection PhpInconsistentReturnPointsInspection */
                return Statement::create($create);

            }catch (\Exception $e){
                ErrorReport(serialize($invoice), $e, 'Listener CreateStatement', __FILE__, 500);
                return $e;
            }
        }else{
            return $invoice->statement;
        }
    }
}
