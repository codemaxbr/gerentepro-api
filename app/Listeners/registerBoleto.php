<?php

namespace App\Listeners;

use App\Services\ModuleService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class registerBoleto
{
    private $service;
    /**
     * @var ModuleService
     */
    private $moduleService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(ModuleService $moduleService)
    {
        $this->moduleService = $moduleService;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $invoice    = $event->invoice;
        $customer   = $event->customer;
        $request    = $event->request;

        $module     = $this->moduleService->getModuleBySlug($event->module, $invoice->account_id);
        $plugin_res = $this->moduleService->getConfigsByModule($module->id, $invoice->account_id);

        $plugin     = (object) $plugin_res[0];

        $cart       = null;
        $total      = null;

        $serviceName = '\App\Services\Plugins\\'.$module->slug.'Service';
        $this->service = new $serviceName($customer, $invoice, $cart, $total, $request, $plugin);

        $responseGateway = $this->service->registerBoleto();

        $event->transaction = $responseGateway->transaction;
        return $responseGateway;
    }
}
