<?php

namespace App\Listeners;

use App\Models\Account;
use App\Models\Invoice;
use App\Models\Plan;
use App\Models\Price;
use App\Services\InvoiceService;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;

class CreateInvoice
{
    /**
     * @var InvoiceService
     */
    private $invoiceService;

    /**
     * Create the event listener.
     *
     * @param InvoiceService $invoiceService
     */
    public function __construct(InvoiceService $invoiceService)
    {
        $this->invoiceService = $invoiceService;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $customer   = $event->customer;
        $request    = $event->request;
        $module     = $event->module;

        $inv = array(
            'customer_id'       => $customer->id,
            'type_invoice_id'   => 2,
            'due'               => Carbon::now()->addDays(5)->format('Y-m-d'),
            'account_id'        => $customer->account_id,
            'total'             => $request->total,
            'uuid'              => Uuid::generate(4)->string,
            'status_id'         => 1
        );

        DB::beginTransaction();

        $invoice = Invoice::create($inv);

        $plan    = Plan::find($request->plan_id);
        $price   = Price::find($request->price_id);
        $domain  = $request->domain;

        $invoice->invoice_items()->create([
            'plan_id'   => $plan->id,
            'price_id'  => $price->id,
            'domain'    => $domain,
            'price'     => $price->price,
            'type'      => 'plan',
            'months'    => $price->payment_cycle->months
        ]);

        if($request->register == 'true') {
            $invoice->invoice_items()->create([
                'description'  => 'Registro de domínio',
                'domain'    => $domain,
                'price'     => 40.00, //todo colocar o preço de Preços de domínio do sistema.
                'type'      => 'domain',
                'months'    => 12
            ]);
        }

        //todo incluir os addons para geração da cobrança
        DB::commit();

        return $event->invoice = $invoice;
    }
}
