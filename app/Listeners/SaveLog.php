<?php

namespace App\Listeners;

use App\Events\LogActivity;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SaveLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(LogActivity $event)
    {
        $model      = $event->model;
        $action    = $event->action;
        $user       = $event->user;
        $customer   = $event->customer;

        $model->logs()->create([
            'action' => $action,
            'account_id' => AccountId(),
            'user_id' => ($user != null) ? $user : null,
            'customer_id' => ($customer != null) ? $customer : null
        ]);

    }
}
