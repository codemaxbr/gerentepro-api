<?php

namespace App\Listeners;

use App\Events\ConfirmPayment;
use App\Models\Transaction;
use App\Services\InvoiceService;
use App\Services\ModuleService;
use App\Services\TransactionService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MakeTransaction
{
    /**
     * @var TransactionService
     */
    private $transactionService;
    /**
     * @var InvoiceService
     */
    private $invoiceService;
    /**
     * @var ModuleService
     */
    private $moduleService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(TransactionService $transactionService, InvoiceService $invoiceService, ModuleService $moduleService)
    {
        $this->transactionService = $transactionService;
        $this->invoiceService = $invoiceService;
        $this->moduleService = $moduleService;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $module      = $event->module;
        $customer    = $event->customer;
        $transaction = $event->transaction;
        $invoice     = $event->invoice;

        $module     = $this->moduleService->getModuleBySlug($event->module, $invoice->account_id);

        if($transaction != null){

            $transactionDB = Transaction::create([
                'invoice_id'    => $invoice->id,
                'customer_id'   => $customer->id,
                'total'         => $invoice->total,
                'module_id'     => $module->id,
                'type_payment_id' => $transaction->type_payment_id,
                'external_id'   => $transaction->id,
                'status_id'     => $transaction->status,
                'url'           => $transaction->url,
                'brand'         => @$transaction->brand,
                'final_card'    => @$transaction->card_number,
                'account_id'    => $invoice->account->id,
                'response'      => serialize($transaction->data)
            ]);

            return $transactionDB;
        }
    }
}
