<?php

namespace App\Listeners;

use App\Models\CreditCard;
use App\Services\ModuleService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class registerCartao
{
    private $service;
    /**
     * @var ModuleService
     */
    private $moduleService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(ModuleService $moduleService)
    {
        $this->moduleService = $moduleService;
    }

    private function checkCartao($request, $customer)
    {
        if(isset($request->payment_profile) && $request->payment_profile != "new"){
            return $request->payment_profile;
        }else{
            $cartao = valida_cartao($request->card_number);
            $n_cartao = limpaNumeros($request->card_number);

            if($cartao[1]){
                $card = [
                    'flag' => $cartao[0],
                    'start_number' => substr($n_cartao, 0, 6),
                    'final_number' => substr($n_cartao, -4),
                    'owner' => $request->card_owner,
                    'expires' => $request->card_exp_month."/".$request->card_exp_year,
                    'customer_id' => $customer->id,
                    'account_id' => $customer->account->id
                ];

                return CreditCard::query()->where(['start_number' => substr($n_cartao, 0, 6), 'final_number' => substr($n_cartao, -4)])->firstOrCreate($card)->payment_profile_id;
            }
        }

    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $invoice    = $event->invoice;
        $customer   = $event->customer;
        $request    = $event->request;

        $module     = $this->moduleService->getModuleBySlug($event->module, $invoice->account_id);
        $plugin_res = $this->moduleService->getConfigsByModule($module->id, $invoice->account_id);

        $plugin     = (object) $plugin_res[0];

        $cart       = null;
        $total      = null;

        $serviceName = '\App\Services\Plugins\\'.$module->slug.'Service';
        $this->service = new $serviceName($customer, $invoice, $cart, $total, $request, $plugin);

        //$payment_profile = $this->checkCartao($request, $customer);
        $responseGateway = $this->service->registerCartao($request);

        $event->transaction = $responseGateway->transaction;
        return $responseGateway;
    }
}
