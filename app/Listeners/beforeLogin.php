<?php
/**
 * Desenvolvido por: Lucas Maia - lucas.codemax@gmail.com
 * WhatsApp: (21) 96438-6937
 *
 * Criado em: 07/10/19 22:01
 * Projeto: gerentepro-api
 */

namespace App\Listeners;

use App\Models\User;
use App\Models\Webhook;
use Illuminate\Auth\Events\Attempting;
use Illuminate\Support\Facades\DB;

class beforeLogin
{
    public function __construct()
    {
        //
    }

    /**
     * @param Attempting $event
     */
    public function handle(Attempting $event)
    {
        $user = User::whereEmail($event->credentials['email'])->first();
        $webhook = Webhook::where(['listener' => __CLASS__, 'account_id' => $user->account_id])->get();

        if($webhook->isNotEmpty())
        {
            /** @var Webhook $hook */
            foreach ($webhook->all() as $hook) {
                // ao invés de rodar o webhook direto, faz o agendamento do serviço de filas (jobs)
                // @todo terminar isso.
                //dump($hook);
            }
        }
    }
}