<?php

namespace App\Listeners;

use App\Events\ValidateModule;
use App\Models\Server;

class ValidateConfig
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ValidateModule  $event
     * @return void
     */
    public function handle(ValidateModule $event)
    {
        $module  = $event->module;
        $request = $event->request;

        if($request->has('server_id'))
        {
            $server = Server::find($request->server_id);
        }
    }
}
