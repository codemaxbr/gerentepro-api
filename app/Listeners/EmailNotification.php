<?php

namespace App\Listeners;

use App\Events\SendEmail;
use App\Mail\InvoiceRemember;
use Illuminate\Support\Facades\Mail;

class EmailNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendEmail  $event
     * @return void
     */
    public function handle(SendEmail $event)
    {
        $user_id = $event->user_id;
        $model = $event->model;
        $to = $event->to;
        $template = $event->template;

        switch ($template):
            case 'invoice_remember': Mail::to($to)->send(new InvoiceRemember($model));
        endswitch;
    }
}
