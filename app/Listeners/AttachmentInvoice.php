<?php

namespace App\Listeners;

use App\Events\ConfirmPayment;
use App\Models\Invoice;
use Illuminate\Database\QueryException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Webpatser\Uuid\Uuid;

class AttachmentInvoice
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object $event
     * @return void
     * @throws \Exception
     */
    public function handle($event)
    {
        $user = $event->user;
        $invoice = $event->invoice;
        $request = $event->request;
        $uuid = Uuid::generate(4);

        try{
            $anexo = $request->file('receipt');
            $imageFileName = 'comprovante_fatura_'.$invoice->id.'.'.$anexo->getClientOriginalExtension();

            if($invoice->attachment == null){
                $s3 = Storage::disk('gcs');
                if(!$s3->exists('/anexos')){
                    $s3->makeDirectory('/anexos/');
                }
                $filePath = '/anexos/'.$imageFileName;
                $s3->put($filePath, fopen($anexo, 'r+'), 'public');

                if($s3->exists($filePath)){
                    $anexo = $invoice->attachment()->create([
                        'name' => 'Comprovante de pagamento - Fatura Nº '.$invoice->id,
                        'uuid' => Uuid::generate(4)->string,
                        'account_id' => $invoice->account_id,
                        'file_url' => $s3->url($filePath)
                    ]);

                    return $anexo;
                }
            }else{
                return $invoice->attachment;
            }

        }catch (\Exception $e){
            if(QueryException::class){
                ErrorReport(serialize($invoice), $e, __FUNCTION__, __FILE__, 500);
            }
        }
    }
}
