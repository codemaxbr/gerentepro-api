<?php
/**
 * Desenvolvido por: Lucas Maia - lucas.codemax@gmail.com
 * WhatsApp: (21) 96438-6937
 *
 * Criado em: 07/10/19 22:02
 * Projeto: gerentepro-api
 */

namespace App\Listeners;

use Illuminate\Database\Events\QueryExecuted;

class DebugDB
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function handle(QueryExecuted $event)
    {
        dd($event);
    }
}