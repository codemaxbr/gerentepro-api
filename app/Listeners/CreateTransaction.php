<?php

namespace App\Listeners;

use App\Events\ConfirmPayment;
use App\Models\Transaction;
use Firebase\JWT\JWT;
use Illuminate\Database\QueryException;

class CreateTransaction
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ConfirmPayment  $event
     * @return void
     */
    public function handle($event)
    {
        $invoice = $event->invoice;
        $request = $event->request;
        $module  = $event->module;

        try{
            $transaction = Transaction::create([
                'invoice_id'        => $invoice->id,
                'customer_id'       => $invoice->customer_id,
                'total'             => $invoice->total,
                'module_id'         => (!is_null($module)) ? $module->id : null,
                'external_id'       => (!is_null($module)) ? $request->external_id : null,
                'type_payment_id'   => $request->type_payment_id,
                'status_id'         => 5,
                'account_id'        => $invoice->account_id
            ]);

            return $transaction;
        }

        catch (\Exception $e){
            ErrorReport(serialize($invoice), $e, 'Listener CreateTransaction', __FILE__, 500);
        }
    }
}
