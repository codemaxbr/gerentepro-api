<?php

namespace App\Listeners;

use App\Events\TerminateSubscription;

class Terminate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TerminateSubscription  $event
     * @return void
     */
    public function handle(TerminateSubscription $event)
    {
        $subscription = $event->subscription;
        $plan = $event->plan;
    }
}
