<?php

namespace App\Listeners;

use App\Events\DomainRegistered;
use App\Services\DomainService;

class RegisterDomain
{
    /**
     * @var DomainService
     */
    private $domainService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(DomainService $domainService)
    {
        $this->domainService = $domainService;
    }

    /**
     * Handle the event.
     *
     * @param  DomainRegistered  $event
     * @return void
     */
    public function handle(DomainRegistered $event)
    {
        //todo estudar a integração com serviços de registro de domínio.
        dump('Registra o dominio no módulo registrante.');
    }
}
