<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Exception;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Cache;

class ResellerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->bearerToken();
        $credentials = JWT::decode($token, 'JhbGciOiJIU', ['HS256']);

        if($credentials->rsl){
            return $next($request);
        }else{
            return response()->json([
                'error' => 'Acesso restrito apenas para Revendedores'
            ], 400);
        }
    }
}
