<?php

namespace App\Http\Middleware;

use App\Models\Account;
use Closure;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Gerencianet\Auth;
use http\Client\Curl\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;

class JwtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->bearerToken();

        if(!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'error' => 'Token not provided.'
            ], Response::HTTP_UNAUTHORIZED);
        }
        try {
            $credentials = JWT::decode($token, 'JhbGciOiJIU', ['HS256']);

            if(Cache::has($token)){
                $user = Cache::get($token);
            }else{
                $user = \App\Models\User::find($credentials->sub);
            }

            if ($user->id == $credentials->sub && $token != $user->remember_token){

                //todo executar evento que reporta tentativa de ataque

                return response()->json([
                    'error' => 'Tentativa XSS Cross-Site Scripting detectado. [xss]'
                ], Response::HTTP_UNAUTHORIZED);
            }

            if($credentials->iss == "lumen-jwt")
            {
                $account_id = $user->account->id;

                if(!Cache::has('account_'.$account_id)){
                    Cache::rememberForever('account_'.$account_id, function () use ($user){
                        return $user->account;
                    });
                }
                return $next($request);
            }else{

                return response()->json([
                    'error' => 'Acesso restrito apenas para Administradores'
                ], Response::HTTP_FORBIDDEN);
            }

        } catch(ExpiredException $e) {
            return response()->json([
                'error' => 'O token expirou.'
            ], Response::HTTP_UNAUTHORIZED);

        } catch(Exception $e) {
            return response()->json([
                'error' => 'Não foi possível decodificar o token.',
                'exception' => $e->getMessage()
            ], Response::HTTP_UNAUTHORIZED);
        }
    }
}
