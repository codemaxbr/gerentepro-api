<?php

namespace App\Http\Middleware;

use App\Models\Customer;
use Closure;
use Firebase\JWT\ExpiredException;
use Exception;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Cache;

class CustomerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->bearerToken();

        if(!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'error' => 'Token not provided.'
            ], 401);
        }
        try {
            $credentials = JWT::decode($token, 'JhbGciOiJIU', ['HS256']);
            $customer_id = $credentials->sub;

            if($credentials->iss == "customer-jwt")
            {
                if(!Cache::has('customer_'.$customer_id)){
                    $customer = Customer::find($customer_id);

                    if($customer){
                        Cache::remember('customer_'.$customer_id, 120, function () use ($customer){
                            return $customer;
                        });
                    }
                }
                return $next($request);
            }else{
                return response()->json([
                    'error' => 'Acesso restrito apenas para Assinantes'
                ], 400);
            }

        } catch(ExpiredException $e) {
            return response()->json([
                'error' => 'Provided token is expired.'
            ], 400);
        } catch(Exception $e) {
            return response()->json([
                'error' => 'An error while decoding token.'
            ], 400);
        }
    }
}
