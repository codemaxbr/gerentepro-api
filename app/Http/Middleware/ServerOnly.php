<?php

namespace App\Http\Middleware;

use Closure;

class ServerOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Pre-Middleware Action
        //dd($request->ip());
        dd($request->server());

        $response = $next($request);

        // Post-Middleware Action

        return $response;
    }
}
