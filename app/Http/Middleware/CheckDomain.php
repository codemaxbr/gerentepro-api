<?php

namespace App\Http\Middleware;

use App\Models\Account;
use Closure;
use Illuminate\Support\Facades\Cache;

//use Illuminate\Support\Facades\Cache;

class CheckDomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $subdomain = $request->segment(2);

        // Extract the subdomain from URL.
        if(!Cache::has('account_'.$subdomain)){
            $account = Account::query()->where(['domain' => $subdomain])->firstOrFail();
            if($account){
                Cache::rememberForever('account_'.$subdomain, function () use ($subdomain, $account){
                    return $account;
                });
            }
        }

        return $next($request);
    }
}
