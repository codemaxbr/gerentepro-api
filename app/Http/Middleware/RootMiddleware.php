<?php

namespace App\Http\Middleware;

use Closure;
use Firebase\JWT\JWT;

class RootMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->bearerToken();
        $credentials = JWT::decode($token, 'JhbGciOiJIU', ['HS256']);

        if($credentials->rsl && $credentials->acc == 1){
            return $next($request);
        }else{
            return response()->json([
                'error' => 'Acesso restrito apenas ao Administrador (Root)'
            ], 400);
        }
    }
}
