<?php

namespace App\Http\Controllers;

use App\Events\Event;
use App\Models\Invoice;
use App\Services\InvoiceService;
use App\Services\TransactionService;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Validator;
use function PHPSTORM_META\type;
use Webpatser\Uuid\Uuid;

class InvoiceController extends Controller
{
    //
    /**
     * @var InvoiceService
     */
    private $invoiceService;
    /**
     * @var TransactionService
     */
    private $transactionService;


    /**
     * InvoiceController constructor.
     */
    public function __construct(InvoiceService $invoiceService, TransactionService $transactionService)
    {
        $this->invoiceService = $invoiceService;
        $this->transactionService = $transactionService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|mixed
     */
    public function index(Request $request)
    {
        if($request->all() == null){
            /**
             * Retorna todas as contas registradas
             */
            $resume = $this->invoiceService->getResume();
            $result = collect($this->invoiceService->getInvoices());

            $data = $result->merge($resume);

            //$data->$resume;

            return $data;

        }else{
            if(isset($request->s)){
                /**
                 * Retorna uma busca por nome
                 */
                return $this->invoiceService->getAll_search($request->s);
            }else{

                /**
                 * Retorna uma busca por filtro
                 */

                /*todo verificar se isso ta funcionando */
                $invoices = Invoice::with(['customer:id,name,email,uuid', 'status_invoice', 'type_invoice']);
                foreach ($request->except(['page']) as $key => $value)
                {
                    $invoices->where($key, $value);
                }

                $invoices->orderBy('id', 'desc');
                return $invoices->paginate(20);
            }
        }
    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        $invoice = $this->invoiceService->getInvoice($uuid);

        if($invoice == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Esta fatura não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        return $invoice;
    }

    public function transactions($id)
    {
        return $this->transactionService->getTransactionsInvoice($id);
    }

    public function remember($uuid, Request $request)
    {
        return $this->invoiceService->sendRemember($uuid, $request);
    }

    public function export()
    {
        return $this->invoiceService->allInvoices();
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function create(Request $request)
    {
        return $this->invoiceService->createInvoice($request);
    }

    /**
     * @param $uuid
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePaid($uuid, Request $request)
    {
        $invoice = $this->invoiceService->getInvoice($uuid);

        if($invoice == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Esta fatura não existe na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        $validator = Validator::make($request->all(), [
            'type_payment_id'   => 'required | integer | exists:type_payments,id',
            'receipt'           => 'required | file'
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else {
            if($invoice->status_id != 5 && $invoice->status_id != 6)
            {
                $result = $this->invoiceService->changePaid($invoice, $request, $this);

                if(isset($result->invoice)){
                    return response()->json([
                        'status'    => 'success',
                        'msg'       => 'A fatura de Nº'.$invoice->id.' foi paga com sucesso!',
                        'data'      => [
                            'invoice'       => $this->invoiceService->getInvoice($invoice->uuid),
                        ]
                    ], Response::HTTP_OK);
                }else{
                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.',
                            'exception' => $result
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }
            }else{
                return response()->json([
                    'status'    => 'success',
                    'msg'       => 'A fatura de Nº'.$invoice->id.' já está paga!',
                    'data'      => [
                        'invoice'       => $this->invoiceService->getInvoice($invoice->uuid),
                    ]
                ], Response::HTTP_OK);
            }
        }
    }

    public function changeDue($uuid, Request $request)
    {
        return $this->invoiceService->changeDue($request, $uuid);
    }

    public function cancel($uuid)
    {
        return $this->invoiceService->cancelInvoice($uuid);
    }

    /**
     * @param $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($uuid)
    {
        return $this->invoiceService->remove($uuid);
    }

    /**
     * @param $uuid
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function nulled($uuid, Request $request)
    {
        return $this->invoiceService->changeNull($uuid, $request);
    }

    /**
     * @param $uuid
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($uuid, Request $request)
    {
        return $this->invoiceService->update($uuid, $request);
    }
}
