<?php

namespace App\Http\Controllers;

use App\Models\Provider;
use App\Services\ProviderService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProviderController extends Controller
{
    /**
     * @var ProviderService
     */
    private $providerService;

    /**
     * ProviderController constructor.
     */
    public function __construct(ProviderService $providerService)
    {
        $this->providerService = $providerService;
    }

    /**
     * @param Request $request
     */
    public function index(Request $request)
    {
        if($request->all() == null){
            /**
             * Retorna todos os fornecedores
             */
            return $this->providerService->allProviders();
        }else{
            if(isset($request->s)){
                /**
                 * Retorna uma busca por nome
                 */
                return $this->providerService->getAll_search($request->s);
            }else{

                /**
                 * Retorna uma busca por filtro
                 */
                $providers = Provider::withCount(['servers', 'debits' => function($query){
                    $query->whereMonth('due', '=', Carbon::now()->format('m')); // Contar apenas os que tiver Conta para pagar no mês atual
                    $query->where('paid', '=', null);
                }])
                ->whereHas('account', function ($q){
                    $q->where('id', AccountId());
                });

                foreach ($request->except(['page']) as $key => $value)
                {
                    if($value != null || $value != ''){
                        $providers->where($key, 'LIKE', "%$value%");
                    }
                }

                $providers->orderBy('name', 'asc');
                return $providers->paginate(20);
            }
        }
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return $this->providerService->createProvider($request);
    }

    /**
     * @param $uuid
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function show($uuid)
    {
        return $this->providerService->getProvider($uuid);
    }


    /**
     * @param $id
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        return $this->providerService->updateProvider($id, $request);
    }

    /**
     * @param Request $request
     */
    public function destroy($id)
    {
        return $this->providerService->deleteProvider($id);
    }

    /**
     * @return int
     */
    public function count()
    {
        return $this->providerService->getTotal();
    }
}
