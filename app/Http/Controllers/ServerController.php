<?php

namespace App\Http\Controllers;

use App\Models\Server;
use App\Services\ServerService;
use Illuminate\Http\Request;

class ServerController extends Controller
{
    /**
     * @var ServerService
     */
    private $serverService;

    /**
     * ServerController constructor.
     */
    public function __construct(ServerService $serverService)
    {
        $this->serverService = $serverService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index(Request $request)
    {
        if($request->all() == null){
            /**
             * Retorna todos os servidores
             */
            return $this->serverService->allServers();
        }else{
            if(isset($request->s)){
                /**
                 * Retorna uma busca por nome
                 */
                return $this->serverService->getAll_search($request->s);
            }else{

                /**
                 * Retorna uma busca por filtro
                 */
                $servers = Server::query()
                    ->whereHas('account', function ($q){
                        $q->where('id', AccountId());
                    });

                foreach ($request->except(['page']) as $key => $value)
                {
                    $servers->where($key, $value);
                }

                $servers->orderBy('name', 'asc');
                return $servers->paginate(20);
            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return $this->serverService->createServer($request);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        return $this->serverService->updateServer($id, $request);
    }

    /**
     * @param $id
     * @return ServerService|ServerService[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|\Illuminate\Http\JsonResponse|null
     */
    public function show($id)
    {
        return $this->serverService->getServer($id);
    }

    /**
     * @param $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return $this->serverService->removeServer($id);
    }
}
