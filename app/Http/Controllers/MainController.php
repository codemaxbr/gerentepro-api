<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Module;
use App\Models\ModulesAccount;
use App\Services\EmailService;
use App\Services\InvoiceService;
use App\Services\ModuleService;
use App\Services\PlanService;
use App\Services\Plugins\NfeService;
use App\Services\StatementService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class MainController extends Controller
{
    /**
     * @var PlanService
     */
    private $planService;
    /**
     * @var ModuleService
     */
    private $moduleService;
    /**
     * @var InvoiceService
     */
    private $invoiceService;
    /**
     * @var StatementService
     */
    private $statementService;
    /**
     * @var EmailService
     */
    private $emailService;
    /**
     * @var NfeService
     */
    private $nfeService;

    /**
     * MainController constructor.
     * @param PlanService $planService
     * @param ModuleService $moduleService
     * @param InvoiceService $invoiceService
     * @param StatementService $statementService
     * @param EmailService $emailService
     */
    public function __construct(
        PlanService $planService,
        ModuleService $moduleService,
        InvoiceService $invoiceService,
        StatementService $statementService,
        EmailService $emailService,
        NfeService $nfeService
    )
    {
        $this->planService = $planService;
        $this->moduleService = $moduleService;
        $this->invoiceService = $invoiceService;
        $this->statementService = $statementService;
        $this->emailService = $emailService;
        $this->nfeService = $nfeService;
    }

    public function modulesAvailable()
    {
        $account = Account::with('modules', 'modules.type_module')->find(AccountId());
        return $account->modules;
    }

    public function filters()
    {
        return [
            'filters' => [
                'types' => [
                    'plans' => $this->planService->getTypes(),
                    'terms' => $this->planService->getTypeTerms(),
                    'modules' => $this->moduleService->getTypes(),
                    'invoices' => $this->invoiceService->getTypes(),
                    'payments' => $this->statementService->getTypes(),
                    'emails' => $this->emailService->getTypes(),
                    'customers' => [
                        ['slug' => 'fisica',   'name' => 'Pessoa Física',   'label' => 'CPF'],
                        ['slug' => 'juridica', 'name' => 'Pessoa Jurídica', 'label' => 'CNPJ'],
                    ]
                ],

                'payments' => [
                    'cycle' => $this->planService->getPaymentCycles(),
                ],

                'modules' => $this->moduleService->getModules(),
            ],
            'verifiers' => [
                'gateways' => [
                    'has_gateway' => $this->moduleService->hasGateway(),
                    'has_config' => $this->moduleService->hasConfig(),
                ],
            ]
        ];
    }

    public function consultaReceita(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required',
            'cpf_cnpj' => [
                'required',
                Rule::unique('customers')->where(function ($query) {
                    $query->where(['account_id' => AccountId()]);
                }),
            ],
            'birthdate' => 'required_if:type,fisica',
        ], [
            'required' => 'Este campo é obrigatório.',
            'required_if' => 'Este campo é obrigatório.',
            'cpf_cnpj.unique' => 'Já existe um cadastro com este CPF / CNPJ.'
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{

            if($request->type == 'fisica')
            {
                $cpf = limpaNumeros($request->cpf_cnpj);
                $nascimento = Carbon::createFromFormat('d/m/Y', $request->birthdate)->toDateString();

                $response = $this->nfeService->consultaCPF($cpf, $nascimento);
            }
            else {
                $cnpj = limpaNumeros($request->cpf_cnpj);
                $response = $this->nfeService->consultaCNPJ($cnpj);
            }
            // passou pela validação
            return \response()->json([
                'status' => 'success',
                'data' => $response
            ]);
        }
    }
}
