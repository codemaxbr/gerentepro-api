<?php

namespace App\Http\Controllers;

use App\Services\EmailService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TemplateEmailController extends Controller
{
    /**
     * @var EmailService
     */
    private $emailService;

    /**
     * TemplateEmailController constructor.
     * @param EmailService $emailService
     */
    public function __construct(EmailService $emailService)
    {
        $this->emailService = $emailService;
    }

    /**
     * @return array
     */
    public function index()
    {
        return $this->emailService->getTemplates();
    }

    public function show($id)
    {
        $template = $this->emailService->getTemplate($id);

        return \response()->json([
            'subject' => $template->subject,
            'content' => $template->template
        ]);
    }

    public function update($id, Request $request)
    {
        return $this->emailService->updateTemplate($id, $request);
    }

    public function signature()
    {
        return $this->emailService->getSignature();
    }

    public function updateSignature(Request $request)
    {
        return $this->emailService->updateSignature($request);
    }
}
