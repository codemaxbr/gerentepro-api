<?php

namespace App\Http\Controllers;

use App\Services\ResellerService;
use Illuminate\Http\Request;

class ResellerController extends Controller
{
    /**
     * @var ResellerService
     */
    private $resellerService;

    /**
     * ResellerController constructor.
     */
    public function __construct(ResellerService $resellerService)
    {
        $this->resellerService = $resellerService;
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index()
    {
        return $this->resellerService->getResellers();
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function show($id)
    {
        return $this->resellerService->getReseller($id);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return $this->resellerService->removeReseller($id);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return $this->resellerService->assignReseller($request);
    }
}
