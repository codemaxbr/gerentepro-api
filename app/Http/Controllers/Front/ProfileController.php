<?php

namespace App\Http\Controllers\Front;

use App\Models\Customer;
use Firebase\JWT\JWT;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ProfileController extends Controller
{

    /**
     * ProfileController constructor.
     */
    public function __construct()
    {
    }

    public function profile(Request $request)
    {
        $credentials = JWT::decode($request->bearerToken(), env('JWT_SECRET'), ['HS256']);
        $customer = Customer::find($credentials->sub);

        if($customer->address == null && $customer->zipcode == null && $customer->number == null && $customer->uf == null && $customer->city == null)
        {
            return \response()->json([
                'incomplete' => true,
                'data' => $customer
            ]);
        }else{
            return \response()->json([
                'incomplete' => false,
                'data' => $customer
            ]);
        }
    }

    public function updateProfile(Request $request)
    {
        $credentials = JWT::decode($request->bearerToken(), env('JWT_SECRET'), ['HS256']);

        $validator = Validator::make($request->all(), [
            'email' => [
                'required',
                'email',
                Rule::unique('customers')->where(function ($query) {
                    $query->where(['account_id' => front_AccountId()]);
                })->ignore($credentials->sub),
            ],
            'type' => 'required',
            'cpf_cnpj' => [
                'required',
                Rule::unique('customers')->where(function ($query) {
                    $query->where(['account_id' => front_AccountId()]);
                })->ignore($credentials->sub),
            ],
            'name' => 'required',
            'phone' => 'required',
            'mobile' => 'required',
            'genre' => 'required',
            'zipcode' => 'required',
            'address' => 'required',
            'number' => 'required',
            'uf' => 'required',
            'city' => 'required',
            'rg' => Rule::requiredIf($request->type == 'fisica'),
            'ins_municipal' => Rule::requiredIf($request->type == 'juridica'),
            'business' => Rule::requiredIf($request->type == 'juridica'),
            'birthdate' => [
                'date',
                Rule::requiredIf($request->type == 'fisica')
            ],
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{
            try{
                $customer = Customer::find($credentials->sub);

                foreach ($request->all() as $input => $value)
                {
                    $customer->$input = $value;
                }

                $customer->update($request->all());

                return response()->json([
                    'status' => 'success',
                    'message' => 'Cadastro atualizado com sucesso.',
                    'data' => [
                        'customer' => $customer
                    ]
                ]);
            }catch (\Exception $e)
            {
                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }
}
