<?php

namespace App\Http\Controllers\Front;

use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CheckoutController extends Controller
{

    /**
     * CheckoutController constructor.
     */
    public function __construct()
    {
    }

    public function processaPagamento(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required | exists:customers,id',
            'domain'      => 'required_with:register',
            'plan_id'     => 'required | exists:plans,id',
            'price_id'    => 'required | exists:prices,id',
            'method'      => 'required',
            'module'      => 'required',
            'total'       => 'required | numeric',
            'card_number' => 'required_if:method,cartao',
            'card_cvv'    => 'required_if:method,cartao',
            'card_exp_month' => 'required_if:method,cartao',
            'card_exp_year'  => 'required_if:method,cartao',
            'card_brand'     => 'required_if:method,cartao',
            'card_owner'     => 'required_if:method,cartao'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors());
        }else{
            $customer = Customer::find($request->customer_id);

            $event = '\App\Events\\'.$request->method.'Payment';
            $responsePayment = Event::dispatch(new $event($request->module, $request, $customer));

            if(!is_null($responsePayment[1])){
                return response()->json(['status' => 'success', 'data' => $responsePayment]);
            }else{
                return response()->json(['status' => 'failed', 'data' => $responsePayment]);
            }
        }
    }

    public function checkField(Request $request)
    {
        $customer = Customer::query()->where($request->all())->get();
        return response()->json($customer->isEmpty());
    }
}
