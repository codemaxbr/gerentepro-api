<?php

namespace App\Http\Controllers\Front;

use App\Models\Plan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlanController extends Controller
{

    /**
     * PlanController constructor.
     */
    public function __construct()
    {
    }

    public function show($uuid)
    {
        $plan = Plan::with(['grids', 'prices', 'prices.payment_cycle'])->where(['uuid' => $uuid])->firstOrFail();
        return $plan;
    }
}
