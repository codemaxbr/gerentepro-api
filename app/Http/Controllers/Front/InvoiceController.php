<?php

namespace App\Http\Controllers\Front;

use App\Models\Customer;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class InvoiceController extends Controller
{
    /**
     * @var Request
     */
    private $request;
    private $customer;

    /**
     * InvoiceController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $credentials = JWT::decode($request->bearerToken(), env('JWT_SECRET'), ['HS256']);
        $this->customer = Customer::find($credentials->sub);
    }

    public function index()
    {
        return $this->customer->invoices()->paginate(20);
    }

    public function show($uuid)
    {
        $invoice = $this->customer->invoices()->where(['uuid' => $uuid])->first();
        if($invoice == null)
        {
            return response()->json([
                'error' => [
                    'message' => 'Não existe esta fatura na base de dados.'
                ]
            ], Response::HTTP_NOT_FOUND);
        }

        return $invoice;
    }
}
