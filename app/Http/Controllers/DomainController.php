<?php

namespace App\Http\Controllers;

use App\Models\Domain;
use App\Services\DomainService;
use Illuminate\Http\Request;

class DomainController extends Controller
{
    /**
     * @var DomainService
     */
    private $domainService;

    /**
     * DomainController constructor.
     * @param DomainService $domainService
     */
    public function __construct(DomainService $domainService)
    {
        $this->domainService = $domainService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index(Request $request)
    {
        if($request->all() == null){
            /**
             * Retorna todos os dominios registrados
             */
            return $this->domainService->getDomains();
        }else{
            if(isset($request->s)){
                /**
                 * Retorna uma busca por domínio
                 */
                return $this->domainService->getAll_search($request->s);
            }else{

                /**
                 * Retorna uma busca por filtro
                 */
                $domains = Domain::query()
                    ->whereHas('account', function ($q){
                        $q->where('id', AccountId());
                    });

                foreach ($request->except(['page']) as $key => $value)
                {
                    $domains->where($key, $value);
                }

                $domains->orderBy('name', 'asc');
                return $domains->paginate(20);
            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return $this->domainService->createDomain($request);
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function prices()
    {
        return $this->domainService->getPriceDomains();
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        return $this->domainService->updateDomain($id, $request);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return $this->domainService->getDomain($id);
    }

    /**
     * @param $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return $this->domainService->removeDomain($id);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storePrice(Request $request)
    {
        return $this->domainService->createPrice($request);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePrice($id, Request $request)
    {
        return $this->domainService->updatePrice($id, $request);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showPrice($id)
    {
        return $this->domainService->getPrice($id);
    }

    /**
     * @param $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function destroyPrice($id)
    {
        return $this->domainService->removePrice($id);
    }

    /**
     * @param $domain
     * @param $tld
     * @return \Illuminate\Http\JsonResponse
     */
    public function whois(Request $request)
    {
        $domain = $request->domain.'.'.$request->tld;
        return $this->domainService->whois($domain);
    }
}
