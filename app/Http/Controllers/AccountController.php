<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Services\AccountService;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Webpatser\Uuid\Uuid;

class AccountController extends Controller
{
    /**
     * @var AccountService
     */
    private $accountService;

    /**
     * AccountController constructor.
     * @param AccountService $accountService
     */
    public function __construct(AccountService $accountService)
    {
        $this->accountService = $accountService;
    }

    /**
     * Listar todas as contas (somente para revendedor)
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index(Request $request)
    {
        if($request->all() == null){
            /**
             * Retorna todas as contas registradas
             */
            return $this->accountService->getAccounts();
        }else{
            if(isset($request->s)){
                /**
                 * Retorna uma busca por nome
                 */
                return $this->accountService->getAll_search($request->s);
            }else{

                /**
                 * Retorna uma busca por filtro
                 */
                $accounts = Account::query();
                foreach ($request->except(['page']) as $key => $value)
                {
                    $accounts->where($key, $value);
                }

                $accounts->orderBy('name_business', 'asc');
                return $accounts->paginate(20);
            }
        }
    }

    /**
     * Mostrar detalhes de uma conta
     *
     * @param $uuid
     * @return AccountService|\Illuminate\Database\Eloquent\Model|\Illuminate\Http\JsonResponse|null|object
     */
    public function info($uuid)
    {
        try{
            $account = $this->accountService->getAccount($uuid);
            return $account;

        }catch (\Exception $e){

            return response()->json([
                'error' => [
                    'message' => $e->getMessage(),
                    'code' => $e->getCode()
                ]
            ], Response::HTTP_NOT_FOUND);
            //ErrorReport($uuid, $e, __FUNCTION__, __FILE__, );
        }
    }

    /**
     * Atualizar dados de uma conta
     *
     * @param $uuid
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update($uuid, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'domain' => [
                'required',
                Rule::unique('accounts')->ignore($uuid, 'uuid'),
            ],
            'reseller_id' => 'integer | exists:resellers,id',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else
        {
            try{
                if ($request->hasFile('logo'))
                {
                    $logo       = $request->file('logo');
                    $logoFile   = $uuid.'.'.$logo->getClientOriginalExtension();
                    $bucket     = Storage::disk('gcs');

                    if (!$bucket->exists('/logos')){
                        $bucket->makeDirectory('/logos/');
                    }

                    $filePath = '/logos/'.$logoFile;
                    $bucket->put($filePath, fopen($logo, 'r+'), 'public');

                    if($bucket->exists($filePath)){
                        $logo_url = $bucket->url($filePath);
                    }
                }

                $dados = $request->all();
                if(isset($logo_url)){
                    $dados['logo'] = $logo_url.'?v='.randomDigit(4);
                }

                $account = $this->accountService->updateAccount($uuid, $dados);

                return response()->json([
                    'status' => 'success',
                    'message' => 'Dados da conta atualizados com sucesso.',
                    'data' => $account
                ], Response::HTTP_OK);

            }catch (\Exception $e)
            {
                return response()->json([
                    'error' => [
                        'message' => $e->getMessage(),
                        'code' => $e->getCode()
                    ]
                ], Response::HTTP_BAD_GATEWAY);
            }
        }
    }

    /*
    |-------------------------------------------------------------------------------
    | Registrar uma nova conta
    |-------------------------------------------------------------------------------
    | URL:            /accounts/register
    | Method:         POST
    |
    | @param Request $request
    | @return \Illuminate\Http\JsonResponse
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'domain'            => 'required | unique:accounts',
            'reseller_id'       => 'integer | exists:resellers,id',
            'name_business'     => 'required | string | max:191',
            'name'              => 'required | string | max:191',
            'email'             => 'required | string | email | max:191 | unique:users',
            'account_plan_id'   => 'integer | exists:account_plans,id'
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{

            try{
                $account = $this->accountService->newAccount($request->all());

                return response()->json([
                    'status' => 'success',
                    'message' => 'Conta criada com sucesso.',
                    'data' => [
                        'account' => $account,
                        'user' => $account->users()->first()
                    ],
                ], Response::HTTP_OK);

            }catch (\Exception $e){
                return response()->json([
                    'error' => [
                        'message' => $e->getMessage(),
                        'code' => $e->getCode()
                    ]
                ], Response::HTTP_BAD_REQUEST);
            }
        }
    }

    /**
     * Remover uma conta
     *
     * @param $uuid
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function destroy($uuid)
    {
        return $this->accountService->removeAccount($uuid);
    }

    /**
     * Ativar uma conta
     *
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    public function activation($token)
    {
        return $this->accountService->activate($token);
    }

    /*
    |-------------------------------------------------------------------------------
    | Buscar uma conta pelo subdomínio
    |-------------------------------------------------------------------------------
    | URL:            /accounts/find/<domain>
    | Method:         GET
    |
    | @return \Illuminate\Http\JsonResponse
    */
    public function getAccount($subdomain)
    {
        $account = $this->accountService->getAccountByDomain($subdomain);
        if($account == null){
            return response()->json([
                'error' => [
                    'message' => 'Essa conta não existe no banco de dados.'
                ]
            ]);
        }else{
            return response()->json($account);
        }
    }

    /*
    |-------------------------------------------------------------------------------
    | Retorna todos os scripts js do formulário de assinatura
    |-------------------------------------------------------------------------------
    | URL:            /accounts/<account_uuid>/scripts
    | Method:         GET
    |
    | @return \Illuminate\Http\JsonResponse
    */
    public function getScripts($uuid)
    {
        return $this->accountService->getScriptsJS($uuid);
    }
}
