<?php

namespace App\Http\Controllers;

use App\Services\ModuleService;
use Illuminate\Http\Request;

class ModuleController extends Controller
{
    /**
     * @var ModuleService
     */
    private $moduleService;

    /**
     * ModuleController constructor.
     */
    public function __construct(ModuleService $moduleService)
    {
        $this->moduleService = $moduleService;
    }

    /**
     * @return ModuleService[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return $this->moduleService->getConfigs();
    }

    public function info($slug)
    {
        return $this->moduleService->getModuleBySlug($slug);
    }

    public function getModule($id)
    {
        return $this->moduleService->getConfigsByModule($id);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return $this->moduleService->createConfig($request);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        return $this->moduleService->updateConfig($id, $request);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return $this->moduleService->removeConfig($id);
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public function show($id)
    {
        return $this->moduleService->getConfig($id);
    }
}
