<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * UserController constructor.
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index(Request $request)
    {
        if($request->all() == null){
            /**
             * Retorna todos os clientes
             */
            return $this->userService->getUsers();
        }else{
            if(isset($request->s)){
                /**
                 * Retorna uma busca por nome
                 */
                return $this->userService->getAll_search($request->s);
            }else{

                /**
                 * Retorna uma busca por filtro
                 */
                $users = User::query()
                    ->whereHas('account', function ($q){
                        $q->where('id', AccountId());
                    });

                foreach ($request->except(['page']) as $key => $value)
                {
                    $users->where($key, $value);
                }

                $users->orderBy('name', 'asc');
                return $users->paginate(20);
            }
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return $this->userService->getUser($id);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        return $this->userService->updateUser($id, $request);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return $this->userService->createUser($request);
    }

    /**
     * @param $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return $this->userService->removeUser($id);
    }
}
