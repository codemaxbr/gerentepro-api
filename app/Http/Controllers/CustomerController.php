<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Customer;
use App\Services\AccountService;
use App\Services\CustomerService;
use Carbon\Carbon;
use cPanel\cPanel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Plesk\Plesk;
use VestaCP\VestaCP;

class CustomerController extends Controller
{
    /**
     * @var CustomerService
     */
    private $customerService;
    /**
     * @var AccountService
     */
    private $accountService;

    /**
     * CustomerController constructor.
     */
    public function __construct(
        CustomerService $customerService,
        AccountService $accountService
    )
    {
        $this->customerService = $customerService;
        $this->accountService = $accountService;
    }

    public function search()
    {
        $result = Customer::with('subscriptions')
            ->where(['account_id' => AccountId()])->orderBy('name', 'asc');
        return $result->paginate(7);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index(Request $request)
    {
        if($request->all() == null){
            /**
             * Retorna todos os clientes
             */
            return $this->customerService->getCustomers();
        }else{
            if(isset($request->s)){
                /**
                 * Retorna uma busca por nome
                 */
                return $this->customerService->getAll_search($request->s);
            }else{

                /**
                 * Retorna uma busca por filtro
                 */
                $customers = Customer::with('subscriptions')
                    ->whereHas('account', function ($q){
                        $q->where('id', AccountId());
                    });

                foreach ($request->except(['page']) as $key => $value)
                {
                    if($value != null || $value != ''){
                        $customers->where($key, 'LIKE', "%$value%");
                    }
                }

                $customers->orderBy('name', 'asc');
                return $customers->paginate(20);
            }
        }
    }

    public function export()
    {
        return $this->customerService->getAll_export();
    }

    /**
     * @param $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($uuid)
    {
        return $this->customerService->getCustomer($uuid);
    }

    public function subscriptions($id)
    {
        return $this->customerService->getSubscriptions($id);
    }

    public function validaUnique(Request $request)
    {
        return $this->customerService->uniqueEmail($request->email);
    }

    public function validaUniqueCPF(Request $request)
    {
        return $this->customerService->uniqueCPF($request->cpf_cnpj);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function count()
    {
        return $this->customerService->getTotal();
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        return $this->customerService->updateCustomer($id, $request);
    }

    public function updateAddress($id, Request $request)
    {
        return $this->customerService->updateAddress($id, $request);
    }

    /**
     * @param $uuid
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function destroy($uuid)
    {
        return $this->customerService->deleteCustomer($uuid);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return $this->customerService->createCustomer($request);
    }
}
