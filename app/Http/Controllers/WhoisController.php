<?php

namespace App\Http\Controllers;

use App\Services\WhoisService;
use Illuminate\Http\Request;

class WhoisController extends Controller
{
    /**
     * @var WhoisService
     */
    private $whoisService;

    /**
     * WhoisController constructor.
     * @param WhoisService $whoisService
     */
    public function __construct(WhoisService $whoisService)
    {
        $this->whoisService = $whoisService;
    }

    public function whois(Request $request)
    {
        if(!empty($request->domain))
        {
            $domain = trim($request->domain);

            $dot = strpos($domain, '.');
            $dominio = substr($domain, 0, $dot);
            $extensao = substr($domain, $dot+1);

            if($whois = $this->get_whois($dominio, $extensao)){

                $array = explode("\n", $whois);

                $regra = '';
                switch($extensao){
                    case 'cc':
                    case 'tv': $regra = substr($array[5], 0, 12) == "No match for"; break;

                    case 'edu':
                    case 'net':

                    case 'com': $regra = substr($array[0], 0, 12) == "No match for"; break;
                    case 'ws': $regra = substr($array[7], 0, 12) == "No match for"; break;
                    case 'org':
                    case 'in':
                    case 'info':
                    case 'mobi':
                    case 'me':
                    case 'xxx': $regra = substr($array[0], 0, 9) == "NOT FOUND"; break;
                    case 'us':
                    case 'co':
                    case 'biz': $regra = substr($array[0], 0, 9) == "Not found"; break;
                    case 'name': $regra = substr($array[22], 0, 8) == "No match"; break;

                    case 'com.br':
                    case 'emp.br':
                    case 'net.br':
                    case 'eco.br':
                    case 'edu.br':
                    case 'blog.br':
                    case 'flog.br':
                    case 'nom.br':
                    case 'vlog.br':
                    case 'wiki.br':
                    case 'arg.br':
                    case 'art.br':
                    case 'esp.br':
                    case 'etc.br':
                    case 'far.br':
                    case 'imb.br':
                    case 'ind.br':
                    case 'inf.br':
                    case 'radio.br':
                    case 'rec.br':
                    case 'srv.br':
                    case 'tmp.br':
                    case 'tur.br':
                    case 'tv.br':
                    case 'am.br':
                    case 'coop.br':
                    case 'fm.br':
                    case 'g12.br':
                    case 'gov.br':
                    case 'mil.br':
                    case 'org.br':
                    case 'psi.br':
                    case 'b.br':
                    case 'jus.br':
                    case 'leg.br':
                    case 'mp.br':
                    case 'adm.br':
                    case 'adv.br':
                    case 'arq.br':
                    case 'ato.br':
                    case 'bio.br':
                    case 'bmd.br':
                    case 'cim.br':
                    case 'cng.br':
                    case 'cnt.br':
                    case 'ecn.br':
                    case 'eng.br':
                    case 'eti.br':
                    case 'fnd.br':
                    case 'fot.br':
                    case 'fst.br':
                    case 'cgf.br':
                    case 'jor.br':
                    case 'lel.br':
                    case 'mat.br':
                    case 'med.br':
                    case 'mus.br':
                    case 'not.br':
                    case 'ntr.br':
                    case 'odo.br':
                    case 'ppg.br':
                    case 'pro.br':
                    case 'psc.br':
                    case 'qsl.br':
                    case 'slg.br':
                    case 'taxi.br':
                    case 'teo.br':
                    case 'trd.br':
                    case 'vet.br':
                    case 'zlg.br':
                        $regra = (substr($array[11], 0, 14) == "% No match for") ? substr($array[11], 0, 14) == "% No match for" : substr($array[8], 0, 14) == "% No match for";
                        break;
                }

                if($regra){
                    $verbose = array(
                        'status' => 'OK',
                        'domain' => $dominio.'.'.$extensao,
                        'price'  => 40.00, //todo Usar o preço de registro do próprio cadastro (Preços de domínio)
                        'result' => 'available'
                    );

                }else{
                    $verbose = array(
                        'status' => 'OK',
                        'domain' => $dominio.'.'.$extensao,
                        'result' => 'registred',
                        'whois'  => $whois
                    );
                    //Aqui exibimos os detalhes do whois do dominio registrado
                }

            }else{
                var_dump($whois);
            }
        }else{
            $verbose = [
                'status' => 'ERROR',
                'result' => 'O campo [domain] é obrigatório.'
            ];
        }

        return response()->json($verbose);
    }

    public function get_whois($domain, $tld){
        if(!$this->whoisService->ValidDomain($domain.'.'.$tld) ){
            return false;
        }

        if( $this->whoisService->Lookup($domain.'.'.$tld)){
            return $this->whoisService->GetData(1);
        }else{
            // se ele não conseguiu com dominios internacionais, ele tentará com brasileiros;
            echo "essa e a hora do registro.br";
        }
    } // Fim da função "get_whois"
}
