<?php

namespace App\Http\Controllers;

use App\Models\Statement;
use App\Services\StatementService;
use Illuminate\Http\Request;

class StatementController extends Controller
{
    /**
     * @var StatementService
     */
    private $statementService;

    /**
     * StatementController constructor.
     */
    public function __construct(StatementService $statementService)
    {
        $this->statementService = $statementService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index(Request $request)
    {
        if($request->all() == null){
            $year = date('Y');
            $month = date('m');

            return $this->statementService->getStatetementsMes($month, $year);
        }else{

            /**
             * Retorna uma busca por filtro
             */
            $statements = Statement::with('customer:id,name,email,cpf_cnpj,type', 'invoice:id,total,uuid', 'type_payment', 'user:id,name,email', 'type_invoice')
                ->whereHas('account', function ($q){
                    $q->where('id', AccountId());
                });

            foreach ($request->except(['page']) as $key => $value)
            {
                $statements->where($key, $value);
            }

            $statements->orderBy('id', 'desc');
            return $statements->paginate(20);

        }
    }

    /**
     * @param $id
     * @return StatementService|StatementService[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|\Illuminate\Http\JsonResponse|null
     */
    public function show($id)
    {
        return $this->statementService->getStatement($id);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove($id, Request $request)
    {
        return $this->statementService->removeStatement($id, $request);
    }

    public function create(Request $request)
    {
        return $this->statementService->makeStatement($request);
    }
}
