<?php

namespace App\Http\Controllers;

use App\Mail\CustomerRegistered;
use App\Mail\TesteMail;
use App\Models\Account;
use App\Models\Customer;
use App\Services\ConfigService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class ConfigController extends Controller
{
    /**
     * @var ConfigService
     */
    private $configService;

    /**
     * ConfigController constructor.
     */
    public function __construct(ConfigService $configService)
    {
        $this->configService = $configService;
    }

    public function businessProfile()
    {
        $account = $this->configService->getAccount();
        return response()->json($account);
    }

    public function updateProfile(Request $request)
    {
        $account = $this->configService->getAccount();
        $logo_url = $account->logo;

        if ($request->hasFile('logo'))
        {
            $logo = $request->file('logo');
            $logoFile = $account->domain.'.'.$logo->getClientOriginalExtension();

            $bucket = Storage::disk('gcs');
            if (!$bucket->exists('/logos')){
                $bucket->makeDirectory('/logos/');
            }

            $filePath = '/logos/'.$logoFile;
            $bucket->put($filePath, fopen($logo, 'r+'), 'public');

            if($bucket->exists($filePath)){
                $logo_url = $bucket->url($filePath);
            }
        }

        return $this->configService->saveAccount($request, $logo_url);
    }

    /**
     * Atualizar configurações personalizadas
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateOption(Request $request)
    {
        return $this->configService->saveOption($request);
    }

    /**
     * Retorna todas as configurações
     * @return array|string
     */
    public function options()
    {
        return $this->configService->getOptions();
    }

    /**
     * Listar todos os templates de email
     * @return array
     */
    public function emails()
    {
        return $this->configService->getEmails();
    }

    public function getTemplate($tag)
    {
        return $this->configService->getEmailbyTag($tag);
    }

    public function testeEmail()
    {
        $customer = Customer::find(42);
        $account = Account::find(1);


        //dump(Mail::failures());

        return view('template1')->with([
            'logo' => $account->logo,
            'content' => 'lorem ipsum',
            'account' => $account,
            'customer' => $customer
        ]);
    }
}
