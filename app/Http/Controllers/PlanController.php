<?php

namespace App\Http\Controllers;

use App\Models\Metadata;
use App\Models\ModulesAccount;
use App\Models\Plan;
use App\Services\PlanService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PlanController extends Controller
{
    /**
     * @var PlanService
     */
    private $planService;

    /**
     * PlanController constructor.
     */
    public function __construct(PlanService $planService)
    {
        $this->planService = $planService;
    }

    public function index(Request $request)
    {
        if($request->all() == null){
            /**
             * Retorna todos os clientes
             */
            return $this->planService->allPlans();
        }else{
            if(isset($request->s)){
                /**
                 * Retorna uma busca por nome
                 */
                return $this->planService->getAll_search($request->s);
            }else{

                /**
                 * Retorna uma busca por filtro
                 */
                $plans = Plan::query()
                    ->whereHas('account', function ($q){
                        $q->where('id', AccountId());
                    });

                foreach ($request->except(['page']) as $key => $value)
                {
                    $plans->where($key, $value);
                }

                $plans->orderBy('id', 'DESC');
                return $plans->paginate(20);
            }
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return $this->planService->getPlan($id);
    }

    /**
     * @param $id
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        return $this->planService->updatePlan($id, $request);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateGrid($id, Request $request)
    {
        return $this->planService->updateGrid($id, $request);
    }

    public function updatePrice($id, Request $request)
    {
        return $this->planService->updatePrice($id, $request);
    }

    public function storePrice(Request $request)
    {
        return $this->planService->storePrice($request);
    }

    public function removePrice($id)
    {
        return $this->planService->removePrice($id);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeGrid($id)
    {
        return $this->planService->deleteGrid($id);
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function storeGrid(Request $request)
    {
        return $this->planService->createGrid($request);
    }

    public function grids()
    {
        return $this->planService->getGrids();
    }

    /**
     * @param $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return $this->planService->deletePlan($id);
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return $this->planService->createPlan($request);
    }

    public function deleteMetadata($uuid)
    {
        /** @var Plan $plan */
        $plan = Plan::with('metadata')->whereUuid($uuid)->first();

        if(is_null($plan->metadata))
        {
            return \response()->json([
                'status' => 'error',
                'message' => 'Não existe configuração de integração para remover'
            ]);
        }

        $plan->metadata->delete();

        return response()->json([
            'status'  => 'success',
            'message' => 'Configuração de Integração removida!',
        ]);
    }

    public function setMetadata(Request $request, $uuid)
    {
        $plan = Plan::with('metadata')->whereUuid($uuid)->first();

        $validator = Validator::make($request->all(), [
            'config_id' => 'required | integer | exists:modules_account,id',
            'module_id' => 'required | integer | exists:modules,id',
            'meta' => 'required',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{
            $meta = $request->meta;
            $meta['name'] = limpaChars($meta['name']);

            $dados = [
                'module_id' => $request->module_id,
                'meta' => serialize($meta),
                'account_id' => $plan->account_id,
                'module_account_id' => $request->config_id
            ];

            if(is_null($plan->metadata))
            {
                $plan->metadata()->create($dados);
            }else{
                $plan->metadata->update($dados);
            }

            return response()->json([
                'status'  => 'success',
                'message' => 'Configuração de Integração concluída!',
            ]);
        }
    }
}
