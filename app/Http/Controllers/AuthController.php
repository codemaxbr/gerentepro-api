<?php

namespace App\Http\Controllers;

use App\Events\CustomerRegistered;
use App\Models\Customer;
use App\Models\Email;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Exception;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Webpatser\Uuid\Uuid;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;
    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function __construct() {

    }

    /**
     * Create a new token.
     *
     * @param  \App\Models\User   $user
     * @return string
     */
    protected function jwt(User $user) {
        $payload = [
            'iss' => "lumen-jwt",
            'sub' => $user->id,
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 7200 // Expiration time
        ];

        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($payload, 'JhbGciOiJIU');
    }

    /**
     * Create a new token customer.
     *
     * @param  \App\Models\Customer $customer
     * @return string
     */
    protected function jwt_front(Customer $customer) {
        $payload = [
            'iss' => "customer-jwt", // Issuer of the token
            'sub' => $customer->id, // Subject of the token
            'acc' => $customer->account_id,
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 7200 // Expiration time
        ];

        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($payload, 'JhbGciOiJIU');
    }

    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     *
     * @param  \App\Models\User   $user
     * @return mixed
     */
    public function authenticate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'required | email',
            'password' => 'required',
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else
        {
            $user = User::with(['account'])->where(['email' => $request->email])->first();
            $permissions = $user->getAllPermissions();

            if (!$user) {
                return response()->json([
                    'errors' => [
                        'email' => ['Usuário não existe com este e-mail.']
                    ]
                ], Response::HTTP_BAD_REQUEST);
            }

            if(!$user->confirmed) {
                return response()->json([
                    'errors' => [
                        'email' => ['Este usuário ainda não confirmou o endereço de email.']
                    ]
                ], Response::HTTP_BAD_REQUEST);
            }

            if(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'confirmed' => 1], true))
            {
                if($user->remember_token)
                {
                    try{
                        $token = $user->remember_token;
                        $credentials = JWT::decode($token, 'JhbGciOiJIU', ['HS256']);

                        return response()->json([
                            'token' => $token,
                            'permissions' => $permissions
                        ], 200);
                    }catch(Exception $e){
                        
                        if(Cache::has($user->remember_token)){
                            Cache::forget($user->remember_token);
                        }else{
                            //$user->remember_token = null;
                            //$user->save();

                            $novo_token = $this->jwt($user);

                            Cache::remember($novo_token, 120, function () use ($user, $novo_token){
                                $user->last_login     = Carbon::now()->toDateTimeString();
                                $user->remember_token = $novo_token;
                                $user->save();
        
                                return $user;
                            });

                            return response()->json([
                                'token' => $novo_token,
                                'permissions' => $permissions
                            ], 200);
                        }
                    }
                    
                }else{
                    $token = $this->jwt($user);

                    Cache::remember($token, 120, function () use ($user, $token){
                        $user->last_login     = Carbon::now()->toDateTimeString();
                        $user->remember_token = $token;
                        $user->save();

                        return $user;
                    });

                    return response()->json([
                        'token' => $token,
                        'permissions' => $permissions
                    ], 200);
                }

            }else{
                // todo limitar tentativas de login??

                return response()->json([
                    'errors' => [
                        'password' => ['Senha incorreta, por favor tente novamente.']
                    ]
                ], Response::HTTP_BAD_REQUEST);
            }
        }
    }

    public function validateToken(Request $request)
    {
        $token = $request->bearerToken();

        if(!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'error' => 'Token not provided.'
            ], Response::HTTP_UNAUTHORIZED);
        }
        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);

            if($credentials->iss == "lumen-jwt")
            {
                if(!Cache::has('user_'.$credentials->sub)){
                    $user = User::find($credentials->sub);

                    if($user){
                        Cache::rememberForever('user_'.$credentials->sub, function () use ($user){
                            return $user;
                        });
                    }
                }

                return Cache::get('user_'.$credentials->sub);
            }else{
                return response()->json([
                    'error' => 'Acesso restrito apenas para Administradores'
                ], Response::HTTP_FORBIDDEN);
            }

        } catch(ExpiredException $e) {
            return response()->json([
                'error' => 'Provided token is expired.'
            ], Response::HTTP_UNAUTHORIZED);
        } catch(\Exception $e) {
            return response()->json([
                'error' => 'An error while decoding token.'
            ], Response::HTTP_UNAUTHORIZED);
        }
    }

    public function customerLogged(Request $request)
    {
        $token = $request->bearerToken();

        if(!$token || $token == null) {
            // Unauthorized response if token not there
            return response()->json([
                'error' => 'Token not provided.'
            ], 401);
        }
        try {
            $credentials = JWT::decode($token, 'JhbGciOiJIU', ['HS256']);
            $customer_id = $credentials->sub;

            if($credentials->iss == "customer-jwt")
            {
                if(!Cache::has('customer_'.$customer_id)){
                    $customer = Customer::find($customer_id);

                    if($customer){
                        Cache::remember('customer_'.$customer_id, 120, function () use ($customer){
                            return $customer;
                        });
                    }
                }

                return Cache::get('customer_'.$customer_id);
            }else{
                return response()->json([
                    'error' => 'Acesso restrito apenas para Assinantes'
                ], 400);
            }

        } catch(ExpiredException $e) {
            return response()->json([
                'error' => 'Provided token is expired.'
            ], 400);
        } catch(\Exception $e) {
            return response()->json([
                'error' => 'An error while decoding token.'
            ], 400);
        }
    }

    public function activateCustomer($uuid)
    {
        if($uuid != NULL){
            $customer = Customer::query()->where(['uuid' => $uuid])->firstOrFail();
            $customer->status = 1;
            $customer->save();

            return \response()->json([
                'status' => 'success',
                'message' => 'Usuário "'.$customer->name.'" ativado com sucesso.'
            ]);
        }else{
            return \response()->json([
                'status' => 'error',
                'message' => 'Não foi especificado o Token UUID para ativação.'
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function authCustomer(Customer $customer)
    {
        $this->validate($this->request, [
            'email'         => 'required|email',
            'password'      => 'required',
            'account_id'    => 'required|exists:accounts,id'
        ]);

        $customer = Customer::where(['email' => $this->request->input('email'), 'account_id' => $this->request->input('account_id')])->first();

        if (!$customer) {
            return response()->json([
                'error' => 'Não existe um cadastro com este e-mail.'
            ], 400);
        }

        if (!$customer->status) {
            return response()->json([
                'error' => 'Parece que você ainda não confirmou seu e-mail. Verifique sua caixa de entrada'
            ], 400);
        }

        // Verify the password and generate the token
        if (Hash::check($this->request->input('password'), $customer->password)) {
            $customer->last_login = Carbon::now()->toDateTimeString();
            $customer->save();

            return response()->json([
                'token' => $this->jwt_front($customer),
                'user' => $customer
            ], 200);
        }

        return response()->json([
            'error' => 'Senha incorreta, tente novamente.'
        ], 400);
    }

    public function registerLoginCustomer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => [
                'required',
                'email',
                Rule::unique('customers')->where(function ($query) use ($request) {
                    $query->where(['account_id' => $request->account_id]);
                }),
            ],
            'type' => 'required',
            'cpf_cnpj' => [
                'required',
                Rule::unique('customers')->where(function ($query) use ($request) {
                    $query->where(['account_id' => $request->account_id]);
                }),
            ],
            'name' => 'required',
            'genre' => 'required'
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{
            try{
                $request['account_id'] = $request->account_id;
                $request['uuid'] = Uuid::generate(4)->string;

                if($request->has('password'))
                {
                    $request['password'] = Hash::make($request->password);
                }

                $customer = Customer::create($request->all());
                Event::dispatch(new CustomerRegistered($customer));

                return response()->json([
                    'token' => $this->jwt_front($customer),
                    'user' => $customer
                ], 200);

            }catch (\Exception $e){

                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }

    public function registerCustomer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => [
                'required',
                'email',
                Rule::unique('customers')->where(function ($query) {
                    $query->where(['account_id' => front_AccountId()]);
                }),
            ],
            'type' => 'required',
            'cpf_cnpj' => [
                'required',
                Rule::unique('customers')->where(function ($query) {
                    $query->where(['account_id' => front_AccountId()]);
                }),
            ],
            'name' => 'required',
            'phone' => 'required',
            'mobile' => 'required',
            'genre' => 'required'
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else{
            try{
                $request['account_id'] = front_AccountId();
                $request['uuid'] = Uuid::generate(4)->string;

                if($request->has('password'))
                {
                    $request['password'] = Hash::make($request->password);
                }

                $customer = Customer::create($request->all());
                Event::dispatch(new CustomerRegistered($customer));

                return response()->json([
                    'status' => 'success',
                    'message' => 'Cadastro realizado com sucesso.',
                    'data' => [
                        'customer' => $customer
                    ]
                ], Response::HTTP_OK);

            }catch (\Exception $e){

                if(QueryException::class){
                    ErrorReport(serialize($request->all()), $e, __FUNCTION__, __FILE__, 500);

                    return response()->json([
                        'error' => [
                            'message' => 'Ocorreu um erro interno do sistema. Nossos desenvolvedores foram notificados do problema para solução mais breve possível.'
                        ]
                    ], Response::HTTP_INTERNAL_SERVER_ERROR);
                }

                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }
}
