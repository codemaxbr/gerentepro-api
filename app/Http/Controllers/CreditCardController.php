<?php

namespace App\Http\Controllers;

use App\Services\CustomerService;
use Illuminate\Http\Request;

class CreditCardController extends Controller
{
    /**
     * @var CustomerService
     */
    private $customerService;

    /**
     * CreditCardController constructor.
     * @param CustomerService $customerService
     */
    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    public function index($customer_uuid)
    {
        return $this->customerService->getCreditCards($customer_uuid);
    }

    public function store($customer_uuid, Request $request)
    {
        return $this->customerService->createCreditCard($customer_uuid, $request);
    }

    public function show($customer_uuid, $id)
    {
        return $this->customerService->getCreditCard($customer_uuid, $id);
    }

    public function update($customer_uuid, $id, Request $request)
    {
        return $this->customerService->updateCreditCard($customer_uuid, $id, $request);
    }

    public function destroy($customer_uuid, $id)
    {
        return $this->customerService->deleteCreditCard($customer_uuid, $id);
    }
}
