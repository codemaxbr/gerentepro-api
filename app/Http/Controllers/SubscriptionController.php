<?php

namespace App\Http\Controllers;

use App\Models\Subscription;
use App\Services\SubscriptionService;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    /**
     * @var SubscriptionService
     */
    private $subscriptionService;


    /**
     * SubscriptionController constructor.
     */
    public function __construct(SubscriptionService $subscriptionService)
    {
        $this->subscriptionService = $subscriptionService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index(Request $request)
    {
        if($request->all() == null){
            /**
             * Retorna todas as contas registradas
             */
            return $this->subscriptionService->getSubscriptions();
        }else{
            if(isset($request->s)){
                /**
                 * Retorna uma busca por nome
                 */
                return $this->subscriptionService->getAll_search($request->s);
            }else{

                /**
                 * Retorna uma busca por filtro
                 */
                $subscriptions = Subscription::with(['customer:id,name,email,cpf_cnpj,type', 'plan:id,uuid,name,payment_cycle_id,price', 'plan.payment_cycle:id,name']);
                foreach ($request->except(['page']) as $key => $value)
                {
                    $subscriptions->where($key, $value);
                }

                $subscriptions->orderBy('id', 'desc');
                return $subscriptions->paginate(20);
            }
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return $this->subscriptionService->getSubscription($id);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        return $this->subscriptionService->update($id, $request);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return $this->subscriptionService->remove($id);
    }
}
