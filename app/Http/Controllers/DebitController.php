<?php

namespace App\Http\Controllers;

use App\Services\DebitService;
use Illuminate\Http\Request;

class DebitController extends Controller
{
    /**
     * @var DebitService
     */
    private $debitService;

    /**
     * DebitController constructor.
     * @param DebitService $debitService
     */
    public function __construct(DebitService $debitService)
    {
        $this->debitService = $debitService;
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index()
    {
        return $this->debitService->getAll();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return $this->debitService->createDebit($request);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return $this->debitService->getDebit($id);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        return $this->debitService->updateDebit($id, $request);
    }

    /**
     * @param $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return $this->debitService->deleteDebit($id);
    }
}
