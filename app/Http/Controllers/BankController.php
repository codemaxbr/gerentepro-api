<?php

namespace App\Http\Controllers;

use App\Services\BankService;
use Illuminate\Http\Request;

class BankController extends Controller
{
    /**
     * @var BankService
     */
    private $bankService;

    /**
     * BankController constructor.
     * @param BankService $bankService
     */
    public function __construct(BankService $bankService)
    {
        $this->bankService = $bankService;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->bankService->getBanks();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return $this->bankService->createBank($request);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        return $this->bankService->updateBank($id, $request);
    }

    /**
     * @param $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return $this->bankService->destroyBank($id);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return $this->bankService->getBank($id);
    }
}
