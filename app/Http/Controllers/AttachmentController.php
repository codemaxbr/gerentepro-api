<?php

namespace App\Http\Controllers;

use App\Services\AttachmentService;
use Illuminate\Http\Request;

class AttachmentController extends Controller
{
    /**
     * @var AttachmentService
     */
    private $attachmentService;

    /**
     * AttachmentController constructor.
     */
    public function __construct(AttachmentService $attachmentService)
    {
        $this->attachmentService = $attachmentService;
    }

    public function index(Request $request)
    {
        if($request->all() == null){
            /**
             * Retorna todas as contas registradas
             */
            return $this->attachmentService->getAttachments();
        }else{
            if(isset($request->s)){
                /**
                 * Retorna uma busca por nome
                 */
                return $this->attachmentService->getAll_search($request->s);
            }else{

                return $this->attachmentService->getAttachments();
            }
        }
    }

    /**
     * @param $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($uuid)
    {
        return $this->attachmentService->getAttachment($uuid);
    }

    public function store(Request $request)
    {
        return $this->attachmentService->createAttachment($request);
    }

    public function update($uuid, Request $request)
    {
        return $this->attachmentService->updateAttachment($uuid, $request);
    }

    public function destroy($uuid)
    {
        return $this->attachmentService->deleteAttachment($uuid);
    }
}
