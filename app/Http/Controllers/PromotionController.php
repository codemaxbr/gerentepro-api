<?php

namespace App\Http\Controllers;

use App\Models\Promotion;
use App\Services\PromotionService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PromotionController extends Controller
{
    /**
     * @var PromotionService
     */
    private $promotionService;

    /**
     * PromotionController constructor.
     * @param PromotionService $promotionService
     */
    public function __construct(PromotionService $promotionService)
    {
        $this->promotionService = $promotionService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index(Request $request)
    {
        if($request->all() == null){
            /**
             * Retorna todos as promoções registradas
             */
            return $this->promotionService->getPromotions();
        }else{
            if(isset($request->s)){
                /**
                 * Retorna uma busca por nome
                 */
                return $this->promotionService->getAll_search($request->s);
            }else{

                /**
                 * Retorna uma busca por filtro
                 */
                $domains = Promotion::query()
                    ->whereHas('account', function ($q){
                        $q->where('id', AccountId());
                    });

                foreach ($request->except(['page']) as $key => $value)
                {
                    $domains->where($key, $value);
                }

                $domains->orderBy('name', 'asc');
                return $domains->paginate(20);
            }
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return $this->promotionService->getPromotion($id);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required | string',
            'type_promotion_id' => 'required | integer | exists:type_promotions,id',
            'value' => 'required | numeric',
            'unique' => 'boolean',
            'qty' => 'required_if:unique,1',
            'code' => 'required_without:unique'
        ]);

        if($validator->fails())
        {
            return response()->json(array('errors' => $validator->errors()), Response::HTTP_BAD_REQUEST);
        }else
        {
            $promo = $this->promotionService->createPromotion($request->all());

            return response()->json([
                'status' => 'success',
                'message' => 'Promoção criada com sucesso.',
                'data' => [
                    'promotion' => $promo
                ]
            ]);
        }
        //return $this->promotionService->createPromotion($request);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        return $this->promotionService->updatePromotion($id, $request);
    }

    /**
     * @param $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return $this->promotionService->deletePromotion($id);
    }

    public function assignVoucher(Request $request, $id)
    {
        return $this->promotionService->assignVoucher($id, $request);
    }

    public function useVoucher($voucer_code, Request $request)
    {
        return $this->promotionService->validateVoucher($voucer_code, $request);
    }

    /**
     * @param $promotion_id
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function vouchers($promotion_id)
    {
        return $this->promotionService->getVouchers($promotion_id);
    }

    /**
     * @param $promotion_id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function removeVouchers($promotion_id)
    {
        return $this->promotionService->deleteVouchers($promotion_id);
    }
}
