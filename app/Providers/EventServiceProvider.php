<?php

namespace App\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

        'Illuminate\Auth\Events\Attempting' => [
            'App\Listeners\beforeLogin',
        ],

        'Illuminate\Auth\Events\Login' => [
            'App\Listeners\afterLogin',
        ],

        'Illuminate\Auth\Events\Logout' => [
            'App\Listeners\Logouted',
        ],

        /////////////////////////////////////////////////////////////

        'App\Events\AccountCreated' => [
            'App\Listeners\AccountActivation'
        ],

        'App\Events\UserInvited' => [
            'App\Listeners\UserConfirmation'
        ],

        'App\Events\CustomerRegistered' => [
            'App\Listeners\NotifyCustomerConfirmation',
        ],

        /**
         * Quando for solicitado um registro de domínio
         */
        'App\Events\DomainRegistered' => [
            'App\Listeners\RegisterDomain'
        ],

        /**
         * Quando uma fatura for criada
         */
        'App\Events\InvoiceCreated' => [
            'App\Listeners\NotifyCustomerInvoice',
        ],

        /**
         * Quando uma fatura for marcada como "paga" manualmente
         */
        'App\Events\InvoiceChangePaid' => [
            'App\Listeners\CreateStatement',
            'App\Listeners\CreateTransaction',
            'App\Listeners\ActivateSubscription',
            //'App\Listeners\AttachmentInvoice',
        ],

        /**
         * Confirmação de pagamento
         */
        'App\Events\ConfirmPayment' => [
            'App\Listeners\CreateStatement',
            'App\Listeners\CreateTransaction',
            'App\Listeners\ActivateSubscription',
        ],

        'App\Events\ValidateModule' => [
            '\App\Listeners\ValidateConfig',
        ],

        'App\Events\TerminateSubscription' => [
            '\App\Listeners\Terminate'
        ],

        'App\Events\SuspendSubscription' => [
            '\App\Listeners\SuspendSubscription',
        ],

        'App\Events\AlterPlanSubscription' => [
            'App\Listeners\AlterPlan'
        ],

        'App\Events\ActiveSubscription' => [
            'App\Listeners\ActivateSubscription',
        ],

        'App\Events\boletoPayment' => [
            'App\Listeners\CreateInvoice',
            'App\Listeners\registerBoleto',
            'App\Listeners\MakeTransaction',
        ],

        'App\Events\LogActivity' => [
            'App\Listeners\SaveLog'
        ],

        'App\Events\SendEmail' => [
            'App\Listeners\EmailNotification'
        ],

        'App\Events\cartaoPayment' => [
            'App\Listeners\CreateInvoice',
            'App\Listeners\registerCartao',
            'App\Listeners\MakeTransaction',
        ],

        'App\Events\CreatePlanModule' => [
            'App\Listeners\registerPlan',
        ],

        'App\Events\RemovePlanModule' => [
            'App\Listeners\deletePlan',
        ],

        'App\Events\UpdatePlanModule' => [
            'App\Listeners\alterPlan',
        ],
    ];
}
