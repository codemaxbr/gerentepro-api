<?php

namespace App\Providers;

use App\Models\Job;
use Illuminate\Queue\Events\JobFailed;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobProcessing;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Schema::defaultStringLength(191);
        $this->app->singleton('Illuminate\Contracts\Routing\ResponseFactory', function ($app) {
            return new \Illuminate\Routing\ResponseFactory(
                $app['Illuminate\Contracts\View\Factory'],
                $app['Illuminate\Routing\Redirector']
            );
        });
    }

    public function boot()
    {
        Queue::failing(function (JobFailed $event)
        {
            $body = json_decode($event->job->getRawBody());
            $payload = unserialize($body->data->command); // Reporta o construct do Job

            $task = $payload->task;
            $task->failed_job_id = $event->job->getJobId();
            $task->save();
        });

        \App\Models\User::  observe(\App\Observers\UserObserver::class);
        \App\Models\Customer::  observe(\App\Observers\CustomerObserver::class);
        \App\Models\Invoice::   observe(\App\Observers\InvoiceObserver::class);
        \App\Models\Metadata::  observe(\App\Observers\MetadataObserver::class);
        \App\Models\ModulesAccount::  observe(\App\Observers\ModuleAccountObserver::class);
    }
}
