<?php

namespace App\Events;

class UpdatePlanModule extends Event
{
    public $plan;
    public $module_account;
    public $params;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($plan, $module_account, $params)
    {
        $this->plan = $plan;
        $this->module_account = $module_account;
        $this->params = $params;
    }
}
