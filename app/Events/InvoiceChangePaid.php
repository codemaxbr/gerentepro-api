<?php

namespace App\Events;

use App\Models\Invoice;
use App\Models\Module;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Queue\SerializesModels;

class InvoiceChangePaid extends Event
{
    use InteractsWithSockets, SerializesModels;

    public $invoice;
    public $request;
    public $user;
    public $module;
    public $parent;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Invoice $invoice, $request, $parent, Module $module = null)
    {
        $this->invoice = $invoice;
        $this->request = $request;
        $this->module  = $module;
        $this->parent = $parent;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
