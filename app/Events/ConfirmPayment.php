<?php

namespace App\Events;

use App\Models\Invoice;
use App\Models\Module;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\Auth;

class ConfirmPayment
{
    use InteractsWithSockets, SerializesModels;

    public $invoice;
    public $request;
    public $user;
    public $module;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Invoice $invoice, $request, Module $module = null)
    {
        $this->invoice = $invoice;
        $this->request = $request;
        $this->user = Auth::user();
        $this->module = $module;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
