<?php

namespace App\Events;

use App\Models\Module;
use App\Models\Server;
use Illuminate\Http\Request;

class ValidateModule extends Event
{
    /**
     * @var Module
     */
    public $module;
    /**
     * @var Server|null
     */
    public $request;

    /**
     * Create a new event instance.
     *
     * @param Module $module
     * @param Server|null $server
     */
    public function __construct(Module $module, Request $request)
    {
        $this->module = $module;
        $this->request = $request;
    }
}
