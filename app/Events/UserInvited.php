<?php

namespace App\Events;

use App\Models\User;

class UserInvited extends Event
{
    /**
     * @var User
     */
    public $user;
    public $code;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, $code)
    {
        $this->user = $user;
        $this->code = $code;
    }
}
