<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CreatePlanModule
{
    use InteractsWithSockets, SerializesModels;
    public $plan;
    public $module_account;
    public $params;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($plan, $module_account, $params)
    {
        $this->plan = $plan;
        $this->module_account = $module_account;
        $this->params = $params;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
