<?php

namespace App\Events;

use App\Models\Account;
use App\Models\User;

class AccountCreated extends Event
{
    /**
     * @var Account
     */
    public $account;
    /**
     * @var User
     */
    public $user;

    /**
     * Create a new event instance.
     *
     * @param Account $account
     */
    public function __construct(Account $account, User $user)
    {
        $this->account = $account;
        $this->user = $user;
    }
}
