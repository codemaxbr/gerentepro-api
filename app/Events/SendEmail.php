<?php

namespace App\Events;

class SendEmail extends Event
{
    public $template;
    public $model;
    public $to;
    public $user_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($template, $model, $to)
    {
        $this->template = $template;
        $this->model = $model;
        $this->to = $to;
        $this->user_id = adminLogged();
    }
}
