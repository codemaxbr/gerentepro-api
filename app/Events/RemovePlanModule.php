<?php

namespace App\Events;

class RemovePlanModule extends Event
{
    public $plan;
    public $module_account;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($plan, $module_account)
    {
        $this->plan = $plan;
        $this->module_account = $module_account;
    }
}
