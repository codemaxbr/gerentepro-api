<?php

namespace App\Events;

use App\Models\Plan;
use App\Models\Subscription;

class TerminateSubscription extends Event
{
    /**
     * @var Subscription
     */
    public $subscription;
    /**
     * @var Plan
     */
    public $plan;

    /**
     * Create a new event instance.
     *
     * @param Subscription $subscription
     * @param Plan $plan
     */
    public function __construct(Subscription $subscription, Plan $plan)
    {
        $this->subscription = $subscription;
        $this->plan = $plan;
    }
}
