<?php

namespace App\Mail;

use App\Models\Account;
use App\Models\Customer;
use App\Services\EmailService;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerRegistered extends Mailable
{
    use Queueable, SerializesModels;
    public static $tag = 'customer_register';
    public static $fields = [
        [
            'title' => 'Dados do cliente',
            'fields' => [
                ['tag' => 'link_ativacao', 'description' => 'Link de ativação do cadastro'],
                ['tag' => 'cliente_id', 'description' => 'ID do cliente'],
                ['tag' => 'cliente_nome', 'description' => 'Nome do cliente'],
                ['tag' => 'cliente_tipo', 'description' => 'Tipo de cadastro'],
                ['tag' => 'cliente_cpf_cnpj', 'description' => 'CPF/CNPJ do cliente'],
                ['tag' => 'cliente_email', 'description' => 'E-mail do cliente'],
                ['tag' => 'cliente_tel', 'description' => 'Telefone do cliente'],
                ['tag' => 'cliente_cel', 'description' => 'Celular do cliente'],
                ['tag' => 'cliente_nascimento', 'description' => 'Data de Nascimento'],
                ['tag' => 'cliente_genero', 'description' => 'Gênero do cliente'],
                ['tag' => 'cliente_endereco', 'description' => 'Endereço do cliente'],
                ['tag' => 'cliente_numero', 'description' => 'Número do endereço'],
                ['tag' => 'cliente_cep', 'description' => 'CEP do endereço'],
                ['tag' => 'cliente_cidade', 'description' => 'Cidade do endereço'],
                ['tag' => 'cliente_bairro', 'description' => 'Bairro do endereço'],
                ['tag' => 'cliente_adicional', 'description' => 'Complemento do endereço'],
                ['tag' => 'cliente_data_registro', 'description' => 'Data de registro do cliente'],
            ]
        ],
        [
            'title' => 'Sua Empresa',
            'fields' => [
                ['tag' => 'empresa_nome', 'description' => 'Nome da sua empresa'],
                ['tag' => 'empresa_email', 'description' => 'Email de contato'],
                ['tag' => 'empresa_telefone', 'description' => 'Telefone de contato'],
                ['tag' => 'empresa_url_termos', 'description' => 'URL de Termos de uso'],
            ]
        ]
    ];

    private $emailService;
    private $customer;
    private $account;

    private $from_email;
    private $from_name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Customer $customer, Account $account)
    {
        $this->customer = $customer;
        $this->account = $account;

        parent::from($account->email_contact, $account->name_business);
    }

    public function fields()
    {
        $empresa = $this->account;
        $cliente = $this->customer;

        return [
            // Sua Empresa
            'empresa_nome'          => $empresa->name_business,
            'empresa_email'         => $empresa->email_contact,
            'empresa_telefone'      => $empresa->phone_contact,
            'empresa_url_termos'    => $empresa->url_terms,
            'link_ativacao'         => 'http://'.$empresa->domain.'.gerentepro.com.br/autenticacao/confirma/'.$cliente->uuid,

            // Dados do Cliente
            'cliente_id'            => $cliente->id,
            'cliente_nome'          => $cliente->name,
            'cliente_tipo'          => 'PESSOA '.strtoupper($cliente->type),
            'cliente_cpf_cnpj'      => $cliente->cpf_cnpj,
            'cliente_email'         => $cliente->email,
            'cliente_tel'           => $cliente->phone,
            'cliente_cel'           => $cliente->mobile,
            'cliente_nascimento'    => $cliente->birthdate->format('d/m/Y'),
            'cliente_genero'        => ($cliente->genre == "M") ? "Masculino" : "Feminino",
            'cliente_endereco'      => $cliente->address,
            'cliente_numero'        => $cliente->number,
            'cliente_cep'           => $cliente->zipcode,
            'cliente_cidade'        => $cliente->city,
            'cliente_bairro'        => $cliente->district,
            'cliente_adicional'     => $cliente->additional,
            'cliente_data_registro' => $cliente->created_at->format('d/m/Y')
        ];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $emailService = new EmailService();
        $template = $emailService->getTemplate(self::$tag, $this->account);

        $m = new \Mustache_Engine();
        $content = $m->render($template->template, $this->fields());
        $subject = $m->render($template->subject, $this->fields());

        return $this->view('template1')->subject($subject)->with(['content' => $content, 'logo' => $this->account->logo]);
    }
}
