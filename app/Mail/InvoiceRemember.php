<?php

namespace App\Mail;

use App\Models\Invoice;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvoiceRemember extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Invoice
     */
    private $invoice;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $content = "
            <p>Olá! {{ customer.name }},</p>
            <p>
                Esté é um e-mail <b>Cobrança Nº {{ invoice.id }}</b>.<br />
                O total da fatura é: R$ {{ invoice.total }}
            </p>
            
            <p>Itens da Fatura</p>
            
            {{#invoice_items}}
                {{plan.name}} - {{price}}
            {{/invoice_items}}
        ";

        $mustache = new \Mustache_Engine();
        $invoice_items = $this->invoice->invoice_items()->with('plan')->get();

        $renderedHtml = $mustache->render(
            $content,
            [
                'websiteUrl' => env('APP_URL'),
                'invoice' => $this->invoice,
                'invoice_items' => $invoice_items->toArray(),
                'customer' => $this->invoice->customer
            ]
        );
        return $this->view('template1')->with(['content' => $renderedHtml])->subject('Lembrete de Cobrança TESTE');
    }
}
