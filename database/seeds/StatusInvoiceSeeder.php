<?php

use Illuminate\Database\Seeder;

class StatusInvoiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\StatusInvoice::create(['name' => 'Pendente']);
        \App\Models\StatusInvoice::create(['name' => 'Atrasado']);
        \App\Models\StatusInvoice::create(['name' => 'Anulado']);
        \App\Models\StatusInvoice::create(['name' => 'Cancelado']);
        \App\Models\StatusInvoice::create(['name' => 'Pago']);
        \App\Models\StatusInvoice::create(['name' => 'Marcado como pago']);
        \App\Models\StatusInvoice::create(['name' => 'Estornado']);
        \App\Models\StatusInvoice::create(['name' => 'Rascunho']);
    }
}
