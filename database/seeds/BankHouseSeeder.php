<?php

use Illuminate\Database\Seeder;

class BankHouseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * [code]
     * [name]
     * [logo]
     *
     * @return void
     */
    public function run()
    {
        \App\Models\BankHouse::create([
            'code' => '001',
            'name' => 'Banco do Brasil',
            'logo' => 'https://s3-sa-east-1.amazonaws.com/gerentep/uploads/modules/bancos/bb.png'
        ]);

        \App\Models\BankHouse::create([
            'code' => '104',
            'name' => 'Caixa Econômica Federal',
            'logo' => 'https://s3-sa-east-1.amazonaws.com/gerentep/uploads/modules/bancos/caixa.png'
        ]);

        \App\Models\BankHouse::create([
            'code' => '033',
            'name' => 'Banco Santander',
            'logo' => 'https://s3-sa-east-1.amazonaws.com/gerentep/uploads/modules/bancos/santander.png'
        ]);

        \App\Models\BankHouse::create([
            'code' => '237',
            'name' => 'Bradesco',
            'logo' => 'https://s3-sa-east-1.amazonaws.com/gerentep/uploads/modules/bancos/bradesco.gif'
        ]);

        \App\Models\BankHouse::create([
            'code' => '341',
            'name' => 'Itaú Unibanco',
            'logo' => 'https://s3-sa-east-1.amazonaws.com/gerentep/uploads/modules/bancos/itau.png'
        ]);
    }
}
