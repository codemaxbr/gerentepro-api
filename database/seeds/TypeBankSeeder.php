<?php

use Illuminate\Database\Seeder;

class TypeBankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\TypeBank::create(['name' => 'Conta Corrente']);
        \App\Models\TypeBank::create(['name' => 'Conta Poupança']);
    }
}
