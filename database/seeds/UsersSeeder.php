<?php

use App\Models\Reseller;
use App\Models\User;
use App\Models\Account;
use App\Services\AccountService;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Webpatser\Uuid\Uuid;

class UsersSeeder extends Seeder
{
    /**
     * @var AccountService
     */
    private $accountService;

    /**
     * UsersSeeder constructor.
     */
    public function __construct(AccountService $accountService)
    {
        $this->accountService = $accountService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $acc = [
            'name_business' => 'Empresa Teste',
            'domain' => 'demo',
            'email_contact' => 'demo@gerentepro.com.br',
            'uuid' => Uuid::generate()->string,
            'status' => 1,
            'reseller_id' => 1,
            'account_plan_id' => 1,
        ];

        $account = Account::create($acc);

        $user = User::create([
            'name' => 'Demonstração',
            'email' => 'lucas.codemax@gmail.com',
            'password' => Hash::make('himura08'),
            'account_id' => $account->id,
            'confirmed' => 1,
            'is_reseller' => 1,
        ]);

        $role = $account->roles()->create([
            'name' => 'root', 
            'description' => 'Administrador'
        ]);

        $role->givePermissionTo(\App\Models\Permission::all());
        $user->assignRole('root');
    }
}
