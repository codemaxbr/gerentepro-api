<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Limpa o cache de Papéis e Permissões
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        //==================================================================
        // Permissões para Clientes
        //==================================================================
        foreach(App\Models\Customer::$permissions as $name => $description){
            Permission::create(['name' => $name, 'description' => $description]);
        }
    }
}
