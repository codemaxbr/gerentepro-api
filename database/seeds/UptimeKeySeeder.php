<?php

use Illuminate\Database\Seeder;

class UptimeKeySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\UptimeKey::create(['api_key' => 'u356294-b863d31f11587b06ce6a5294']);
        \App\Models\UptimeKey::create(['api_key' => 'u357326-d090548fb3241e98bae66f79']);
        \App\Models\UptimeKey::create(['api_key' => 'u357327-f66eb8bd6e75a27454168cd6']);
        \App\Models\UptimeKey::create(['api_key' => 'u357335-c23333313725d7a040a6bf2c']);
        \App\Models\UptimeKey::create(['api_key' => 'u357337-651b27c48a22a8088d660167']);
        \App\Models\UptimeKey::create(['api_key' => 'u357348-eb0ac007bd6f8d2e78f8eecd']);
    }
}
