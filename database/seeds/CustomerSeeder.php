<?php

use App\Services\CustomerService;
use Illuminate\Database\Seeder;
use Webpatser\Uuid\Uuid;

class CustomerSeeder extends Seeder
{
    /**
     * @var CustomerService
     */
    private $customerService;

    /**
     * CustomerSeeder constructor.
     */
    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('pt_BR');

        for ($i = 0; $i < 40; $i++)
        {
            $cliente = [
                'name' => $faker->name,
                'uuid' => Uuid::generate(4)->string,
                'type' => 'fisica',
                'cpf_cnpj'  => Mask('###.###.###-##', randomNumber(11)),
                'email' => $faker->freeEmail,
                'phone' => $faker->phoneNumber,
                'mobile' => $faker->phoneNumber,
                'birthdate' => $faker->date(),
                'account_id' => 1
            ];

            \App\Models\Customer::create($cliente);
        }
    }
}
