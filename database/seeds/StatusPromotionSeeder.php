<?php

use Illuminate\Database\Seeder;

class StatusPromotionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\StatusPromotion::create(['name' => 'Indisponível']);
        \App\Models\StatusPromotion::create(['name' => 'Disponível']);
        \App\Models\StatusPromotion::create(['name' => 'Encerrada']);
    }
}
