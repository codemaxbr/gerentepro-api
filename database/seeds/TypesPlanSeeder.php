<?php

use App\Services\ConfigService;
use Illuminate\Database\Seeder;

class TypesPlanSeeder extends Seeder
{
    /**
     * @var ConfigService
     */
    private $configService;

    /**
     * TypesPlanSeeder constructor.
     * @param ConfigService $configService
     */
    public function __construct(ConfigService $configService)
    {
        $this->configService = $configService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->configService->createTypePlan('Hospedagem de Sites', 'host');
        $this->configService->createTypePlan('Revenda de Hospedagem', 'reseller');
        $this->configService->createTypePlan('Streaming', 'streaming');
        $this->configService->createTypePlan('Servidor Dedicado/VPS', 'server');
        $this->configService->createTypePlan('Conteúdo por Assinatura', 'content');
        $this->configService->createTypePlan('Licença de Software', 'license');
        $this->configService->createTypePlan('Produto/Serviço Independente', 'service');
        $this->configService->createTypePlan('Produto físico', 'product');
    }
}
