<?php

use Illuminate\Database\Seeder;
use App\Models\Email;

class EmailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Clientes
         */
        Email::create(['title' => 'Cliente cadastrado',  'tag' => 'customer_register',   'type_email_id' => 1]);
        Email::create(['title' => 'Recuperar senha',     'tag' => 'password_forget',     'type_email_id' => 1]);
        Email::create(['title' => 'Confirmar senha',     'tag' => 'confirm_password',    'type_email_id' => 1]);

        /**
         * Faturas
         */
        Email::create(['title' => 'Fatura criada',       'tag' => 'invoice_created',     'type_email_id' => 2]);
    }
}
