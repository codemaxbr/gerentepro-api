<?php

use App\Models\TypeEmail;
use Illuminate\Database\Seeder;

class TypeEmailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TypeEmail::create(['name' => 'Clientes']);
        TypeEmail::create(['name' => 'Faturas']);
        TypeEmail::create(['name' => 'Planos']);
        TypeEmail::create(['name' => 'Assinaturas']);
        TypeEmail::create(['name' => 'Domínios']);
        TypeEmail::create(['name' => 'Helpdesk']);
    }
}
