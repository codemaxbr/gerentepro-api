<?php

use Illuminate\Database\Seeder;

class TypeServerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\TypeServer::create(['name' => 'Ubuntu',         'logo' => 'https://s3-sa-east-1.amazonaws.com/gerentep/uploads/modules/os/ubuntu.png']);
        \App\Models\TypeServer::create(['name' => 'CentOS',         'logo' => 'https://s3-sa-east-1.amazonaws.com/gerentep/uploads/modules/os/centos.png']);
        \App\Models\TypeServer::create(['name' => 'RedHat',         'logo' => 'https://s3-sa-east-1.amazonaws.com/gerentep/uploads/modules/os/redhat.png']);
        \App\Models\TypeServer::create(['name' => 'OpenSuse',       'logo' => 'https://s3-sa-east-1.amazonaws.com/gerentep/uploads/modules/os/opensuse.png']);
        \App\Models\TypeServer::create(['name' => 'Windows Server', 'logo' => 'https://s3-sa-east-1.amazonaws.com/gerentep/uploads/modules/os/windows.png']);
        \App\Models\TypeServer::create(['name' => 'Amazon AWS',     'logo' => 'https://s3-sa-east-1.amazonaws.com/gerentep/uploads/modules/os/amazon.png']);
        \App\Models\TypeServer::create(['name' => 'Debian',         'logo' => 'https://s3-sa-east-1.amazonaws.com/gerentep/uploads/modules/os/debian.png']);
    }
}
