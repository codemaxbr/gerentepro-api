<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PermissionSeeder::class,
            UptimeKeySeeder::class,
            TypesPlanSeeder::class,
            TypesTermsSeeder::class,
            TypesModulesSeeder::class,
            TypesPaymentSeeder::class,
            TypeInvoiceSeeder::class,
            TypesAddressSeeder::class,
            TypePromotionSeeder::class,
            TypeServerSeeder::class,
            TypeEmailSeeder::class,
            TemplateEmailSeeder::class,
            EmailSeeder::class,
            BankHouseSeeder::class,
            TypeBankSeeder::class,
            PaymentsCycleSeeder::class,
            FeaturesAccountSeeder::class,
            TypesActivitiesSeeder::class,
            ModuleSeeder::class,
            StatusInvoiceSeeder::class,
            StatusPromotionSeeder::class,
            ResellerSeeder::class,
            UsersSeeder::class,
            CustomerSeeder::class,
        ]);
    }
}
