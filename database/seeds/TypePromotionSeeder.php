<?php

use App\Models\TypePromotion;
use Illuminate\Database\Seeder;

class TypePromotionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TypePromotion::create(['name' => 'Porcentagem']);
        TypePromotion::create(['name' => 'Desconto no valor']);
        TypePromotion::create(['name' => 'Isenção da taxa de setup']);
    }
}
