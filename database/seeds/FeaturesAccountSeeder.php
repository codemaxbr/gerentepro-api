<?php

use Illuminate\Database\Seeder;
use App\Models\AccountPlan;

class FeaturesAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*
         |----------------------------------------------------------------------------------------------------------------------------
         | Preço por usuário
         |----------------------------------------------------------------------------------------------------------------------------
         */

        /** @var AccountPlan $iniciante */
        $iniciante    = AccountPlan::create(['name' => 'Iniciante', 'price_month' => 0.00, 'status' => 1, 'type_term_id' => 2]);
        /** @var AccountPlan $autonomo */
        $autonomo     = AccountPlan::create(['name' => 'Autônomo',  'price_month' => 29.90, 'status' => 1, 'type_term_id' => 1]);
        /** @var AccountPlan $empresa */
        $empresa      = AccountPlan::create(['name' => 'Empresa',   'price_month' => 49.90, 'status' => 1, 'type_term_id' => 1]);
        /** @var AccountPlan $proprietario */
        $proprietario = AccountPlan::create(['name' => 'Proprietario', 'price_month' => 3999.90, 'status' => 0, 'type_term_id' => 3]);

        /*
         |----------------------------------------------------------------------------------------------------------------------------
         | Plano Iniciante
         |----------------------------------------------------------------------------------------------------------------------------
         */
        $iniciante->features()->create(['model' => App\Models\Customer::class, 'limit' => 200, 'available' => true]);
        $iniciante->features()->create(['model' => App\Models\User::class,     'limit' => 3, 'available' => true]);
    }
}
