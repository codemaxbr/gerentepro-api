<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description');
            $table->integer('attempts')->default(0);
            $table->integer('completed')->default(0);
            $table->integer('parts')->default(1);
            $table->enum('status', ['running', 'waiting', 'failed', 'completed']);
            $table->text('response')->nullable();

            $table->integer('module_id')->unsigned()->nullable();
            $table->bigInteger('failed_job_id')->unsigned()->nullable();
            $table->morphs('target');
            $table->integer('account_id')->unsigned();

            $table->timestamps();
        });

        Schema::table('tasks', function($table) {
            $table->foreign('account_id')
                ->references('id')
                ->on('accounts')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('module_id')
                ->references('id')
                ->on('modules')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
