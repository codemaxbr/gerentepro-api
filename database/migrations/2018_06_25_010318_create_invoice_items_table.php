<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateInvoiceItemsTable.
 */
class CreateInvoiceItemsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoice_items', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('invoice_id')->unsigned();
            $table->integer('plan_id')->unsigned()->nullable();
            $table->integer('price_id')->unsigned()->nullable();
            $table->string('domain')->nullable();
            $table->string('description');
            $table->decimal('price', 13,2);
            $table->decimal('discount', 13,2)->nullable();
            $table->decimal('tax', 13,2)->nullable();
            $table->string('type')->nullable();
            $table->integer('months')->nullable();
            $table->timestamps();
		});

        Schema::table('invoice_items', function($table) {
            $table->foreign('invoice_id')
                ->references('id')
                ->on('invoices')
                ->onDelete('cascade');

            $table->foreign('plan_id')
                ->references('id')
                ->on('plans')
                ->onDelete('cascade');

            $table->foreign('price_id')
                ->references('id')
                ->on('prices')
                ->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoice_items');
	}
}
