<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreatePlansTable.
 */
class CreatePlansTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plans', function(Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            $table->string('name');
            $table->boolean('status')->default('1');
            $table->integer('email_template_id')->nullable();
            $table->boolean('domain')->default('0');
            $table->integer('trial')->nullable()->default(0);
            $table->integer('visibility')->nullable()->default('0');
            $table->decimal('tax', 13,2)->default(0)->nullable();
            $table->integer('module_id')->nullable();
            $table->string('server')->nullable();
            $table->text('config')->nullable();
            $table->timestamps();
            $table->integer('account_id')->unsigned();

            $table->foreign('account_id')
                ->references('id')
                ->on('accounts')
                ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('plans');
	}
}
