<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateBanksTable.
 */
class CreateBanksTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('banks', function(Blueprint $table) {
            $table->increments('id');
            $table->string('owner');
            $table->string('wallet');
            $table->integer('type_bank_id')->unsigned();
            $table->integer('bank_house_id')->unsigned();
            $table->string('agency');
            $table->string('account');
            $table->integer('digit');
            $table->integer('account_id')->unsigned();
            $table->timestamps();

            $table->foreign('account_id')
                ->references('id')
                ->on('accounts')
                ->onDelete('cascade');

            $table->foreign('bank_house_id')
                ->references('id')
                ->on('bank_houses')
                ->onDelete('cascade');

            $table->foreign('type_bank_id')
                ->references('id')
                ->on('type_banks')
                ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('banks');
	}
}
