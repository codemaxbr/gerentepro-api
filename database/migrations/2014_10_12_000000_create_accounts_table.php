<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateAccountsTable.
 */
class CreateAccountsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('accounts', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name_business')->nullable();
            $table->uuid('uuid')->unique();
            $table->string('logo')->nullable();
            $table->string('domain')->unique();
            $table->string('email_contact')->nullable();
            $table->string('phone_contact')->nullable();
            $table->string('url_terms')->nullable();
            $table->boolean('status')->default('0');
            $table->integer('reseller_id')->unsigned()->default(1);
            $table->integer('account_plan_id')->unsigned()->default(1);
            $table->integer('payment_cycle_id')->unsigned()->nullable();
            $table->integer('type_payment_id')->unsigned()->nullable();
            $table->date('expires')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('reseller_id')
                ->references('id')
                ->on('resellers');

            $table->foreign('account_plan_id')
                ->references('id')
                ->on('account_plans')
                ->onDelete('cascade');

            $table->foreign('payment_cycle_id')
                ->references('id')
                ->on('payment_cycles')
                ->onDelete('cascade');

            $table->foreign('type_payment_id')
                ->references('id')
                ->on('type_payments')
                ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('accounts');
	}
}
