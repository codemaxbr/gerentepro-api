<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('Nome da promoção');
            $table->uuid('uuid');
            $table->integer('type_promotion_id')->unsigned()->comment('Tipo da promoção');
            $table->decimal('value', 13,2)->comment('Valor do desconto');

            $table->date('start')->nullable()->comment('Data de início');
            $table->date('expires')->nullable()->comment('Data de expiração');

            $table->decimal('total_min', 13,2)->nullable()->comment('Total mínimo do pedido');
            $table->integer('qty')->default(1)->comment('Quantidade');
            $table->boolean('multi_uses')->default(0)->comment('Permitir uso multiplo');
            $table->boolean('unique')->default(0)->comment('Único por cliente');
            $table->boolean('only_registers')->default(0)->comment('Somente novos clientes');
            $table->string('prefix', 10)->nullable()->comment('Prefixo');
            $table->integer('status_promotion_id')->unsigned()->default(1)->comment('Status da promoção');
            $table->boolean('upgrade_plan')->default(0)->comment('Permitir Upgrade/Downgrade de plano');
            $table->integer('account_id')->unsigned();
            $table->timestamps();

            $table->foreign('account_id')
                ->references('id')
                ->on('accounts')
                ->onDelete('cascade');

            $table->foreign('type_promotion_id')
                ->references('id')
                ->on('type_promotions')
                ->onDelete('cascade');

            $table->foreign('status_promotion_id')
                ->references('id')
                ->on('status_promotions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
